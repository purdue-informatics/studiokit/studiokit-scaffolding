namespace StudioKit.Scaffolding.Models;

public enum LockDownBrowserCalculatorType
{
	Standard,
	Scientific
}