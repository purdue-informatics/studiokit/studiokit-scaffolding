using StudioKit.Data;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.Scaffolding.Models;

public abstract class EntityUserRole : ModelBase, IEntityUserRole
{
	[Required]
	public string UserId { get; set; }

	[Required]
	public string RoleId { get; set; }

	public abstract int EntityId { get; set; }

	[ForeignKey(nameof(UserId))]
	public IUser User { get; set; }

	[ForeignKey(nameof(RoleId))]
	public Role Role { get; set; }
}