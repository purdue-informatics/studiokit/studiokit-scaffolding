﻿namespace StudioKit.Scaffolding.Models.Interfaces;

public interface IEntityArtifact
{
	Artifact Artifact { get; set; }
}