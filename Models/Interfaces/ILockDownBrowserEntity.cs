namespace StudioKit.Scaffolding.Models.Interfaces;

public interface ILockDownBrowserBaseEntity
{
	public bool IsLockDownBrowserRequired { get; set; }

	public bool IsLockDownBrowserRequiredForResults { get; set; }

	public bool IsLockDownBrowserHighSecurityEnabled { get; set; }

	public string LockDownBrowserProctorExitPassword { get; set; }

	public string LockDownBrowserCalculatorType { get; set; }

	public bool IsLockDownBrowserPrintingEnabled { get; set; }
}

public interface ILockDownBrowserEntity : ILockDownBrowserBaseEntity
{
	public int Id { get; set; }

	public LockDownBrowserCalculatorType? LockDownBrowserCalculatorTypeEnum { get; set; }
}