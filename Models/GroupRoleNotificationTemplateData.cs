﻿using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Notification.BusinessLogic.Models;
using System.Collections.Generic;

namespace StudioKit.Scaffolding.Models;

public class GroupRoleNotificationTemplateData<TGroup> : BaseNotificationTemplateData<TGroup>
	where TGroup : BaseGroup
{
	public Role Role { get; set; }

	public IUser UpdatedByUser { get; set; }

	public IEnumerable<IUser> UpdatedUsers;

	public List<ExternalGroup> RemovedExternalGroups;
}