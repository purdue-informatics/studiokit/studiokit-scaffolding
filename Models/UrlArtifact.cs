﻿using StudioKit.Data.Entity.Extensions;
using System.Collections.Generic;

namespace StudioKit.Scaffolding.Models;

public class UrlArtifact : Artifact
{
	#region Cloning

	public sealed class UrlArtifactEqualityComparer : IEqualityComparer<UrlArtifact>
	{
		public bool Equals(UrlArtifact x, UrlArtifact y)
		{
			if (ReferenceEquals(x, y)) return true;
			if (ReferenceEquals(x, null)) return false;
			if (ReferenceEquals(y, null)) return false;
			if (!x.GetType().EntityTypeEquals(y.GetType())) return false;
			return string.Equals(x.Url, y.Url);
		}

		public int GetHashCode(UrlArtifact obj)
		{
			return (obj.Url != null ? obj.Url.GetHashCode() : 0);
		}
	}

	public override object Clone(string userId)
	{
		var artifact = (UrlArtifact)base.Clone(userId);
		artifact.Url = Url;
		return artifact;
	}

	protected override bool IsCloneEqual(Artifact artifact)
	{
		return artifact is UrlArtifact urlArtifact && new UrlArtifactEqualityComparer().Equals(this, urlArtifact);
	}

	#endregion Cloning
}