using StudioKit.Data;
using StudioKit.Notification.Models.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.Scaffolding.Models;

public class Activity : ModelBase, IActivity
{
	[Required]
	[StringLength(128)]
	public string Name { get; set; }

	[InverseProperty(nameof(RoleActivity.Activity))]
	public ICollection<RoleActivity> RoleActivities { get; set; }
}