using StudioKit.Data;
using StudioKit.ExternalProvider.Models.Interfaces;
using StudioKit.ExternalProvider.UniTime.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.Scaffolding.Models;

public class ExternalTerm : ModelBase, IExternalTerm
{
	[Required]
	public int ExternalProviderId { get; set; }

	[ForeignKey(nameof(ExternalProviderId))]
	public ExternalProvider.Models.ExternalProvider ExternalProvider { get; set; }

	[Required]
	[StringLength(450)]
	public string ExternalId { get; set; }

	public string Name { get; set; }

	[Required]
	public DateTime StartDate { get; set; }

	[Required]
	public DateTime EndDate { get; set; }

	public ICollection<UniTimeGroup> UniTimeGroups { get; set; }
}