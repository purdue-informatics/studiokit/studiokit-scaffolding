using StudioKit.Data;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Notification.Models.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.Scaffolding.Models;

public class RoleActivity : ModelBase, IRoleActivity<Activity>
{
	[Required]
	public string RoleId { get; set; }

	[ForeignKey(nameof(RoleId))]
	public Role Role { get; set; }

	[Required]
	public int ActivityId { get; set; }

	[ForeignKey(nameof(ActivityId))]
	public Activity Activity { get; set; }
}