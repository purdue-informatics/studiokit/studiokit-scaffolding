using StudioKit.Data;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Interfaces;
using StudioKit.ExternalProvider.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.Scaffolding.Models;

public abstract class BaseGroup : ModelBase, IOwnable, IGroup<ExternalTerm, GroupUserRole, ExternalGroup>,
	Notification.Models.Interfaces.IGroup
{
	public string Name { get; set; }

	public bool IsDeleted { get; set; }

	public int? ExternalTermId { get; set; }

	[ForeignKey(nameof(ExternalTermId))]
	public ExternalTerm ExternalTerm { get; set; }

	public DateTime? StartDate { get; set; }

	public DateTime? EndDate { get; set; }

	[Required]
	public string CreatedById { get; set; }

	[ForeignKey(nameof(CreatedById))]
	public IUser User { get; set; }

	public ICollection<GroupUserRole> GroupUserRoles { get; set; }

	public ICollection<GroupUserRoleLog> GroupUserRoleLogs { get; set; }

	public ICollection<ExternalGroup> ExternalGroups { get; set; }

	public Type UserRoleAssociatedType => typeof(GroupUserRole);
}