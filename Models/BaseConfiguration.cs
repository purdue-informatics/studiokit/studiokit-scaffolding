﻿using StudioKit.Caliper.Interfaces;
using StudioKit.Security.BusinessLogic.Interfaces;
using StudioKit.Sharding.Models;
using StudioKit.Utilities.Extensions;
using System.Collections.Generic;
using System.Linq;

namespace StudioKit.Scaffolding.Models;

public class BaseConfiguration : BaseShardConfiguration, ICaliperConfiguration, ICertificateConfiguration
{
	#region ICaliperConfiguration

	public bool CaliperEnabled { get; set; }

	public string CaliperEventStoreClientId { get; set; }

	public string CaliperEventStoreClientSecret { get; set; }

	public string CaliperEventStoreHostname { get; set; }

	public string CaliperPersonNamespace { get; set; }

	#endregion ICaliperConfiguration

	#region ICertificateConfiguration

	public string RsaKeyPair { get; set; }

	#endregion ICertificateConfiguration

	public byte[] Image { get; set; }

	public bool TrialEnabled { get; set; }

	public int? TrialDefaultDayLimit { get; set; }

	public string UserSupportEmail { get; set; }

	public string AllowedDomains { get; set; }

	public bool IsInstructorSandboxEnabled { get; set; }

	public int? DemoExpirationDayLimit { get; set; }

	public bool ValidateAllowedDomains(string scopedIdentifier)
	{
		if (string.IsNullOrWhiteSpace(AllowedDomains))
			return true;

		if (!scopedIdentifier.HasValidDomainScope())
			return false;

		var domains = new List<string>(AllowedDomains.Split(','));
		var userDomain = scopedIdentifier.GetDomain();

		// checks if user email domain contains any allowed domains
		// e.g. "person@purdue.edu" contains "purdue.edu"
		// e.g. "person@ecn.purdue.edu" contains "purdue.edu"
		return domains.Any(emailDomain => userDomain.Normalize().ToUpperInvariant().Contains(emailDomain.Normalize().ToUpperInvariant()));
	}
}