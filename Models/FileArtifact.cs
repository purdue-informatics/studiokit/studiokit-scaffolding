﻿using StudioKit.Data.Entity.Extensions;
using System.Collections.Generic;

namespace StudioKit.Scaffolding.Models;

public class FileArtifact : Artifact
{
	public string FileName { get; set; }

	#region Cloning

	public override object Clone(string userId)
	{
		var artifact = (FileArtifact)base.Clone(userId);
		artifact.FileName = FileName;
		return artifact;
	}

	private sealed class ClonedFileArtifactEqualityComparer : IEqualityComparer<FileArtifact>
	{
		public bool Equals(FileArtifact x, FileArtifact y)
		{
			if (ReferenceEquals(x, y)) return true;
			if (ReferenceEquals(x, null)) return false;
			if (ReferenceEquals(y, null)) return false;
			if (!x.GetType().EntityTypeEquals(y.GetType())) return false;
			return string.Equals(x.FileName, y.FileName);
		}

		public int GetHashCode(FileArtifact obj)
		{
			return (obj.FileName != null ? obj.FileName.GetHashCode() : 0);
		}
	}

	protected override bool IsCloneEqual(Artifact artifact)
	{
		return artifact is FileArtifact fileArtifact &&
				new ClonedFileArtifactEqualityComparer().Equals(this, fileArtifact);
	}

	#endregion Cloning
}