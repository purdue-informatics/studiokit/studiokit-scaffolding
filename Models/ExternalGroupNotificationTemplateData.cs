﻿using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Notification.BusinessLogic.Models;
using System.Collections.Generic;

namespace StudioKit.Scaffolding.Models;

public class ExternalGroupNotificationTemplateData<TGroup> : BaseNotificationTemplateData<TGroup>
	where TGroup : BaseGroup
{
	public List<ExternalGroup> ExternalGroups { get; set; }

	public IUser UpdatedByUser { get; set; }
}