using StudioKit.Data.Entity.Identity.Interfaces;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.Scaffolding.Models;

public class GroupUserRole : EntityUserRole, IGroupUserRole
{
	public int GroupId { get; set; }

	[ForeignKey(nameof(GroupId))]
	public BaseGroup Group { get; set; }

	public override int EntityId { get => GroupId; set => GroupId = value; }

	public bool IsExternal { get; set; }
}