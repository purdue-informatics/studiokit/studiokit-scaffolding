﻿# StudioKit.Scaffolding

## Structure

* Common
	* Classes that are shared across many layers
* Models
	* Models and interfaces which represent the application data
* DataAccess
	* Classes related to accessing data from the database, cache, or storage
* BusinessLogic
	* Classes which contain all business logic for the application
* Service
	* External service using WebApi, which exposes endpoints that call BusinessLogic services
