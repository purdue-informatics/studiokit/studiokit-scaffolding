﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using StudioKit.Scaffolding.DataAccess.Interfaces;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Service.Controllers;

[Route("api/externalTerms")]
[ApiController]
public class ExternalTermsController : ControllerBase
{
	private readonly IBaseDbContext _dbContext;

	public ExternalTermsController(IBaseDbContext dbContext)
	{
		_dbContext = dbContext;
	}

	// GET: api/externalTerms
	[AllowAnonymous]
	[HttpGet]
	public async Task<IActionResult> GetAsync(CancellationToken cancellationToken = default)
	{
		var result = await _dbContext.ExternalTerms
			.Select(x => new { x.Id, x.ExternalProviderId, x.Name, x.ExternalId, x.StartDate, x.EndDate }).ToListAsync(cancellationToken);
		return Ok(result);
	}
}