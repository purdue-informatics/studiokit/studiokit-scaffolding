﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StudioKit.Scaffolding.Service.Models.ViewModels;
using StudioKit.Security.BusinessLogic.Interfaces;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Service.Controllers;

[Route("api/jwks")]
[ApiController]
public class JsonWebKeyController : ControllerBase
{
	private readonly ICertificateService _certificateService;

	public JsonWebKeyController(ICertificateService certificateService)
	{
		_certificateService = certificateService ?? throw new ArgumentNullException(nameof(certificateService));
	}

	[AllowAnonymous]
	[HttpGet]
	public async Task<IActionResult> GetAsync(CancellationToken cancellationToken = default)
	{
		var keySet = await _certificateService.GetJsonWebKeySetAsync(cancellationToken);
		return Ok(new JsonWebKeySetViewModel(keySet));
	}
}