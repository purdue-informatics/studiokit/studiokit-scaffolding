﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using StudioKit.Scaffolding.DataAccess.Interfaces;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Service.Controllers;

[AllowAnonymous]
[Route("api/identityProviders")]
[ApiController]
public class IdentityProvidersController : ControllerBase
{
	private readonly IBaseDbContext _dbContext;

	public IdentityProvidersController(IBaseDbContext dbContext)
	{
		_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
	}

	// GET: api/identityProviders
	[HttpGet]
	public async Task<IActionResult> GetAsync(CancellationToken cancellationToken = default)
	{
		var externalProviders = await _dbContext.IdentityProviders.ToListAsync(cancellationToken);
		return Ok(externalProviders);
	}
}