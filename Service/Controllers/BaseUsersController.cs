﻿using Microsoft.AspNetCore.Mvc;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Service.Models.ViewModels;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Service.Controllers;

[ApiController]
public class BaseUsersController<TUser, TUserBusinessModel, TUserEditBusinessModel, TUserViewModel> : ControllerBase
	where TUser : IUser, new()
	where TUserBusinessModel : BaseUserBusinessModel<TUser>, new()
	where TUserEditBusinessModel : BaseUserEditBusinessModel<TUser>, new()
	where TUserViewModel : BaseUserViewModel<TUser, TUserBusinessModel>, new()
{
	private readonly IBaseUserService<TUser, TUserBusinessModel, TUserEditBusinessModel> _baseUserService;

	public BaseUsersController(IBaseUserService<TUser, TUserBusinessModel, TUserEditBusinessModel> baseUserService)
	{
		_baseUserService = baseUserService ?? throw new ArgumentNullException(nameof(baseUserService));
	}

	[HttpGet]
	public async Task<IActionResult> GetAsync(string keywords, CancellationToken cancellationToken = default)
	{
		var users = await _baseUserService.GetAsync(User, keywords, cancellationToken);
		return Ok(users.Select(u =>
		{
			var viewModel = new TUserViewModel().MapFrom(u);
			return viewModel;
		}));
	}

	[HttpGet("{userId}")]
	public virtual async Task<IActionResult> GetUserByIdAsync(string userId, CancellationToken cancellationToken = default)
	{
		var user = await _baseUserService.GetAsync(userId, User, cancellationToken);
		var viewModel = new TUserViewModel().MapFrom(user);
		return Ok(viewModel);
	}
}