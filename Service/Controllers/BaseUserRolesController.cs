﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.Service.Models.RequestModels;
using StudioKit.Scaffolding.Service.Models.ViewModels;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Service.Controllers;

[ApiController]
public class BaseUserRolesController<TUser, TViewModel, TCreatedViewModel> : ControllerBase
	where TUser : IdentityUser, IUser, new()
	where TViewModel : BaseUserRoleViewModel<TUser>, new()
	where TCreatedViewModel : BaseCreatedUserRolesViewModel<TUser>, new()
{
	private readonly IUserRoleService<TUser> _userRoleService;

	public BaseUserRolesController(IUserRoleService<TUser> userRoleService)
	{
		_userRoleService = userRoleService ?? throw new ArgumentNullException(nameof(userRoleService));
	}

	[HttpGet("")]
	public async Task<IActionResult> GetUserRolesAsync(string roleName, CancellationToken cancellationToken = default)
	{
		var userRoles = await _userRoleService.GetUserRolesAsync(roleName, User, cancellationToken);
		return Ok(userRoles.Select(ur => new TViewModel().MapFrom(ur)));
	}

	[HttpPost("")]
	public async Task<IActionResult> CreateUserRolesAsync([FromBody] AddUserRolesRequestModel requestModel,
		CancellationToken cancellationToken = default)
	{
		var createdUserRoles =
			await _userRoleService.CreateUserRolesAsync(requestModel.RoleName, requestModel.Identifiers, User, cancellationToken);
		return Ok(new TCreatedViewModel().MapFrom(createdUserRoles));
	}

	[HttpDelete("{userId}")]
	public async Task<IActionResult> DeleteUserRoleAsync(string userId, string roleName, CancellationToken cancellationToken = default)
	{
		await _userRoleService.DeleteUserRoleAsync(userId, roleName, User, cancellationToken);
		return NoContent();
	}
}