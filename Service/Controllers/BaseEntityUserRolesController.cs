﻿using Microsoft.AspNetCore.Mvc;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.Service.Models.RequestModels;
using StudioKit.Scaffolding.Service.Models.ViewModels;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Service.Controllers;

[ApiController]
public class BaseEntityUserRolesController<TUser, TEntityUserRole, TViewModel, TCreatedViewModel> : ControllerBase
	where TUser : IUser
	where TEntityUserRole : IEntityUserRole
	where TViewModel : BaseEntityUserRoleViewModel<TUser, TEntityUserRole>, new()
	where TCreatedViewModel : BaseCreatedEntityUserRolesViewModel<TUser, TEntityUserRole>, new()
{
	private readonly IEntityUserRoleService<TUser, TEntityUserRole> _service;

	public BaseEntityUserRolesController(IEntityUserRoleService<TUser, TEntityUserRole> service)
	{
		_service = service ?? throw new ArgumentNullException(nameof(service));
	}

	[HttpGet]
	public async Task<IActionResult> GetEntityUserRolesAsync(int entityId, CancellationToken cancellationToken = default)
	{
		var entityUserRoles = await _service.GetEntityUserRolesAsync(entityId, User, cancellationToken);
		return Ok(entityUserRoles.Select(eur => new TViewModel().MapFrom(eur)));
	}

	[HttpPost]
	public async Task<IActionResult> CreateEntityUserRoleAsync([FromBody] AddEntityUserRolesRequestModel requestModel,
		CancellationToken cancellationToken = default)
	{
		var createdEntityUserRoles = await _service.CreateEntityUserRolesAsync(requestModel.EntityId, requestModel.Identifiers,
			requestModel.RoleName, User, cancellationToken);
		return Ok(new TCreatedViewModel().MapFrom(createdEntityUserRoles));
	}

	[HttpPut("{entityUserRoleId:int}")]
	public async Task<IActionResult> UpdateEntityUserRoleAsync(int entityUserRoleId, string roleName,
		CancellationToken cancellationToken = default)
	{
		var updatedEntityUserRole = await _service.UpdateEntityUserRoleAsync(entityUserRoleId, roleName, User, cancellationToken);
		return Ok(new TViewModel().MapFrom(updatedEntityUserRole));
	}

	[HttpDelete("{entityUserRoleId:int}")]
	public async Task<IActionResult> DeleteEntityUserRoleAsync(int entityUserRoleId, CancellationToken cancellationToken = default)
	{
		await _service.DeleteEntityUserRoleAsync(entityUserRoleId, User, cancellationToken);
		return NoContent();
	}
}