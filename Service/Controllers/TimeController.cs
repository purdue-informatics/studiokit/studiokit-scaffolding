﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace StudioKit.Scaffolding.Service.Controllers;

[ApiController]
[Route("api/time")]
public class TimeController : ControllerBase
{
	[AllowAnonymous]
	[HttpGet]
	public IActionResult Get()
	{
		return Ok(DateTime.UtcNow);
	}
}