﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Service.Models.ViewModels;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Service.Controllers;

[Route("api/appClients")]
[ApiController]
public class AppClientsController : ControllerBase
{
	private readonly IAppClientService _appClientService;

	public AppClientsController(IAppClientService appClientService)
	{
		_appClientService = appClientService ?? throw new ArgumentNullException(nameof(appClientService));
	}

	[AllowAnonymous]
	[HttpGet("{clientId}")]
	public async Task<IActionResult> GetAsync(string clientId, CancellationToken cancellationToken = default)
	{
		var client = await _appClientService.GetAsync(clientId, cancellationToken);
		return Ok(new AppClientViewModel(client));
	}

	[HttpPut("{clientId}")]
	public async Task<IActionResult> PutAsync(string clientId, [FromBody] AppClientUpdateBusinessModel businessModel,
		CancellationToken cancellationToken = default)
	{
		var updatedClient = await _appClientService.UpdateAsync(clientId, businessModel, User, cancellationToken);
		return Ok(new AppClientViewModel(updatedClient));
	}
}