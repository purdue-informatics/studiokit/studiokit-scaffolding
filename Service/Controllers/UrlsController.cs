using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using StudioKit.TransientFaultHandling.Http.Interfaces;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Service.Controllers;

[Route("api/url")]
[ApiController]
public class UrlsController : ControllerBase
{
	private readonly Func<IHttpClient> _getHttpClient;

	public UrlsController(Func<IHttpClient> getHttpClient)
	{
		_getHttpClient = getHttpClient;
	}

	//GET: api/url/headers?url={ url }
	[HttpGet("headers")]
	public async Task<IActionResult> GetHeadersAsync(string url, CancellationToken cancellationToken = default)
	{
		if (string.IsNullOrWhiteSpace(url))
		{
			return BadRequest("Url link is required");
		}

		HttpResponseMessage response = null;
		var client = _getHttpClient();
		try
		{
			response = await client.SendAsync(new HttpRequestMessage(HttpMethod.Get, new Uri(url)), cancellationToken);
		}
		catch (Exception)
		{
			return Ok(response);
		}

		var result = new Dictionary<string, IEnumerable<string>>();
		foreach (var header in response.Headers)
		{
			result[header.Key] = header.Value;
		}

		foreach (var contentHeader in response.Content.Headers)
		{
			result[contentHeader.Key] = contentHeader.Value;
		}

		var settings = new JsonSerializerSettings
		{
			ContractResolver = new DefaultContractResolver()
		};

		return new JsonResult(result, settings);
	}
}