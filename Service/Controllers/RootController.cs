﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using System;

namespace StudioKit.Scaffolding.Service.Controllers;

[AllowAnonymous]
[Route("/")]
[ApiController]
public class RootController : ControllerBase
{
	[HttpGet("")]
	public IActionResult Index()
	{
		return Ok();
	}

	[HttpGet("/error-development")]
	public IActionResult HandleErrorDevelopment([FromServices] IHostEnvironment hostEnvironment)
	{
		if (!hostEnvironment.IsDevelopment())
		{
			return NotFound();
		}

		var exceptionHandlerFeature = HttpContext.Features.Get<IExceptionHandlerFeature>()!;

		var detail = exceptionHandlerFeature?.Error != null
			? $"{exceptionHandlerFeature.Error.Message}{Environment.NewLine}{exceptionHandlerFeature.Error.StackTrace}"
			: null;

		return Problem(detail: detail);
	}

	[HttpGet("/error")]
	public IActionResult Error()
	{
		return Problem();
	}

	[HttpGet("/status-code/{statusCode:int}")]
	public IActionResult GetStatusCode(int statusCode)
	{
		return Problem(statusCode: statusCode);
	}
}