﻿using Microsoft.AspNetCore.Mvc;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.Service.Models.ViewModels;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Service.Controllers;

[Route("api/ltiLaunches/{ltiLaunchId:int}")]
[ApiController]
public class LtiLaunchesController : ControllerBase
{
	private readonly ILtiLaunchService _ltiLaunchService;

	public LtiLaunchesController(ILtiLaunchService ltiLaunchService)
	{
		_ltiLaunchService = ltiLaunchService;
	}

	[HttpGet("")]
	public async Task<IActionResult> GetAsync(int ltiLaunchId, CancellationToken cancellationToken = default)
	{
		var ltiLaunch = await _ltiLaunchService.GetLtiLaunchAsync(ltiLaunchId, User, cancellationToken);
		return Ok(new LtiLaunchViewModel(ltiLaunch));
	}

	[HttpPost("deepLinkingResponse")]
	public async Task<string> GetDeepLinkingResponseAsync(int ltiLaunchId, [FromBody] DeepLinkingResponseRequest deepLinkingResponseRequest,
		CancellationToken cancellationToken = default)
	{
		return await _ltiLaunchService.GetDeepLinkingResponseAsync(ltiLaunchId, deepLinkingResponseRequest, User, cancellationToken);
	}
}