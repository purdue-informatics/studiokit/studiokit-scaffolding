using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Service.Models.RequestModels;
using StudioKit.Scaffolding.Service.Models.ViewModels;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Service.Controllers;

[ApiController]
public class
	BaseGroupsController<TGroup, TGroupBusinessModel, TGroupEditBusinessModel, TGroupEditRequestModel, TGroupViewModel,
		TUser> : ControllerBase
	where TGroup : BaseGroup, new()
	where TGroupBusinessModel : BaseGroupBusinessModel<TGroup, TUser>, new()
	where TGroupEditBusinessModel : BaseGroupEditBusinessModel<TGroup>, new()
	where TGroupEditRequestModel : BaseGroupEditRequestModel
	where TGroupViewModel : BaseGroupViewModel<TGroup, TGroupBusinessModel, TUser>, new()
	where TUser : IdentityUser, IUser

{
	private readonly IBaseGroupService<TGroup, TGroupBusinessModel, TGroupEditBusinessModel, TUser> _baseGroupService;

	public BaseGroupsController(IBaseGroupService<TGroup, TGroupBusinessModel, TGroupEditBusinessModel, TUser> baseGroupService)
	{
		_baseGroupService = baseGroupService;
	}

	#region Group

	[HttpGet]
	public async Task<IActionResult> GetAsync(string keywords = null, DateTime? date = null, bool queryAll = false, string userId = null,
		bool includeIfSoftDeleted = false, CancellationToken cancellationToken = default)
	{
		var groups = await _baseGroupService.GetAsync(User, keywords, date, queryAll, userId, includeIfSoftDeleted, cancellationToken);
		return Ok(groups.Select(CreateViewModel));
	}

	[HttpGet("{groupId:int}")]
	public async Task<IActionResult> GetAsync(int groupId, CancellationToken cancellationToken = default)
	{
		var group = await _baseGroupService.GetAsync(groupId, User, cancellationToken);
		return Ok(CreateViewModel(group));
	}

	[HttpPost]
	public async Task<IActionResult> CreateOrCopyAsync(
		[FromBody(EmptyBodyBehavior = EmptyBodyBehavior.Allow)] TGroupEditRequestModel groupEditRequestModel,
		[FromQuery] int? sourceId = null, CancellationToken cancellationToken = default)
	{
		var groupEditBusinessModel = new TGroupEditBusinessModel
		{
			Name = groupEditRequestModel.Name,
			ExternalTermId = groupEditRequestModel.ExternalTermId,
			StartDate = groupEditRequestModel.StartDate,
			EndDate = groupEditRequestModel.EndDate,
			ExternalGroups = groupEditRequestModel.ExternalGroups
		};
		var groupBusinessModel = sourceId == null
			? await _baseGroupService.CreateAsync(groupEditBusinessModel, User, cancellationToken)
			: await _baseGroupService.CopyAsync(groupEditBusinessModel, sourceId.Value, User, cancellationToken);

		return Ok(CreateViewModel(groupBusinessModel));
	}

	[HttpPut("{groupId:int}")]
	public async Task<IActionResult> UpdateAsync(int groupId, [FromBody] TGroupEditRequestModel groupEditRequestModel,
		CancellationToken cancellationToken = default)
	{
		var groupEditBusinessModel = new TGroupEditBusinessModel
		{
			Name = groupEditRequestModel.Name,
			ExternalTermId = groupEditRequestModel.ExternalTermId,
			StartDate = groupEditRequestModel.StartDate,
			EndDate = groupEditRequestModel.EndDate,
			ExternalGroups = groupEditRequestModel.ExternalGroups,
			IsDeleted = groupEditRequestModel.IsDeleted
		};
		var groupBusinessModel = await _baseGroupService.UpdateAsync(groupId, groupEditBusinessModel, User, cancellationToken);
		return Ok(CreateViewModel(groupBusinessModel));
	}

	[HttpDelete("{groupId:int}")]
	public async Task<IActionResult> DeleteAsync(int groupId, CancellationToken cancellationToken = default)
	{
		await _baseGroupService.DeleteAsync(groupId, User, cancellationToken);
		return NoContent();
	}

	#endregion Group

	#region Roster Sync

	[HttpGet("{groupId:int}/externalGroups")]
	public async Task<IActionResult> GetExternalGroupsAsync(int groupId, CancellationToken cancellationToken = default)
	{
		var externalGroups = await _baseGroupService.GetExternalGroupsAsync(groupId, User, cancellationToken);
		return Ok(externalGroups.Select(eg => new ExternalGroupViewModel(eg)));
	}

	[HttpPost("{groupId:int}/syncRoster")]
	public async Task<IActionResult> SyncRosterAsync(int groupId, CancellationToken cancellationToken = default)
	{
		await _baseGroupService.SyncRosterAsync(groupId, User, cancellationToken);
		return Ok();
	}

	[HttpGet("syncAll")]
	public async Task<IActionResult> SyncAllAsync(CancellationToken cancellationToken = default)
	{
		await _baseGroupService.SyncAllRostersAsync(User, cancellationToken);
		return Ok();
	}

	#endregion Roster Sync

	#region Helpers

	private static IViewModel<TGroupBusinessModel> CreateViewModel(TGroupBusinessModel businessModel)
	{
		var viewModel = new TGroupViewModel().MapFrom(businessModel);
		return viewModel;
	}

	#endregion Helpers
}