﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StudioKit.Configuration;

namespace StudioKit.Scaffolding.Service.Controllers;

[Route("api/version")]
[ApiController]
public class VersionController : ControllerBase
{
	private readonly IRelease _release;

	public VersionController(IRelease release)
	{
		_release = release;
	}

	[AllowAnonymous]
	[HttpGet]
	public IActionResult Get()
	{
		return Ok(_release.Version);
	}
}