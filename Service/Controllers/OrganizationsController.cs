﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.Service.Filters;
using StudioKit.Sharding;
using StudioKit.Sharding.Models;
using StudioKit.Sharding.Utils;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Service.Controllers;

[Route("api/organizations")]
[ApiController]
public class OrganizationsController : ControllerBase
{
	private readonly IOrganizationService _organizationService;

	public OrganizationsController(IOrganizationService organizationService)
	{
		_organizationService = organizationService;
	}

	[AllowAnonymous]
	[AllowRootDomain]
	[HttpGet]
	public async Task<IActionResult> GetAsync(CancellationToken cancellationToken = default)
	{
		var result = new List<Organization>();
		await ShardUtils.PerformAsyncFuncOnAllShards(ShardManager.Instance.DefaultShardMap, async shardKey =>
		{
			result.Add(await _organizationService.GetOrganizationAsync(shardKey, cancellationToken));
		});
		return Ok(result);
	}
}