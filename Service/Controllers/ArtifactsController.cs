using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using StudioKit.Notification;
using StudioKit.Scaffolding.BusinessLogic.Services;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Service.Controllers;

[Route("api/artifacts")]
[ApiController]
public class ArtifactsController : ControllerBase
{
	private readonly ArtifactLeasingService _artifactLeasingService;

	public ArtifactsController(ArtifactLeasingService artifactLeasingService)
	{
		_artifactLeasingService = artifactLeasingService;
	}

	[AllowAnonymous]
	[HttpGet("{artifactToken}", Name = nameof(GetAssignmentArtifactFromTokenAsync))]
	public async Task<IActionResult> GetAssignmentArtifactFromTokenAsync(string artifactToken,
		CancellationToken cancellationToken = default)
	{
		var artifact = await _artifactLeasingService.ArtifactFromTokenAsync(artifactToken, cancellationToken);

		// get fileName and mimeType
		var uri = new Uri(artifact.Url);
		var fileName = uri.Segments[^1];
		var mimeType = MimeTypes.GetMimeType(fileName);
		var contentType = new MediaTypeHeaderValue(mimeType);

		// read artifact to stream
		var stream = new MemoryStream();
		await _artifactLeasingService.StreamArtifactAsync(artifact, stream, cancellationToken);
		// reset stream to start so it can be read to the result
		stream.Position = 0;
		var result = new FileStreamResult(stream, contentType)
		{
			FileDownloadName = fileName
		};

		return result;
	}
}