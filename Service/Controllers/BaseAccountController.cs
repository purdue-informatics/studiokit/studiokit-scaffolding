﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using StudioKit.Configuration;
using StudioKit.Data.Entity.Identity;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Encryption;
using StudioKit.ErrorHandling;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ErrorHandling.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Utils;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.Scaffolding.BusinessLogic.Extensions;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.BusinessLogic.Services;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Properties;
using StudioKit.Scaffolding.Service.Models.RequestModels;
using StudioKit.Scaffolding.Service.Models.ViewModels;
using StudioKit.Security.Common;
using StudioKit.Sharding.Interfaces;
using StudioKit.TransientFaultHandling.Http.Interfaces;
using StudioKit.Utilities.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace StudioKit.Scaffolding.Service.Controllers;

[ApiController]
public class BaseAccountController<TUser, TGroup, TUserBusinessModel, TUserEditBusinessModel, TUserViewModel> : ControllerBase
	where TUser : IdentityUser, IUser, new()
	where TGroup : BaseGroup, new()
	where TUserBusinessModel : BaseUserBusinessModel<TUser>, new()
	where TUserEditBusinessModel : BaseUserEditBusinessModel<TUser>, new()
	where TUserViewModel : BaseUserViewModel<TUser, TUserBusinessModel>, new()
{
	private readonly IHostEnvironment _environment;
	private readonly IUserFactory<TUser> _userFactory;
	private readonly IUserManager<TUser> _userManager;
	private readonly IBaseUserService<TUser, TUserBusinessModel, TUserEditBusinessModel> _userService;
	private readonly PurdueCasService _purdueCasService;
	private readonly ILtiService _ltiService;
	private readonly ILogger<BaseAccountController<TUser, TGroup, TUserBusinessModel, TUserEditBusinessModel, TUserViewModel>> _logger;
	private readonly IErrorHandler _errorHandler;
	private readonly IOAuthService _oauthService;
	private readonly Func<IHttpClient> _getHttpClient;
	private readonly ILockDownBrowserService _lockDownBrowserService;
	private readonly IShardKeyProvider _shardKeyProvider;

	public BaseAccountController(
		IHostEnvironment environment,
		IUserFactory<TUser> userFactory,
		IUserManager<TUser> userManager,
		IBaseUserService<TUser, TUserBusinessModel, TUserEditBusinessModel> userService,
		PurdueCasService purdueCasService,
		ILtiService ltiService,
		ILogger<BaseAccountController<TUser, TGroup, TUserBusinessModel, TUserEditBusinessModel, TUserViewModel>> logger,
		IErrorHandler errorHandler,
		IOAuthService oauthService,
		Func<IHttpClient> getHttpClient,
		ILockDownBrowserService lockDownBrowserService,
		IShardKeyProvider shardKeyProvider)
	{
		_environment = environment;
		_userFactory = userFactory;
		_userManager = userManager;
		_userService = userService;
		_purdueCasService = purdueCasService;
		_ltiService = ltiService;
		_logger = logger;
		_errorHandler = errorHandler;
		_oauthService = oauthService;
		_getHttpClient = getHttpClient;
		_lockDownBrowserService = lockDownBrowserService;
		_shardKeyProvider = shardKeyProvider;
	}

	// GET api/account/userInfo
	public virtual async Task<IActionResult> GetUserInfoAsync(CancellationToken cancellationToken)
	{
		var userId = User.Identity.GetUserId();
		var user = await _userService.GetAsync(userId, User, cancellationToken);
		var viewModel = new TUserViewModel();
		viewModel.MapFrom(user);

		// set IsImpersonated value based on the ImpersonatorUserId claim
		if (User.Identity is ClaimsIdentity claimsIdentity)
		{
			var impersonatorUserIdClaim = claimsIdentity.Claims
				.SingleOrDefault(c => c.Type.Equals(ScaffoldingClaimTypes.ImpersonatorUserId));
			if (impersonatorUserIdClaim != null)
				viewModel.IsImpersonated = true;
		}

		// populate the LDB test url if there is an LDB secret in app settings
		if (!string.IsNullOrWhiteSpace(EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.LockDownBrowserSecret1)))
		{
			viewModel.LockDownBrowserTestLaunchUrl = _lockDownBrowserService.GetTestLaunchUrl(userId);
		}

		return Ok(viewModel);
	}

	#region OAuth

	[AllowAnonymous]
	[HttpPost("~/token")]
	public async Task<IActionResult> GetTokenAsync([FromForm] Dictionary<string, string> formData, CancellationToken cancellationToken)
	{
		try
		{
			var userAgent = Request.Headers.ContainsKey(HeaderNames.UserAgent) ? Request.Headers[HeaderNames.UserAgent].ToString() : "";
			var ipAddress = Request.HttpContext.Connection.RemoteIpAddress?.ToString();
			var tokenResponse = await _oauthService.GetTokenAsync(formData, userAgent, ipAddress, cancellationToken);
			return Ok(tokenResponse);
		}
		catch (BadRequestException e)
		{
			return BadRequest(new Dictionary<string, string> { { "error", e.Message } });
		}
	}

	#endregion OAuth

	#region CAS

	[AllowAnonymous]
	[HttpPost("casV1")]
	public async Task<IActionResult> CasV1Async(LoginRequestModel model, CancellationToken cancellationToken)
	{
		if (!ModelState.IsValid)
			return BadRequest(ModelState);
		var shardKey = _shardKeyProvider.GetShardKey();
		var externalLoginInfo = await _purdueCasService.ValidateV1CredentialsAsync(
			shardKey,
			model,
			cancellationToken);
		var user = GetUser(externalLoginInfo);
		return await FinishSignInAsync(user, externalLoginInfo.Principal.Claims.ToList(), externalLoginInfo,
			cancellationToken: cancellationToken);
	}

	[AllowAnonymous]
	[HttpGet("validateCasTicket")]
	public async Task<IActionResult> ValidateCasTicketAsync(string ticket, CancellationToken cancellationToken, string service = null)
	{
		var shardKey = _shardKeyProvider.GetShardKey();
		var externalLoginInfo = await _purdueCasService.ValidateTicketAsync(
			shardKey,
			ticket,
			service,
			cancellationToken);
		var user = GetUser(externalLoginInfo);
		return await FinishSignInAsync(user, externalLoginInfo.Principal.Claims.ToList(), externalLoginInfo,
			cancellationToken: cancellationToken);
	}

	#endregion CAS

	#region Local

	// POST api/account/local
	[AllowAnonymous]
	[HttpPost("local")]
	public async Task<IActionResult> LocalAsync(LocalLoginRequestModel model, CancellationToken cancellationToken)
	{
		if (!_environment.IsDevelopment())
			return NotFound();

		if (!ModelState.IsValid)
			return BadRequest(ModelState);

		var email = $"{model.Username}@localhost";
		var user = _userFactory.CreateUser(model.Username, email, uid: model.Username);

		var claims = new List<Claim>();

		if (model.IsInstructor)
		{
			claims.Add(new Claim(ClaimTypes.Role, BaseRole.Creator));
		}

		if (model.ShouldInsertMockLtiClaims)
		{
			var ltiClaimsDictionary = TestUtils.GetDeepLinkingClaimsDictionary();

			// remove/update some specific claims to avoid conflicts/changing user info
			ltiClaimsDictionary.Remove(LtiClaim.Roles);
			ltiClaimsDictionary.Remove(OidcTokenClaim.GivenName);
			ltiClaimsDictionary.Remove(OidcTokenClaim.FamilyName);
			ltiClaimsDictionary.Remove(OidcTokenClaim.MiddleName);
			ltiClaimsDictionary.Remove(OidcTokenClaim.Name);

			ltiClaimsDictionary[JsonWebTokenClaim.Subject] = model.Username;
			ltiClaimsDictionary[OidcTokenClaim.Email] = email;

			var ltiClaims = TestUtils.GetClaims(ltiClaimsDictionary).ToList();

			var updatedClaims = ClaimUtils.UpdateExternalClaimsIssuer(ltiClaims);
			var commonClaims = ClaimUtils.GetCommonClaims(updatedClaims);
			var brightspaceClaims = ClaimUtils.GetBrightspaceClaims(updatedClaims);
			var applicationRoleClaims = ClaimUtils.GetApplicationRoleClaims(updatedClaims);
			var combinedClaims = updatedClaims
				.Concat(commonClaims)
				.Concat(brightspaceClaims)
				.Concat(applicationRoleClaims);

			claims.AddRange(combinedClaims);
		}

		return await FinishSignInAsync(user, claims, null, cancellationToken: cancellationToken);
	}

	#endregion Local

	#region Lti

	/// <summary>
	/// LTI launch starts with an OpenID Connect launch
	/// (https://www.imsglobal.org/spec/security/v1p0/#openid_connect_launch_flow).  This is because
	/// the auth process begins in the Platform (i.e. Brightspace) vs. out on the internets when you
	/// are in an app and initiate auth from the app to an auth provider.  This endpoint generates
	/// an authentication request as a response such that the user's browser is redirected to the
	/// Tool's OpenID Connect Authentication Endpoint (provided when you register an LTI tool) with
	/// this auth request. The platform then redirects back here (via the redirect_uri that we provide
	/// here and that is registered in the platform) with an id_token that contains auth info used for the
	/// actual LTI launch.
	/// </summary>
	/// <param name="formData">OpenID Connect parameters</param>
	/// <param name="cancellationToken"></param>
	/// <returns></returns>
	[AllowAnonymous]
	[HttpPost("~/api/openidlogin")]
	public async Task<IActionResult> OpenIdLogin([FromForm] Dictionary<string, string> formData, CancellationToken cancellationToken)
	{
		var redirectUri = await _ltiService.PrepareOidcRedirectAsync(formData,
			EncryptedConfigurationManager.GetSetting("TIER") == Tier.Local
				? await GetNgrokTunnelAsync() + LtiConstants.LaunchPath
				: Url.Link("Lti1P3Launch", null), cancellationToken);
		return Redirect(redirectUri);
	}

	/// <summary>
	/// From an LTI launch request: validate the request, authenticate the user from provided claims, create an <see cref="LtiLaunch"/>
	/// record in the database that stores state from this launch request, and redirect the user agent to the frontend with an OAuth code.
	/// </summary>
	/// <param name="formData">LTI Launch parameters</param>
	/// <param name="cancellationToken"></param>
	/// <returns></returns>
	[AllowAnonymous]
	[HttpPost($"~{LtiConstants.LaunchPath}", Name = "Lti1P3Launch")]
	public async Task<IActionResult> Lti1P3LaunchAsync([FromForm] Dictionary<string, string> formData, CancellationToken cancellationToken)
	{
		string responseUrl;
		var ltiErrorReturnUrl = Request.GetWebUrlFromRequest();
		var queryParams = HttpUtility.ParseQueryString(string.Empty);
		IExceptionUser exceptionUser = null;
		try
		{
			var token = formData["id_token"];
			await _ltiService.AssertLtiLaunchRequestIsValidAsync(formData["state"], token);

			var launchPresentationReturnUrl = _ltiService.GetLaunchPresentationReturnUrl(token);
			if (!string.IsNullOrWhiteSpace(launchPresentationReturnUrl))
				ltiErrorReturnUrl = launchPresentationReturnUrl;

			var externalLoginInfo = await _ltiService.GetLoginInfoAsync(token, cancellationToken);
			var user = GetUser(externalLoginInfo);
			var claims = externalLoginInfo.Principal.Claims.ToList();

			// store user in case of exceptions
			exceptionUser = externalLoginInfo.Principal.Identity.ToExceptionUser();

			await _userManager.AssertAndAddImpersonatorUserIdClaimIfNeededAsync(claims);

			var (updatedUser, signInClaims) = await _userManager.FinishSignInAsync(user,
				claims,
				externalLoginInfo,
				Request.GetShardKeyFromHost(), cancellationToken);
			var launchResponse = await _ltiService.GetLaunchResponseAsync(token, updatedUser.Id, claims, cancellationToken);

			var responsePath = launchResponse.LtiLaunchId.HasValue
				? $"{LtiConstants.WebLaunchPath}/{launchResponse.LtiLaunchId}"
				: launchResponse.GroupId.HasValue && !launchResponse.TargetLinkUri.Contains("/courses")
					? $"/courses/{launchResponse.GroupId}"
					: "";

			responseUrl = $"{launchResponse.TargetLinkUri}{responsePath}";
			queryParams["code"] = _oauthService.GetAuthorizationCode(updatedUser, signInClaims);
		}
		catch (Exception e)
		{
			// change response to redirect to the error return url
			responseUrl = ltiErrorReturnUrl;

			// obfuscate the error details when not running in development mode unless it is a ForbiddenException
			queryParams[LtiConstants.ErrorKey] = e is not ForbiddenException && !_environment.IsDevelopment()
				? "An error was encountered while processing your request."
				: e.Message;

			_logger.LogError(e.Message);
			// do not send forbidden exceptions for lti launch to error handler
			if (e is not ForbiddenException)
			{
				_errorHandler.CaptureException(e, exceptionUser);
			}
		}

		return Redirect($"{responseUrl}{(queryParams.Count > 0 ? $"?{queryParams}" : "")}");
	}

	#endregion Lti

	#region Impersonation

	[HttpGet("startImpersonation")]
	public async Task<IActionResult> StartImpersonationAsync(string userId, CancellationToken cancellationToken)
	{
		var response = await _userService.StartImpersonationAsync(userId, User, cancellationToken);
		return Ok(response);
	}

	[HttpGet("stopImpersonation", Name = "StopImpersonation")]
	public async Task<IActionResult> StopImpersonationAsync(CancellationToken cancellationToken)
	{
		var response = await _userService.StopImpersonationAsync(User, cancellationToken);
		return Ok(response);
	}

	#endregion Impersonation

	#region LockDown Browser

	[AllowAnonymous]
	[HttpGet("~/api/ldb/test")]
	public IActionResult LockDownBrowserTest(string userId, int? entityId, string redirectPath, string title)
	{
		if (!_environment.IsDevelopment())
			return NotFound();

		var launchUrl = _lockDownBrowserService.GetLaunchUrl(
			userId,
			entityId,
			redirectPath,
			title);

		return new ContentResult
		{
			Content = "<html><body>" +
					$"<p>userId: {userId}</p>" +
					$"<p>entityId: {entityId}</p>" +
					$"<p>redirectPath: {redirectPath}</p>" +
					$"<p>title: {title}</p>" +
					$"<p>launchUrl: {launchUrl}</p>" +
					$"<p><a href=\"{launchUrl}\">Launch LDB</a></p>" +
					"</body></html>",
			ContentType = "text/html"
		};
	}

	[AllowAnonymous]
	[HttpGet($"~{LockDownBrowserConstants.RestartPath}")]
	public async Task<IActionResult> LockDownBrowserRestart(string userId, int? entityId, string redirectPath,
		CancellationToken cancellationToken)
	{
		var baseUrl = Request.GetApiUrlFromRequest();
		var redirectUrl = $"{baseUrl}{LockDownBrowserConstants.ChallengePath}";
		var queryParams = HttpUtility.ParseQueryString(string.Empty);

		try
		{
			queryParams = await _lockDownBrowserService.HandleRestartAsync(userId, entityId, redirectPath, cancellationToken);
		}
		catch (Exception e)
		{
			// change to redirect errors to the web
			redirectUrl = Request.GetWebUrlFromRequest();

			// obfuscate the error details when not running in development mode
			queryParams["error"] = !_environment.IsDevelopment()
				? Strings.LockDownBrowserRestartError
				: e.Message;

			_logger.LogError(e.Message);
			_errorHandler.CaptureException(e, new ExceptionUser
			{
				// use as much user info as we have available at the moment
				Id = userId
			});
		}

		return Redirect($"{redirectUrl}{(queryParams.AllKeys.Length > 0 ? $"?{queryParams}" : "")}");
	}

	[AllowAnonymous]
	[HttpGet($"~{LockDownBrowserConstants.ChallengePath}")]
	public async Task<IActionResult> LockDownBrowserChallenge(string challengeId, CancellationToken cancellationToken)
	{
		var redirectUrl = Request.GetWebUrlFromRequest();
		var redirectQueryParams = HttpUtility.ParseQueryString(string.Empty);
		IExceptionUser exceptionUser = null;

		try
		{
			var (user, entityId, redirectPath, queryParams) =
				await _lockDownBrowserService.HandleChallengeAsync(challengeId, Request, cancellationToken);

			// store user in case of exceptions
			exceptionUser = new ExceptionUser
			{
				Id = user.Id,
				UserName = user.UserName,
				Email = user.Email
			};

			var additionalClaims = new List<Claim>();
			if (entityId.HasValue)
			{
				additionalClaims.Add(new Claim(ScaffoldingClaimTypes.LockDownBrowserEntityId, entityId.ToString(), ClaimValueTypes.String));
			}

			var oauthCode = _oauthService.GetAuthorizationCode(user, additionalClaims);
			queryParams.Add("code", oauthCode);

			queryParams.Add("redirectPath", redirectPath);

			var baseWebUrl = Request.GetWebUrlFromRequest();
			redirectUrl = $"{baseWebUrl}{LockDownBrowserConstants.WebLaunchPath}";
			redirectQueryParams = queryParams;
		}
		catch (Exception e)
		{
			// obfuscate the error details when not running in development mode
			redirectQueryParams["error"] = !_environment.IsDevelopment()
				? Strings.LockDownBrowserRestartError
				: e.Message;

			_logger.LogError(e.Message);
			_errorHandler.CaptureException(e, exceptionUser);
		}

		return Redirect($"{redirectUrl}{(redirectQueryParams.AllKeys.Length > 0 ? $"?{redirectQueryParams}" : "")}");
	}

	#endregion LockDown Browser

	#region Helpers

	private TUser GetUser(ExternalLoginInfo externalLoginInfo)
	{
		// find the userName claim for the default issuer, e.g. for the app not an external username
		var userNameClaim = externalLoginInfo.Principal.Claims.First(c =>
			c.Type == ClaimTypes.Name &&
			c.Issuer == ClaimsIdentity.DefaultIssuer);
		var email = externalLoginInfo.Principal.FindFirstValue(ClaimTypes.Email);
		var userName = userNameClaim.Value;
		return _userFactory.CreateUser(userName, email);
	}

	private async Task<IActionResult> FinishSignInAsync(TUser user, List<Claim> claims, UserLoginInfo userLoginInfo, string shardKey = null,
		string returnUrl = null, CancellationToken cancellationToken = default)
	{
		var (updatedUser, signInClaims) = await _userManager.FinishSignInAsync(user,
			claims,
			userLoginInfo,
			shardKey ?? Request.GetShardKeyFromHost(),
			cancellationToken);

		var code = _oauthService.GetAuthorizationCode(updatedUser, signInClaims);

		// Return a JSON object if no returnUrl is set
		if (string.IsNullOrWhiteSpace(returnUrl))
			return Ok(new CodeResponse { Code = code });

		// Add "code" as a query parameter for returnUrl redirect
		return Redirect($"{returnUrl}?code={WebUtility.UrlEncode(code)}");
	}

	/// <summary>
	/// Using ngrok's http API, get the https url of the currently running tunnel
	/// </summary>
	/// <returns></returns>
	private async Task<string> GetNgrokTunnelAsync()
	{
		var result = await _getHttpClient().GetStringAsync(new Uri("http://localhost:4040/api/tunnels"));
		var deserializedResult = JsonConvert.DeserializeObject<dynamic>(result);
		if (deserializedResult == null)
		{
			throw new Exception("Couldn't parse response from ngrok");
		}

		return (string)((IEnumerable<JToken>)deserializedResult.tunnels.Children()).Single(t =>
			(string)t["proto"] == "https")["public_url"];
	}

	#endregion Helpers
}