﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StudioKit.Scaffolding.DataAccess.Interfaces;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Service.Controllers;

[Route("api/license")]
[ApiController]
public class LicenseController : ControllerBase
{
	private readonly IBaseDbContext _dbContext;

	public LicenseController(IBaseDbContext dbContext)
	{
		_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
	}

	[AllowAnonymous]
	[HttpGet]
	public async Task<IActionResult> GetAsync(CancellationToken cancellationToken = default)
	{
		var license = await _dbContext.GetLicenseAsync(cancellationToken);
		if (license == null)
			return Ok();
		return Ok(license);
	}
}