﻿using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace StudioKit.Scaffolding.Service.Controllers;

[Route("api/files")]
[ApiController]
public class FilesController : ControllerBase
{
	[HttpGet("processResult")]
	public static HttpResponseMessage ProcessResult(string data, string fileName)
	{
		if (string.IsNullOrEmpty(data))
			throw new HttpRequestException("data missing");

		var content = new StringContent(data);
		var response = new HttpResponseMessage(HttpStatusCode.OK)
		{
			Content = content,
		};

		response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/csv");
		response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
		{
			FileName = fileName
		};

		return response;
	}
}