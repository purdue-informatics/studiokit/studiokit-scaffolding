﻿using Microsoft.AspNetCore.Mvc;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.Service.Models.ViewModels;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Service.Controllers;

[Route("api/externalProviders")]
[ApiController]
public class ExternalProvidersController : ControllerBase
{
	private readonly IExternalProviderService _externalProviderService;

	public ExternalProvidersController(IExternalProviderService externalProviderService)
	{
		_externalProviderService = externalProviderService ?? throw new ArgumentNullException(nameof(externalProviderService));
	}

	[HttpGet("")]
	public async Task<IActionResult> GetAsync(CancellationToken cancellationToken = default)
	{
		var externalProviders = await _externalProviderService.GetAllAsync(User, cancellationToken);
		return Ok(externalProviders.Select(ep => new ExternalProviderViewModel(ep)));
	}
}