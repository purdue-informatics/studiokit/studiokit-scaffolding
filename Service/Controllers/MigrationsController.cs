﻿using Microsoft.AspNetCore.Mvc;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Service.Controllers;

[Route("api/migrations")]
[ApiController]
public class MigrationsController : ControllerBase
{
	private readonly IMigrationService _migrationService;

	public MigrationsController(IMigrationService migrationService)
	{
		_migrationService = migrationService ?? throw new ArgumentNullException(nameof(migrationService));
	}

	[HttpGet]
	public async Task<IActionResult> GetMigrationsAsync(CancellationToken cancellationToken = default)
	{
		var result = await _migrationService.GetMigrationStatusesAsync(User, cancellationToken);
		return Ok(result);
	}

	[HttpPost("migrate")]
	public async Task<IActionResult> MigrateAsync(string targetMigration = null, CancellationToken cancellationToken = default)
	{
		var result = await _migrationService.MigrateAsync(User, targetMigration, cancellationToken);
		return Ok(result);
	}

	[HttpPost("seed")]
	public async Task<IActionResult> SeedAsync(CancellationToken cancellationToken = default)
	{
		await _migrationService.SeedAsync(User, cancellationToken);
		return Ok();
	}
}