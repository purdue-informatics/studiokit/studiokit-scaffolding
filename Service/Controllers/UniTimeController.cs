﻿using Microsoft.AspNetCore.Mvc;
using StudioKit.Scaffolding.BusinessLogic.Extensions;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.Service.Models.ViewModels;
using StudioKit.Sharding;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Service.Controllers;

[Route("api/uniTime")]
[ApiController]
public class UniTimeController : ControllerBase
{
	private readonly IUniTimeService _uniTimeService;

	public UniTimeController(IUniTimeService uniTimeService)
	{
		_uniTimeService = uniTimeService ?? throw new ArgumentNullException(nameof(uniTimeService));
	}

	[HttpGet("instructorSchedule")]
	public async Task<IActionResult> GetInstructorScheduleAsync(string userId, int externalTermId,
		CancellationToken cancellationToken = default)
	{
		var shardKey = Request.GetShardKeyFromHost();
		if (!shardKey.ToLowerInvariant().Contains(ShardDomainConstants.PurdueShardKey))
			throw new NotSupportedException();
		var instructorSchedule = await _uniTimeService.GetInstructorScheduleAsync(userId, externalTermId, User, cancellationToken);
		instructorSchedule.ExternalGroups = instructorSchedule.ExternalGroups.Select(eg => new UniTimeGroupViewModel(eg));
		return Ok(instructorSchedule);
	}
}