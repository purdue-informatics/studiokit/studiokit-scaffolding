using Microsoft.ApplicationInsights.Channel;
using Microsoft.ApplicationInsights.Extensibility;
using StudioKit.Security.BusinessLogic.Interfaces;
using StudioKit.Utilities.Extensions;

namespace StudioKit.Scaffolding.Service;

public class PrincipalTelemetryInitializer(IPrincipalProvider principalProvider) : ITelemetryInitializer
{
	public void Initialize(ITelemetry telemetry)
	{
		telemetry.Context.User.AuthenticatedUserId = principalProvider.GetPrincipal()?.Identity?.GetUserId();
	}
}