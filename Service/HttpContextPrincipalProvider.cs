﻿using Microsoft.AspNetCore.Http;
using StudioKit.Security.BusinessLogic.Interfaces;
using System.Security.Principal;

namespace StudioKit.Scaffolding.Service;

public class HttpContextPrincipalProvider(IHttpContextAccessor httpContextAccessor) : IPrincipalProvider
{
	public IPrincipal GetPrincipal()
	{
		return httpContextAccessor.HttpContext?.User;
	}
}