﻿using Microsoft.Azure.SqlDatabase.ElasticScale.ShardManagement;
using StudioKit.ErrorHandling.WebApi;
using System;

namespace StudioKit.Scaffolding.Service.Filters;

public class ShardWebApiExceptionFilterAttribute : StaticWebApiExceptionFilterAttribute
{
	public override bool IsExceptionIgnored(Exception exception)
	{
		return base.IsExceptionIgnored(exception) || exception is ShardManagementException;
	}
}