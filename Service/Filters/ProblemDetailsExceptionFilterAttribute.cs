﻿using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Hosting;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ErrorHandling.WebApi;
using System;
using System.Net;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Service.Filters;

public class ProblemDetailsExceptionFilterAttribute : ExceptionFilterAttribute
{
	private readonly IHostEnvironment _hostEnvironment;
	private readonly ProblemDetailsFactory _problemDetailsFactory;

	public ProblemDetailsExceptionFilterAttribute(
		IHostEnvironment hostEnvironment,
		ProblemDetailsFactory problemDetailsFactory)
	{
		_hostEnvironment = hostEnvironment;
		_problemDetailsFactory = problemDetailsFactory;
	}

	public override Task OnExceptionAsync(ExceptionContext context)
	{
		// default to 500 and the provided exception
		var statusCode = HttpStatusCode.InternalServerError;
		var e = context.Exception;

		// check for specific exception types
		switch (context.Exception)
		{
			case EntityNotFoundException entityNotFoundException:
				statusCode = HttpStatusCode.NotFound;
				e = entityNotFoundException;
				break;

			case ForbiddenException forbiddenException:
				statusCode = HttpStatusCode.Forbidden;
				e = forbiddenException;
				break;

			case UnauthorizedException unauthorizedException:
				statusCode = HttpStatusCode.Unauthorized;
				e = unauthorizedException;
				break;

			case ValidationException validationException:
				statusCode = HttpStatusCode.BadRequest;
				e = validationException;
				break;

			case BadRequestException badRequestException:
				statusCode = HttpStatusCode.BadRequest;
				e = badRequestException;
				break;
		}

		var detail = !_hostEnvironment.IsDevelopment()
			? e.Message
			: $"{e.Message}{Environment.NewLine}{e.StackTrace}";

		ProblemDetails problemDetails;
		if (e is ValidationException ve)
		{
			var modelStateDictionary = new ModelStateDictionary();
			// convert ValidationException Errors to ModelStateDictionary
			foreach (var validationFailure in ve.Errors)
			{
				modelStateDictionary.AddModelError(validationFailure.PropertyName, validationFailure.ErrorMessage);
			}

			problemDetails = _problemDetailsFactory.CreateValidationProblemDetails(
				context.HttpContext,
				modelStateDictionary,
				(int?)statusCode,
				detail: detail);
		}
		else
		{
			problemDetails = _problemDetailsFactory.CreateProblemDetails(
				context.HttpContext,
				(int?)statusCode,
				detail: detail);
		}

		context.Result = ProblemDetailsUtils.CreateProblemDetailsObjectResult(problemDetails);
		context.ExceptionHandled = true;
		return Task.CompletedTask;
	}
}