﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using StudioKit.ErrorHandling.WebApi;
using StudioKit.Scaffolding.BusinessLogic.Extensions;
using StudioKit.Sharding;
using System.Linq;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Service.Filters;

public class RootDomainFilter : IAsyncResourceFilter
{
	private readonly ProblemDetailsFactory _problemDetailsFactory;

	public RootDomainFilter(ProblemDetailsFactory problemDetailsFactory)
	{
		_problemDetailsFactory = problemDetailsFactory;
	}

	public Task OnResourceExecutionAsync(ResourceExecutingContext context, ResourceExecutionDelegate next)
	{
		var shardKey = context.HttpContext.Request.GetShardKeyFromHost();
		if (shardKey == ShardDomainConstants.RootShardKey
			&& !context.ActionDescriptor.FilterDescriptors.Any(x => x.Filter is AllowRootDomainAttribute)
			)
		{
			var problemDetails = _problemDetailsFactory.CreateProblemDetails(
				context.HttpContext,
				StatusCodes.Status404NotFound);
			context.Result = ProblemDetailsUtils.CreateProblemDetailsObjectResult(problemDetails);
			return Task.CompletedTask;
		}

		return next.Invoke();
	}
}