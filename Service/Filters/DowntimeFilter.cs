﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using StudioKit.Configuration;
using StudioKit.Encryption;
using StudioKit.ErrorHandling.WebApi;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Service.Filters;

public class DowntimeFilter : IAsyncResourceFilter
{
	private readonly ProblemDetailsFactory _problemDetailsFactory;

	public DowntimeFilter(ProblemDetailsFactory problemDetailsFactory)
	{
		_problemDetailsFactory = problemDetailsFactory;
	}

	public Task OnResourceExecutionAsync(ResourceExecutingContext context, ResourceExecutionDelegate next)
	{
		var value = EncryptedConfigurationManager.GetSetting(BaseAppSetting.IsDowntime);
		if (string.IsNullOrWhiteSpace(value))
		{
			return next.Invoke();
		}

		var isDowntime = bool.Parse(value);
		if (isDowntime)
		{
			var problemDetails = _problemDetailsFactory.CreateProblemDetails(
				context.HttpContext,
				StatusCodes.Status403Forbidden,
				title: "We'll be right back!",
				detail: "Our developers are currently hard at work. Normal service will be restored soon.");
			context.Result = ProblemDetailsUtils.CreateProblemDetailsObjectResult(problemDetails);
			return Task.CompletedTask;
		}

		return next.Invoke();
	}
}