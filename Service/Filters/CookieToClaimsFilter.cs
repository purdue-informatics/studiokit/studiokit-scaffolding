using Microsoft.AspNetCore.Mvc.Filters;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Service.Filters;

public class CookieToClaimsFilter : IAsyncResourceFilter
{
	public static readonly List<(string cookieKey, string claimType)> CookiesToConvertToClaims = new();

	/// <summary>
	/// For each request cookie whose key is specified by <c>cookieKey</c> in this class' static list
	/// <see cref="CookiesToConvertToClaims"/>, add a claim with <c>claimType</c> as the claim's type
	/// and the cookie's value as the claim's value.
	/// </summary>
	public Task OnResourceExecutionAsync(ResourceExecutingContext context, ResourceExecutionDelegate next)
	{
		if (CookiesToConvertToClaims.Any())
		{
			var user = context.HttpContext.User;
			if (user.Identity is ClaimsIdentity { IsAuthenticated: true } identity)
				foreach (var (cookieKey, claimType) in CookiesToConvertToClaims)
				{
					var cookieValue = context.HttpContext.Request.Cookies[cookieKey];
					if (!string.IsNullOrEmpty(cookieValue))
						identity.AddClaim(new Claim(claimType, cookieValue));
				}
		}

		return next.Invoke();
	}
}