﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace StudioKit.Scaffolding.Service.Filters;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
public class AllowRootDomainAttribute : ActionFilterAttribute
{
}