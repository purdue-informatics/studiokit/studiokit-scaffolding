using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.StaticFiles;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Properties;
using System;
using System.IO;

namespace StudioKit.Scaffolding.Service.Utils;

public static class ArtifactUtils
{
	private const string TypeDiscriminatorName = "typename";

	public static IArtifactBusinessModel ExtractArtifactRequestModel(HttpRequest httpRequest)
	{
		var fileCollection = httpRequest.Form.Files;
		var formData = httpRequest.Form;
		var typename = formData.ContainsKey(TypeDiscriminatorName) ? formData[TypeDiscriminatorName].ToString() : null;
		if (string.IsNullOrWhiteSpace(typename))
		{
			throw new ValidationException(string.Format(Strings.ParameterRequired, TypeDiscriminatorName));
		}

		switch (typename)
		{
			case nameof(TextArtifact):
				return new TextArtifactBusinessModel
				{
					Text = formData.ContainsKey(nameof(TextArtifactBusinessModel.Text))
						? formData[nameof(TextArtifactBusinessModel.Text)].ToString().Trim()
						: null,
					WordCount = Convert.ToInt32(formData[nameof(TextArtifactBusinessModel.WordCount)])
				};

			case nameof(FileArtifact):
				if (fileCollection == null || fileCollection.Count < 1)
				{
					throw new ValidationException(Strings.FileRequired);
				}

				var postedFile = fileCollection[0];

				new FileExtensionContentTypeProvider().TryGetContentType(postedFile.FileName, out var contentType);
				contentType ??= "application/octet-stream";

				return new FileArtifactBusinessModel
				{
					// IE returns a file name with the full path of the local computer
					FileName = Path.GetFileName(postedFile.FileName),
					File = postedFile,
					ContentType = contentType
				};

			case nameof(UrlArtifact):
				return new UrlArtifactBusinessModel
				{
					Url = formData.ContainsKey(nameof(UrlArtifactBusinessModel.Url))
						? formData[nameof(UrlArtifactBusinessModel.Url)].ToString().Trim()
						: null,
				};

			default:
				throw new ValidationException(string.Format(Strings.ParameterInvalid, TypeDiscriminatorName));
		}
	}
}