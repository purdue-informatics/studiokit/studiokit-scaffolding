﻿using Autofac;
using Autofac.Core.Lifetime;
using Azure;
using Azure.AI.OpenAI;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using StudioKit.Caching.Redis;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Data.Utils;
using StudioKit.Encryption;
using StudioKit.ErrorHandling;
using StudioKit.ErrorHandling.Interfaces;
using StudioKit.ExternalProvider.Lti.DataAccess;
using StudioKit.ExternalProvider.Models.Interfaces;
using StudioKit.Notification.DataAccess.Interfaces;
using StudioKit.Notification.Models;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Services;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.DataAccess;
using StudioKit.Scaffolding.DataAccess.Interfaces;
using StudioKit.Scaffolding.Models;
using StudioKit.Security.BusinessLogic.Interfaces;
using StudioKit.Security.Entity.Identity.Interfaces;
using StudioKit.Security.Entity.Identity.Models;
using StudioKit.Security.Models;
using StudioKit.Sharding;
using StudioKit.Sharding.Entity.Identity.Models;
using StudioKit.Sharding.Entity.Interfaces;
using StudioKit.Sharding.Entity.Utils;
using StudioKit.Sharding.Interfaces;
using StudioKit.TransientFaultHandling.Http;
using StudioKit.Utilities;
using StudioKit.Utilities.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Activity = StudioKit.Scaffolding.Models.Activity;

namespace StudioKit.Scaffolding.Service.Utils;

public class BaseScriptUtils<TStartup, TContext, TUser, TGroup, TConfiguration, TExternalGradable, TNotificationLog> : IDisposable
	where TStartup : BaseStartup<TContext, TUser, TGroup, TConfiguration, TExternalGradable>
	where TContext : BaseDbContext<TUser, TGroup, TConfiguration>
	where TUser : IdentityUser, IUser, new()
	where TGroup : BaseGroup, new()
	where TConfiguration : BaseConfiguration, new()
	where TExternalGradable : class, IExternalGradable, new()
	where TNotificationLog : BaseNotificationLog<TGroup>, new()
{
	protected readonly string ConnectionString;
	private readonly string _shardKey;
	private readonly Action<ContainerBuilder> _builderOverrides;
	private IHttpContextAccessor _httpContextAccessor;

	public ILifetimeScope Scope { get; set; }

	public BaseScriptUtils(
		string connectionString,
		string shardKey = "purdue",
		Action<ContainerBuilder> builderOverrides = null)
	{
		var connectionStringBuilder = new SqlConnectionStringBuilder(connectionString);
		if (!SqlLocalDbUtils.IsLocalDb(connectionString))
		{
			connectionStringBuilder.TrustServerCertificate = true;
		}

		ConnectionString = connectionStringBuilder.ToString();
		_shardKey = shardKey;
		_builderOverrides = builderOverrides;

		Setup();
	}

	public void Dispose()
	{
		Scope.Dispose();
	}

	protected virtual ContainerBuilder CreateContainerBuilder(TStartup startup, IConfigurationRoot configuration)
	{
		ErrorHandler.Instance = new ScriptErrorHandler();

		var builder = new ContainerBuilder();

		// Setup AutoFac Dependency Injection and override registrations with locally defined instances
		startup.ConfigureContainer(builder);

		var loggerFactory = LoggingUtils.ConfigureLoggerFactory(configuration);
		builder.Register(c => loggerFactory)
			.As<ILoggerFactory>()
			.SingleInstance();

		builder.RegisterGeneric(typeof(Logger<>))
			.As(typeof(ILogger<>))
			.SingleInstance();

		builder.Register(c => new SimpleHttpClientFactory())
			.As<IHttpClientFactory>();

		var dateTimeProvider = new DateTimeProvider();
		builder.Register(c => dateTimeProvider)
			.As<IDateTimeProvider>()
			.SingleInstance();

		var shardKeyProvider = new SimpleShardKeyProvider(_shardKey);
		builder.Register(c => shardKeyProvider)
			.As<IShardKeyProvider>()
			.SingleInstance();

		_httpContextAccessor = new ScriptHttpContextAccessor();
		var principalProvider = new HttpContextPrincipalProvider(_httpContextAccessor);
		builder.Register(c => principalProvider)
			.As<IPrincipalProvider>()
			.SingleInstance();

		var optionsBuilder = new DbContextOptionsBuilder<TContext>()
			.EnableSensitiveDataLogging()
			.UseSqliteOrSqlServer(ConnectionString);

		var dbContext = (TContext)Activator.CreateInstance(typeof(TContext),
			BindingFlags.CreateInstance | BindingFlags.Public | BindingFlags.Instance | BindingFlags.OptionalParamBinding,
			null, new object[] { optionsBuilder.Options, dateTimeProvider, principalProvider, loggerFactory },
			CultureInfo.CurrentCulture);

		builder.Register(c => dbContext)
			.AsSelf()
			.As<IBaseDbContext>()
			.As<IShardDbContext<TConfiguration, License>>()
			.As<IIdentityClientDbContext<AppClient, IdentityServiceClient, ServiceClientRole, RefreshToken>>()
			.As<DbContext>()
			.As<ILtiLaunchDbContext<TGroup, GroupUserRole, ExternalTerm, ExternalGroup, ExternalGroupUser>>()
			.As<INotificationDbContext<TNotificationLog, TUser, TGroup, RoleActivity, Activity, GroupUserRole>>()
			.SingleInstance();

		// Only set the redis connection string for the database we are connecting to with LINQPad
		builder.RegisterType<RedisConnectionFactory>()
			.AsSelf()
			.WithParameter((pi, _) => pi.ParameterType == typeof(IDictionary<string, string>) && pi.Name == "connectionStrings",
				(_, _) => new Dictionary<string, string>
				{
					{
						_shardKey,
						EncryptedConfigurationManager.TryDecryptSettingValue(dbContext
							.Configurations
							.OrderByDescending(c => c.Id)
							.First().RedisConnectionString)
					}
				})
			.SingleInstance();

		#region IdentityCore

		builder.RegisterType<RoleStore<TContext>>()
			.As<IRoleStore<Role>>();

		var userStore = new UserStore<TUser, TContext>(dbContext);
		builder.Register(_ => userStore)
			.As<IUserStore<TUser>>();

		var userManager = new UserManager<TContext, TUser, TGroup, TConfiguration>(
			userStore, null, null, null, null, null, null, null, null, dbContext, null, null);
		builder.Register(_ => userManager)
			.As<IUserManager<TUser>>();

		builder.RegisterType<UpperInvariantLookupNormalizer>()
			.As<ILookupNormalizer>();

		builder.RegisterType<IdentityErrorDescriber>().AsSelf();

		#endregion

		// NOTE: OpenAIClient logging is not set up to work with ScriptUtils
		var openAiEndpoint = EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.OpenAiEndpoint);
		var openAiKey = EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.OpenAiApiKey);
		if (!string.IsNullOrWhiteSpace(openAiEndpoint) && !string.IsNullOrWhiteSpace(openAiKey))
		{
			builder.RegisterType<OpenAIClient>()
				.AsSelf()
				.WithParameter(new TypedParameter(typeof(Uri), new Uri(openAiEndpoint)))
				.WithParameter(new TypedParameter(typeof(AzureKeyCredential), new AzureKeyCredential(openAiKey)))
				.WithParameter(new TypedParameter(typeof(OpenAIClientOptions), new OpenAIClientOptions()));
		}

		_builderOverrides?.Invoke(builder);

		return builder;
	}

	protected virtual void ConfigureShard(TStartup startup)
	{
		startup.ConfigureShard(null, null);
	}

	private void Setup()
	{
		var configuration = new ConfigurationBuilder()
			.SetBasePath(Directory.GetCurrentDirectory())
			.AddJsonFile("appsettings.json", true, true)
			.AddJsonFile("appsettings.Development.json", true, true)
			.AddEnvironmentVariables()
			.Build();
		EncryptedConfigurationManager.Configuration = configuration;
		var startup = (TStartup)Activator.CreateInstance(typeof(TStartup), configuration);
		var builder = CreateContainerBuilder(startup, configuration);
		var container = builder.Build();
		Scope = container.BeginLifetimeScope(MatchingScopeLifetimeTags.RequestLifetimeScopeTag);

		ConfigureShard(startup);
	}

	public async Task<IPrincipal> CreateUserPrincipalAsync(string userId, bool setHttpContextUser = false)
	{
		var dbContext = Scope.Resolve<TContext>();
		var user = await dbContext.Users.SingleAsync(u => u.Id == userId);
		var identity = new ClaimsIdentity(new List<Claim>
		{
			new(ClaimTypes.NameIdentifier, user.Id, ClaimValueTypes.String),
			new(ClaimTypes.Name, user.UserName, ClaimValueTypes.String)
		});
		var principal = new ClaimsPrincipal(identity);
		if (setHttpContextUser)
		{
			Debug.Assert(_httpContextAccessor.HttpContext != null, "_httpContextAccessor.HttpContext != null");
			_httpContextAccessor.HttpContext.User = principal;
		}

		return principal;
	}
}

public class ScriptErrorHandler : IErrorHandler
{
	public void CaptureException(Exception exception)
	{
		CaptureException(exception, null);
	}

	public void CaptureException(Exception exception, IExceptionUser user)
	{
		CaptureException(exception, null, null);
	}

	public void CaptureException(Exception exception, IExceptionUser user, object extra)
	{
		Console.WriteLine(exception);
	}
}

public class ScriptHttpContextAccessor : IHttpContextAccessor
{
	public ScriptHttpContextAccessor()
	{
		HttpContext = new DefaultHttpContext();
	}

	public HttpContext HttpContext { get; set; }
}