﻿using CsvHelper;
using CsvHelper.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Service.MediaTypeFormatters;

public class CsvMediaTypeFormatter : TextOutputFormatter
{
	private const string MediaTypeHeader = "text/csv";
	private readonly CsvConfiguration _configuration;

	public CsvMediaTypeFormatter(CsvConfiguration configuration)
	{
		_configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));

		SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse(MediaTypeHeader));
		SupportedEncodings.Add(Encoding.UTF8);
		SupportedEncodings.Add(Encoding.Unicode);
	}

	protected override bool CanWriteType(Type type)
	{
		return type != null && IsTypeOfIEnumerable(type);
	}

	public override bool CanWriteResult(OutputFormatterCanWriteContext context)
	{
		// do NOT allow the formatter to write if the request does NOT have a ContentType set
		return context.ContentType.HasValue && base.CanWriteResult(context);
	}


	public override async Task WriteResponseBodyAsync(OutputFormatterWriteContext context, Encoding selectedEncoding)
	{
		var httpContext = context.HttpContext;

		if (context.Object == null)
			return;

		var stringBuilder = new StringBuilder();
		await using (var textWriter = new StringWriter(stringBuilder))
		await using (var csvWriter = new CsvWriter(textWriter, configuration: _configuration))
			await csvWriter.WriteRecordsAsync(context.Object as IEnumerable<object>);

		await httpContext.Response.WriteAsync(stringBuilder.ToString(), selectedEncoding);
	}

	#region Private methods

	private static bool IsTypeOfIEnumerable(Type type)
	{
		return type.GetInterfaces().Any(interfaceType => interfaceType == typeof(IEnumerable));
	}

	#endregion Private methods
}