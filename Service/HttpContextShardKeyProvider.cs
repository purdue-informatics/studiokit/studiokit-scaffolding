﻿using FluentValidation;
using Microsoft.AspNetCore.Http;
using StudioKit.Scaffolding.BusinessLogic.Extensions;
using StudioKit.Sharding.Interfaces;
using StudioKit.Sharding.Properties;

namespace StudioKit.Scaffolding.Service;

public class HttpContextShardKeyProvider(IHttpContextAccessor httpContextAccessor) : IShardKeyProvider
{
	public string GetShardKey(bool shouldThrowIfNullOrWhitespace = true)
	{
		var shardKey = httpContextAccessor.HttpContext?.Request.GetShardKeyFromHost();
		if (string.IsNullOrWhiteSpace(shardKey) && shouldThrowIfNullOrWhitespace)
			throw new ValidationException(Strings.ShardKeyRequired);
		return shardKey;
	}
}