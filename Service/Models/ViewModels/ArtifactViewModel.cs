﻿using StudioKit.Scaffolding.Models;
using System;

namespace StudioKit.Scaffolding.Service.Models.ViewModels;

public class ArtifactViewModel
{
	public ArtifactViewModel(Artifact artifact, bool includeFileName = true)
	{
		if (artifact == null) throw new ArgumentNullException(nameof(artifact));

		Id = artifact.Id;
		// add date as a cache buster value
		Url = $"{artifact.Url}?v={artifact.DateLastUpdated:O}";
		DateStored = artifact.DateStored;
		DateLastUpdated = artifact.DateLastUpdated;

		switch (artifact)
		{
			case TextArtifact t:
				WordCount = t.WordCount;
				Typename = nameof(TextArtifact);
				break;

			case FileArtifact f:
				if (includeFileName)
					FileName = f.FileName;
				Typename = nameof(FileArtifact);
				break;

			case UrlArtifact _:
				Typename = nameof(UrlArtifact);
				break;
		}
	}

	public int Id { get; set; }

	public string Url { get; set; }

	public string FileName { get; set; }

	public int? WordCount { get; set; }

	public string Typename { get; set; }

	public DateTime DateStored { get; set; }

	public DateTime DateLastUpdated { get; set; }
}