﻿using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Models;

namespace StudioKit.Scaffolding.Service.Models.ViewModels;

public class BaseCreatedGroupUserRolesViewModel<TUser> : BaseCreatedEntityUserRolesViewModel<TUser, GroupUserRole>
	where TUser : IUser
{
	protected override IViewModel<EntityUserRoleBusinessModel<TUser, GroupUserRole>> MapEntityUserRole(
		EntityUserRoleBusinessModel<TUser, GroupUserRole> businessModel)
	{
		return new BaseGroupUserRoleViewModel<TUser>().MapFrom(businessModel);
	}
}