﻿using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using System.Collections.Generic;
using System.Linq;

namespace StudioKit.Scaffolding.Service.Models.ViewModels;

public class BaseCreatedEntityUserRolesViewModel<TUser, TEntityUserRole> : IViewModel<
	CreatedEntityUserRolesBusinessModel<TUser, TEntityUserRole>>
	where TUser : IUser
	where TEntityUserRole : IEntityUserRole
{
	public virtual IViewModel<CreatedEntityUserRolesBusinessModel<TUser, TEntityUserRole>> MapFrom(
		CreatedEntityUserRolesBusinessModel<TUser, TEntityUserRole> model)
	{
		AddedUserRoles = model.AddedUserRoles.Select(MapEntityUserRole).ToList();
		ExistingUserRoles = model.ExistingUserRoles.Select(MapEntityUserRole).ToList();
		BlockedUserRoles = model.BlockedUserRoles.Select(MapEntityUserRole).ToList();
		InvalidIdentifiers = model.InvalidIdentifiers.ToList();
		InvalidDomainIdentifiers = model.InvalidDomainIdentifiers.ToList();
		return this;
	}

	protected virtual IViewModel<EntityUserRoleBusinessModel<TUser, TEntityUserRole>> MapEntityUserRole(
		EntityUserRoleBusinessModel<TUser, TEntityUserRole> businessModel)
	{
		return new BaseEntityUserRoleViewModel<TUser, TEntityUserRole>().MapFrom(businessModel);
	}

	/// <summary>
	/// EntityUserRoles that were added to the Entity Role, sent in the create request
	/// </summary>
	public IList<IViewModel<EntityUserRoleBusinessModel<TUser, TEntityUserRole>>> AddedUserRoles { get; set; }

	/// <summary>
	/// EntityUserRoles that were already in the Entity Role, sent in the create request
	/// </summary>
	public IList<IViewModel<EntityUserRoleBusinessModel<TUser, TEntityUserRole>>> ExistingUserRoles { get; set; }

	/// <summary>
	/// EntityUserRoles that were blocked from being added because multiple roles per user is not allowed
	/// </summary>
	public IList<IViewModel<EntityUserRoleBusinessModel<TUser, TEntityUserRole>>> BlockedUserRoles { get; set; }

	public IList<string> InvalidIdentifiers { get; set; }

	public string AllowedDomains { get; set; }

	public IList<string> InvalidDomainIdentifiers { get; set; }
}