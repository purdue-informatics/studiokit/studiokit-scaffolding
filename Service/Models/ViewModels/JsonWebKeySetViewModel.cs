using Microsoft.IdentityModel.Tokens;
using System.Collections.Generic;
using System.Linq;

namespace StudioKit.Scaffolding.Service.Models.ViewModels;

public class JsonWebKeySetViewModel
{
	public JsonWebKeySetViewModel(JsonWebKeySet jsonWebKeySet)
	{
		Keys = jsonWebKeySet.Keys
			.Select(k => new JsonWebKeyViewModel(k))
			.ToList();
	}

	public IList<JsonWebKeyViewModel> Keys { get; set; }
}