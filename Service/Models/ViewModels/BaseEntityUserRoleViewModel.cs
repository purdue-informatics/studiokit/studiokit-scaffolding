﻿using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;

namespace StudioKit.Scaffolding.Service.Models.ViewModels;

public class BaseEntityUserRoleViewModel<TUser, TEntityUserRole> : IViewModel<EntityUserRoleBusinessModel<TUser, TEntityUserRole>>
	where TUser : IUser
	where TEntityUserRole : IEntityUserRole
{
	public virtual IViewModel<EntityUserRoleBusinessModel<TUser, TEntityUserRole>> MapFrom(
		EntityUserRoleBusinessModel<TUser, TEntityUserRole> model)
	{
		Id = model.EntityUserRole.Id;
		EntityId = model.EntityUserRole.EntityId;
		UserId = model.User.Id;
		FirstName = model.User.FirstName;
		LastName = model.User.LastName;
		Email = model.User.Email;
		Uid = model.User.Uid;
		Role = model.Role;
		return this;
	}

	public int Id { get; set; }

	public int EntityId { get; set; }

	public string UserId { get; set; }

	public string FirstName { get; set; }

	public string LastName { get; set; }

	public string Email { get; set; }

	public string Uid { get; set; }

	public string Role { get; set; }
}