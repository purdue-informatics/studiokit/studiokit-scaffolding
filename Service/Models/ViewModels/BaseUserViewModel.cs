﻿using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StudioKit.Scaffolding.Service.Models.ViewModels;

public class BaseUserViewModel<TUser, TUserBusinessModel> : IViewModel<TUserBusinessModel>
	where TUser : IUser
	where TUserBusinessModel : BaseUserBusinessModel<TUser>
{
	public virtual IViewModel<TUserBusinessModel> MapFrom(TUserBusinessModel businessModel)
	{
		if (businessModel == null) throw new ArgumentNullException(nameof(businessModel));
		if (businessModel.User == null) throw new ArgumentNullException(nameof(businessModel.User));

		var user = businessModel.User;
		Id = user.Id;
		UserName = user.UserName;
		FirstName = user.FirstName;
		LastName = user.LastName;
		Email = user.Email;
		Uid = user.Uid;
		CareerAccountAlias = user.CareerAccountAlias;
		EmployeeNumber = user.EmployeeNumber;
		Puid = user.Puid;
		DateStored = user.DateStored;
		DateLastUpdated = user.DateLastUpdated;

		Roles = businessModel.Roles.ToList();
		Activities = businessModel.Activities.ToList();
		return this;
	}

	public string Id { get; set; }

	public string UserName { get; set; }

	public string FirstName { get; set; }

	public string LastName { get; set; }

	public string Uid { get; set; }

	public string CareerAccountAlias { get; set; }

	public string EmployeeNumber { get; set; }

	public string Puid { get; set; }

	public string Email { get; set; }

	public DateTime DateStored { get; set; }

	public DateTime DateLastUpdated { get; set; }

	public bool IsImpersonated { get; set; }

	public IList<string> Roles { get; set; } = new List<string>();

	public IList<string> Activities { get; set; } = new List<string>();

	public string LockDownBrowserTestLaunchUrl { get; set; }
}