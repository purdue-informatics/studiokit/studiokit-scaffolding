﻿using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Models;

namespace StudioKit.Scaffolding.Service.Models.ViewModels;

public class BaseGroupUserRoleViewModel<TUser> : BaseEntityUserRoleViewModel<TUser, GroupUserRole>
	where TUser : IUser
{
	public override IViewModel<EntityUserRoleBusinessModel<TUser, GroupUserRole>> MapFrom(
		EntityUserRoleBusinessModel<TUser, GroupUserRole> model)
	{
		base.MapFrom(model);
		IsExternal = model.EntityUserRole.IsExternal;
		return this;
	}

	public bool IsExternal { get; set; }
}