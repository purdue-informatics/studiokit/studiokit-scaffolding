﻿using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.Scaffolding.BusinessLogic.Models;
using System;

namespace StudioKit.Scaffolding.Service.Models.ViewModels;

public class LtiLaunchViewModel : IExternalGroupEditBusinessModel
{
	public LtiLaunchViewModel(LtiLaunch ltiLaunch)
	{
		if (ltiLaunch == null) throw new ArgumentNullException(nameof(ltiLaunch));

		Id = ltiLaunch.Id;
		UserId = ltiLaunch.CreatedById;
		ExternalProviderId = ltiLaunch.ExternalProviderId;
		ExternalId = ltiLaunch.ExternalId;
		Name = ltiLaunch.Name;
		Description = ltiLaunch.Description;
		GroupId = ltiLaunch.GroupId;
		CourseSectionStartDate = ltiLaunch.CourseSectionStartDate;
		CourseSectionEndDate = ltiLaunch.CourseSectionEndDate;
		Roles = ltiLaunch.Roles;
		RosterUrl = ltiLaunch.RosterUrl;
		GradesUrl = ltiLaunch.GradesUrl;
		LtiVersion = ltiLaunch.LtiVersion;
		ReturnUrl = ltiLaunch.ReturnUrl;
		DeepLinkingReturnUrl = ltiLaunch.DeepLinkingReturnUrl;
	}

	public int Id { get; set; }

	public string Typename => nameof(LtiLaunch);

	public string UserId { get; set; }

	public int ExternalProviderId { get; set; }

	public string ExternalId { get; set; }

	public string Name { get; set; }

	public string Description { get; set; }

	/// <summary>
	/// When not null, indicates that a group exists for the launch and does not need to be created by the front-end.
	/// </summary>
	public int? GroupId { get; set; }

	public DateTime? CourseSectionStartDate { get; set; }

	public DateTime? CourseSectionEndDate { get; set; }

	public string Roles { get; set; }

	public string RosterUrl { get; set; }

	public string GradesUrl { get; set; }

	public string LtiVersion { get; set; }

	public string ReturnUrl { get; set; }

	public bool? IsAutoGradePushEnabled { get; set; }

	/// <summary>
	/// The platform URL to which the browser should be redirected when the deep link selection process is complete.
	/// </summary>
	public string DeepLinkingReturnUrl { get; set; }
}