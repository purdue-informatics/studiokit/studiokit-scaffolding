using Microsoft.IdentityModel.Tokens;

namespace StudioKit.Scaffolding.Service.Models.ViewModels;

public class JsonWebKeyViewModel
{
	public JsonWebKeyViewModel(JsonWebKey jsonWebKey)
	{
		Alg = jsonWebKey.Alg;
		Kty = jsonWebKey.Kty;
		Use = jsonWebKey.Use;
		Kid = jsonWebKey.Kid;
		E = jsonWebKey.E;
		N = jsonWebKey.N;
	}

	public string Alg { get; set; }

	public string Kty { get; set; }

	public string Use { get; set; }

	public string Kid { get; set; }

	public string E { get; set; }

	public string N { get; set; }
}