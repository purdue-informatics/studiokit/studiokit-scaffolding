﻿using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using System.Collections.Generic;
using System.Linq;

namespace StudioKit.Scaffolding.Service.Models.ViewModels;

public class BaseCreatedUserRolesViewModel<TUser> : IViewModel<CreatedUserRolesBusinessModel<TUser>>
	where TUser : IUser
{
	public IViewModel<CreatedUserRolesBusinessModel<TUser>> MapFrom(CreatedUserRolesBusinessModel<TUser> model)
	{
		AddedUserRoles = model.AddedUserRoles
			.Select(au => new BaseUserRoleViewModel<TUser>().MapFrom(au))
			.ToList();
		ExistingUserRoles = model.ExistingUserRoles
			.Select(eu => new BaseUserRoleViewModel<TUser>().MapFrom(eu))
			.ToList();
		InvalidIdentifiers = model.InvalidIdentifiers.ToList();
		AllowedDomains = model.AllowedDomains;
		InvalidDomainIdentifiers = model.InvalidDomainIdentifiers.ToList();
		return this;
	}

	/// <summary>
	/// UserRoles that were added to the Role, sent in the create request
	/// </summary>
	public IList<IViewModel<UserRoleBusinessModel<TUser>>> AddedUserRoles { get; set; }

	/// <summary>
	/// UserRoles that were already in the Role, sent in the create request
	/// </summary>
	public IList<IViewModel<UserRoleBusinessModel<TUser>>> ExistingUserRoles { get; set; }

	public IList<string> InvalidIdentifiers { get; set; }

	public string AllowedDomains { get; set; }

	public IList<string> InvalidDomainIdentifiers { get; set; }
}