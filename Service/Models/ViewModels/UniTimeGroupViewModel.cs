﻿using StudioKit.ExternalProvider.UniTime.Models;
using StudioKit.Scaffolding.BusinessLogic.Models;

namespace StudioKit.Scaffolding.Service.Models.ViewModels;

public class UniTimeGroupViewModel : UniTimeGroupBusinessModel
{
	public UniTimeGroupViewModel()
	{
	}

	public UniTimeGroupViewModel(UniTimeGroupBusinessModel businessModel)
	{
		Id = businessModel.Id;
		ExternalProviderId = businessModel.ExternalProviderId;
		ExternalId = businessModel.ExternalId;
		Name = businessModel.Name;
		Description = businessModel.Description;
		UserId = businessModel.UserId;
	}

	public string Typename => nameof(UniTimeGroup);
}