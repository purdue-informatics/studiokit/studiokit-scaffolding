﻿using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.UniTime.Models;
using System;

namespace StudioKit.Scaffolding.Service.Models.ViewModels;

public class ExternalProviderViewModel
{
	public ExternalProviderViewModel(ExternalProvider.Models.ExternalProvider externalProvider)
	{
		if (externalProvider == null) throw new ArgumentNullException(nameof(externalProvider));

		Id = externalProvider.Id;
		Name = externalProvider.Name;
		RosterSyncEnabled = externalProvider.RosterSyncEnabled;
		GradePushEnabled = externalProvider.GradePushEnabled;
		TermSyncEnabled = externalProvider.TermSyncEnabled;

		switch (externalProvider)
		{
			case UniTimeExternalProvider:
				Typename = nameof(UniTimeExternalProvider);
				break;

			case LtiExternalProvider ltiExternalProvider:
				Typename = nameof(LtiExternalProvider);
				ShouldDisplayCreateNonLtiGroupAlert = ltiExternalProvider.ShouldDisplayCreateNonLtiGroupAlert;
				break;
		}
	}

	public int Id { get; set; }

	public string Typename { get; set; }

	public string Name { get; set; }

	public bool TermSyncEnabled { get; set; }

	public bool RosterSyncEnabled { get; set; }

	public bool GradePushEnabled { get; set; }

	public bool ShouldDisplayCreateNonLtiGroupAlert { get; set; }
}