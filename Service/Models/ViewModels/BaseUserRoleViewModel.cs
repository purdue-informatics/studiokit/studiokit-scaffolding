﻿using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;

namespace StudioKit.Scaffolding.Service.Models.ViewModels;

public class BaseUserRoleViewModel<TUser> : IViewModel<UserRoleBusinessModel<TUser>>
	where TUser : IUser
{
	public IViewModel<UserRoleBusinessModel<TUser>> MapFrom(UserRoleBusinessModel<TUser> model)
	{
		Id = model.User.Id;
		FirstName = model.User.FirstName;
		LastName = model.User.LastName;
		Email = model.User.Email;
		Uid = model.User.Uid;
		Role = model.Role;
		return this;
	}

	public string Id { get; set; }

	public string FirstName { get; set; }

	public string LastName { get; set; }

	public string Email { get; set; }

	public string Uid { get; set; }

	public string Role { get; set; }
}