﻿using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Models;

namespace StudioKit.Scaffolding.Service.Models.ViewModels;

public class ExternalGroupViewModel : ExternalGroupBusinessModel
{
	public ExternalGroupViewModel(ExternalGroupBusinessModel businessModel)
	{
		Id = businessModel.Id;
		GroupId = businessModel.GroupId;
		ExternalId = businessModel.ExternalId;
		ExternalProviderId = businessModel.ExternalProviderId;
		Name = businessModel.Name;
		Description = businessModel.Description;
		RosterUrl = businessModel.RosterUrl;
		GradesUrl = businessModel.GradesUrl;
		UserId = businessModel.UserId;
		IsAutoGradePushEnabled = businessModel.IsAutoGradePushEnabled;
	}

	public string Typename => nameof(ExternalGroup);
}