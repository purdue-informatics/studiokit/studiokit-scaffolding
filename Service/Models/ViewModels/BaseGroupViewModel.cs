using Microsoft.AspNetCore.Identity;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StudioKit.Scaffolding.Service.Models.ViewModels;

public class BaseGroupViewModel<TGroup, TGroupBusinessModel, TUser> : IViewModel<TGroupBusinessModel>
	where TUser : IdentityUser, IUser
	where TGroup : BaseGroup
	where TGroupBusinessModel : BaseGroupBusinessModel<TGroup, TUser>
{
	public virtual IViewModel<TGroupBusinessModel> MapFrom(TGroupBusinessModel businessModel)
	{
		if (businessModel == null) throw new ArgumentNullException(nameof(businessModel));
		if (businessModel.Group == null) throw new ArgumentNullException(nameof(businessModel.Group));

		Id = businessModel.Group.Id;
		ExternalTermId = businessModel.Group.ExternalTermId;
		Name = businessModel.Group.Name;
		StartDate = businessModel.Group.StartDate;
		EndDate = businessModel.Group.EndDate;
		IsDeleted = businessModel.Group.IsDeleted;
		IsRosterSyncEnabled = businessModel.IsRosterSyncEnabled;
		ExternalGroups = businessModel.ExternalGroups
			.Select(b => new ExternalGroupViewModel(b))
			.ToList();

		Roles = businessModel.Roles.ToList();
		Activities = businessModel.Activities.ToList();
		Owners = businessModel.Owners
			.Select(o => new BaseEntityUserViewModel<TUser>().MapFrom(o))
			.ToList();
		Graders = businessModel.Graders
			.Select(o => new BaseEntityUserViewModel<TUser>().MapFrom(o))
			.ToList();
		return this;
	}

	public int Id { get; set; }

	public string Name { get; set; }

	public int? ExternalTermId { get; set; }

	public DateTime? StartDate { get; set; }

	public DateTime? EndDate { get; set; }

	public bool IsDeleted { get; private set; }

	public bool IsRosterSyncEnabled { get; set; }

	public IList<string> Roles { get; set; } = new List<string>();

	public IList<string> Activities { get; set; } = new List<string>();

	public IList<IViewModel<EntityUserBusinessModel<TUser>>> Owners { get; set; } =
		new List<IViewModel<EntityUserBusinessModel<TUser>>>();

	public IList<IViewModel<EntityUserBusinessModel<TUser>>> Graders { get; set; } =
		new List<IViewModel<EntityUserBusinessModel<TUser>>>();

	public IList<ExternalGroupViewModel> ExternalGroups { get; set; } = new List<ExternalGroupViewModel>();
}