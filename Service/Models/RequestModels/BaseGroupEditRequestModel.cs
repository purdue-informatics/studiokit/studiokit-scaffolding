﻿using System;
using System.Collections.Generic;

namespace StudioKit.Scaffolding.Service.Models.RequestModels;

public class BaseGroupEditRequestModel
{
	public string Name { get; set; }

	public int? ExternalTermId { get; set; }

	public DateTime? StartDate { get; set; }

	public DateTime? EndDate { get; set; }

	public bool? IsDeleted { get; set; }

	public IEnumerable<IExternalGroupEditRequestModel> ExternalGroups { get; set; } = new List<IExternalGroupEditRequestModel>();
}