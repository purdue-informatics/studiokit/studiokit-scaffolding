using System.ComponentModel.DataAnnotations;

namespace StudioKit.Scaffolding.Service.Models.RequestModels;

public class LocalLoginRequestModel
{
	[Display(Name = "Username")]
	public string Username { get; set; }

	public bool IsInstructor { get; set; }

	public bool ShouldInsertMockLtiClaims { get; set; }
}