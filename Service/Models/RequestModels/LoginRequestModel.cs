using System.ComponentModel.DataAnnotations;

namespace StudioKit.Scaffolding.Service.Models.RequestModels;

public class LoginRequestModel
{
	[Display(Name = "Username")]
	public string Username { get; set; }

	[StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
	[DataType(DataType.Password)]
	[Display(Name = "Password")]
	public string Password { get; set; }
}