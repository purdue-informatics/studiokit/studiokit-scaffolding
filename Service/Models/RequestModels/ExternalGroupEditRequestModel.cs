﻿using Newtonsoft.Json;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Service.Models.Converters;

namespace StudioKit.Scaffolding.Service.Models.RequestModels;

[JsonConverter(typeof(ExternalGroupEditRequestModelConverter))]
public interface IExternalGroupEditRequestModel : IExternalGroupEditBusinessModel
{
}

public class ExternalGroupEditRequestModel : ExternalGroupEditBusinessModel, IExternalGroupEditRequestModel
{
}

public class LtiLaunchExternalGroupEditRequestModel : LtiLaunchExternalGroupEditBusinessModel, IExternalGroupEditRequestModel
{
}

public class UniTimeGroupExternalGroupEditRequestModel : UniTimeGroupExternalGroupEditBusinessModel, IExternalGroupEditRequestModel
{
}