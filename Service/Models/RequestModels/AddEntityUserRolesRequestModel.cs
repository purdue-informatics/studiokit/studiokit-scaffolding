﻿using System.Collections.Generic;

namespace StudioKit.Scaffolding.Service.Models.RequestModels;

public class AddEntityUserRolesRequestModel
{
	public int EntityId { get; set; }

	public List<string> Identifiers { get; set; }

	public string RoleName { get; set; }
}