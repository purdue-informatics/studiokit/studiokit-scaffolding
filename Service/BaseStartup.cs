using Autofac;
using Autofac.Core;
using Autofac.Extensions.DependencyInjection;
using Azure;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using CsvHelper.Configuration;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Azure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using StudioKit.Caching.Interfaces;
using StudioKit.Caching.Redis;
using StudioKit.Caliper;
using StudioKit.Caliper.Controllers.API;
using StudioKit.Caliper.Interfaces;
using StudioKit.Cloud.Storage;
using StudioKit.Cloud.Storage.Blob;
using StudioKit.Cloud.Storage.Queue;
using StudioKit.Configuration;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Data.Entity.Identity.Services;
using StudioKit.Encryption;
using StudioKit.ErrorHandling;
using StudioKit.ErrorHandling.Interfaces;
using StudioKit.ErrorHandling.WebApi;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Services;
using StudioKit.ExternalProvider.Models.Interfaces;
using StudioKit.ExternalProvider.UniTime.BusinessLogic.Services;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.BusinessLogic.Services;
using StudioKit.Scaffolding.BusinessLogic.Services.Authorization;
using StudioKit.Scaffolding.BusinessLogic.Services.OAuth;
using StudioKit.Scaffolding.BusinessLogic.Utils;
using StudioKit.Scaffolding.BusinessLogic.Validators;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.DataAccess;
using StudioKit.Scaffolding.DataAccess.Queues.QueueManagers;
using StudioKit.Scaffolding.DataAccess.Queues.QueueMessages;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Service.Filters;
using StudioKit.Scaffolding.Service.MediaTypeFormatters;
using StudioKit.Security.BusinessLogic.Interfaces;
using StudioKit.Security.BusinessLogic.Services;
using StudioKit.Sharding;
using StudioKit.Sharding.Entity.Identity.Models;
using StudioKit.Sharding.Interfaces;
using StudioKit.Sharding.Utils;
using StudioKit.TransientFaultHandling.Http;
using StudioKit.TransientFaultHandling.Http.Interfaces;
using StudioKit.UserInfoService;
using StudioKit.UserInfoService.UserIdentityService;
using StudioKit.Utilities;
using StudioKit.Utilities.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;

namespace StudioKit.Scaffolding.Service;

public abstract class BaseStartup<TContext, TUser, TGroup, TConfiguration, TExternalGradable>
	where TContext : BaseDbContext<TUser, TGroup, TConfiguration>
	where TUser : IdentityUser, IUser, new()
	where TGroup : BaseGroup, new()
	where TConfiguration : BaseConfiguration, new()
	where TExternalGradable : class, IExternalGradable, new()
{
	protected BaseStartup(IConfiguration configuration)
	{
		Configuration = configuration;
		EncryptedConfigurationManager.Configuration = Configuration;

		// Lockdown browser cookie
		CookieToClaimsFilter.CookiesToConvertToClaims.Add((LockDownBrowserKey.ClientIsLockDownBrowser,
			ScaffoldingClaimTypes.ClientIsLockDownBrowser));
	}

	public IConfiguration Configuration { get; private set; }

	public ILifetimeScope AutofacContainer { get; private set; }

	/// <summary>
	/// This method gets called by the runtime. Use this method to add services to the container.
	/// </summary>
	/// <param name="services"></param>
	public virtual void ConfigureServices(IServiceCollection services)
	{
		services.AddOptions();

		services.AddLogging();

		services.AddApplicationInsightsTelemetry();

		services
			.AddIdentity<TUser, Role>()
			.AddEntityFrameworkStores<TContext>()
			.AddDefaultTokenProviders();

		services.AddScoped<IUserStore<TUser>, UserStore<TUser, TContext>>();

		// Set up DataProtection key sharing using Azure Blob Storage
		// https://docs.microsoft.com/en-us/aspnet/core/security/data-protection/implementation/key-storage-providers?view=aspnetcore-6.0&tabs=visual-studio#azure-storage
		const string keysContainerName = "keys";
		const string keysBlobName = "data-protection.xml";
		var container = new BlobContainerClient(AzureStorage.StorageConnectionString, keysContainerName);
		container.CreateIfNotExists();
		var blobClient = new BlobClient(AzureStorage.StorageConnectionString, keysContainerName, keysBlobName);
		var dataProtectionBuilder = services.AddDataProtection()
			.PersistKeysToAzureBlobStorage(blobClient);

		// Load the app's encryption cert to use to protect the DataProtection keys at rest
		var thumbprint = EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.DataProtectionCertificateThumbprint);
		using (var certStore = new X509Store(StoreName.My, StoreLocation.CurrentUser))
		{
			certStore.Open(OpenFlags.ReadOnly);

			var certCollection = certStore.Certificates.Find(X509FindType.FindByThumbprint, thumbprint, false);

			// Get the first cert with the thumbprint
			var cert = certCollection.FirstOrDefault();

			if (cert is null)
				throw new Exception($"Certificate with thumbprint {thumbprint} was not found");

			dataProtectionBuilder.ProtectKeysWithCertificate(cert);
		}

		services
			.AddAuthentication(options =>
			{
				options.DefaultScheme = OAuthSchemeConstants.Name;
				options.DefaultChallengeScheme = OAuthSchemeConstants.Name;
				options.DefaultAuthenticateScheme = OAuthSchemeConstants.Name;
			})
			.AddScheme<OAuthSchemeOptions, OAuthHandler>(OAuthSchemeConstants.Name, null);

		services.AddCors();

		services.AddControllersWithViews(ConfigureMvcOptions)
			// Newtonsoft.Json is added for compatibility reasons
			// The recommended approach is to use System.Text.Json for serialization
			// Visit the following link for more guidance about moving away from Newtonsoft.Json to System.Text.Json
			// https://docs.microsoft.com/dotnet/standard/serialization/system-text-json-migrate-from-newtonsoft-how-to
			.AddNewtonsoftJson(options =>
			{
				// Enable camelCase JSON Formatting and UTC Dates
				options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
				options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
				// Handle EF self referencing loop
				options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
			});

		services.AddResponseCompression();

		services.AddHttpClient(string.Empty)
			.ConfigurePrimaryHttpMessageHandler<DecompressingHttpClientHandler>();

		services.AddTransient<ProblemDetailsFactory, CustomProblemDetailsFactory>();

		var openAiEndpoint = EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.OpenAiEndpoint);
		var openAiKey = EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.OpenAiApiKey);
		if (!string.IsNullOrWhiteSpace(openAiEndpoint) && !string.IsNullOrWhiteSpace(openAiKey))
		{
			services.AddAzureClients(azureBuilder =>
			{
				azureBuilder.ConfigureDefaults(EncryptedConfigurationManager.Configuration.GetSection(ScaffoldingAppSetting.AzureDefaults));
				azureBuilder.AddOpenAIClient(new Uri(openAiEndpoint), new AzureKeyCredential(openAiKey));
			});
		}

		services.Configure<FormOptions>(x =>
		{
			// Handle requests up to 1 GB
			x.MultipartBodyLengthLimit = 1073741824;
		});
	}

	/// <summary>
	/// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
	/// </summary>
	/// <param name="app"></param>
	/// <param name="env"></param>
	public virtual void Configure(IApplicationBuilder app, IWebHostEnvironment env)
	{
		AutofacContainer = app.ApplicationServices.GetAutofacRoot();

		var tier = EncryptedConfigurationManager.GetSetting(BaseAppSetting.Tier);

		app.UseExceptionHandler(env.IsDevelopment() ? "/error-development" : "/error");

		app.UseStatusCodePagesWithReExecute("/status-code/{0}");

		if (tier != Tier.Local)
		{
			app.UseHttpsRedirection();
			app.UseHsts();
		}

		ConfigureResponseCaching(app, env);
		ConfigureShard(app, env);

		// Allow cross-origin access for JS on LOCAL and DEV
		if (tier != Tier.QA && tier != Tier.Prod)
			app.UseCors(corsPolicyBuilder =>
				corsPolicyBuilder
					.AllowAnyOrigin()
					.AllowAnyMethod()
					.AllowAnyHeader());

		app.UseRouting();

		// Enable Sentry performance monitoring
		app.UseSentryTracing();

		app.UseAuthentication();
		app.UseAuthorization();

		app.UseResponseCompression();

		app.UseEndpoints(endpoints =>
		{
			endpoints.MapControllers();
		});

		// create artifact storage container if needed
		var container = new BlobContainerClient(AzureStorage.StorageConnectionString, "artifact");
		container.CreateIfNotExists();
		container.SetAccessPolicy(PublicAccessType.Blob);
	}

	public virtual void ConfigureContainer(ContainerBuilder builder)
	{
		builder.RegisterType<DecompressingHttpClientHandler>()
			.AsSelf();

		builder.RegisterType<CaliperEventStoreController>()
			.AsSelf();

		ErrorHandler.Instance = new SentryErrorHandler();
		builder.RegisterInstance(ErrorHandler.Instance)
			.As<IErrorHandler>()
			.ExternallyOwned();

		builder.RegisterType<DateTimeProvider>()
			.As<IDateTimeProvider>()
			.SingleInstance();

		builder.RegisterType<HttpContextPrincipalProvider>()
			.As<IPrincipalProvider>()
			.SingleInstance();

		builder.RegisterType<PrincipalTelemetryInitializer>()
			.As<ITelemetryInitializer>()
			.SingleInstance();

		builder.RegisterType<HttpContextShardKeyProvider>()
			.As<IShardKeyProvider>()
			.SingleInstance();

		// Authorization Services

		builder.RegisterType<AppClientAuthorizationService<TContext>>()
			.As<IAppClientAuthorizationService>()
			.InstancePerLifetimeScope();

		builder.RegisterType<UserRoleAuthorizationService<TContext>>()
			.As<IUserRoleAuthorizationService>()
			.AsSelf()
			.InstancePerLifetimeScope();

		builder.RegisterType<GroupAuthorizationService<TContext>>()
			.As<IGroupAuthorizationService>()
			.Named<IEntityUserRoleAuthorizationService>("groupAuthorizationService")
			.InstancePerLifetimeScope();

		builder.RegisterType<ExternalGroupAuthorizationService<TContext>>()
			.As<IExternalGroupAuthorizationService>()
			.InstancePerLifetimeScope();

		builder.RegisterType<ExternalProviderAuthorizationService<TContext>>()
			.As<IExternalProviderAuthorizationService>()
			.InstancePerLifetimeScope();

		builder.RegisterType<LtiLaunchAuthorizationService<TContext, TGroup>>()
			.As<ILtiLaunchAuthorizationService>()
			.InstancePerLifetimeScope();

		builder.RegisterType<MigrationAuthorizationService<TContext>>()
			.As<IMigrationAuthorizationService>()
			.InstancePerLifetimeScope();

		// StudioKit.Security Services

		builder.RegisterType<CertificateService>()
			.As<ICertificateService>()
			.InstancePerLifetimeScope();

		// BusinessLogic Services

		builder.RegisterType<AppClientService<TContext>>()
			.As<IAppClientService>()
			.InstancePerLifetimeScope();

		builder.RegisterType<OAuthService<TContext, TUser, TGroup, TConfiguration>>()
			.As<IOAuthService>()
			.InstancePerLifetimeScope();

		builder.RegisterType<UserRoleService<TContext, TUser, TGroup, TConfiguration>>()
			.As<IUserRoleService<TUser>>()
			.InstancePerLifetimeScope();

		builder.RegisterType<UniTimeService<TContext, TUser, TGroup, TConfiguration>>()
			.As<IUniTimeService>()
			.InstancePerLifetimeScope();

		builder.RegisterType<UniTimeRosterProviderService<TUser, ExternalGroup, ExternalGroupUser>>()
			.AsSelf()
			.InstancePerLifetimeScope();

		builder.RegisterType<RoleStore<TContext>>()
			.AsSelf()
			.InstancePerLifetimeScope();

		builder.RegisterType<RoleManager>()
			.AsSelf()
			.InstancePerLifetimeScope();

		builder.RegisterType<UserStore<TUser, TContext>>()
			.AsSelf()
			.As<IUserStore<TUser>>()
			.InstancePerLifetimeScope();

		builder.RegisterType<BaseUserFactory<TUser>>()
			.As<IUserFactory<TUser>>()
			.InstancePerLifetimeScope();

		builder.RegisterType<OrganizationService<TContext, TConfiguration, License>>()
			.As<IOrganizationService>();

		builder.RegisterType<PurdueCasService>()
			.AsSelf();

		builder.RegisterType<ExternalProviderService<TContext, TUser, TGroup, TConfiguration>>()
			.As<IExternalProviderService>()
			.InstancePerLifetimeScope();

		builder
			.RegisterType<BaseGroupService<TContext, TUser, TGroup, TConfiguration, BaseGroupBusinessModel<TGroup, TUser>,
				BaseGroupEditBusinessModel<TGroup>>>()
			.As<IBaseGroupService<TGroup, BaseGroupBusinessModel<TGroup, TUser>, BaseGroupEditBusinessModel<TGroup>, TUser>>()
			.InstancePerLifetimeScope();

		builder.RegisterType<BaseGroupUserRoleService<TContext, TUser, TGroup, TConfiguration>>()
			.WithParameter(ResolvedParameter.ForNamed<IEntityUserRoleAuthorizationService>("groupAuthorizationService"))
			.AsSelf()
			.InstancePerLifetimeScope();

		builder.RegisterType<MigrationService>()
			.As<IMigrationService>()
			.InstancePerLifetimeScope();

		builder.RegisterType<LockDownBrowserConfiguration>()
			.As<ILockDownBrowserConfiguration>()
			.InstancePerLifetimeScope();

		builder.RegisterType<LockDownBrowserUtils>()
			.As<ILockDownBrowserUtils>()
			.InstancePerLifetimeScope();

		builder.RegisterType<LockDownBrowserService<TContext, TUser, TGroup, TConfiguration>>()
			.As<ILockDownBrowserService>()
			.InstancePerLifetimeScope();

		builder.RegisterType<OpenAIClientService>()
			.As<IOpenAIClientService>()
			.InstancePerLifetimeScope();

		// BusinessLogic.Validators

		builder.RegisterType<BaseGroupBusinessModelValidator<TGroup>>()
			.AsSelf()
			.InstancePerLifetimeScope();

		builder.RegisterType<ArtifactBusinessModelValidator>()
			.AsSelf()
			.InstancePerLifetimeScope();

		// Storage + Artifacts

		builder.RegisterType<AzureBlobStorage>()
			.As<IBlobStorage>()
			.WithParameter("containerName", "artifact")
			.InstancePerLifetimeScope();

		builder.RegisterType<ArtifactLeasingService>()
			.AsSelf()
			.WithParameters(new List<Parameter>
			{
				new NamedParameter("totpSecretKey", EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.ArtifactTotpSecretKey)),
				new NamedParameter("totpDurationSeconds",
					EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.ArtifactTotpDurationSeconds))
			})
			.InstancePerLifetimeScope();

		// Caliper
		builder.RegisterType<CaliperEventStoreService>()
			.AsSelf()
			.InstancePerLifetimeScope();

		builder.RegisterType<BaseConfigurationService<TContext, TUser, TGroup, TConfiguration>>()
			.As<ICaliperConfigurationProvider>()
			.As<ICertificateConfigurationProvider>()
			.As<IBaseConfigurationService<TConfiguration>>()
			.InstancePerLifetimeScope();

		// UniTime
		builder.RegisterType<UniTimeApiService>()
			.AsSelf()
			.InstancePerLifetimeScope();

		// UserInfo
		builder.RegisterType<UserInfoService.UserInfoService>()
			.AsSelf()
			.As<IUserInfoService>()
			.InstancePerLifetimeScope();
		builder.RegisterType<UserIdentityServiceClient>()
			.AsSelf();

		// QueueManagers
		builder.RegisterType<SyncGroupRosterQueueManager>()
			.As<IQueueManager<SyncGroupRosterMessage>>()
			.AsSelf();
		builder.RegisterType<SyncAllRostersQueueManager>()
			.As<IQueueManager<SyncAllRostersMessage>>()
			.AsSelf();

		// Lti Services

		builder.RegisterType<JsonWebTokenService<TGroup, GroupUserRole, ExternalTerm, ExternalGroup, ExternalGroupUser>>()
			.As<IJsonWebTokenService>()
			.InstancePerLifetimeScope();

		builder.RegisterType<LtiService<TGroup, GroupUserRole, ExternalTerm, ExternalGroup, ExternalGroupUser, TExternalGradable>>()
			.As<ILtiService>()
			.InstancePerLifetimeScope();

		builder.RegisterType<LtiKeySetProviderService>()
			.As<ILtiKeySetProviderService>()
			.InstancePerLifetimeScope();

		builder.RegisterType<LtiLaunchService<TContext, TGroup>>()
			.As<ILtiLaunchService>()
			.InstancePerLifetimeScope();

		builder.Register<Func<IHttpClient>>(c =>
		{
			var httpClientFactory = c.Resolve<IHttpClientFactory>();
			var release = c.Resolve<IRelease>();
			var logger = c.Resolve<ILogger<RetryingHttpClient>>();
			return () => new RetryingHttpClient(httpClientFactory, release, logger);
		});

		builder.RegisterType<LtiApiService>()
			.As<ILtiApiService>()
			.InstancePerLifetimeScope();

		builder.RegisterType<RedisConnectionFactory>()
			.AsSelf()
			.WithParameter((pi, _) => pi.ParameterType == typeof(IDictionary<string, string>) && pi.Name == "connectionStrings",
				(_, ctx) =>
				{
					// build and provide a mapping of shardKeys to Redis connection strings for all shards in the application
					var connectionStrings = new Dictionary<string, string>();
					if (ShardManager.Instance.DefaultShardMap != null)
					{
						ShardUtils.PerformOnAllShards(ShardManager.Instance.DefaultShardMap, shardKey =>
						{
							connectionStrings[shardKey] = EncryptedConfigurationManager.TryDecryptSettingValue(
								ctx.Resolve<Func<string, TContext>>()(shardKey)
									.Configurations
									.OrderByDescending(c => c.Id)
									.First().RedisConnectionString);
						});
					}

					return connectionStrings;
				})
			.SingleInstance();

		builder.RegisterType<RedisClient>()
			.As<IAsyncCacheClient>();
	}

	protected virtual void ConfigureMvcOptions(MvcOptions mvcOptions)
	{
		// CSV Formatters
		mvcOptions.OutputFormatters.Insert(0, new CsvMediaTypeFormatter(new CsvConfiguration(CultureInfo.InvariantCulture)));

		// Authorize all requests by default
		mvcOptions.Filters.Add(new AuthorizeFilter());

		// Add Root Domain Default Filter
		mvcOptions.Filters.Add<RootDomainFilter>();

		// Add downtime kill switch filter
		mvcOptions.Filters.Add<DowntimeFilter>();

		// Add filter to translate cookies to claims
		mvcOptions.Filters.Add<CookieToClaimsFilter>();

		// Add Custom Exception Handling
		mvcOptions.Filters.Add<ProblemDetailsExceptionFilterAttribute>();

		// Add Exception Handling
		mvcOptions.Filters.Add<ShardWebApiExceptionFilterAttribute>();
	}

	protected virtual void ConfigureResponseCaching(IApplicationBuilder app, IWebHostEnvironment env)
	{
		app.Use((context, next) =>
		{
			context.Response.Headers["Cache-Control"] = "no-store, must-revalidate";
			return next.Invoke();
		});
	}

	public abstract void ConfigureShard(IApplicationBuilder app, IWebHostEnvironment env);
}