# SQL Scripts

## Data Refresh

The following scripts are used for the QA Data Refresh Pipelines in the Azure Data Factory.

### Stored Procedures

* `spBackupConstraintsAndTruncateTables.sql`
	* Script that creates the `spBackupConstraintsAndTruncateTables` stored procedure. Run on each app’s shard database. Can be run multiple
	  times in order to make updates.
	* Removes Indexes and FKs from all tables that will be truncated, backs them to `[dbo].[__RestoreConstraintScripts]`
	  for `spRestoreConstrants` to restore later, then truncates the tables
	* **NOTE:** Currently, the list of tables excluded from truncation is the same across apps, but this _might_ need to be customized per
	  app
* `spRestoreConstrants.sql`
	* Script that creates the `spRestoreConstrants` stored procedure. Run on each app’s shard database. Can be run multiple
	  times in order to make updates.
	* Restores the Indexes and FKs that were removed by `spBackupConstraintsAndTruncateTables`

### Scripts copied into Azure Data Factory

* `CreateDataRefreshHistory.sql`
	* Script that creates the `[dbo].[__RestoreConstraintScripts]` table. Run once on each app’s shard database.
* `SelectLastDataRefreshDate.sql`
	* Gets the last date that the data refresh succeeded
	* Used in a "Lookup" activity to only copy Artifacts in the `[App]CopyArtifactsToQA`
	  pipeline that have changed since the last refresh
* `SelectTablesToCopy.sql`
	* **NOTE:** Make a copy for each app and customize this script
	* Used in a "Lookup" activity to select which tables should be copied from PROD to QA
* `CheckForIdentity.sql`
	* Used in a "Copy data" activity to disable Identity Insert on each table that will be copied, if the table has identity insert enabled
* `RestoreAzureDevopsClientRole.sql`
	* Used in a "Script" activity to give the "azure_devops" `ServiceClient` the "DatabaseAdmin" role, after it is copied from PROD to QA
* `UpdateDataRefreshHistory.sql`
	* Counter-part to `SelectLastDataRefreshDate.sql`
	* Used in a "Script" activity to store the last data refresh date at the end of a successful refresh
* `UpdateArtifactUrls.sql`
	* **NOTE:** Make a copy for each app and customize this script
	* Used in a "Script" activity to update all Artifacts Urls in the QA database to point to the QA storage account,
	  after they are copied from PROD
