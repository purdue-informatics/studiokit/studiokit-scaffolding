-- Query is copied into an Azure Data Factory Script Activity
INSERT INTO [dbo].[__DataRefreshHistory] ([DateStarted], [DateEnded])
VALUES ('@{variables('runStartDate')}', '@{utcnow('s')}')