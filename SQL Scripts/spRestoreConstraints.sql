SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('[dbo].[spRestoreConstraints]', 'P') IS NOT NULL
	DROP PROCEDURE [dbo].[spRestoreConstraints]; 
GO
CREATE PROCEDURE [dbo].[spRestoreConstraints]
	-- 1 = Will not commit the transaction
	@Debug BIT = 1,
	-- 1 = Will print executed statements (except SELECTs)
	@Verbose BIT = 1
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON

	BEGIN TRANSACTION RestoreTransaction;

	DECLARE @script nvarchar(MAX);
	DECLARE @i INT;

	PRINT 'Running spRestoreConstraints with'
		+ ', Debug = ' + CASE WHEN @Debug = 1 THEN 'true' ELSE 'false' END
		+ ', Verbose = ' + CASE WHEN @Verbose = 1 THEN 'true' ELSE 'false' END
		+ '...';

	IF @Debug = 1
		PRINT 'DEBUG MODE: TRANSACTION WILL NOT BE COMMITTED'

	-- Always check if the Restore Constraint Scripts Table contains any previous scripts, and restore them before proceeding
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '__RestoreConstraintScripts')
		IF EXISTS(SELECT 1 FROM [dbo].[__RestoreConstraintScripts])
			BEGIN
				PRINT 'Running Restore Constraint Scripts from [dbo].[__RestoreConstraintScripts]...';

				SET @i = 1;
				WHILE (@i <= (SELECT MAX(Id) FROM [dbo].[__RestoreConstraintScripts]))
				BEGIN
					SET @script = (SELECT Script FROM [dbo].[__RestoreConstraintScripts] WHERE Id = @i);

					IF @Verbose = 1
						PRINT @script;
					IF @Debug = 0
						EXEC (@script);

					SET @i = @i + 1;
				END

				PRINT 'Truncating [dbo].[__RestoreConstraintScripts]...';

				IF @Verbose = 1
					PRINT 'TRUNCATE TABLE [dbo].[__RestoreConstraintScripts]';

				TRUNCATE TABLE [dbo].[__RestoreConstraintScripts];
			END
		ELSE
			PRINT 'No rows exist in [dbo].[__RestoreConstraintScripts]';
	ELSE
		PRINT 'Table [dbo].[__RestoreConstraintScripts] does not exist';

	IF @Debug = 0
		COMMIT TRANSACTION RestoreTransaction;
	ELSE
		ROLLBACK TRANSACTION RestoreTransaction;

	PRINT 'Process Completed';
END