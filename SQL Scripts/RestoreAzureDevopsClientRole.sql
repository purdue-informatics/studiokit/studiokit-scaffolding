-- Query is copied into an Azure Data Factory Script Activity
INSERT INTO [dbo].[ServiceClientRoles] (ClientId, RoleId)
SELECT N'azure_devops', [r].[Id]
FROM [dbo].[AspNetRoles] AS [r]
WHERE [r].[Name] = N'DatabaseAdmin'