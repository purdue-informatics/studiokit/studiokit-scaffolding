SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID('[dbo].[spBackupConstraintsAndTruncateTables]', 'P') IS NOT NULL
	DROP PROCEDURE [dbo].[spBackupConstraintsAndTruncateTables];
GO
CREATE PROCEDURE [dbo].[spBackupConstraintsAndTruncateTables]
	-- 0 = Will not restore dropped constraints after truncation, but they will be persisted to [dbo].[__RestoreConstraintScripts]
	-- 1 = Will restore dropped constraints after truncation and they will not persisted to [dbo].[__RestoreConstraintScripts]
	@ShouldRestoreAfterTruncate BIT = 1,
	-- 1 = Will not commit the transaction
	@Debug BIT = 1,
	-- 1 = Will print executed statements (except SELECTs)
	@Verbose BIT = 1
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON

	BEGIN TRANSACTION TruncateTransaction;

	DECLARE @script NVARCHAR(MAX);
	DECLARE @i INT;
	DECLARE @rowCount INT;

	PRINT 'Running spBackupConstraintsAndTruncateTables with'
		+ ' ShouldRestoreAfterTruncate = ' + CASE WHEN @ShouldRestoreAfterTruncate = 1 THEN 'true' ELSE 'false' END
		+ ', Debug = ' + CASE WHEN @Debug = 1 THEN 'true' ELSE 'false' END
		+ ', Verbose = ' + CASE WHEN @Verbose = 1 THEN 'true' ELSE 'false' END
		+ '...';

	IF @Debug = 1
		PRINT 'DEBUG MODE: TRANSACTION WILL NOT BE COMMITTED';

	-- Create Constraint Restore Scripts Table, if needed
	IF @ShouldRestoreAfterTruncate = 0
		BEGIN
			IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '__RestoreConstraintScripts')
				BEGIN
					PRINT 'Creating Constraint Restore Scripts Table [dbo].[__RestoreConstraintScripts]...';

					IF @Verbose = 1
						PRINT 'CREATE TABLE [dbo].[__RestoreConstraintScripts]
(
	Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	Script NVARCHAR(MAX) NOT NULL
)';

					CREATE TABLE [dbo].[__RestoreConstraintScripts]
					(
						Id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
						Script NVARCHAR(MAX) NOT NULL
					);
				END
		END

	-- Check if the Restore Constraint Scripts Table contains any previous scripts
	-- IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '__RestoreConstraintScripts')
	-- 	BEGIN
	-- 		PRINT 'Checking [dbo].[__RestoreConstraintScripts] for previous scripts...';
	-- 		IF EXISTS(SELECT 1 FROM [dbo].[__RestoreConstraintScripts])
	-- 			BEGIN
	-- 				;THROW 51000, '[dbo].[__RestoreConstraintScripts] is not empty. Run [dbo].[spRestoreConstraints]', 1;
	-- 			END
	-- 	END

	PRINT 'Loading tables to truncate...';

	SELECT
		TABLE_SCHEMA AS SchemaName,
		TABLE_NAME AS TableName
	INTO #TablesToTruncate
	FROM information_schema.TABLES
	WHERE TABLE_TYPE = 'BASE TABLE'
	AND TABLE_SCHEMA = 'dbo'
	-- excluded tables
	AND TABLE_NAME NOT IN (
		'__MigrationHistory',
		'__EFMigrationsHistory',
		'__DataRefreshHistory',
		'__RestoreConstraintScripts',
		'Configurations',
		'Clients',
		'IdentityProviders',
		'RefreshTokens',
		'ExternalProviders'
	);

	SET @rowCount = (SELECT COUNT(*) FROM #TablesToTruncate);
	PRINT 'Loaded ' + CONVERT(NVARCHAR, @rowCount) + ' tables';

	PRINT 'Loading FKs from tables to truncate...';

	-- Get FKs for tables to truncate
	-- will have multiple rows for each column in the constraint, e.g. compound keys
	SELECT
		SCHEMA_NAME(fk.schema_id) AS SchemaName,
		fk.Name AS ConstraintName,
		OBJECT_NAME(fk.parent_object_id) AS TableName,
		SCHEMA_NAME(t.SCHEMA_ID) AS ReferencedSchemaName,
		OBJECT_NAME(fk.referenced_object_id) AS ReferencedTableName,
		fc.constraint_column_id,
		COL_NAME(fk.parent_object_id, fc.parent_column_id) AS ColumnName,
		COL_NAME(fk.referenced_object_id, fc.referenced_column_id) AS ReferencedColumnName,
		REPLACE(fk.delete_referential_action_desc, '_', ' ') AS DeleteAction,
		REPLACE(fk.update_referential_action_desc, '_', ' ') AS UpdateAction
	INTO #FKColumns
	FROM sys.foreign_keys AS fk
		JOIN sys.foreign_key_columns AS fc
			ON fk.object_id = fc.constraint_object_id
		JOIN #TablesToTruncate AS tbl
			ON OBJECT_NAME(fc.referenced_object_id) = tbl.TableName
		JOIN sys.tables AS t
			ON OBJECT_NAME(t.object_id) = tbl.TableName
			AND SCHEMA_NAME(t.schema_id) = tbl.SchemaName
			AND t.OBJECT_ID = fc.referenced_object_id;

	-- Group FKs together to account for compound keys in the FKs, do final processing
	SELECT
		QUOTENAME(fk.ConstraintName) AS ConstraintName,
		QUOTENAME(fk.SchemaName) + '.' + QUOTENAME(fk.TableName) AS TableName,
		QUOTENAME(fk.ReferencedSchemaName) + '.' + QUOTENAME(fk.ReferencedTableName) AS ReferencedTableName,
		(CASE WHEN fk.DeleteAction = 'NO ACTION' THEN '' ELSE ' ON DELETE ' + fk.DeleteAction END) AS DeleteAction,
		(CASE WHEN fk.UpdateAction = 'NO ACTION' THEN '' ELSE ' ON UPDATE ' + fk.UpdateAction END) AS UpdateAction,
		STUFF((
			SELECT ', ' + QUOTENAME(fk2.ColumnName)
			FROM #FKColumns fk2
			WHERE fk2.ConstraintName = fk.ConstraintName and fk2.SchemaName = fk.SchemaName
			ORDER BY fk2.constraint_column_id
			FOR XML PATH('')
		),1,2,'') AS ColumnNames,
		STUFF((
			SELECT ', ' + QUOTENAME(fk2.ReferencedColumnName)
			FROM #FKColumns fk2
			WHERE fk2.ConstraintName = fk.ConstraintName and fk2.SchemaName = fk.SchemaName
			ORDER BY fk2.constraint_column_id
			FOR XML PATH('')
		),1,2,'') AS ReferencedColumnNames
	INTO #FKs
	FROM #FKColumns fk
	GROUP BY fk.SchemaName, fk.ConstraintName, fk.TableName, fk.ReferencedSchemaName, fk.ReferencedTableName, fk.DeleteAction, fk.UpdateAction;

	SET @rowCount = (SELECT COUNT(*) FROM #FKs);
	PRINT 'Loaded ' + CONVERT(NVARCHAR, @rowCount) + ' FKs';

	PRINT 'Loading Indexes from tables to truncate...'

	-- Get Indexes and PKs for tables to truncate
	-- will have multiple rows for each column in the index
	SELECT
		SCHEMA_NAME(t.schema_id) AS SchemaName,
		t.name AS TableName,
		ind.name AS IndexName,
		col.name AS ColumnName,
		ind.is_primary_key AS IsPrimaryKey,
		ind.is_unique AS IsIndexUnique,
		ind.type_desc AS ClusteredKeyword,
		ic.key_ordinal AS ColumnKeyOrdinal,
		ic.is_included_column AS IsColumnIncluded,
		ic.is_descending_key AS IsColumnDescending,
		ind.filter_definition AS IndexFilter
	INTO #IndexColumns
	FROM
		sys.indexes AS ind
		JOIN sys.index_columns AS ic
			ON ind.object_id = ic.object_id
			AND ind.index_id = ic.index_id
		JOIN sys.columns AS col
			ON ic.object_id = col.object_id
			AND ic.column_id = col.column_id
		JOIN #TablesToTruncate AS tbl
			ON OBJECT_NAME(ind.object_id) = tbl.TableName
		JOIN sys.tables AS t
			ON OBJECT_NAME(t.object_id) = tbl.TableName
			AND SCHEMA_NAME(t.schema_id) = tbl.SchemaName
			AND t.OBJECT_ID = ind.object_id
	WHERE
		t.is_ms_shipped = 0
	ORDER BY
		t.schema_id, t.name, ind.name, ic.is_included_column, ic.key_ordinal;

	-- Group IndexColumns together for each Index, do final processing
	SELECT
		QUOTENAME(ic.IndexName) AS IndexName,
		QUOTENAME(ic.SchemaName) + '.' + QUOTENAME(ic.TableName) AS TableName,
		ic.IsPrimaryKey,
		(CASE WHEN ic.IsIndexUnique = 1 THEN 'UNIQUE' ELSE NULL END) AS UniqueKeyword,
		ic.ClusteredKeyword,
		STUFF((
			SELECT ', ' + QUOTENAME(ic2.ColumnName) + (CASE WHEN ic2.IsColumnDescending = 1 THEN ' DESC' ELSE ' ASC' END)
			FROM #IndexColumns ic2
			WHERE ic2.IndexName = ic.IndexName AND ic2.SchemaName = ic.SchemaName AND ic2.IsColumnIncluded = 0
			ORDER BY ic2.ColumnKeyOrdinal
			FOR XML PATH('')
		),1,2,'') AS ColumnNames,
		STUFF((
			SELECT ', ' + QUOTENAME(ic2.ColumnName)
			FROM #IndexColumns ic2
			WHERE ic2.IndexName = ic.IndexName AND ic2.SchemaName = ic.SchemaName AND ic2.IsColumnIncluded = 1
			ORDER BY ic2.ColumnKeyOrdinal
			FOR XML PATH('')
		),1,2,'') AS IncludedColumnNames,
		ic.IndexFilter
	INTO #Indexes
	FROM #IndexColumns ic
	GROUP BY ic.SchemaName, ic.IndexName, ic.TableName, ic.IsPrimaryKey, ic.IsIndexUnique, ic.ClusteredKeyword, ic.IsColumnDescending, ic.IndexFilter;

	SET @rowCount = (SELECT COUNT(*) FROM #Indexes);
	PRINT 'Loaded ' + CONVERT(NVARCHAR, @rowCount) + ' Indexes';

	PRINT 'Adding Drop Index scripts in #Scripts...';

	SELECT
		IDENTITY(INT,1,1) as Id,
		'DROP INDEX ' + ind.IndexName + ' ON ' + ind.TableName AS Script
	INTO #Scripts
	FROM #Indexes AS ind
	WHERE ind.IsPrimaryKey = 0;

	PRINT 'Adding Drop FK scripts in #Scripts...';

	INSERT INTO #Scripts (Script)
	SELECT
		'ALTER TABLE ' + fk.TableName + ' DROP CONSTRAINT ' + fk.ConstraintName AS Script
	FROM #FKs AS fk;

	PRINT 'Adding Truncate Table scripts in #Scripts...';

	INSERT INTO #Scripts (Script)
	SELECT DISTINCT
		'TRUNCATE TABLE ' + QUOTENAME(tbl.SchemaName) + '.' + QUOTENAME(tbl.TableName) AS Script
	FROM #TablesToTruncate AS tbl;

	PRINT 'Creating Restore FK scripts...';

	-- Create Restore FK Scripts in a new temporary table
	SELECT
		IDENTITY(INT,1,1) as Id,
		'ALTER TABLE ' + fk.TableName +
		' WITH CHECK ADD CONSTRAINT ' + fk.ConstraintName +
		' FOREIGN KEY ('+ fk.ColumnNames +')' +
		' REFERENCES ' + fk.ReferencedTableName +' ('+ fk.ReferencedColumnNames +')' +
		fk.DeleteAction COLLATE Latin1_General_CI_AS_KS_WS +
		fk.UpdateAction COLLATE Latin1_General_CI_AS_KS_WS AS Script
	INTO #RestoreFKScripts
	FROM #FKs AS fk;

	-- Copy Restore FK Scripts to the correct table, depending on if they will be run now or later
	IF @ShouldRestoreAfterTruncate = 0
		BEGIN
			PRINT 'Persisting Restore FK scripts to [dbo].[__RestoreConstraintScripts]...';

			INSERT INTO [dbo].[__RestoreConstraintScripts]
			SELECT Script
			FROM #RestoreFKScripts;

			SET @rowCount = (SELECT COUNT(*) FROM [dbo].[__RestoreConstraintScripts] WHERE [Script] LIKE N'%FOREIGN KEY%');
			PRINT 'Persisted ' + CONVERT(NVARCHAR, @rowCount) + ' Restore FK scripts';
		END
	ELSE
		BEGIN
			PRINT 'Copying Restore FK scripts to #Scripts...';

			INSERT INTO #Scripts (Script)
			SELECT Script
			FROM #RestoreFKScripts;
		END

	PRINT 'Creating Restore Index scripts...';

	-- Create Restore Index Scripts in a new temporary table
	SELECT
		IDENTITY(INT,1,1) as Id,
		'CREATE ' +
		(CASE WHEN ind.UniqueKeyword IS NOT NULL THEN ind.UniqueKeyword + ' ' ELSE '' END) +
		(CASE WHEN ind.ClusteredKeyword IS NOT NULL THEN ind.ClusteredKeyword + ' ' ELSE '' END) COLLATE Latin1_General_CI_AS_KS_WS +
		'INDEX ' + ind.IndexName +
		' ON ' + ind.TableName + '(' + ind.ColumnNames + ')' +
		(CASE WHEN ind.IncludedColumnNames IS NOT NULL THEN ' INCLUDE (' + ind.IncludedColumnNames + ')' ELSE '' END) +
		(CASE WHEN ind.IndexFilter IS NOT NULL THEN ' WHERE ' + ind.IndexFilter ELSE '' END) AS Script
	INTO #RestoreIndexScripts
	FROM #Indexes AS ind
	WHERE ind.IsPrimaryKey = 0;

	-- Copy Restore Index Scripts to the correct table, depending on if they will be run now or later
	IF @ShouldRestoreAfterTruncate = 0
		BEGIN
			PRINT 'Persisting Restore Index scripts to [dbo].[__RestoreConstraintScripts]...';

			INSERT INTO [dbo].[__RestoreConstraintScripts]
			SELECT Script
			FROM #RestoreIndexScripts;

			SET @rowCount = (SELECT COUNT(*) FROM [dbo].[__RestoreConstraintScripts] WHERE [Script] LIKE N'CREATE%');
			PRINT 'Persisted ' + CONVERT(NVARCHAR, @rowCount) + ' Restore Index scripts';
		END
	ELSE
		BEGIN
			PRINT 'Copying Restore Index scripts to #Scripts...';

			INSERT INTO #Scripts (Script)
			SELECT Script
			FROM #RestoreIndexScripts;
		END

	IF @ShouldRestoreAfterTruncate = 0
		PRINT 'Dropping Indexes and FKs, then Truncating Tables...';
	ELSE
		PRINT 'Dropping Indexes and FKs, then Truncating Tables, then Restoring FKs, and Indexes...';

	SET @i = 1;
	WHILE (@i <= (SELECT MAX(Id) FROM #Scripts))
	BEGIN
		SET @script = (SELECT Script FROM #Scripts WHERE Id = @i);

		IF @Verbose = 1
			PRINT @script;
		EXEC (@script);

		SET @i = @i + 1;
	END

	-- Drop Temp Tables
	PRINT 'Dropping temporary tables...';
	DROP TABLE #Scripts;
	DROP TABLE #RestoreIndexScripts;
	DROP TABLE #RestoreFKScripts;
	DROP TABLE #Indexes;
	DROP TABLE #IndexColumns;
	DROP TABLE #FKs;
	DROP TABLE #FKColumns;
	DROP TABLE #TablesToTruncate;

	IF @Debug = 0
		COMMIT TRANSACTION TruncateTransaction;
	ELSE
		ROLLBACK TRANSACTION TruncateTransaction;

	PRINT 'Process Completed';
END