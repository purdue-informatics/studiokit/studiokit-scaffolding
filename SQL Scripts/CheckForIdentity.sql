IF EXISTS (SELECT * FROM (SELECT OBJECTPROPERTY(OBJECT_ID('AspNetRoles'), 'TableHasIdentity')) AS X(HasIdentity) WHERE HasIdentity = 1)
	PRINT 'Yes'
ELSE
	PRINT 'No'

-- Query is copied into the "Pre-copy script" property in Azure Data Factory
-- IF EXISTS (SELECT * FROM (SELECT OBJECTPROPERTY(OBJECT_ID('@{item().TABLE_NAME}'), 'TableHasIdentity')) AS X(HasIdentity) WHERE HasIdentity = 1)
-- 	SET IDENTITY_INSERT [@{item().TABLE_SCHEMA}].[@{item().TABLE_NAME}] ON