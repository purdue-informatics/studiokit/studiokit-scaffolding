-- Query is copied into an Azure Data Factory Script Activity
-- NOTE: Make a copy for each app and customize this script
UPDATE [dbo].[Artifacts]
SET [Url] = REPLACE([Url], N'https://variatestorage', N'https://variateqastorage')
WHERE [Url] LIKE 'https://variatestorage%'