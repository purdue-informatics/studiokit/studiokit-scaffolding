-- Query is copied into an Azure Data Factory Lookup Activity
-- NOTE: Make a copy for each app and customize this script
SELECT TABLE_SCHEMA, TABLE_NAME
FROM information_schema.TABLES
WHERE TABLE_TYPE = 'BASE TABLE'
  AND TABLE_SCHEMA = 'dbo'
  AND TABLE_NAME NOT IN (
	-- tier specific data
	'__MigrationHistory',
	'__EFMigrationsHistory',
	'Configurations',
	'Clients',
	'ServiceClientRoles',
	'IdentityProviders',
	'RefreshTokens',
	-- don't copy LTI related entities
	'ExternalProviders',
	'ExternalGroups',
	'ExternalGroupUsers',
	'LtiLaunches',
	-- not used, don't have DateStored column
	'AspNetUserTokens',
	'AspNetRoleClaims'
	-- NOTE: ADD ADDITIONAL APP SPECIFIC TABLES, e.g. ExternalGroupAssessments vs. ExternalGroupAssignments
	)
ORDER BY TABLE_NAME