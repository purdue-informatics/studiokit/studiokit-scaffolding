using StudioKit.Scaffolding.Models;
using System;
using System.Collections.Generic;

namespace StudioKit.Scaffolding.BusinessLogic.Models;

public class BaseGroupEditBusinessModel<TGroup>
	where TGroup : BaseGroup, new()
{
	public TGroup Map()
	{
		return new()
		{
			Name = Name,
			ExternalTermId = ExternalTermId,
			StartDate = StartDate,
			EndDate = EndDate
		};
	}

	public string Name { get; set; }

	public int? ExternalTermId { get; set; }

	public DateTime? StartDate { get; set; }

	public DateTime? EndDate { get; set; }

	public bool? IsDeleted { get; set; }

	public IEnumerable<IExternalGroupEditBusinessModel> ExternalGroups { get; set; } = new List<IExternalGroupEditBusinessModel>();
}