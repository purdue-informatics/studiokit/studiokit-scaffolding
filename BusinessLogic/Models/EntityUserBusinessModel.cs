﻿using StudioKit.Data.Entity.Identity.Interfaces;

namespace StudioKit.Scaffolding.BusinessLogic.Models;

public class EntityUserBusinessModel<TUser>
	where TUser : IUser
{
	public EntityUserBusinessModel()
	{
	}

	public EntityUserBusinessModel(TUser user, int entityId)
	{
		User = user;
		EntityId = entityId;
	}

	public TUser User { get; set; }

	public int EntityId { get; set; }
}