using StudioKit.Data.Entity.Identity.Interfaces;

namespace StudioKit.Scaffolding.BusinessLogic.Models;

public class UserRoleBusinessModel<TUser>
	where TUser : IUser
{
	public UserRoleBusinessModel(TUser user, string role)
	{
		User = user;
		Role = role;
	}

	public TUser User { get; set; }

	public string Role { get; set; }
}