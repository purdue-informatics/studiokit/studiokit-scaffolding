﻿using StudioKit.Data.Entity.Identity.Interfaces;
using System.Collections.Generic;

namespace StudioKit.Scaffolding.BusinessLogic.Models;

public class CreatedUserRolesBusinessModel<TUser> where TUser : IUser
{
	/// <summary>
	/// UserRoles that were added to the Role, sent in the create request
	/// </summary>
	public IEnumerable<UserRoleBusinessModel<TUser>> AddedUserRoles { get; set; }

	/// <summary>
	/// UserRoles that were already in the Role, sent in the create request
	/// </summary>
	public IEnumerable<UserRoleBusinessModel<TUser>> ExistingUserRoles { get; set; }

	public IEnumerable<string> InvalidIdentifiers { get; set; }

	public string AllowedDomains { get; set; }

	public IEnumerable<string> InvalidDomainIdentifiers { get; set; }
}