﻿using StudioKit.ExternalProvider.Models.Interfaces;

namespace StudioKit.Scaffolding.BusinessLogic.Models;

public class ExternalGroupBusinessModel
{
	public ExternalGroupBusinessModel()
	{
	}

	public ExternalGroupBusinessModel(IExternalGroup externalGroup)
	{
		Id = externalGroup.Id;
		GroupId = externalGroup.GroupId;
		ExternalId = externalGroup.ExternalId;
		ExternalProviderId = externalGroup.ExternalProviderId;
		Name = externalGroup.Name;
		Description = externalGroup.Description;
		RosterUrl = externalGroup.RosterUrl;
		GradesUrl = externalGroup.GradesUrl;
		UserId = externalGroup.UserId;
		IsAutoGradePushEnabled = externalGroup.IsAutoGradePushEnabled;
	}

	public int Id { get; set; }

	public int GroupId { get; set; }

	public string ExternalId { get; set; }

	public int ExternalProviderId { get; set; }

	public string Name { get; set; }

	public string Description { get; set; }

	public string RosterUrl { get; set; }

	public string GradesUrl { get; set; }

	public string UserId { get; set; }

	public bool? IsAutoGradePushEnabled { get; set; }
}