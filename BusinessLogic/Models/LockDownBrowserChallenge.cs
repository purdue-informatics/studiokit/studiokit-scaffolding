namespace StudioKit.Scaffolding.BusinessLogic.Models;

public class LockDownBrowserChallenge
{
	public string Id { get; init; }

	public string UserId { get; init; }

	public int? EntityId { get; init; }

	public string RedirectPath { get; init; }

	public string Nonce { get; init; }

	public override string ToString()
	{
		return $"{Id}.{UserId}.{EntityId}.{RedirectPath}.{Nonce}";
	}
}