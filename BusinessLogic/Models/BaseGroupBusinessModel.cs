﻿using Microsoft.AspNetCore.Identity;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Scaffolding.Models;
using System.Collections.Generic;

namespace StudioKit.Scaffolding.BusinessLogic.Models;

public class BaseGroupBusinessModel<TGroup, TUser>
	where TUser : IdentityUser, IUser
	where TGroup : BaseGroup
{
	public TGroup Group { get; set; }

	public bool IsRosterSyncEnabled { get; set; }

	public IEnumerable<string> Roles { get; set; } = new List<string>();

	public IEnumerable<string> Activities { get; set; } = new List<string>();

	public IEnumerable<EntityUserBusinessModel<TUser>> Owners { get; set; } = new List<EntityUserBusinessModel<TUser>>();

	public IEnumerable<EntityUserBusinessModel<TUser>> Graders { get; set; } = new List<EntityUserBusinessModel<TUser>>();

	public IEnumerable<ExternalGroupBusinessModel> ExternalGroups { get; set; } = new List<ExternalGroupBusinessModel>();
}