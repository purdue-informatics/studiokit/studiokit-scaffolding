﻿using StudioKit.Data.Entity.Identity.Interfaces;

namespace StudioKit.Scaffolding.BusinessLogic.Models;

public class BaseUserEditBusinessModel<TUser>
	where TUser : IUser, new()
{
	public TUser Map()
	{
		return new()
		{
			FirstName = FirstName,
			LastName = LastName
		};
	}

	public string FirstName { get; set; }

	public string LastName { get; set; }
}