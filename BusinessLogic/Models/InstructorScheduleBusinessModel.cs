﻿using System.Collections.Generic;

namespace StudioKit.Scaffolding.BusinessLogic.Models;

public class InstructorScheduleBusinessModel
{
	public string FirstName { get; set; }

	public string LastName { get; set; }

	public string UserId { get; set; }

	public IEnumerable<UniTimeGroupBusinessModel> ExternalGroups { get; set; }
}