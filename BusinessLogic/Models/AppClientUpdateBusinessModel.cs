namespace StudioKit.Scaffolding.BusinessLogic.Models;

public class AppClientUpdateBusinessModel
{
	public string CurrentVersion { get; set; }

	public string MinVersion { get; set; }
}