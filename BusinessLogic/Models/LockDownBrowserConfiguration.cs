using StudioKit.Encryption;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.Common;
using System.Diagnostics.CodeAnalysis;

namespace StudioKit.Scaffolding.BusinessLogic.Models;

public class LockDownBrowserConfiguration : ILockDownBrowserConfiguration
{
	public LockDownBrowserConfiguration()
	{
		Secret1 = EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.LockDownBrowserSecret1);
		Secret2 = EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.LockDownBrowserSecret2);
		SecretIV = EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.LockDownBrowserSecretIV);
		SecretIndex = EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.LockDownBrowserSecretIndex);
		SecretVersion = EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.LockDownBrowserSecretVersion);
		DomainAllowList = EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.LockDownBrowserDomainAllowList);
	}

	public string Secret1 { get; }

	public string Secret2 { get; }

	[SuppressMessage("ReSharper", "InconsistentNaming")]
	public string SecretIV { get; }

	public string SecretIndex { get; }

	public string SecretVersion { get; }

	public string DomainAllowList { get; }
}