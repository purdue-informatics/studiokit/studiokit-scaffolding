﻿using Microsoft.AspNetCore.Http;

namespace StudioKit.Scaffolding.BusinessLogic.Models;

public interface IArtifactBusinessModel
{
}

public class FileArtifactBusinessModel : IArtifactBusinessModel
{
	public string FileName { get; set; }
	public string ContentType { get; set; }
	public IFormFile File { get; set; }
}

public class TextArtifactBusinessModel : IArtifactBusinessModel
{
	public string Text { get; set; }
	public int WordCount { get; set; }
}

public class UrlArtifactBusinessModel : IArtifactBusinessModel
{
	public string Url { get; set; }
}