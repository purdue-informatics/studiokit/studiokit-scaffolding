﻿using StudioKit.Data.Entity.Identity.Interfaces;
using System.Collections.Generic;

namespace StudioKit.Scaffolding.BusinessLogic.Models;

public class BaseUserBusinessModel<TUser>
	where TUser : IUser
{
	public TUser User { get; set; }

	public IEnumerable<string> Roles { get; set; } = new List<string>();

	public IEnumerable<string> Activities { get; set; } = new List<string>();
}