namespace StudioKit.Scaffolding.BusinessLogic.Models;

public enum LockDownBrowserEncryptionKeyType
{
	Launch,
	Challenge,
	ChallengeResponse,
	ProctorExitPassword
}