﻿using StudioKit.Data.Entity.Identity.Interfaces;
using System.Collections.Generic;

namespace StudioKit.Scaffolding.BusinessLogic.Models;

public class CreatedEntityUserRolesBusinessModel<TUser, TEntityUserRole>
	where TUser : IUser
	where TEntityUserRole : IEntityUserRole
{
	/// <summary>
	/// EntityUserRoles that were added to the Entity Role, sent in the create request
	/// </summary>
	public IEnumerable<EntityUserRoleBusinessModel<TUser, TEntityUserRole>> AddedUserRoles { get; set; }

	/// <summary>
	/// EntityUserRoles that were already in the Entity Role, sent in the create request
	/// </summary>
	public IEnumerable<EntityUserRoleBusinessModel<TUser, TEntityUserRole>> ExistingUserRoles { get; set; }

	/// <summary>
	/// EntityUserRoles that were blocked from being added because multiple roles per user is not allowed
	/// </summary>
	public IEnumerable<EntityUserRoleBusinessModel<TUser, TEntityUserRole>> BlockedUserRoles { get; set; }

	public IEnumerable<string> InvalidIdentifiers { get; set; }

	public string AllowedDomains { get; set; }

	public IEnumerable<string> InvalidDomainIdentifiers { get; set; }
}