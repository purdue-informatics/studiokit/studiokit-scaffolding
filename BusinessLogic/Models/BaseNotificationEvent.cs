﻿namespace StudioKit.Scaffolding.BusinessLogic.Models;

public static class BaseNotificationEvent
{
	public const string RoleAddedToGroup = "RoleAddedToGroup";
	public const string RoleRemovedFromGroup = "RoleRemovedFromGroup";
	public const string ExternalGroupAdded = "ExternalGroupAdded";
	public const string ExternalGroupRemoved = "ExternalGroupRemoved";
}