using StudioKit.Data.Entity.Identity.Interfaces;

namespace StudioKit.Scaffolding.BusinessLogic.Models;

public class EntityUserRoleBusinessModel<TUser, TEntityUserRole> : UserRoleBusinessModel<TUser>
	where TUser : IUser
	where TEntityUserRole : IEntityUserRole
{
	public EntityUserRoleBusinessModel(TUser user, string role, TEntityUserRole entityUserRole) : base(user, role)
	{
		EntityUserRole = entityUserRole;
	}

	public TEntityUserRole EntityUserRole { get; set; }
}