﻿namespace StudioKit.Scaffolding.BusinessLogic.Models;

public class CodeResponse
{
	public string Code { get; set; }
}