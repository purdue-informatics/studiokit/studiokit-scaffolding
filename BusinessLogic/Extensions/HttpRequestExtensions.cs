﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using StudioKit.Encryption;
using StudioKit.Sharding;
using StudioKit.Sharding.Extensions;
using System;

namespace StudioKit.Scaffolding.BusinessLogic.Extensions;

public static class HttpRequestExtensions
{
	public static string GetShardKeyFromHost(this HttpRequest request)
	{
		var uri = new Uri(request.GetEncodedUrl());
		if (uri.HostNameType != UriHostNameType.Dns)
			return null;

		// the default value is expected to throw a ShardManagementException when used as a ShardKey
		var shardKey = ShardDomainConstants.InvalidShardKey;

		var host = uri.DnsSafeHost;
		if (host == ShardDomainConstants.Localhost || host.Contains("ngrok.io") || host.Contains("vm-host"))
		{
			var localhostShardKey = EncryptedConfigurationManager.GetSetting(ShardAppSetting.LocalhostShardKey);
			if (!string.IsNullOrWhiteSpace(localhostShardKey))
				return localhostShardKey;

			// localhost defaults to Purdue
			shardKey = ShardDomainConstants.PurdueShardKey;
		}
		else
		{
			// Use query param override
			if (request.Query.Keys.Contains("shardKey"))
				return request.Query["shardKey"];

			// Azure URLs => treat as Purdue domain
			if (host.Contains(ShardDomainConstants.AzureCloudServiceHostname) || host.Contains(ShardDomainConstants.AzureWebAppHostname))
				return ShardDomainConstants.PurdueShardKey;

			// Root Domains
			var hostParts = host.Split('.');
			if (ShardDomainConfiguration.Instance.RootDomainParts.Contains(hostParts[0]))
				return ShardDomainConstants.RootShardKey;

			// Use subdomain
			var subdomainShardKey = host
				.Replace($".{ShardDomainConfiguration.Instance.Hostname}", "")
				.FromSubdomainToShardKey();
			if (!string.IsNullOrWhiteSpace(subdomainShardKey))
				shardKey = subdomainShardKey;
		}

		return shardKey;
	}

	public static string GetApiUrlFromRequest(this HttpRequest request)
	{
		var shardKey = request.GetShardKeyFromHost();
		return ShardDomainConfiguration.Instance.BaseUrlWithShardKey(shardKey);
	}

	public static string GetWebUrlFromRequest(this HttpRequest request)
	{
		var shardKey = request.GetShardKeyFromHost();
		return ShardDomainConfiguration.Instance.BaseUrlWithShardKey(shardKey, true);
	}
}