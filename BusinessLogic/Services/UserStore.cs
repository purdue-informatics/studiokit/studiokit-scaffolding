﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Scaffolding.DataAccess.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services;

public class UserStore<TUser, TContext> : Microsoft.AspNetCore.Identity.EntityFrameworkCore.UserStore<TUser, Role, TContext, string,
	UserClaim, UserRole, UserLogin, IdentityUserToken<string>, IdentityRoleClaim<string>>
	where TContext : DbContext, IBaseDbContext
	where TUser : IdentityUser, IUser, new()
{
	public UserStore(TContext context) : base(context)
	{
	}

	public override async Task<IList<Claim>> GetClaimsAsync(TUser user, CancellationToken cancellationToken = default)
	{
		// load claims from the db with claim issuers
		var claims = (await Context.Set<UserClaim>().Where(uc => uc.UserId == user.Id).ToListAsync(cancellationToken))
			.Select(uc => new Claim(uc.ClaimType, uc.ClaimValue, uc.ClaimValueType, uc.ClaimIssuer, uc.ClaimOriginalIssuer))
			.ToList();

		if (claims.All(sc => sc.Type != ClaimTypes.Email))
			claims.Add(new Claim(ClaimTypes.Email, user.Email));

		return claims;
	}
}