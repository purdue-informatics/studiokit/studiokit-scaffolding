﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using StudioKit.Configuration;
using StudioKit.Data.Entity.Identity;
using StudioKit.Encryption;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.Service.Models.RequestModels;
using StudioKit.TransientFaultHandling.Http.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace StudioKit.Scaffolding.BusinessLogic.Services;

public class PurdueCasService
{
	private readonly ILogger<PurdueCasService> _logger;
	private readonly Func<IHttpClient> _getHttpClient;

	public PurdueCasService(ILogger<PurdueCasService> logger, Func<IHttpClient> getHttpClient)
	{
		_logger = logger;
		_getHttpClient = getHttpClient;
	}

	public async Task<ExternalLoginInfo> ValidateV1CredentialsAsync(
		string shardKey,
		LoginRequestModel model,
		CancellationToken cancellationToken = default)
	{
		if (string.IsNullOrEmpty(shardKey)) throw new ArgumentNullException(nameof(shardKey));
		if (model == null) throw new ArgumentNullException(nameof(model));

		// validate that the user is allowed to log in using this method
		var tier = EncryptedConfigurationManager.GetSetting(BaseAppSetting.Tier);
		// on QA or PROD, only users listed as SuperAdmins are allowed
		var isSuperAdmin = tier is Tier.QA or Tier.Prod &&
							new List<string>(EncryptedConfigurationManager.GetSetting(BaseAppSetting.SuperAdminEmailAddresses).Split(','))
								.Contains($"{model.Username}@purdue.edu");
		// anyone is allowed on non-QA/PROD tiers
		var isAllowed = (tier != Tier.QA && tier != Tier.Prod) || isSuperAdmin;
		if (!isAllowed) throw new ForbiddenException();

		// get a CAS Ticket using a headless browser and the service for the given shard
		var service = PurdueCas.GetService(shardKey);
		var ticket = await LoadTicketAsync(service, model, cancellationToken);
		var externalLoginInfo = await ValidateTicketAsync(shardKey, ticket, service, cancellationToken);
		var claimsIdentity = externalLoginInfo.Principal.Identity as ClaimsIdentity;

		if (tier != Tier.QA && tier != Tier.Prod)
		{
			// Setup user as a Instructor on LOCAL or DEV
			claimsIdentity?.AddClaim(new Claim(ClaimTypes.Role, BaseRole.Creator));
		}
		else if (isSuperAdmin)
		{
			// Setup user as a SuperAdmin on QA or PROD
			claimsIdentity?.AddClaim(new Claim(ClaimTypes.Role, BaseRole.SuperAdmin));
		}

		return externalLoginInfo;
	}

	public async Task<ExternalLoginInfo> ValidateTicketAsync(
		string shardKey,
		string ticket,
		string service = null,
		CancellationToken cancellationToken = default)
	{
		if (string.IsNullOrEmpty(shardKey)) throw new ArgumentNullException(nameof(shardKey));
		if (string.IsNullOrEmpty(ticket)) throw new ArgumentNullException(nameof(ticket));

		// validate the service
		var defaultService = PurdueCas.GetService(shardKey);
		var tier = EncryptedConfigurationManager.GetSetting(BaseAppSetting.Tier);
		// only the default service is allowed on DEV, QA, and PROD
		if (service != null && service != defaultService && tier != Tier.Local)
			throw new ForbiddenException();

		_logger.LogDebug($"CAS ticket: {ticket}");
		var validateUrl = new Uri(PurdueCas.ConstructUrl(PurdueCas.ValidateUrl, service ?? defaultService, ticket));
		var httpClient = _getHttpClient();
		httpClient.RetryCount = 0;
		var result = await httpClient.GetStringAsync(validateUrl, cancellationToken);
		_logger.LogInformation($"Ticket validation result: {result}");
		XNamespace ns = "http://www.yale.edu/tp/cas";
		var parsedDocument = XDocument.Parse(result);

		if (parsedDocument.Descendants().Any(n => n.Name == ns + "authenticationFailure"))
			throw new Exception("Could not validate CAS ticket");

		var userNode =
			parsedDocument
				.Descendants(ns + "serviceResponse")
				.Descendants(ns + "authenticationSuccess")
				.Descendants(ns + "attributes")
				.ToList();
		var login = userNode.Descendants(ns + "login").First().Value;
		// remove middle initial from first name
		var firstName = Regex.Replace(userNode.Descendants(ns + "firstname").FirstOrDefault()?.Value ?? string.Empty, " .$", string.Empty);
		var lastName = userNode.Descendants(ns + "lastname").FirstOrDefault()?.Value;
		var emailAddress = userNode.Descendants(ns + "email").FirstOrDefault()?.Value;
		var puid = userNode.Descendants(ns + "puid").First().Value;
		var i2A2Characteristics =
			userNode.Descendants(ns + "i2a2characteristics").FirstOrDefault()?.Value.Split(',') ?? Array.Empty<string>();
		var casUser = new PurdueCasUser
		{
			Login = login,
			// if we don't have a first name, fall back to using lastname as first name (and vice versa)
			FirstName = !string.IsNullOrEmpty(firstName) ? firstName : lastName,
			LastName = !string.IsNullOrEmpty(lastName) ? lastName : firstName,
			// if we don't have an email address, fall back to using login@purdue
			EmailAddress = !string.IsNullOrEmpty(emailAddress) ? emailAddress : login + "@purdue.edu",
			Puid = puid,
			I2A2Characteristics = i2A2Characteristics
		};
		return GetExternalLoginInfo(casUser);
	}

	private async Task<string> LoadTicketAsync(string service, LoginRequestModel model, CancellationToken cancellationToken)
	{
		_logger.LogDebug($"Loading CAS ticket for \"{model.Username}\" with service \"{service}\"...");

		var headlessCasTicketUrl = $"{PurdueCas.HeadlessCasBaseUrl}/GetCasTicket?code={PurdueCas.HeadlessCasHostKey}";
		var payload = new
		{
			Service = service,
			model.Username,
			model.Password
		};
		var payloadString = JsonConvert.SerializeObject(
			payload,
			new JsonSerializerSettings
			{
				ContractResolver = new CamelCasePropertyNamesContractResolver()
			});
		var httpClient = _getHttpClient();
		httpClient.RetryCount = 0;
		httpClient.Timeout = 180;
		var ticket = await httpClient.PostForStringAsync(
			new Uri(headlessCasTicketUrl),
			_ => new StringContent(payloadString, Encoding.UTF8, "application/json"),
			cancellationToken,
			throwIfNotSuccessful: true);

		_logger.LogDebug($"Loaded CAS ticket: {ticket}");

		return ticket;
	}

	private static ExternalLoginInfo GetExternalLoginInfo(PurdueCasUser purdueCasUser)
	{
		var claims = new List<Claim>();

		// set the Name claim to be used as the user's UserName for the app
		claims.Add(new Claim(ClaimTypes.Name, purdueCasUser.EmailAddress ?? $"{purdueCasUser.Login}@purdue.edu"));

		if (!string.IsNullOrWhiteSpace(purdueCasUser.Login))
		{
			// Common
			claims.Add(new Claim(ClaimTypes.Name, purdueCasUser.Login, ClaimValueTypes.String, PurdueCas.LoginProvider));
			// CAS
			claims.Add(new Claim(ScaffoldingClaimTypes.CasLogin, purdueCasUser.Login, ClaimValueTypes.String, PurdueCas.LoginProvider));
		}

		if (!string.IsNullOrWhiteSpace(purdueCasUser.Puid))
		{
			// Common
			claims.Add(new Claim(ClaimTypes.NameIdentifier, purdueCasUser.Puid, ClaimValueTypes.String, PurdueCas.LoginProvider));
			// CAS
			claims.Add(new Claim(ScaffoldingClaimTypes.CasPuid, purdueCasUser.Puid, ClaimValueTypes.String, PurdueCas.LoginProvider));
		}

		if (!string.IsNullOrWhiteSpace(purdueCasUser.EmailAddress))
		{
			claims.Add(new Claim(ClaimTypes.Email, purdueCasUser.EmailAddress, ClaimValueTypes.String, PurdueCas.LoginProvider));
		}

		if (!string.IsNullOrWhiteSpace(purdueCasUser.FirstName))
			claims.Add(new Claim(ClaimTypes.GivenName, purdueCasUser.FirstName, ClaimValueTypes.String, PurdueCas.LoginProvider));

		if (!string.IsNullOrWhiteSpace(purdueCasUser.LastName))
			claims.Add(new Claim(ClaimTypes.Surname, purdueCasUser.LastName, ClaimValueTypes.String, PurdueCas.LoginProvider));

		if (purdueCasUser.I2A2Characteristics != null && purdueCasUser.I2A2Characteristics.Any())
		{
			claims.Add(new Claim(ScaffoldingClaimTypes.CasI2A2Characteristics, string.Join(",", purdueCasUser.I2A2Characteristics),
				ClaimValueTypes.String, PurdueCas.LoginProvider));

			// Application Role
			if (PurdueI2A2Characteristics.IsPurdueInstructor(purdueCasUser.I2A2Characteristics))
				claims.Add(new Claim(ClaimTypes.Role, BaseRole.Creator));
		}

		return new ExternalLoginInfo(
			new ClaimsPrincipal(new ClaimsIdentity(claims)),
			PurdueCas.LoginProvider,
			purdueCasUser.Login,
			"Purdue CAS");
	}
}