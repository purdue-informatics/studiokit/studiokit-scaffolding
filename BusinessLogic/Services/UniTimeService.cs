﻿using FluentValidation;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.ExternalProvider.UniTime.BusinessLogic.Services;
using StudioKit.ExternalProvider.UniTime.Models;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.DataAccess;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Properties;
using StudioKit.Utilities.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services;

public class UniTimeService<TContext, TUser, TGroup, TConfiguration> : IUniTimeService
	where TContext : BaseDbContext<TUser, TGroup, TConfiguration>
	where TUser : IdentityUser, IUser, new()
	where TGroup : BaseGroup, new()
	where TConfiguration : BaseConfiguration, new()
{
	private readonly IExternalGroupAuthorizationService _authorizationService;
	private readonly TContext _dbContext;
	private readonly UniTimeRosterProviderService<TUser, ExternalGroup, ExternalGroupUser> _uniTimeRosterProviderService;

	public UniTimeService(IExternalGroupAuthorizationService authorizationService,
		TContext dbContext,
		UniTimeRosterProviderService<TUser, ExternalGroup, ExternalGroupUser> uniTimeRosterProviderService)
	{
		_authorizationService = authorizationService ?? throw new ArgumentNullException(nameof(authorizationService));
		_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
		_uniTimeRosterProviderService =
			uniTimeRosterProviderService ?? throw new ArgumentNullException(nameof(uniTimeRosterProviderService));
	}

	public async Task<InstructorScheduleBusinessModel> GetInstructorScheduleAsync(string userId, int externalTermId, IPrincipal principal,
		CancellationToken cancellationToken = default)
	{
		if (string.IsNullOrWhiteSpace(userId)) throw new ArgumentNullException(nameof(userId));
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		if (userId != principal.Identity.GetUserId())
			await _authorizationService.AssertCanReadExternalGroupsAnyAsync(principal, cancellationToken);

		var user = await _dbContext.Users.SingleAsync(u => u.Id.Equals(userId), cancellationToken);
		if (string.IsNullOrWhiteSpace(user.Puid))
			throw new Exception("Puid is required to load UniTime Instructor Schedule");
		var puidWithoutLeadingZeros = Regex.Replace(user.Puid, @"^0*(.+)$", "$1");

		var externalTerm = await _dbContext.FindEntityAsync<ExternalTerm>(externalTermId, cancellationToken: cancellationToken);

		var uniTimeExternalProvider = await _dbContext.ExternalProviders.OfType<UniTimeExternalProvider>().SingleAsync(cancellationToken);
		var instructorSchedule = (await _uniTimeRosterProviderService.GetInstructorScheduleAsync(
				uniTimeExternalProvider,
				puidWithoutLeadingZeros,
				externalTerm.ExternalId,
				CancellationToken.None))
			.ToList();

		var uniTimeGroups = await _dbContext.UniTimeGroups
			.Where(u =>
				u.CreatedById.Equals(userId) &&
				u.ExternalTermId.Equals(externalTermId))
			.ToListAsync(cancellationToken);

		var uniTimeGroupsToAdd = instructorSchedule
			.Where(i => !uniTimeGroups.Any(ug =>
				ug.ExternalId.Equals(i.ExternalId) &&
				ug.ExternalProviderId.Equals(i.ExternalProviderId)))
			.Select(i => new UniTimeGroup
			{
				ExternalProviderId = i.ExternalProviderId,
				ExternalTermId = externalTermId,
				CreatedById = userId,
				ExternalId = i.ExternalId,
				Name = i.Name,
				Description = i.Description
			})
			.ToList();

		var uniTimeGroupsToRemove = uniTimeGroups
			.Where(i => !instructorSchedule.Any(ug =>
				ug.ExternalId.Equals(i.ExternalId) &&
				ug.ExternalProviderId.Equals(i.ExternalProviderId)))
			.ToList();

		if (uniTimeGroupsToAdd.Any() || uniTimeGroupsToRemove.Any())
		{
			if (uniTimeGroupsToAdd.Any())
				_dbContext.UniTimeGroups.AddRange(uniTimeGroupsToAdd);

			if (uniTimeGroupsToRemove.Any())
				_dbContext.UniTimeGroups.RemoveRange(uniTimeGroupsToRemove);

			await _dbContext.SaveChangesAsync(cancellationToken);
		}

		return new InstructorScheduleBusinessModel
		{
			UserId = user.Id,
			FirstName = user.FirstName,
			LastName = user.LastName,
			ExternalGroups = uniTimeGroups
				.Concat(uniTimeGroupsToAdd)
				.Except(uniTimeGroupsToRemove)
				.Select(eg => new UniTimeGroupBusinessModel(eg))
		};
	}

	public async Task<List<ExternalGroup>> GetExternalGroupsToAddAsync(
		BaseGroup group,
		int? externalTermId,
		List<ExternalGroup> existingExternalGroups,
		IEnumerable<IExternalGroupEditBusinessModel> externalGroups,
		CancellationToken cancellationToken = default)
	{
		var incomingUniTimeGroups = externalGroups
			.OfType<UniTimeGroupExternalGroupEditBusinessModel>()
			.ToList();
		if (!incomingUniTimeGroups.Any())
			return new List<ExternalGroup>();

		var uniTimeGroupIds = incomingUniTimeGroups.Select(ug => ug.Id).ToList();

		var uniTimeGroups = await _dbContext.UniTimeGroups
			.Where(ug => uniTimeGroupIds.Contains(ug.Id))
			.ToListAsync(cancellationToken);

		if (uniTimeGroups.Any(uniTimeGroup =>
				existingExternalGroups.Any(externalGroup =>
					externalGroup.ExternalProviderId.Equals(uniTimeGroup.ExternalProviderId) &&
					externalGroup.ExternalId.Equals(uniTimeGroup.ExternalId))))
			throw new ValidationException(string.Format(Strings.ExternalGroupAlreadyExists, nameof(UniTimeGroup)));

		if (incomingUniTimeGroups.Any(incoming =>
				uniTimeGroups.Any(uniTimeGroup =>
					uniTimeGroup.Id.Equals(incoming.Id) &&
					!uniTimeGroup.CreatedById.Equals(incoming.UserId))))
			throw new ValidationException(string.Format(Strings.ExternalGroupsInvalidUserIds, nameof(UniTimeGroup)));

		if (externalTermId.HasValue &&
			uniTimeGroups.Any(ug => !ug.ExternalTermId.Equals(externalTermId)))
			throw new ValidationException(string.Format(Strings.ExternalGroupsInvalidExternalTermId, nameof(UniTimeGroup)));

		// Note: IsAutoGradePushEnabled is always `null` for UniTime

		return uniTimeGroups
			.Select(uniTimeGroup => new ExternalGroup
			{
				Group = group,
				Name = uniTimeGroup.Name,
				Description = uniTimeGroup.Description,
				ExternalProviderId = uniTimeGroup.ExternalProviderId,
				ExternalId = uniTimeGroup.ExternalId,
				UserId = uniTimeGroup.CreatedById
			})
			.ToList();
	}
}