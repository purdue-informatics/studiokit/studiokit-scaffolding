using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using StudioKit.Data.Entity.Identity.Models;
using System.Collections.Generic;

namespace StudioKit.Scaffolding.BusinessLogic.Services;

public class RoleManager : RoleManager<Role>
{
	public RoleManager(IRoleStore<Role> store,
		IEnumerable<IRoleValidator<Role>> roleValidators,
		ILookupNormalizer keyNormalizer,
		IdentityErrorDescriber errors,
		ILogger<RoleManager> logger) : base(store, roleValidators, keyNormalizer, errors, logger)
	{
	}
}