﻿using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using StudioKit.Sharding;
using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services;

public class MigrationService : IMigrationService
{
	private readonly IMigrationAuthorizationService _authorizationService;

	public MigrationService(IMigrationAuthorizationService authorizationService)
	{
		_authorizationService = authorizationService ?? throw new ArgumentNullException(nameof(authorizationService));
	}

	public async Task<List<MigrationStatus>> GetMigrationStatusesAsync(IPrincipal principal, CancellationToken cancellationToken = default)
	{
		await _authorizationService.AssertCanReadAsync(principal, cancellationToken);
		return await ShardManager.Instance.GetAllShardDatabaseMigrationStatusesAsync(cancellationToken);
	}

	public async Task<List<MigrationStatus>> MigrateAsync(IPrincipal principal, string targetMigration = null,
		CancellationToken cancellationToken = default)
	{
		await _authorizationService.AssertCanMigrateAsync(principal, cancellationToken);
		return await ShardManager.Instance.MigrateAndSeedAllShardDatabasesAsync(targetMigration, cancellationToken);
	}

	public async Task SeedAsync(IPrincipal principal, CancellationToken cancellationToken = default)
	{
		await _authorizationService.AssertCanMigrateAsync(principal, cancellationToken);
		await ShardManager.Instance.SeedAllShardDatabasesAsync(cancellationToken);
	}
}