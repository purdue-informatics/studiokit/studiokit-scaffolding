using Microsoft.EntityFrameworkCore;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.DataAccess.Interfaces;
using StudioKit.Security.Models;
using System;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services;

public class AppClientService<TContext> : IAppClientService
	where TContext : DbContext, IBaseDbContext
{
	private readonly IAppClientAuthorizationService _authorizationService;
	private readonly TContext _dbContext;

	public AppClientService(IAppClientAuthorizationService authorizationService, TContext dbContext)
	{
		_authorizationService = authorizationService ?? throw new ArgumentNullException(nameof(authorizationService));
		_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
	}

	public async Task<AppClient> GetAsync(string clientId, CancellationToken cancellationToken = default)
	{
		if (string.IsNullOrWhiteSpace(clientId)) throw new ArgumentNullException(nameof(clientId));

		var client = await _dbContext.AppClients.SingleOrDefaultAsync(c => c.ClientId.Equals(clientId), cancellationToken);
		return client;
	}

	public async Task<AppClient> UpdateAsync(string clientId, AppClientUpdateBusinessModel businessModel, IPrincipal principal,
		CancellationToken cancellationToken = default)
	{
		if (string.IsNullOrWhiteSpace(clientId)) throw new ArgumentNullException(nameof(clientId));
		if (businessModel == null) throw new ArgumentNullException(nameof(businessModel));
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		await _authorizationService.AssertCanUpdateAsync(principal, cancellationToken);

		var client = await _dbContext.AppClients.SingleOrDefaultAsync(c => c.ClientId.Equals(clientId), cancellationToken);

		if (businessModel.CurrentVersion != client.CurrentVersion)
			client.CurrentVersion = businessModel.CurrentVersion;
		if (businessModel.MinVersion != client.MinVersion)
			client.MinVersion = businessModel.MinVersion;

		await _dbContext.SaveChangesAsync(cancellationToken);

		return client;
	}
}