﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Scaffolding.DataAccess.Interfaces;

namespace StudioKit.Scaffolding.BusinessLogic.Services;

public class RoleStore<TContext> : RoleStore<Role, TContext, string, UserRole, IdentityRoleClaim<string>>, IQueryableRoleStore<Role>
	where TContext : DbContext, IBaseDbContext
{
	public RoleStore(TContext context) : base(context)
	{
	}
}