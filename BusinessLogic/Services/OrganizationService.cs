﻿using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.Models;
using StudioKit.Sharding;
using StudioKit.Sharding.Entity.Identity.Models;
using StudioKit.Sharding.Entity.Interfaces;
using StudioKit.Sharding.Models;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services;

public class OrganizationService<TContext, TConfiguration, TLicense> : IOrganizationService
	where TContext : IShardDbContext<TConfiguration, TLicense>, IDisposable
	where TConfiguration : BaseConfiguration, new()
	where TLicense : License, new()
{
	private readonly Func<string, TContext> _dbContextProvider;

	public OrganizationService(Func<string, TContext> dbContextProvider)
	{
		_dbContextProvider = dbContextProvider;
	}

	public async Task<Organization> GetOrganizationAsync(string shardKey, CancellationToken cancellationToken = default)
	{
		using (var dbContext = _dbContextProvider(shardKey))
		{
			var configuration = await dbContext.GetConfigurationAsync(cancellationToken);
			return new Organization
			{
				Name = configuration.Name,
				Url = ShardDomainConfiguration.Instance.BaseUrlWithShardKey(shardKey),
				Image = configuration.Image
			};
		}
	}
}