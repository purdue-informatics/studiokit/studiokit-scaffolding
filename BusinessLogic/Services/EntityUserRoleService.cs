﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.DataAccess;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Properties;
using StudioKit.Sharding.Interfaces;
using StudioKit.UserInfoService;
using StudioKit.Utilities.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services;

public abstract class EntityUserRoleService<TContext, TUser, TGroup, TConfiguration, TEntityUserRole> :
	IEntityUserRoleService<TEntityUserRole>,
	IEntityUserRoleService<TUser, TEntityUserRole>
	where TContext : BaseDbContext<TUser, TGroup, TConfiguration>
	where TUser : IdentityUser, IUser, new()
	where TGroup : BaseGroup, new()
	where TConfiguration : BaseConfiguration, new()
	where TEntityUserRole : EntityUserRole, new()
{
	private readonly TContext _dbContext;
	private readonly IShardKeyProvider _shardKeyProvider;
	private readonly IUserInfoService _userInfoService;
	private readonly IEntityUserRoleAuthorizationService _authorizationService;
	private readonly RoleManager _roleManager;
	private readonly List<string> _validEntityRoleNames;
	private readonly IUserFactory<TUser> _userFactory;

	protected EntityUserRoleService(TContext dbContext,
		IShardKeyProvider shardKeyProvider,
		IUserInfoService userInfoService,
		IEntityUserRoleAuthorizationService authorizationService,
		RoleManager roleManager,
		List<string> validEntityRoleNames,
		IUserFactory<TUser> userFactory)
	{
		_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
		_shardKeyProvider = shardKeyProvider ?? throw new ArgumentNullException(nameof(shardKeyProvider));
		_userInfoService = userInfoService ?? throw new ArgumentNullException(nameof(userInfoService));
		_authorizationService = authorizationService ?? throw new ArgumentNullException(nameof(authorizationService));
		_roleManager = roleManager ?? throw new ArgumentNullException(nameof(roleManager));
		_validEntityRoleNames = validEntityRoleNames ?? throw new ArgumentNullException(nameof(validEntityRoleNames));
		_userFactory = userFactory ?? throw new ArgumentNullException(nameof(userFactory));
	}

	#region StudioKit.Data.Entity.Identity

	public async Task AddRangeAsync(List<TEntityUserRole> entityUserRoles, IPrincipal principal,
		CancellationToken cancellationToken = default)
	{
		await OnBeforeAddRangeAsync(entityUserRoles, cancellationToken);
		_dbContext.Set<TEntityUserRole>().AddRange(entityUserRoles);
		await _dbContext.SaveChangesAsync(cancellationToken);
		await OnAfterAddRangeAsync(entityUserRoles, principal, cancellationToken);
	}

	public async Task DeleteRangeAsync(List<TEntityUserRole> entityUserRoles, IPrincipal principal, bool throwIfRemovingAllOwners = true,
		CancellationToken cancellationToken = default)
	{
		// persist values to tuples for use after delete
		var entityUserRoleTuples = entityUserRoles
			.Select(eur => (eur.EntityId, eur.UserId, eur.Role.Name))
			.ToList();

		await OnBeforeDeleteRangeAsync(entityUserRoles, throwIfRemovingAllOwners, cancellationToken);
		_dbContext.Set<TEntityUserRole>().RemoveRange(entityUserRoles);
		await _dbContext.SaveChangesAsync(cancellationToken);
		await OnAfterDeleteRangeAsync(entityUserRoleTuples, principal, cancellationToken);
	}

	#endregion StudioKit.Data.Entity.Identity

	#region StudioKit.Scaffolding.BusinessLogic

	public async Task<IEnumerable<EntityUserRoleBusinessModel<TUser, TEntityUserRole>>> GetEntityUserRolesAsync(
		int entityId,
		IPrincipal principal,
		CancellationToken cancellationToken = default)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));
		await _authorizationService.AssertCanReadEntityUserRolesAsync(entityId, principal, cancellationToken);
		return await GetEntityUserRoleBusinessModelsAsync(entityId, cancellationToken);
	}

	public async Task<CreatedEntityUserRolesBusinessModel<TUser, TEntityUserRole>> CreateEntityUserRolesAsync(
		int entityId,
		List<string> identifiers,
		string roleName,
		IPrincipal principal,
		CancellationToken cancellationToken = default)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));
		if (identifiers == null || identifiers.Count == 0) throw new ArgumentNullException(nameof(identifiers));
		if (string.IsNullOrWhiteSpace(roleName)) throw new ArgumentNullException(nameof(roleName));

		await _authorizationService.AssertCanModifyEntityUserRolesAsync(entityId, principal, cancellationToken);

		AssertEntityRoleIsValid(roleName);

		var role = await _roleManager.FindByNameAsync(roleName);
		if (role == null)
			throw new EntityNotFoundException(string.Format(Strings.RoleNotFound, roleName));

		var shardKey = _shardKeyProvider.GetShardKey();
		var configuration = await _dbContext.GetConfigurationAsync(cancellationToken);
		var usersResult = await _dbContext.GetOrCreateUsersByIdentifiersAsync(
			identifiers, configuration, shardKey, _userFactory, _userInfoService, cancellationToken);
		var userIds = usersResult.Users.Select(user => user.Id).ToList();

		// load existing entity user roles for the provided users
		var existingEntityUserRoles = await GetEntityUserRolesQueryable(entityId)
			.Where(eur => userIds.Contains(eur.UserId))
			.Include(eur => eur.Role)
			.ToListAsync(cancellationToken);

		var existingEntityUserRolesInRole = existingEntityUserRoles
			.Where(eur => eur.RoleId == role.Id)
			.ToList();

		var userIdsWithOtherRoles = existingEntityUserRoles
			.Where(eur => eur.RoleId != role.Id)
			.Select(eur => eur.UserId)
			.ToList();

		// find user ids to that are already in the requested entity role
		var existingUserIdsInRole = existingEntityUserRolesInRole
			.Select(eur => eur.UserId)
			.ToList();

		// create new EntityUserRoles
		var entityUserRolesToAdd = userIds
			// exclude users that already have the role
			.Except(existingUserIdsInRole)
			.Select(userId =>
			{
				var entityUserRole = CreateEntityUserRole(entityId);
				entityUserRole.UserId = userId;
				entityUserRole.Role = role;
				entityUserRole.RoleId = role.Id;
				return entityUserRole;
			})
			.ToList();

		// if multiple roles per user is not allowed, exclude those
		var entityUserRolesToBlock = new List<TEntityUserRole>();
		if (!AreMultipleRolesAllowed() && userIdsWithOtherRoles.Any())
		{
			entityUserRolesToBlock = entityUserRolesToAdd.Where(eur =>
					userIdsWithOtherRoles.Contains(eur.UserId))
				.ToList();
			entityUserRolesToAdd = entityUserRolesToAdd.Except(entityUserRolesToBlock).ToList();
		}

		if (entityUserRolesToAdd.Any())
		{
			await AddRangeAsync(entityUserRolesToAdd, principal, cancellationToken);
		}

		return new CreatedEntityUserRolesBusinessModel<TUser, TEntityUserRole>
		{
			AddedUserRoles = entityUserRolesToAdd
				.Select(eur => new EntityUserRoleBusinessModel<TUser, TEntityUserRole>(
					usersResult.Users.Single(u => u.Id.Equals(eur.UserId)),
					roleName,
					eur)),
			ExistingUserRoles = existingEntityUserRolesInRole
				.Select(eur => new EntityUserRoleBusinessModel<TUser, TEntityUserRole>(
					usersResult.Users.Single(u => u.Id.Equals(eur.UserId)),
					roleName,
					eur)),
			BlockedUserRoles = entityUserRolesToBlock
				.Select(eur => new EntityUserRoleBusinessModel<TUser, TEntityUserRole>(
					usersResult.Users.Single(u => u.Id.Equals(eur.UserId)),
					roleName,
					eur)),
			AllowedDomains = usersResult.AllowedDomains,
			InvalidIdentifiers = usersResult.InvalidIdentifiers,
			InvalidDomainIdentifiers = usersResult.InvalidDomainIdentifiers
		};
	}

	public async Task<EntityUserRoleBusinessModel<TUser, TEntityUserRole>> UpdateEntityUserRoleAsync(
		int entityUserRoleId,
		string roleName,
		IPrincipal principal,
		CancellationToken cancellationToken = default)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));
		if (string.IsNullOrWhiteSpace(roleName)) throw new ArgumentNullException(nameof(roleName));

		var entityUserRole = await GetEntityUserRoleAsync(entityUserRoleId, cancellationToken);
		await _authorizationService.AssertCanModifyEntityUserRolesAsync(entityUserRole.EntityId, principal, cancellationToken);

		AssertEntityRoleIsValid(roleName);

		if (AreMultipleRolesAllowed())
		{
			var existingEntityUserRole = await GetEntityUserRolesQueryable(entityUserRole.EntityId)
				.Where(eur => eur.UserId == entityUserRole.UserId && eur.Role.Name == roleName)
				.SingleOrDefaultAsync(cancellationToken);
			if (existingEntityUserRole != null)
			{
				throw new ForbiddenException(
					string.Format(Strings.EntityUserRoleCannotUpdateAlreadyExists, roleName, entityUserRole.UserId));
			}
		}

		var role = await _roleManager.FindByNameAsync(roleName);
		if (role == null)
			throw new EntityNotFoundException(string.Format(Strings.RoleNotFound, roleName));

		var user = await _dbContext.Users.SingleAsync(u => u.Id.Equals(entityUserRole.UserId), cancellationToken);

		if (entityUserRole.RoleId != role.Id)
		{
			var originalRole = entityUserRole.Role;
			await OnBeforeUpdateAsync(entityUserRole, role, principal, cancellationToken);
			entityUserRole.RoleId = role.Id;
			entityUserRole.Role = role;
			await _dbContext.SaveChangesAsync(cancellationToken);
			await OnAfterUpdateAsync(entityUserRole, originalRole, principal, cancellationToken);
		}

		return new EntityUserRoleBusinessModel<TUser, TEntityUserRole>(user, roleName, entityUserRole);
	}

	public async Task DeleteEntityUserRoleAsync(
		int entityUserRoleId,
		IPrincipal principal,
		CancellationToken cancellationToken = default)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		var entityUserRole = await GetEntityUserRoleAsync(entityUserRoleId, cancellationToken);

		var shardKey = _shardKeyProvider.GetShardKey();
		if (principal.Identity.GetUserId() == entityUserRole.UserId)
		{
			await _authorizationService.AssertCanDeleteOwnEntityUserRoleAsync(entityUserRole.EntityId, principal,
				cancellationToken);
		}
		else
		{
			await _authorizationService.AssertCanModifyEntityUserRolesAsync(entityUserRole.EntityId, principal,
				cancellationToken);
		}

		// save the name of the role before we disconnect it from the EntityUserRole
		var deletedRoleName = entityUserRole.Role.Name;
		await OnBeforeDeleteAsync(entityUserRole, cancellationToken);
		_dbContext.Set<TEntityUserRole>().Remove(entityUserRole);
		await _dbContext.SaveChangesAsync(cancellationToken);
		await OnAfterDeleteAsync(entityUserRole.EntityId, entityUserRole.UserId, deletedRoleName, shardKey,
			principal, cancellationToken);
	}

	#endregion StudioKit.Scaffolding.BusinessLogic

	#region Protected Methods

	protected abstract Task<TEntityUserRole> GetEntityUserRoleAsync(int entityUserRoleId, CancellationToken cancellationToken = default);

	protected abstract IQueryable<TEntityUserRole> GetEntityUserRolesQueryable(int entityId);

	protected abstract TEntityUserRole CreateEntityUserRole(int entityId);

	private async Task<IEnumerable<EntityUserRoleBusinessModel<TUser, TEntityUserRole>>> GetEntityUserRoleBusinessModelsAsync(int entityId,
		CancellationToken cancellationToken = default)
	{
		return (await GetEntityUserRolesQueryable(entityId)
				.Join(
					_dbContext.Users,
					eur => eur.UserId,
					u => u.Id,
					(eur, u) => new { EntityUserRole = eur, User = u })
				.Join(
					_dbContext.Roles,
					euru => euru.EntityUserRole.RoleId,
					r => r.Id,
					(euru, r) => new { euru.EntityUserRole, euru.User, Role = r })
				.ToListAsync(cancellationToken))
			.Select(o => new EntityUserRoleBusinessModel<TUser, TEntityUserRole>(o.User, o.Role.Name, o.EntityUserRole));
	}

	protected virtual bool AreMultipleRolesAllowed()
	{
		return false;
	}

	protected virtual Task OnBeforeAddAsync(TEntityUserRole entityUserRole, CancellationToken cancellationToken = default)
	{
		return Task.CompletedTask;
	}

	protected virtual Task OnAfterAddAsync(TEntityUserRole entityUserRole, IPrincipal principal,
		CancellationToken cancellationToken = default)
	{
		return Task.CompletedTask;
	}

	protected virtual Task OnBeforeAddRangeAsync(List<TEntityUserRole> entityUserRoles, CancellationToken cancellationToken = default)
	{
		return Task.CompletedTask;
	}

	protected virtual Task OnAfterAddRangeAsync(List<TEntityUserRole> entityUserRoles,
		IPrincipal principal, CancellationToken cancellationToken = default)
	{
		return Task.CompletedTask;
	}

	protected virtual Task OnBeforeUpdateAsync(TEntityUserRole entityUserRole, Role newRole, IPrincipal principal,
		CancellationToken cancellationToken = default)
	{
		return Task.CompletedTask;
	}

	protected virtual Task OnAfterUpdateAsync(TEntityUserRole entityUserRole, Role originalRole, IPrincipal principal,
		CancellationToken cancellationToken = default)
	{
		return Task.CompletedTask;
	}

	protected virtual Task OnBeforeDeleteAsync(TEntityUserRole entityUserRole, CancellationToken cancellationToken = default)
	{
		return Task.CompletedTask;
	}

	protected virtual Task OnAfterDeleteAsync(int entityId, string userId, string roleName, string shardKey,
		IPrincipal principal, CancellationToken cancellationToken = default)
	{
		return Task.CompletedTask;
	}

	protected virtual Task OnBeforeDeleteRangeAsync(List<TEntityUserRole> entityUserRoles, bool throwIfRemovingAllOwners = true,
		CancellationToken cancellationToken = default)
	{
		return Task.CompletedTask;
	}

	protected virtual Task OnAfterDeleteRangeAsync(List<(int EntityId, string UserId, string RoleName)> entityUserRoles,
		IPrincipal principal, CancellationToken cancellationToken = default)
	{
		return Task.CompletedTask;
	}

	#endregion Protected Methods

	#region Private Methods

	private void AssertEntityRoleIsValid(string roleName)
	{
		if (!_validEntityRoleNames.Contains(roleName))
			throw new ArgumentOutOfRangeException(nameof(roleName), string.Format(Strings.EntityRoleNotValid, roleName));
	}

	#endregion Private Methods
}