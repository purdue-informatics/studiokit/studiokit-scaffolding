﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.DataAccess;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Properties;
using StudioKit.Utilities.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services;

public class BaseUserService<TContext, TUser, TGroup, TConfiguration, TUserBusinessModel, TUserEditBusinessModel> : IBaseUserService<TUser,
	TUserBusinessModel, TUserEditBusinessModel>
	where TContext : BaseDbContext<TUser, TGroup, TConfiguration>
	where TUser : IdentityUser, IUser, new()
	where TGroup : BaseGroup, new()
	where TConfiguration : BaseConfiguration, new()
	where TUserBusinessModel : BaseUserBusinessModel<TUser>, new()
	where TUserEditBusinessModel : BaseUserEditBusinessModel<TUser>, new()
{
	private readonly TContext _dbContext;
	private readonly IUserManager<TUser> _userManager;
	private readonly IUserAuthorizationService _userAuthorizationService;
	private readonly IOAuthService _oauthService;

	public BaseUserService(
		TContext dbContext,
		IUserManager<TUser> userManager,
		IUserAuthorizationService userAuthorizationService,
		IOAuthService oauthService)
	{
		_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
		_userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
		_userAuthorizationService = userAuthorizationService ?? throw new ArgumentNullException(nameof(userAuthorizationService));
		_oauthService = oauthService ?? throw new ArgumentNullException(nameof(oauthService));
	}

	public Task<IEnumerable<TUserBusinessModel>> GetAllAsync(IPrincipal principal, CancellationToken cancellationToken = default,
		IDictionary<string, int> relationIds = null)
	{
		throw new NotSupportedException();
	}

	public async Task<TUserBusinessModel> GetAsync(string userId, IPrincipal principal, CancellationToken cancellationToken = default,
		IDictionary<string, int> relationIds = null)
	{
		if (string.IsNullOrWhiteSpace(userId)) throw new ArgumentNullException(nameof(userId));
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		await _userAuthorizationService.AssertCanReadAsync(userId, principal, cancellationToken);

		var user = await _userManager.FindByIdAsync(userId);
		var roles = await _dbContext.UserRoles
			.Where(ur => ur.UserId == userId)
			.Join(_dbContext.RoleActivities, r => r.RoleId, ra => ra.RoleId, (n, ra) => ra.Role.Name)
			.Distinct()
			.ToListAsync(cancellationToken);
		var activities = await _dbContext.UserRoles
			.Where(ur => ur.UserId == userId)
			.Join(_dbContext.RoleActivities, r => r.RoleId, ra => ra.RoleId, (n, ra) => ra.Activity.Name)
			.Distinct()
			.ToListAsync(cancellationToken);

		return new TUserBusinessModel
		{
			User = user,
			Roles = roles,
			Activities = activities
		};
	}

	public Task<TUserBusinessModel> CreateAsync(TUserEditBusinessModel businessModel, IPrincipal principal,
		CancellationToken cancellationToken = default, IDictionary<string, int> relationIds = null)
	{
		throw new NotImplementedException();
	}

	public Task<TUserBusinessModel> UpdateAsync(string userId, TUserEditBusinessModel businessModel, IPrincipal principal,
		CancellationToken cancellationToken = default, IDictionary<string, int> relationIds = null)
	{
		throw new NotImplementedException();
	}

	public Task DeleteAsync(string userId, IPrincipal principal, CancellationToken cancellationToken = default,
		IDictionary<string, int> relationIds = null)
	{
		throw new NotSupportedException();
	}

	public async Task<IEnumerable<TUserBusinessModel>> GetAsync(IPrincipal principal, string keywords,
		CancellationToken cancellationToken = default)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));
		if (string.IsNullOrWhiteSpace(keywords)) throw new ArgumentNullException(nameof(keywords));

		await _userAuthorizationService.AssertCanReadAnyAsync(principal, cancellationToken);

		var keywordList = keywords.Split(' ').Select(k => k.ToLower()).Distinct().ToList();

		var usersQueryable = _dbContext.GetUsersMatchingKeywordsQueryable(_dbContext.Users, keywordList);
		var users = await usersQueryable.ToListAsync(cancellationToken);
		var userRoles = await usersQueryable
			.Join(_dbContext.IdentityUserRoles, u => u.Id, ur => ur.UserId, (u, ur) => new { UserId = u.Id, ur.RoleId })
			.Join(_dbContext.Roles, n => n.RoleId, r => r.Id, (n, r) => new { n.UserId, r.Name })
			.ToListAsync(cancellationToken);
		var userRoleActivities = await usersQueryable
			.Join(_dbContext.IdentityUserRoles, u => u.Id, ur => ur.UserId, (u, ur) => new { UserId = u.Id, ur.RoleId })
			.Join(_dbContext.RoleActivities, n => n.RoleId, ra => ra.RoleId, (n, ra) => new { n.UserId, ra.Activity.Name })
			.ToListAsync(cancellationToken);

		return users.Select(u => new TUserBusinessModel
		{
			User = u,
			Roles = userRoles.Where(ur => ur.UserId.Equals(u.Id)).Select(ur => ur.Name).Distinct(),
			Activities = userRoleActivities.Where(ur => ur.UserId.Equals(u.Id)).Select(ur => ur.Name).Distinct()
		});
	}

	/// <inheritdoc/>
	/// <exception cref="ArgumentNullException"></exception>
	/// <exception cref="ForbiddenException"></exception>
	/// <exception cref="EntityNotFoundException"></exception>
	public async Task<CodeResponse> StartImpersonationAsync(string userId, IPrincipal principal,
		CancellationToken cancellationToken = default)
	{
		if (string.IsNullOrWhiteSpace(userId)) throw new ArgumentNullException(nameof(userId));
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		if (principal.Identity is ClaimsIdentity claimsIdentity &&
			claimsIdentity.Claims.Any(c => c.Type.Equals(ScaffoldingClaimTypes.ImpersonatorUserId)))
			throw new ForbiddenException(Strings.ImpersonationAlreadyStarted);

		await _userAuthorizationService.AssertCanImpersonateAsync(userId, principal, cancellationToken);

		var currentUserId = principal.Identity.GetUserId();

		var user = await _dbContext.Users.SingleOrDefaultAsync(u => u.Id.Equals(userId), cancellationToken);
		if (user == null)
			throw new EntityNotFoundException(string.Format(Strings.EntityNotFoundById, typeof(TUser).Name, userId));

		var impersonationClaims = new List<Claim>
		{
			new Claim(ScaffoldingClaimTypes.ImpersonatorUserId, currentUserId, ClaimValueTypes.String)
		};
		var code = _oauthService.GetAuthorizationCode(user, impersonationClaims);
		return new CodeResponse
		{
			Code = code
		};
	}

	/// <inheritdoc/>
	/// <exception cref="ArgumentNullException"></exception>
	/// <exception cref="ForbiddenException"></exception>
	/// <exception cref="EntityNotFoundException"></exception>
	public async Task<CodeResponse> StopImpersonationAsync(IPrincipal principal, CancellationToken cancellationToken = default)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		Claim impersonatorClaim = null;
		if (principal.Identity is ClaimsIdentity claimsIdentity)
			impersonatorClaim = claimsIdentity.Claims.FirstOrDefault(c => c.Type.Equals(ScaffoldingClaimTypes.ImpersonatorUserId));

		if (impersonatorClaim == null)
			throw new ForbiddenException(Strings.ImpersonationNotStarted);

		var user = await _dbContext.Users.SingleOrDefaultAsync(u => u.Id.Equals(impersonatorClaim.Value), cancellationToken);
		if (user == null)
			throw new EntityNotFoundException(string.Format(Strings.EntityNotFoundById, typeof(TUser).Name, impersonatorClaim.Value));

		var code = _oauthService.GetAuthorizationCode(user, new List<Claim>());
		return new CodeResponse
		{
			Code = code
		};
	}
}