﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using StudioKit.Cloud.Storage.Queue;
using StudioKit.Data.Entity.Extensions;
using StudioKit.Data.Entity.Identity;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Models.Interfaces;
using StudioKit.Notification.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.DataAccess;
using StudioKit.Scaffolding.DataAccess.Queues.QueueMessages;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Properties;
using StudioKit.Security.BusinessLogic.Models;
using StudioKit.Sharding.Interfaces;
using StudioKit.UserInfoService;
using StudioKit.Utilities.Extensions;
using StudioKit.Utilities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services;

public class BaseGroupUserRoleService<TContext, TUser, TGroup, TConfiguration> :
	EntityUserRoleService<TContext, TUser, TGroup, TConfiguration, GroupUserRole>
	where TContext : BaseDbContext<TUser, TGroup, TConfiguration>
	where TUser : IdentityUser, IUser, new()
	where TGroup : BaseGroup, new()
	where TConfiguration : BaseConfiguration, new()
{
	private readonly TContext _dbContext;
	private readonly IQueueManager<SyncGroupRosterMessage> _syncGroupRosterQueueManager;
	private readonly INotificationService<TGroup> _notificationService;
	private readonly IDateTimeProvider _dateTimeProvider;

	public BaseGroupUserRoleService(TContext dbContext,
		IShardKeyProvider shardKeyProvider,
		IUserInfoService userInfoService,
		IEntityUserRoleAuthorizationService authorizationService,
		RoleManager roleManager,
		IQueueManager<SyncGroupRosterMessage> syncGroupRosterQueueManager,
		IDateTimeProvider dateTimeProvider,
		IUserFactory<TUser> userFactory,
		INotificationService<TGroup> notificationService = null) : base(dbContext, shardKeyProvider, userInfoService,
		authorizationService, roleManager, BaseRole.GroupRoles, userFactory)
	{
		_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
		_syncGroupRosterQueueManager = syncGroupRosterQueueManager ?? throw new ArgumentNullException(nameof(syncGroupRosterQueueManager));
		_dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));

		// optional
		_notificationService = notificationService;
	}

	protected override async Task<GroupUserRole> GetEntityUserRoleAsync(int entityUserRoleId, CancellationToken cancellationToken = default)
	{
		var groupUserRole = await _dbContext.GroupUserRoles
			.Include(gur => gur.Role)
			.SingleOrDefaultAsync(gur => gur.Id.Equals(entityUserRoleId), cancellationToken);
		if (groupUserRole == null)
			throw new EntityNotFoundException(string.Format(Strings.EntityNotFoundById, nameof(GroupUserRole), entityUserRoleId));

		return groupUserRole;
	}

	protected override IQueryable<GroupUserRole> GetEntityUserRolesQueryable(int entityId)
	{
		return _dbContext.GroupUserRoles.Where(gur => gur.GroupId.Equals(entityId));
	}

	protected override GroupUserRole CreateEntityUserRole(int entityId)
	{
		return new GroupUserRole
		{
			GroupId = entityId
		};
	}

	protected override bool AreMultipleRolesAllowed()
	{
		return true;
	}

	protected override async Task OnBeforeAddAsync(GroupUserRole entityUserRole, CancellationToken cancellationToken = default)
	{
		await OnBeforeAddRangeAsync(new List<GroupUserRole> { entityUserRole }, cancellationToken);
	}

	protected override async Task OnAfterAddAsync(GroupUserRole entityUserRole, IPrincipal principal,
		CancellationToken cancellationToken = default)
	{
		await OnAfterAddRangeAsync(new List<GroupUserRole> { entityUserRole }, principal, cancellationToken);
	}

	protected override async Task OnBeforeAddRangeAsync(List<GroupUserRole> entityUserRoles, CancellationToken cancellationToken = default)
	{
		if (!entityUserRoles.Any())
			return;

		var groupId = entityUserRoles.First().GroupId;
		var group = await _dbContext.Groups
			.Include(g => g.ExternalTerm)
			.SingleAsync(g => g.Id.Equals(groupId), cancellationToken);

		// roles are not always referenced by entityUserRoles before add
		var roles = await _dbContext.Roles.ToListAsync(cancellationToken);

		foreach (var entityUserRole in entityUserRoles)
		{
			AssertNotAddingLearnersToEndedGroup(roles.Single(r => r.Id.Equals(entityUserRole.RoleId)), group);
		}

		_dbContext.GroupUserRoleLogs.AddRange(entityUserRoles.Select(entityUserRole => new GroupUserRoleLog
		{
			UserId = entityUserRole.UserId,
			GroupId = entityUserRole.GroupId,
			RoleId = entityUserRole.RoleId,
			Type = GroupUserRoleLogType.Added
		}));
	}

	protected override async Task OnAfterAddRangeAsync(List<GroupUserRole> entityUserRoles, IPrincipal principal,
		CancellationToken cancellationToken = default)
	{
		if (!entityUserRoles.Any())
			return;

		var groupId = entityUserRoles.First().GroupId;

		// notify for each unique role
		foreach (var g in entityUserRoles.GroupBy(n => n.Role.Name))
		{
			await NotifyUserRoleChangesIfNeededAsync(
				groupId,
				g.Select(ur => ur.UserId),
				g.Key,
				false,
				principal,
				null,
				cancellationToken);
		}
	}

	protected override async Task OnBeforeUpdateAsync(GroupUserRole groupUserRole, Role newRole,
		IPrincipal principal, CancellationToken cancellationToken = default)
	{
		// Only need to check when the role being updated is currently the owner and changing to something else
		if (groupUserRole.Role.Name.Equals(BaseRole.GroupOwner))
		{
			await AssertNotRemovingAllOwnersAsync(groupUserRole.GroupId, new List<GroupUserRole> { groupUserRole }, cancellationToken);
		}
	}

	protected override async Task OnAfterUpdateAsync(GroupUserRole groupUserRole, Role originalRole, IPrincipal principal,
		CancellationToken cancellationToken = default)
	{
		// Send 'removed' notification if needed for the original role
		await NotifyUserRoleChangesIfNeededAsync(
			groupUserRole.EntityId,
			new List<string> { groupUserRole.UserId },
			originalRole.Name,
			true,
			principal,
			null,
			cancellationToken);

		// Send 'added' notification if needed for the new role
		await NotifyUserRoleChangesIfNeededAsync(
			groupUserRole.EntityId,
			new List<string> { groupUserRole.UserId },
			groupUserRole.Role.Name,
			false,
			principal,
			null,
			cancellationToken);
	}

	protected override async Task OnBeforeDeleteAsync(GroupUserRole entityUserRole, CancellationToken cancellationToken = default)
	{
		await OnBeforeDeleteRangeAsync(new List<GroupUserRole> { entityUserRole }, cancellationToken: cancellationToken);
	}

	protected override async Task OnAfterDeleteAsync(int entityId, string userId, string roleName, string shardKey,
		IPrincipal principal, CancellationToken cancellationToken = default)
	{
		var removedExternalGroups = await RemoveExternalGroupsIfNeededAsync(entityId, userId, shardKey, cancellationToken);
		await NotifyUserRoleChangesIfNeededAsync(
			entityId,
			new List<string> { userId },
			roleName,
			true,
			principal,
			removedExternalGroups,
			cancellationToken);
	}

	protected override async Task OnBeforeDeleteRangeAsync(List<GroupUserRole> entityUserRoles, bool throwIfRemovingAllOwners = true,
		CancellationToken cancellationToken = default)
	{
		if (!entityUserRoles.Any())
			return;

		var groupId = entityUserRoles.First().GroupId;

		if (throwIfRemovingAllOwners)
			await AssertNotRemovingAllOwnersAsync(
				groupId,
				entityUserRoles.Where(eur => eur.Role.Name.Equals(BaseRole.GroupOwner)).ToList(),
				cancellationToken);

		var group = await _dbContext.Groups
			.Include(g => g.ExternalTerm)
			.SingleAsync(g => g.Id.Equals(groupId), cancellationToken);

		AssertNotRemovingUsersFromEndedGroup(group);

		_dbContext.GroupUserRoleLogs.AddRange(entityUserRoles.Select(entityUserRole => new GroupUserRoleLog
		{
			UserId = entityUserRole.UserId,
			GroupId = entityUserRole.GroupId,
			RoleId = entityUserRole.RoleId,
			Type = GroupUserRoleLogType.Removed
		}));
	}

	protected override async Task OnAfterDeleteRangeAsync(
		List<(int EntityId, string UserId, string RoleName)> entityUserRoles, IPrincipal principal,
		CancellationToken cancellationToken = default)
	{
		if (!entityUserRoles.Any())
			return;

		var groupId = entityUserRoles.First().EntityId;

		// notify for each unique role
		foreach (var g in entityUserRoles.GroupBy(n => n.RoleName))
		{
			await NotifyUserRoleChangesIfNeededAsync(
				groupId,
				g.Select(ur => ur.UserId),
				g.Key,
				true,
				principal,
				null, // bulk removed instructors should never remove ExternalGroups (e.g. from RosterSync or Lti Launch)
				cancellationToken);
		}
	}

	#region Private Methods

	private void AssertNotAddingLearnersToEndedGroup(Role role, TGroup group)
	{
		if (role.Name.Equals(BaseRole.GroupLearner) &&
			group.IsEnded<BaseGroup, ExternalTerm, GroupUserRole, ExternalGroup>(_dateTimeProvider
				.UtcNow))
		{
			throw new ForbiddenException(Strings.EndedGroupCannotAddLearner);
		}
	}

	private void AssertNotRemovingUsersFromEndedGroup(TGroup group)
	{
		if (group.IsEnded<BaseGroup, ExternalTerm, GroupUserRole, ExternalGroup>(_dateTimeProvider.UtcNow))
		{
			throw new ForbiddenException(Strings.EndedGroupCannotRemoveUser);
		}
	}

	private async Task AssertNotRemovingAllOwnersAsync(int groupId, IReadOnlyCollection<GroupUserRole> ownersToRemove,
		CancellationToken cancellationToken = default)
	{
		if (!ownersToRemove.Any())
			return;

		var ownerCount = await _dbContext.GroupUserRoles.CountAsync(
			eur => eur.GroupId == groupId && eur.Role.Name.Equals(BaseRole.GroupOwner),
			cancellationToken);
		if (ownerCount == ownersToRemove.Count)
		{
			throw new ForbiddenException(Strings.CannotRemoveLastEntityOwner);
		}
	}

	private async Task<List<ExternalGroup>> RemoveExternalGroupsIfNeededAsync(int groupId, string instructorUserId, string shardKey,
		CancellationToken cancellationToken = default)
	{
		var externalGroups = await _dbContext.ExternalGroups
			.Where(eg =>
				// belongs to the group
				eg.GroupId.Equals(groupId) &&
				// is connected to the instructor
				eg.UserId.Equals(instructorUserId))
			.Include(eg => eg.ExternalProvider)
			.ToListAsync(cancellationToken);
		var externalGroupsToRemove = externalGroups
			// is NOT an LTI external group
			.Where(eg => !eg.ExternalProvider.GetType().EntityTypeEquals(typeof(LtiExternalProvider)))
			.ToList();
		if (!externalGroupsToRemove.Any())
		{
			return new List<ExternalGroup>();
		}

		var isRemovingRosterSyncedExternalGroups = externalGroupsToRemove
			.Any(eg => eg.ExternalProvider.RosterSyncEnabled);

		_dbContext.ExternalGroups.RemoveRange(externalGroupsToRemove);
		await _dbContext.SaveChangesAsync(cancellationToken);

		if (!isRemovingRosterSyncedExternalGroups)
		{
			return externalGroupsToRemove;
		}

		await _syncGroupRosterQueueManager.AddMessageAsync(
			new SyncGroupRosterMessage
			{
				ShardKey = shardKey,
				GroupId = groupId
			}, cancellationToken: cancellationToken);
		return externalGroupsToRemove;
	}

	private async Task NotifyUserRoleChangesIfNeededAsync(int groupId, IEnumerable<string> userIds, string roleName,
		bool isDeleting, IPrincipal principal, List<ExternalGroup> removedExternalGroups = null,
		CancellationToken cancellationToken = default)
	{
		// Notifications are not configured
		if (_notificationService == null)
			return;

		// UserRoleChange notifications only are sent if from another user
		if (principal is SystemPrincipal)
			return;

		// Only send notifications for changes to GroupOwner or GroupGrader roles
		if (roleName != BaseRole.GroupOwner && roleName != BaseRole.GroupGrader)
			return;

		var userId = principal.Identity.GetUserId();
		var updateByUser = await _dbContext.Users.SingleAsync(u => u.Id == userId, cancellationToken);
		var updatedUsers = await _dbContext.Users.Where(u => userIds.Contains(u.Id)).ToListAsync(cancellationToken);

		foreach (var updatedUser in updatedUsers)
		{
			await _notificationService.NotifyUserAsync<GroupRoleNotificationTemplateData<TGroup>>(
				isDeleting
					? BaseNotificationEvent.RoleRemovedFromGroup
					: BaseNotificationEvent.RoleAddedToGroup,
				updatedUser, new SystemPrincipal(), groupId, td =>
				{
					td.Role = new Role(roleName);
					td.UpdatedByUser = updateByUser;
					return Task.FromResult(td);
				}, cancellationToken);
		}

		await _notificationService.NotifyGroupAsync<GroupRoleNotificationTemplateData<TGroup>>(groupId,
			isDeleting ? BaseNotificationEvent.RoleRemovedFromGroup : BaseNotificationEvent.RoleAddedToGroup,
			new SystemPrincipal(), td =>
			{
				td.Role = new Role(roleName);
				td.UpdatedUsers = updatedUsers;
				td.UpdatedByUser = updateByUser;
				td.RemovedExternalGroups = removedExternalGroups;
				return Task.FromResult(td);
			},
			users => users.Where(u => !userIds.Contains(u.Id)), cancellationToken);
	}

	#endregion Private Methods
}