﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using StudioKit.Caliper.Interfaces;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.DataAccess;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Properties;
using StudioKit.Security.BusinessLogic.Interfaces;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services;

public class BaseConfigurationService<TContext, TUser, TGroup, TConfiguration> : IBaseConfigurationService<TConfiguration>
	where TContext : BaseDbContext<TUser, TGroup, TConfiguration>
	where TUser : IdentityUser, IUser, new()
	where TGroup : BaseGroup, new()
	where TConfiguration : BaseConfiguration, new()
{
	private readonly TContext _dbContext;

	public BaseConfigurationService(TContext dbContext)
	{
		_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
	}

	public async Task<TConfiguration> GetConfigurationAsync(CancellationToken cancellationToken = default)
	{
		var configuration = await _dbContext.Configurations.OrderByDescending(c => c.Id).FirstOrDefaultAsync(cancellationToken);
		if (configuration == null)
			throw new EntityNotFoundException(Strings.ConfigurationNotFound);
		return configuration;
	}

	public async Task<ICaliperConfiguration> GetCaliperConfigurationAsync(CancellationToken cancellationToken = default)
	{
		return await GetConfigurationAsync(cancellationToken);
	}

	public async Task<ICertificateConfiguration> GetCertificateConfigurationAsync(CancellationToken cancellationToken = default)
	{
		return await GetConfigurationAsync(cancellationToken);
	}
}