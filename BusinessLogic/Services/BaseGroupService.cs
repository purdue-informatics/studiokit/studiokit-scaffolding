using FluentValidation;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using StudioKit.Cloud.Storage.Queue;
using StudioKit.Data.Entity.Identity;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Models.Interfaces;
using StudioKit.ExternalProvider.UniTime.Models;
using StudioKit.Notification.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.BusinessLogic.Validators;
using StudioKit.Scaffolding.DataAccess;
using StudioKit.Scaffolding.DataAccess.Queues.QueueMessages;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Properties;
using StudioKit.Security.BusinessLogic.Models;
using StudioKit.Sharding.Interfaces;
using StudioKit.Utilities.Extensions;
using StudioKit.Utilities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services;

/// <summary>
/// This class handles basic CRUD operations for groups of users. Subclasses will need to
/// implement additional logic and call superclass methods.
/// </summary>
/// <typeparam name="TContext">The <see cref="DbContext"/> or subtype to use for persistence</typeparam>
/// <typeparam name="TUser">The Identity user type</typeparam>
/// <typeparam name="TGroup"><see cref="BaseGroup"/> or a derived type</typeparam>
/// <typeparam name="TConfiguration">Shard configuration type - <see cref="BaseConfiguration"/> or subtype</typeparam>
/// <typeparam name="TGroupBusinessModel"><see cref="BaseGroupBusinessModel{TGroup,TUser}"/> or subtype</typeparam>
/// <typeparam name="TGroupEditBusinessModel"><see cref="BaseGroupEditBusinessModel{TGroup}"/> or subtype</typeparam>
public class BaseGroupService<TContext, TUser, TGroup, TConfiguration, TGroupBusinessModel, TGroupEditBusinessModel> : IBaseGroupService<
	TGroup, TGroupBusinessModel, TGroupEditBusinessModel, TUser>
	where TContext : BaseDbContext<TUser, TGroup, TConfiguration>
	where TUser : IdentityUser, IUser, new()
	where TGroup : BaseGroup, new()
	where TConfiguration : BaseConfiguration, new()
	where TGroupBusinessModel : BaseGroupBusinessModel<TGroup, TUser>, new()
	where TGroupEditBusinessModel : BaseGroupEditBusinessModel<TGroup>, new()
{
	private readonly IGroupAuthorizationService _authorizationService;
	private readonly BaseGroupBusinessModelValidator<TGroup> _businessModelValidator;
	private readonly TContext _dbContext;
	private readonly IQueueManager<SyncAllRostersMessage> _syncAllRostersQueueManager;
	private readonly IQueueManager<SyncGroupRosterMessage> _syncGroupRosterQueueManager;
	private readonly ILtiLaunchService _ltiLaunchService;
	private readonly IUniTimeService _uniTimeService;
	private readonly IShardKeyProvider _shardKeyProvider;
	private readonly INotificationService<TGroup> _notificationService;
	private readonly IDateTimeProvider _dateTimeProvider;
	private readonly BaseGroupUserRoleService<TContext, TUser, TGroup, TConfiguration> _baseGroupUserRoleService;

	public BaseGroupService(TContext dbContext,
		IGroupAuthorizationService authorizationService,
		BaseGroupBusinessModelValidator<TGroup> businessModelValidator,
		IQueueManager<SyncAllRostersMessage> syncAllRostersQueueManager,
		IQueueManager<SyncGroupRosterMessage> syncGroupRosterQueueManager,
		ILtiLaunchService ltiLaunchService,
		IUniTimeService uniTimeService,
		IShardKeyProvider shardKeyProvider,
		BaseGroupUserRoleService<TContext, TUser, TGroup, TConfiguration> baseGroupUserRoleService,
		IDateTimeProvider dateTimeProvider,
		INotificationService<TGroup> notificationService = null)
	{
		_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
		_authorizationService = authorizationService ?? throw new ArgumentNullException(nameof(authorizationService));
		_businessModelValidator = businessModelValidator ?? throw new ArgumentNullException(nameof(businessModelValidator));
		_syncAllRostersQueueManager = syncAllRostersQueueManager ?? throw new ArgumentNullException(nameof(syncAllRostersQueueManager));
		_syncGroupRosterQueueManager = syncGroupRosterQueueManager ?? throw new ArgumentNullException(nameof(syncGroupRosterQueueManager));
		_ltiLaunchService = ltiLaunchService ?? throw new ArgumentNullException(nameof(ltiLaunchService));
		_uniTimeService = uniTimeService ?? throw new ArgumentNullException(nameof(uniTimeService));
		_shardKeyProvider = shardKeyProvider ?? throw new ArgumentNullException(nameof(shardKeyProvider));
		_baseGroupUserRoleService = baseGroupUserRoleService ?? throw new ArgumentNullException(nameof(baseGroupUserRoleService));
		_dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));

		// Optional
		_notificationService = notificationService;
	}

	public Task<IEnumerable<TGroupBusinessModel>> GetAllAsync(IPrincipal principal, CancellationToken cancellationToken = default,
		IDictionary<string, int> relationIds = null)
	{
		throw new NotSupportedException();
	}

	public async Task<TGroupBusinessModel> GetAsync(int groupId, IPrincipal principal, CancellationToken cancellationToken = default,
		IDictionary<string, int> relationIds = null)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));
		await _authorizationService.AssertCanReadAsync(groupId, principal, cancellationToken);
		var group = await _dbContext.FindEntityAsync<TGroup>(groupId, false, cancellationToken: cancellationToken);
		return await CreateBusinessModelAsync(group, principal.Identity.GetUserId(), cancellationToken);
	}

	public async Task<TGroupBusinessModel> CreateAsync(TGroupEditBusinessModel businessModel, IPrincipal principal,
		CancellationToken cancellationToken = default, IDictionary<string, int> relationIds = null)
	{
		if (businessModel == null) throw new ArgumentNullException(nameof(businessModel));
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		await _authorizationService.AssertCanCreateAsync(principal, cancellationToken);
		if (businessModel.ExternalGroups != null)
			await _authorizationService.AssertCanConnectOwnExternalGroupAsync(principal, cancellationToken);
		await _businessModelValidator.AssertIsValidAsync(businessModel, cancellationToken);

		var userId = principal.Identity.GetUserId();

		var groupOwnerRoleId =
			(await _dbContext.Roles.SingleAsync(r => r.Name.Equals(BaseRole.GroupOwner), cancellationToken))
			.Id;
		var group = businessModel.Map();
		group.CreatedById = principal.Identity.GetUserId();

		if (businessModel.ExternalTermId.HasValue)
		{
			var externalTerm =
				await _dbContext.FindEntityAsync<ExternalTerm>(businessModel.ExternalTermId.Value,
					cancellationToken: cancellationToken);
			group.ExternalTerm = externalTerm;
			group.StartDate = null;
			group.EndDate = null;
		}
		else
		{
			group.ExternalTermId = null;
			group.StartDate = businessModel.StartDate;
			group.EndDate = businessModel.EndDate;
		}

		_dbContext.Groups.Add(group);
		_dbContext.GroupUserRoles.Add(new GroupUserRole
		{
			Group = group,
			UserId = userId,
			RoleId = groupOwnerRoleId
		});
		_dbContext.GroupUserRoleLogs.Add(new GroupUserRoleLog
		{
			Group = group,
			UserId = userId,
			RoleId = groupOwnerRoleId,
			Type = GroupUserRoleLogType.Added
		});

		var (addedExternalGroups, removedExternalGroups) = businessModel.ExternalGroups != null
			? await UpdateExternalGroupsAsync(group, group.ExternalTermId, new List<ExternalGroup>(),
				businessModel.ExternalGroups.ToList(), principal, cancellationToken)
			: (new List<ExternalGroup>(), new List<ExternalGroup>());

		await _dbContext.SaveChangesAsync(cancellationToken);

		await AfterSaveAsync(group.Id, addedExternalGroups, removedExternalGroups, true, principal, cancellationToken);

		return await CreateBusinessModelAsync(group, userId, cancellationToken);
	}

	public async Task<TGroupBusinessModel> UpdateAsync(int groupId, TGroupEditBusinessModel businessModel, IPrincipal principal,
		CancellationToken cancellationToken = default, IDictionary<string, int> relationIds = null)
	{
		if (businessModel == null) throw new ArgumentNullException(nameof(businessModel));
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		await _authorizationService.AssertCanUpdateAsync(groupId, principal, cancellationToken);
		await _businessModelValidator.AssertIsValidAsync(businessModel, cancellationToken);

		// Finding the group whether or not it's deleted
		var existingGroup = await _dbContext.FindEntityAsync<TGroup>(groupId, false, cancellationToken: cancellationToken);

		var shouldSyncRoster = false;
		var existingExternalGroups = await _dbContext.ExternalGroups
			.Where(eg => eg.GroupId.Equals(groupId))
			.Include(eg => eg.ExternalProvider)
			.ToListAsync(cancellationToken);
		var existingUniTimeExternalGroups = existingExternalGroups
			.Where(e => e.ExternalProvider is UniTimeExternalProvider)
			.ToList();
		var existingLtiExternalGroups = existingExternalGroups
			.Where(e => e.ExternalProvider is LtiExternalProvider)
			.ToList();

		var incomingExternalGroups = businessModel.ExternalGroups.ToList();
		var addedExternalGroups = new List<ExternalGroup>();
		var removedExternalGroups = new List<ExternalGroup>();

		if (existingGroup.IsDeleted)
		{
			var hasChanges = businessModel.Name != existingGroup.Name ||
							businessModel.StartDate != existingGroup.StartDate ||
							businessModel.EndDate != existingGroup.EndDate ||
							businessModel.ExternalTermId != existingGroup.ExternalTermId ||
							// new external groups
							incomingExternalGroups.OfType<ExternalGroupEditBusinessModel>().Count() != incomingExternalGroups.Count ||
							// changes to existing external groups
							existingExternalGroups.Any() && incomingExternalGroups.OfType<ExternalGroupEditBusinessModel>().Count() !=
							existingExternalGroups.Count;

			// If trying to make changes or not trying to restore the group
			if (hasChanges || !businessModel.IsDeleted.HasValue)
				throw new ForbiddenException(string.Format(Strings.GroupCannotUpdateDeleted, groupId));

			// If trying to delete an already deleted group do nothing. PUT should be idempotent
			if (businessModel.IsDeleted.Value)
				return await CreateBusinessModelAsync(existingGroup, principal.Identity.GetUserId(), cancellationToken);

			// We got here because IsDeleted is specified and set to false. Restore group
			existingGroup.IsDeleted = false;
			shouldSyncRoster = existingExternalGroups.Any();
		}
		else if (businessModel.IsDeleted.HasValue && businessModel.IsDeleted.Value)
		{
			// If trying to delete a group, you are not allowed any other updates
			await _authorizationService.AssertCanDeleteAsync(groupId, principal, cancellationToken);
			await _dbContext.DeleteGroupAsync(existingGroup, cancellationToken);
		}
		else
		{
			// If updating ExternalTerm an has UniTime ExternalGroups
			if (existingGroup.ExternalTermId.HasValue &&
				businessModel.ExternalTermId != existingGroup.ExternalTermId && existingUniTimeExternalGroups.Any())
			{
				AssertCanRemoveExternalGroups(existingUniTimeExternalGroups);

				// remove UniTime ExternalGroups if changing/removing term
				_dbContext.ExternalGroups.RemoveRange(existingUniTimeExternalGroups);
				removedExternalGroups.AddRange(existingUniTimeExternalGroups);

				// exclude already removed groups from UpdateExternalGroupsAsync
				existingExternalGroups = existingExternalGroups.Except(existingUniTimeExternalGroups).ToList();

				shouldSyncRoster = true;
			}

			if (businessModel.ExternalTermId.HasValue)
			{
				// ensure externalTerm exists using FindEntityAsync
				var externalTerm =
					await _dbContext.FindEntityAsync<ExternalTerm>(businessModel.ExternalTermId.Value,
						cancellationToken: cancellationToken);
				existingGroup.ExternalTermId = externalTerm.Id;
				existingGroup.ExternalTerm = externalTerm;
				existingGroup.StartDate = null;
				existingGroup.EndDate = null;
			}
			else
			{
				existingGroup.ExternalTermId = null;
				existingGroup.StartDate = businessModel.StartDate;
				existingGroup.EndDate = businessModel.EndDate;
			}

			existingGroup.Name = businessModel.Name;

			// update ExternalGroups in the dbContext, but don't save yet
			var (updateAddedExternalGroups, updateRemovedExternalGroups) = await UpdateExternalGroupsAsync(existingGroup,
				existingGroup.ExternalTermId, existingExternalGroups, incomingExternalGroups, principal, cancellationToken);
			addedExternalGroups.AddRange(updateAddedExternalGroups);
			removedExternalGroups.AddRange(updateRemovedExternalGroups);
			if (addedExternalGroups.Any() || updateRemovedExternalGroups.Any())
			{
				shouldSyncRoster = true;
			}
		}

		await _dbContext.SaveChangesAsync(cancellationToken);

		await AfterSaveAsync(groupId, addedExternalGroups, removedExternalGroups, shouldSyncRoster, principal, cancellationToken);

		return await CreateBusinessModelAsync(existingGroup, principal.Identity.GetUserId(), cancellationToken);
	}

	public async Task DeleteAsync(int groupId, IPrincipal principal, CancellationToken cancellationToken = default,
		IDictionary<string, int> relationIds = null)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));
		await _authorizationService.AssertCanDeleteAsync(groupId, principal, cancellationToken);
		throw new NotImplementedException();
	}

	public async Task<IEnumerable<TGroupBusinessModel>> GetAsync(
		IPrincipal principal,
		string keywords = null,
		DateTime? date = null,
		bool queryAll = false,
		string userId = null,
		bool includeIfSoftDeleted = false,
		CancellationToken cancellationToken = default)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));
		var principalUserId = principal.Identity.GetUserId();
		if (queryAll || userId != null && userId != principalUserId)
			await _authorizationService.AssertCanReadAnyAsync(principal, cancellationToken);

		if (string.IsNullOrWhiteSpace(keywords) && date == null && queryAll)
			throw new ValidationException(Strings.GroupQueryAllRequiredArguments);

		var queryUserId = userId ?? principalUserId;
		var groupsQueryable = _dbContext.GetUserGroupsQueryable(queryAll, queryUserId, includeIfSoftDeleted);

		var keywordList = keywords?.Split(' ').Select(k => k.ToLower()).Distinct().ToList();
		if (keywordList != null && keywordList.Any())
		{
			// Find groups matching the keywords
			var matchingGroupsQueryable = keywordList.Aggregate(
				groupsQueryable,
				(current, k) =>
					current.Where(g =>
						g.Id.ToString().Contains(k) ||
						g.Name.ToLower().Contains(k) ||
						g.ExternalTermId != null &&
						g.ExternalTerm.Name.ToLower().Contains(k)
					)
			);

			// Find groups with external groups matching the keywords
			var externalGroupsMatchingKeywordsQueryable = keywordList.Aggregate(
				_dbContext.ExternalGroups.AsQueryable(),
				(current, k) =>
					current.Where(eg =>
						eg.ExternalId.ToLower().Contains(k) ||
						eg.Name.ToLower().Contains(k) ||
						eg.Description.ToLower().Contains(k)
					)
			);
			var matchingExternalGroupsQueryable = groupsQueryable
				.Join(
					externalGroupsMatchingKeywordsQueryable,
					g => g.Id,
					eg => eg.GroupId,
					(g, eg) => g);

			// Find groups that are owned by users matching the keywords
			var matchingOwnersGroupsQueryable = groupsQueryable
				.Join(_dbContext.GroupUserRoles, g => g.Id, ur => ur.GroupId,
					(g, ur) => new { Group = g, UserRole = ur })
				.Join(_dbContext.Roles.Where(r => r.Name.Equals(BaseRole.GroupOwner)), n => n.UserRole.RoleId, r => r.Id,
					(n, r) => new { n.Group, UserRoleUserId = n.UserRole.UserId })
				.Join(_dbContext.GetUsersMatchingKeywordsQueryable(_dbContext.Users, keywordList), x => x.UserRoleUserId, u => u.Id,
					(x, u) => x.Group);

			groupsQueryable = matchingGroupsQueryable
				.Union(matchingExternalGroupsQueryable)
				.Union(matchingOwnersGroupsQueryable);
		}

		if (date != null)
		{
			// date is assumed to be at midnight of the current user's timezone
			// filter the search based on the entire day
			var startDate = date.Value;
			var endDate = date.Value.AddDays(1);
			groupsQueryable = groupsQueryable
				.Where(g =>
					// within ExternalTerm date range
					g.ExternalTerm != null &&
					(startDate >= g.ExternalTerm.StartDate && startDate <= g.ExternalTerm.EndDate ||
					endDate >= g.ExternalTerm.StartDate && endDate <= g.ExternalTerm.EndDate) ||
					// within Group date range
					g.StartDate.HasValue && g.EndDate.HasValue &&
					(startDate >= g.StartDate && startDate <= g.EndDate ||
					endDate >= g.StartDate && endDate <= g.EndDate));
		}

		var groups = await groupsQueryable.ToListAsync(cancellationToken);
		var activitiesAndRolesByGroup =
			await _dbContext.UserEntityRolesAndActivitiesAsync<TGroup, GroupUserRole>(queryUserId, groupsQueryable, cancellationToken);
		var groupOwners = await GetOwnersAsync(groupsQueryable, cancellationToken);
		var externalGroups = await groupsQueryable
			.SelectMany(g => g.ExternalGroups)
			.ToListAsync(cancellationToken);
		var groupsWithRosterSync = await groupsQueryable
			.Where(g => g.ExternalGroups
				.Select(eg => eg.ExternalProvider)
				.Any(ep => ep.RosterSyncEnabled))
			.Select(g => g.Id)
			.ToListAsync(cancellationToken);
		// Note: does not use CreateBusinessModel method. More efficient this way
		return groups.Select(group =>
		{
			var (roles, activities) = activitiesAndRolesByGroup[group.Id];
			return new TGroupBusinessModel
			{
				Group = group,
				Roles = roles,
				Activities = activities,
				Owners = groupOwners.Where(go => go.EntityId.Equals(group.Id)),
				ExternalGroups = externalGroups.Where(eg => eg.GroupId.Equals(group.Id))
					.Select(eg => new ExternalGroupBusinessModel(eg)),
				IsRosterSyncEnabled = groupsWithRosterSync.Contains(group.Id)
			};
		});
	}

	public async Task<TGroupBusinessModel> CopyAsync(TGroupEditBusinessModel businessEditModel, int sourceGroupId, IPrincipal principal,
		CancellationToken cancellationToken = default)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		//can read the source group
		await _authorizationService.AssertCanReadAsync(sourceGroupId, principal, cancellationToken);

		//can update and add entity user roles in the source group - the user should be GroupOwner of the group to be able to copy
		await _authorizationService.AssertCanUpdateAsync(sourceGroupId, principal, cancellationToken);
		await _authorizationService.AssertCanModifyEntityUserRolesAsync(sourceGroupId, principal,
			cancellationToken);

		var groupBusinessModel = await CreateAsync(businessEditModel, principal, cancellationToken);

		//can update and add entity user roles in the target group - the user should be GroupOwner of the new group
		await _authorizationService.AssertCanUpdateAsync(groupBusinessModel.Group.Id, principal, cancellationToken);
		await _authorizationService.AssertCanModifyEntityUserRolesAsync(groupBusinessModel.Group.Id, principal,
			cancellationToken);

		await CopyOwnersAndGradersAsync(sourceGroupId, groupBusinessModel.Group.Id, principal, cancellationToken);

		return await CreateBusinessModelAsync(groupBusinessModel.Group, principal.Identity.GetUserId(), cancellationToken);
	}

	#region ExternalProviders + Roster Sync

	private static void AssertCanRemoveExternalGroups(List<ExternalGroup> externalGroups)
	{
		// cannot remove LTI ExternalGroups
		if (externalGroups.Any(eg => eg.ExternalProvider is LtiExternalProvider))
			throw new ForbiddenException(Strings.ExternalGroupsCannotRemoveIfLtiExternalProvider);
	}

	public async Task<IEnumerable<ExternalGroupBusinessModel>> GetExternalGroupsAsync(int groupId, IPrincipal principal,
		CancellationToken cancellationToken = default)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		await _authorizationService.AssertCanReadExternalGroupsAsync(groupId, principal, cancellationToken);
		var externalGroups = await _dbContext.ExternalGroups.Where(eg => eg.GroupId.Equals(groupId)).ToListAsync(cancellationToken);
		return externalGroups.Select(eg => new ExternalGroupBusinessModel(eg));
	}

	private async Task<(List<ExternalGroup> addedGroups, List<ExternalGroup> removedGroups)> UpdateExternalGroupsAsync(TGroup group,
		int? externalTermId, List<ExternalGroup> existingExternalGroups, List<IExternalGroupEditBusinessModel> externalGroups,
		IPrincipal principal, CancellationToken cancellationToken = default)
	{
		var addedExternalGroups = new List<ExternalGroup>();
		var removedExternalGroups = new List<ExternalGroup>();

		// no changes, just return
		if (!existingExternalGroups.Any() && !externalGroups.Any())
			return (addedExternalGroups, removedExternalGroups);

		var uniTimeExternalGroupsToAdd = await _uniTimeService.GetExternalGroupsToAddAsync(
			group,
			externalTermId,
			existingExternalGroups,
			externalGroups,
			cancellationToken);
		var (ltiExternalGroupsToAdd, _) = await _ltiLaunchService.GetExternalGroupsToAddAsync(
			group,
			existingExternalGroups,
			externalGroups,
			cancellationToken);
		var externalGroupsToKeep = externalGroups
			.OfType<ExternalGroupEditBusinessModel>()
			.ToList();
		var externalGroupsToUpdate = existingExternalGroups
			.Where(e => externalGroupsToKeep.Any(egk => egk.Id.Equals(e.Id) && e.IsAutoGradePushEnabled != egk.IsAutoGradePushEnabled))
			.Select(e => new { Existing = e, Incoming = externalGroupsToKeep.First(egk => egk.Id.Equals(e.Id)) })
			.ToList();
		var externalGroupsToRemove = existingExternalGroups
			.Where(e => !externalGroupsToKeep.Any(egk => egk.Id.Equals(e.Id)))
			.ToList();

		var groupIsEnded = group.IsEnded<TGroup, ExternalTerm, GroupUserRole, ExternalGroup>(_dateTimeProvider.UtcNow);
		if (groupIsEnded && (uniTimeExternalGroupsToAdd.Any() || ltiExternalGroupsToAdd.Any() || externalGroupsToUpdate.Any() ||
							externalGroupsToRemove.Any()))
		{
			throw new ForbiddenException(Strings.EndedGroupCannotChangeExternalGroups);
		}

		// add new ExternalGroups from UniTimeGroups, if any
		if (uniTimeExternalGroupsToAdd.Any())
		{
			await _authorizationService.AssertCanConnectAnyExternalGroupAsync(principal, cancellationToken);

			_dbContext.ExternalGroups.AddRange(uniTimeExternalGroupsToAdd);
			addedExternalGroups.AddRange(uniTimeExternalGroupsToAdd);
		}

		// add new ExternalGroups from LtiLaunches, if any
		if (ltiExternalGroupsToAdd.Any())
		{
			await _authorizationService.AssertCanConnectAnyExternalGroupAsync(principal, cancellationToken);

			_dbContext.ExternalGroups.AddRange(ltiExternalGroupsToAdd);
			addedExternalGroups.AddRange(ltiExternalGroupsToAdd);
		}

		// update existing ExternalGroups
		if (externalGroupsToUpdate.Any())
		{
			await _authorizationService.AssertCanConnectAnyExternalGroupAsync(principal, cancellationToken);

			if (externalGroupsToUpdate.Any(externalGroupPair =>
					// IsAutoGradePushEnabled has value when it should not
					externalGroupPair.Incoming.IsAutoGradePushEnabled.HasValue &&
					(!externalGroupPair.Existing.ExternalProvider.GradePushEnabled ||
					externalGroupPair.Existing.GradesUrl == null ||
					externalGroupPair.Existing.GradesUrl.Trim() == string.Empty) ||
					// IsAutoGradePushEnabled has no value when it should
					!externalGroupPair.Incoming.IsAutoGradePushEnabled.HasValue &&
					externalGroupPair.Existing.ExternalProvider.GradePushEnabled &&
					externalGroupPair.Existing.GradesUrl != null &&
					externalGroupPair.Existing.GradesUrl.Trim() != string.Empty))
				throw new ValidationException(Strings.ExternalGroupsUpdateInvalidAutoGradePushEnabled);

			foreach (var externalGroupPair in externalGroupsToUpdate)
			{
				externalGroupPair.Existing.IsAutoGradePushEnabled = externalGroupPair.Incoming.IsAutoGradePushEnabled;
			}
		}

		// remove existing ExternalGroups
		if (externalGroupsToRemove.Any())
		{
			await _authorizationService.AssertCanConnectAnyExternalGroupAsync(principal, cancellationToken);

			AssertCanRemoveExternalGroups(externalGroupsToRemove);
			_dbContext.ExternalGroups.RemoveRange(externalGroupsToRemove);
			removedExternalGroups.AddRange(externalGroupsToRemove);
		}

		return (addedExternalGroups, removedExternalGroups);
	}

	public async Task SyncRosterAsync(int groupId, IPrincipal principal, CancellationToken cancellationToken = default)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		await _authorizationService.AssertCanSyncRosterAsync(groupId, principal, cancellationToken);
		var shardKey = _shardKeyProvider.GetShardKey();

		await _syncGroupRosterQueueManager.AddMessageAsync(
			new SyncGroupRosterMessage
			{
				ShardKey = shardKey,
				GroupId = groupId
			}, cancellationToken: cancellationToken);
	}

	public async Task SyncAllRostersAsync(IPrincipal principal,
		CancellationToken cancellationToken = default)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		await _authorizationService.AssertCanSyncAllRostersAsync(principal, cancellationToken);
		var shardKey = _shardKeyProvider.GetShardKey();

		await _syncAllRostersQueueManager.AddMessageAsync(
			new SyncAllRostersMessage
			{
				ShardKey = shardKey
			}, cancellationToken: cancellationToken);
	}

	#endregion ExternalProviders + Roster Sync

	#region Private Methods

	private async Task<IEnumerable<EntityUserBusinessModel<TUser>>> GetOwnersAsync(IQueryable<TGroup> groupQueryable,
		CancellationToken cancellationToken = default)
	{
		return await groupQueryable.SelectMany(g => g.GroupUserRoles)
			.Where(gur => gur.Role.Name.Equals(BaseRole.GroupOwner))
			.Join(_dbContext.Users, gur => gur.UserId, u => u.Id,
				(gur, u) => new EntityUserBusinessModel<TUser>
				{
					User = u,
					EntityId = gur.GroupId
				})
			.ToListAsync(cancellationToken);
	}

	private async Task<IEnumerable<EntityUserBusinessModel<TUser>>> GetGradersAsync(IQueryable<TGroup> groupQueryable,
		CancellationToken cancellationToken = default)
	{
		return await groupQueryable.SelectMany(g => g.GroupUserRoles)
			.Where(gur => gur.Role.Name.Equals(BaseRole.GroupGrader))
			.Join(_dbContext.Users, gur => gur.UserId, u => u.Id,
				(gur, u) => new EntityUserBusinessModel<TUser>
				{
					User = u,
					EntityId = gur.GroupId
				})
			.ToListAsync(cancellationToken);
	}

	private async Task<TGroupBusinessModel> CreateBusinessModelAsync(TGroup group, string userId,
		CancellationToken cancellationToken = default)
	{
		var (userGroupRoles, userGroupActivities) =
			await _dbContext.UserEntityRolesAndActivitiesAsync<BaseGroup, GroupUserRole>(userId, group, cancellationToken);
		var groupOwners = await GetOwnersAsync(_dbContext.Groups.Where(g => g.Id.Equals(group.Id)), cancellationToken);
		var groupGraders = await GetGradersAsync(_dbContext.Groups.Where(g => g.Id.Equals(group.Id)), cancellationToken);
		var isRosterSyncEnabled = await _dbContext.ExternalGroups
			.Where(eg => eg.GroupId.Equals(group.Id))
			.Select(eg => eg.ExternalProvider)
			.AnyAsync(ep => ep.RosterSyncEnabled, cancellationToken);
		var externalGroups = await _dbContext.ExternalGroups
			.Where(eg => eg.GroupId.Equals(group.Id))
			.ToListAsync(cancellationToken);
		var businessModel = new TGroupBusinessModel
		{
			Group = group,
			IsRosterSyncEnabled = isRosterSyncEnabled,
			Roles = userGroupRoles,
			Activities = userGroupActivities,
			Owners = groupOwners,
			Graders = groupGraders,
			ExternalGroups = externalGroups.Select(e => new ExternalGroupBusinessModel(e))
		};
		return businessModel;
	}

	private async Task CopyOwnersAndGradersAsync(int sourceGroupId, int targetGroupId, IPrincipal principal,
		CancellationToken cancellationToken = default)
	{
		var userId = principal.Identity.GetUserId();
		var sourceGroupQueryable = _dbContext.Groups.Where(g => g.Id.Equals(sourceGroupId));
		var groupOwnerRole = (await _dbContext.Roles.SingleAsync(r => r.Name.Equals(BaseRole.GroupOwner), cancellationToken));
		var groupGraderRole = (await _dbContext.Roles.SingleAsync(r => r.Name.Equals(BaseRole.GroupGrader), cancellationToken));

		var owners = await GetOwnersAsync(sourceGroupQueryable, cancellationToken);
		var graders = await GetGradersAsync(sourceGroupQueryable, cancellationToken);

		//don't copy the user who created the group
		var ownersEmail = owners.Where(o => !o.User.Id.Equals(userId)).Select(s => s.User.Email).ToList();
		var gradersEmail = graders.Where(o => !o.User.Id.Equals(userId)).Select(s => s.User.Email).ToList();

		if (ownersEmail.Any())
			await _baseGroupUserRoleService.CreateEntityUserRolesAsync(targetGroupId, ownersEmail, groupOwnerRole.Name, principal,
				cancellationToken);
		if (gradersEmail.Any())
			await _baseGroupUserRoleService.CreateEntityUserRolesAsync(targetGroupId, gradersEmail, groupGraderRole.Name, principal,
				cancellationToken);
	}

	private async Task AfterSaveAsync(
		int groupId,
		IReadOnlyCollection<ExternalGroup> addedExternalGroups,
		IReadOnlyCollection<ExternalGroup> removedExternalGroups,
		bool shouldSyncRoster,
		IPrincipal principal,
		CancellationToken cancellationToken = default)
	{
		if (addedExternalGroups.Any())
		{
			await NotifyUsersAsync(groupId, addedExternalGroups, false, principal, cancellationToken);
		}

		if (removedExternalGroups.Any())
		{
			await NotifyUsersAsync(groupId, removedExternalGroups, true, principal, cancellationToken);
		}

		if (shouldSyncRoster)
		{
			await _syncGroupRosterQueueManager.AddMessageAsync(new SyncGroupRosterMessage
			{
				ShardKey = _shardKeyProvider.GetShardKey(),
				GroupId = groupId
			}, cancellationToken: cancellationToken);
		}
	}

	private async Task NotifyUsersAsync(int groupId, IEnumerable<ExternalGroup> externalGroups, bool isRemoving, IPrincipal user,
		CancellationToken cancellationToken)
	{
		// Notifications are not configured
		if (_notificationService == null) return;

		var userId = user.Identity.GetUserId();
		var updatedByUser = await _dbContext.Users.SingleAsync(u => u.Id == userId, cancellationToken);
		await _notificationService.NotifyGroupAsync<ExternalGroupNotificationTemplateData<TGroup>>(groupId,
			isRemoving ? BaseNotificationEvent.ExternalGroupRemoved : BaseNotificationEvent.ExternalGroupAdded,
			new SystemPrincipal(),
			td =>
			{
				td.ExternalGroups = externalGroups.ToList();
				td.UpdatedByUser = updatedByUser;
				return Task.FromResult(td);
			}, null, cancellationToken);
	}

	#endregion Private Methods
}