﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;
using StudioKit.ErrorHandling.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using System;
using System.Net;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services.OAuth;

public class OAuthHandler : AuthenticationHandler<OAuthSchemeOptions>
{
	private readonly IOAuthService _oauthService;
	private readonly ProblemDetailsFactory _problemDetailsFactory;
	private readonly IErrorHandler _errorHandler;

	private const string BearerPrefix = "Bearer ";

	public OAuthHandler(
		IOptionsMonitor<OAuthSchemeOptions> options,
		ILoggerFactory logger,
		UrlEncoder encoder,
		IOAuthService oauthService,
		ProblemDetailsFactory problemDetailsFactory,
		IErrorHandler errorHandler)
		: base(options, logger, encoder)
	{
		_oauthService = oauthService ?? throw new ArgumentNullException(nameof(oauthService));
		_problemDetailsFactory = problemDetailsFactory ?? throw new ArgumentNullException(nameof(problemDetailsFactory));
		_errorHandler = errorHandler ?? throw new ArgumentNullException(nameof(errorHandler));
	}

	protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
	{
		if (!Request.Headers.TryGetValue(HeaderNames.Authorization, out var value))
		{
			Logger.LogDebug("No Authorization Header");
			return AuthenticateResult.NoResult();
		}

		string authorizationHeader = value;
		if (string.IsNullOrEmpty(authorizationHeader) ||
			!authorizationHeader.StartsWith(BearerPrefix, StringComparison.OrdinalIgnoreCase))
		{
			Logger.LogDebug("Empty or Invalid Authorization Header");
			return AuthenticateResult.NoResult();
		}

		var token = authorizationHeader.Substring(BearerPrefix.Length).Trim();
		if (string.IsNullOrEmpty(token))
		{
			Logger.LogDebug("No Token Provided");
			return AuthenticateResult.Fail(HttpStatusCode.Unauthorized.ToString());
		}

		try
		{
			var ticket = await _oauthService.ValidateTokenAsync(token);
			return AuthenticateResult.Success(ticket);
		}
		catch (Exception e)
		{
			_errorHandler.CaptureException(e);
			return AuthenticateResult.Fail(e.Message);
		}
	}

	protected override async Task HandleChallengeAsync(AuthenticationProperties properties)
	{
		Response.StatusCode = StatusCodes.Status401Unauthorized;
		Response.Headers.Append(HeaderNames.WWWAuthenticate, BearerPrefix.Trim());
		var problemDetails = _problemDetailsFactory.CreateProblemDetails(
			Response.HttpContext,
			Response.StatusCode);
		await Response.WriteAsJsonAsync(problemDetails);
	}

	protected override async Task HandleForbiddenAsync(AuthenticationProperties properties)
	{
		Response.StatusCode = StatusCodes.Status403Forbidden;
		var problemDetails = _problemDetailsFactory.CreateProblemDetails(
			Response.HttpContext,
			Response.StatusCode);
		await Response.WriteAsJsonAsync(problemDetails);
	}
}