﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ErrorHandling.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.DataAccess;
using StudioKit.Scaffolding.Models;
using StudioKit.Security.Common;
using StudioKit.Security.Entity.Identity.Models;
using StudioKit.Security.Models;
using StudioKit.Sharding;
using StudioKit.Sharding.Interfaces;
using StudioKit.Utilities.Extensions;
using StudioKit.Utilities.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services.OAuth;

public class OAuthService<TContext, TUser, TGroup, TConfiguration> : IOAuthService
	where TContext : BaseDbContext<TUser, TGroup, TConfiguration>
	where TUser : IdentityUser, IUser, new()
	where TGroup : BaseGroup, new()
	where TConfiguration : BaseConfiguration, new()
{
	private readonly TContext _dbContext;
	private readonly IDateTimeProvider _dateTimeProvider;
	private readonly IDataProtector _protector;
	private readonly IShardKeyProvider _shardKeyProvider;
	private readonly TicketSerializer _serializer;
	private readonly ILogger<OAuthService<TContext, TUser, TGroup, TConfiguration>> _logger;
	private readonly IErrorHandler _errorHandler;

	private const int AuthorizationCodeExpirationMinutes = 1;
	private const int AccessTokenExpirationHours = 2;
	private const int RefreshTokenExpirationMonths = 3;
	private const string DefaultClientId = "web";
	private const string IdentityProviderClaimType = "http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider";

	public OAuthService(
		TContext dbContext,
		IDateTimeProvider dateTimeProvider,
		IDataProtectionProvider dataProtectionProvider,
		IShardKeyProvider shardKeyProvider,
		ILogger<OAuthService<TContext, TUser, TGroup, TConfiguration>> logger,
		IErrorHandler errorHandler)
	{
		_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
		_dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
		_protector = dataProtectionProvider?.CreateProtector(OAuthSchemeConstants.Name) ??
					throw new ArgumentNullException(nameof(dataProtectionProvider));
		_shardKeyProvider = shardKeyProvider ?? throw new ArgumentNullException(nameof(shardKeyProvider));
		_serializer = new TicketSerializer();
		_logger = logger ?? throw new ArgumentNullException(nameof(logger));
		_errorHandler = errorHandler ?? throw new ArgumentNullException(nameof(errorHandler));
	}

	public string GetAuthorizationCode(IUser user, List<Claim> claims)
	{
		var tokenClaims = new List<Claim>
		{
			new(ClaimTypes.NameIdentifier, user.Id, ClaimValueTypes.String),
			new(ClaimTypes.Name, user.UserName, ClaimValueTypes.String),
			new(IdentityProviderClaimType, OAuthSchemeConstants.Name, ClaimValueTypes.String)
		};

		// Add the ImpersonatorUserId claim to the token if it exists
		var impersonatorUserIdClaim =
			claims.SingleOrDefault(c => c.Type.Equals(ScaffoldingClaimTypes.ImpersonatorUserId));
		if (impersonatorUserIdClaim != null)
			tokenClaims.Add(impersonatorUserIdClaim);

		// Add the LockDownBrowser claim to the token if it exists
		var lockDownBrowserEntityIdClaim =
			claims.SingleOrDefault(c => c.Type.Equals(ScaffoldingClaimTypes.LockDownBrowserEntityId));
		if (lockDownBrowserEntityIdClaim != null)
			tokenClaims.Add(lockDownBrowserEntityIdClaim);

		var identity = new ClaimsIdentity(tokenClaims, OAuthSchemeConstants.Name, ClaimTypes.Name, ClaimTypes.Role);
		var principal = new ClaimsPrincipal(identity);
		var ticket = new AuthenticationTicket(principal, OAuthSchemeConstants.Name);
		ticket.Properties.ExpiresUtc = _dateTimeProvider.UtcNow.AddMinutes(AuthorizationCodeExpirationMinutes);
		ticket.Properties.Items.Add(OAuthRequestKey.ClientId, DefaultClientId);

		return EncodeTicket(ticket);
	}

	public async Task<Dictionary<string, string>> GetTokenAsync(Dictionary<string, string> formData, string userAgent,
		string ipAddress = null, CancellationToken cancellationToken = default)
	{
		var allowedGrantTypes = new[] { OAuthGrantType.ClientCredentials, OAuthGrantType.AuthorizationCode, OAuthGrantType.RefreshToken };
		var grantType = formData.ContainsKey(OAuthRequestKey.GrantType) ? formData[OAuthRequestKey.GrantType] : null;
		if (string.IsNullOrWhiteSpace(grantType) || !allowedGrantTypes.Contains(grantType))
		{
			_logger.LogDebug($"{OAuthErrorResponse.UnsupportedGrantType}: {grantType}");
			throw new BadRequestException(OAuthErrorResponse.UnsupportedGrantType);
		}

		var clientId = formData.ContainsKey(OAuthRequestKey.ClientId) ? formData[OAuthRequestKey.ClientId] : null;
		var clientSecret = formData.ContainsKey(OAuthRequestKey.ClientSecret) ? formData[OAuthRequestKey.ClientSecret] : null;
		var client = await ValidateClientAsync(clientId, clientSecret, grantType, cancellationToken);

		AuthenticationTicket ticket = null;
		if (grantType == OAuthGrantType.ClientCredentials)
		{
			ticket = await CreateClientCredentialsTicketAsync(client, cancellationToken);
		}
		else if (grantType == OAuthGrantType.AuthorizationCode)
		{
			var code = formData.ContainsKey(OAuthRequestKey.Code) ? formData[OAuthRequestKey.Code] : null;
			ticket = await ValidateAuthorizationCodeAsync(code, clientId, cancellationToken);
		}
		else if (grantType == OAuthGrantType.RefreshToken)
		{
			var refreshToken = formData.ContainsKey(OAuthRequestKey.RefreshToken) ? formData[OAuthRequestKey.RefreshToken] : null;
			ticket = await ValidateRefreshTokenAsync(refreshToken, clientId, cancellationToken);
		}

		if (ticket == null)
		{
			_logger.LogDebug($"{OAuthErrorResponse.InvalidGrant}: ticket could not be created or validated");
			throw new BadRequestException(OAuthErrorResponse.InvalidGrant);
		}

		var accessTokenIssuedUtc = _dateTimeProvider.UtcNow;
		var accessTokenExpiresUtc = accessTokenIssuedUtc.AddHours(AccessTokenExpirationHours);
		var accessTokenExpiresIn = (accessTokenExpiresUtc - accessTokenIssuedUtc).TotalSeconds;

		var tokenTicket = new AuthenticationTicket(ticket.Principal, ticket.Properties, OAuthSchemeConstants.Name);
		tokenTicket.Properties.IssuedUtc = accessTokenIssuedUtc;
		tokenTicket.Properties.ExpiresUtc = accessTokenExpiresUtc;

		var accessToken = EncodeTicket(tokenTicket);
		var newRefreshToken = client.Grants.Contains(OAuthGrantType.RefreshToken)
			? await CreateRefreshTokenAsync(tokenTicket, cancellationToken)
			: null;

		if (grantType == OAuthGrantType.AuthorizationCode)
		{
			// Track Login
			var login = new Login
			{
				UserId = ticket.Principal.Identity.GetUserId(),
				UserAgent = userAgent,
				IpAddress = ipAddress
			};

			var impersonatorUserIdClaim = ticket.Principal.Claims
				.SingleOrDefault(c => c.Type.Equals(ScaffoldingClaimTypes.ImpersonatorUserId));
			if (impersonatorUserIdClaim != null)
				login.LastUpdatedById = impersonatorUserIdClaim.Value;

			_dbContext.Logins.Add(login);
			await _dbContext.SaveChangesAsync(cancellationToken);
		}

		var newTokenDictionary = new Dictionary<string, string>
		{
			{ "access_token", accessToken },
			{ "token_type", "bearer" },
			{ "expires_in ", accessTokenExpiresIn.ToString(CultureInfo.InvariantCulture) },
			{ ".expires", accessTokenExpiresUtc.ToString("o") },
			{ "client_id", clientId },
			{ ".issued", accessTokenIssuedUtc.ToString("o") }
		};

		if (newRefreshToken != null)
		{
			newTokenDictionary["refresh_token"] = newRefreshToken;
		}

		return newTokenDictionary;
	}

	public async Task<AuthenticationTicket> ValidateTokenAsync(string token, CancellationToken cancellationToken = default)
	{
		var tokenTicket = DecodeTicket(token);
		if (tokenTicket == null)
		{
			_logger.LogDebug("Unauthorized: ticket is null");
			throw new UnauthorizedException();
		}

		// do not allow access on invalid shards
		var shardKey = _shardKeyProvider.GetShardKey();
		if (shardKey == ShardDomainConstants.InvalidShardKey)
		{
			_logger.LogDebug($"Unauthorized: invalid shard key {shardKey}");
			throw new UnauthorizedException();
		}

		// validate the ticket is not expired
		if (tokenTicket.Properties.ExpiresUtc <= _dateTimeProvider.UtcNow)
		{
			_logger.LogDebug($"Unauthorized: ticket is expired {tokenTicket.Properties.ExpiresUtc}");
			throw new UnauthorizedException();
		}

		// validate the claims issuer
		if (tokenTicket.Principal.Claims.All(c => c.Issuer != ClaimsIdentity.DefaultIssuer))
		{
			_logger.LogDebug("Unauthorized: ticket has claims from an invalid issuer");
			throw new UnauthorizedException();
		}

		// validate the identity provider
		if (!tokenTicket.Principal.Claims.Any(c => c.Type == IdentityProviderClaimType && c.Value == OAuthSchemeConstants.Name))
		{
			_logger.LogDebug("Unauthorized: ticket has claims from an invalid identity provider");
			throw new UnauthorizedException();
		}

		// validate that the principal exists in the database
		var principalId = tokenTicket.Principal.Identity.GetUserId();
		var principalExists =
			await _dbContext.Users.AnyAsync(u => u.Id.Equals(principalId), cancellationToken) ||
			await _dbContext.ServiceClients.AnyAsync(c => c.ClientId.Equals(principalId), cancellationToken);
		if (!principalExists)
		{
			_logger.LogDebug($"Unauthorized: ticket's principal does not exist in the db (principalId: {principalId})");
			throw new UnauthorizedException();
		}

		return tokenTicket;
	}

	#region Private Methods

	#region Tickets

	private byte[] SerializeAndEncryptTicket(AuthenticationTicket ticket)
	{
		var serializedTicket = _serializer.Serialize(ticket);
		var encryptedTicket = _protector.Protect(serializedTicket);
		return encryptedTicket;
	}

	private string EncodeTicket(AuthenticationTicket ticket)
	{
		return Convert.ToBase64String(SerializeAndEncryptTicket(ticket));
	}

	private AuthenticationTicket DecryptAndDeserializeTicket(byte[] encryptedTicket)
	{
		try
		{
			var serializedTicket = _protector.Unprotect(encryptedTicket);
			var ticket = _serializer.Deserialize(serializedTicket);
			_logger.LogDebug($"ticket: {ticket}");
			return ticket;
		}
		catch (CryptographicException e)
		{
			_logger.LogDebug("could not decrypt or deserialize ticket");
			_errorHandler.CaptureException(e);
			// could not decrypt the token, e.g. it did not use the current DataProtection Cert
			return null;
		}
	}

	private AuthenticationTicket DecodeTicket(string encodedTicket)
	{
		var encryptedTicket = Convert.FromBase64String(encodedTicket);
		return DecryptAndDeserializeTicket(encryptedTicket);
	}

	#endregion Tickets

	private async Task<Client> ValidateClientAsync(string clientId, string clientSecret, string grantType,
		CancellationToken cancellationToken)
	{
		if (string.IsNullOrWhiteSpace(clientId) || string.IsNullOrWhiteSpace(clientSecret))
		{
			_logger.LogDebug(
				$"{OAuthErrorResponse.InvalidClient}: clientId is null? {string.IsNullOrWhiteSpace(clientId)}, clientSecret is null? {string.IsNullOrWhiteSpace(clientSecret)}");
			throw new BadRequestException(OAuthErrorResponse.InvalidClient);
		}

		var client = await _dbContext.Clients.SingleOrDefaultAsync(c =>
			c.ClientId == clientId &&
			c.Secret == clientSecret, cancellationToken);

		if (client == null)
		{
			_logger.LogDebug($"{OAuthErrorResponse.InvalidClient}: could not load client from db");
			throw new BadRequestException(OAuthErrorResponse.InvalidClient);
		}

		if (!client.Grants.Contains(grantType))
		{
			_logger.LogDebug($"{OAuthErrorResponse.UnauthorizedClient}: unsupported client grantType {grantType}");
			throw new BadRequestException(OAuthErrorResponse.UnauthorizedClient);
		}

		return client;
	}

	private async Task<AuthenticationTicket> CreateClientCredentialsTicketAsync(Client client,
		CancellationToken cancellationToken = default)
	{
		if (client is IdentityServiceClient serviceClient)
		{
			var identity = new ClaimsIdentity(new[]
				{
					new Claim(ClaimTypes.NameIdentifier, serviceClient.ClientId),
					new(IdentityProviderClaimType, OAuthSchemeConstants.Name, ClaimValueTypes.String)
				},
				OAuthSchemeConstants.Name);
			var roles = await _dbContext.ServiceClientRoles
				.Where(r => r.ClientId.Equals(serviceClient.ClientId))
				.Select(r => r.Role)
				.ToListAsync(cancellationToken);
			foreach (var role in roles)
			{
				identity.AddClaim(new Claim(ClaimTypes.Role, role.Name));
			}

			return new AuthenticationTicket(new ClaimsPrincipal(identity), OAuthSchemeConstants.Name);
		}

		_logger.LogDebug($"{OAuthErrorResponse.InvalidClient}: client is not an IdentityServiceClient");
		throw new BadRequestException(OAuthErrorResponse.InvalidClient);
	}

	private async Task<AuthenticationTicket> ValidateAuthorizationCodeAsync(string code, string clientId,
		CancellationToken cancellationToken)
	{
		if (string.IsNullOrWhiteSpace(code))
		{
			_logger.LogDebug($"{OAuthErrorResponse.InvalidGrant}: code is null");
			throw new BadRequestException(OAuthErrorResponse.InvalidGrant);
		}

		var codeTicket = DecodeTicket(code);
		await ValidateTicketAsync(codeTicket, clientId, cancellationToken);

		return codeTicket;
	}

	private async Task<string> CreateRefreshTokenAsync(AuthenticationTicket accessTokenTicket, CancellationToken cancellationToken)
	{
		var expiresUtc = _dateTimeProvider.UtcNow.AddMonths(RefreshTokenExpirationMonths);

		// use same principal and properties as the accessTokenTicket
		var refreshTokenTicket =
			new AuthenticationTicket(accessTokenTicket.Principal, accessTokenTicket.Properties, OAuthSchemeConstants.Name);
		refreshTokenTicket.Properties.IssuedUtc = _dateTimeProvider.UtcNow;
		refreshTokenTicket.Properties.ExpiresUtc = expiresUtc;

		// create unique key
		var guid = Guid.NewGuid().ToString();

		// store refresh token in the database
		_dbContext.RefreshTokens.Add(new RefreshToken
		{
			Token = guid,
			AuthenticationTicket = SerializeAndEncryptTicket(refreshTokenTicket),
			AuthenticationTicketExpiration = expiresUtc
		});
		await _dbContext.SaveChangesAsync(cancellationToken);

		return guid;
	}

	private async Task<AuthenticationTicket> ValidateRefreshTokenAsync(string refreshToken, string clientId,
		CancellationToken cancellationToken)
	{
		if (string.IsNullOrWhiteSpace(refreshToken))
		{
			_logger.LogDebug($"{OAuthErrorResponse.InvalidGrant}: refreshToken is null");
			throw new BadRequestException(OAuthErrorResponse.InvalidGrant);
		}

		// load refreshToken from database
		var storedRefreshToken = await _dbContext.RefreshTokens.SingleOrDefaultAsync(r => r.Token == refreshToken, cancellationToken);
		if (storedRefreshToken == null)
		{
			_logger.LogDebug($"{OAuthErrorResponse.InvalidGrant}: could not load refreshToken from db");
			throw new BadRequestException(OAuthErrorResponse.InvalidGrant);
		}

		var refreshTokenTicket = DecryptAndDeserializeTicket(storedRefreshToken.AuthenticationTicket);
		await ValidateTicketAsync(refreshTokenTicket, clientId, cancellationToken);

		// delete refreshToken now that it has been used
		_dbContext.RefreshTokens.Remove(storedRefreshToken);
		// delete any expired refreshTokens
		// This seems to be a good trade-off between the RefreshToken table bloat and having to run a cleanup daemon
		_dbContext.RemoveRange(_dbContext.RefreshTokens.Where(r => r.AuthenticationTicketExpiration <= _dateTimeProvider.UtcNow));
		await _dbContext.SaveChangesAsync(cancellationToken);

		return refreshTokenTicket;
	}

	private async Task ValidateTicketAsync(AuthenticationTicket ticket, string clientId, CancellationToken cancellationToken)
	{
		if (ticket == null)
		{
			_logger.LogDebug($"{OAuthErrorResponse.InvalidGrant}: ticket is null");
			throw new BadRequestException(OAuthErrorResponse.InvalidGrant);
		}

		// validate that "client_id" in the ticket matches the current request
		if (!ticket.Properties.Items.ContainsKey(OAuthRequestKey.ClientId) ||
			ticket.Properties.Items[OAuthRequestKey.ClientId] != clientId)
		{
			_logger.LogDebug($"{OAuthErrorResponse.InvalidGrant}: ticket's clientId does not match request's clientId");
			throw new BadRequestException(OAuthErrorResponse.InvalidGrant);
		}

		// validate the ticket is not expired
		if (ticket.Properties.ExpiresUtc <= _dateTimeProvider.UtcNow)
		{
			_logger.LogDebug($"{OAuthErrorResponse.InvalidGrant}: ticket is expired ({ticket.Properties.ExpiresUtc})");
			throw new BadRequestException(OAuthErrorResponse.InvalidGrant);
		}

		// validate that the principal exists
		var userId = ticket.Principal.Identity.GetUserId();
		if (!await _dbContext.Users.AnyAsync(u => u.Id == userId, cancellationToken))
		{
			_logger.LogDebug($"{OAuthErrorResponse.InvalidGrant}: ticket's userId could not be loaded from db ({userId})");
			throw new BadRequestException(OAuthErrorResponse.InvalidGrant);
		}
	}

	#endregion Private Methods
}