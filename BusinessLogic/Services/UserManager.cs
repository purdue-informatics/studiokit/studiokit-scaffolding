﻿using FluentValidation;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using StudioKit.Data.Entity.Identity;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.DataAccess;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services;

/// <summary>
/// The application user manager used in this application. UserManager is defined in AspNetCore.Identity and is used by the application.
/// </summary>
public class UserManager<TContext, TUser, TGroup, TConfiguration> : UserManager<TUser>, IUserManager<TUser>
	where TContext : BaseDbContext<TUser, TGroup, TConfiguration>
	where TUser : IdentityUser, IUser, new()
	where TGroup : BaseGroup, new()
	where TConfiguration : BaseConfiguration, new()
{
	private readonly TContext _dbContext;
	private readonly IInstructorSandboxService<TUser> _instructorSandboxService;
	private readonly IUserSetupService<TUser> _userSetupService;

	public UserManager(IUserStore<TUser> store,
		IOptions<IdentityOptions> optionsAccessor,
		IPasswordHasher<TUser> passwordHasher,
		IEnumerable<IUserValidator<TUser>> userValidators,
		IEnumerable<IPasswordValidator<TUser>> passwordValidators,
		ILookupNormalizer keyNormalizer,
		IdentityErrorDescriber errors,
		IServiceProvider services,
		ILogger<UserManager<TContext, TUser, TGroup, TConfiguration>> logger,
		TContext dbContext,
		IInstructorSandboxService<TUser> instructorSandboxService,
		IUserSetupService<TUser> userSetupService) : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators,
		keyNormalizer, errors, services, logger)
	{
		_dbContext = dbContext;
		_instructorSandboxService = instructorSandboxService;
		_userSetupService = userSetupService;
	}

	public override async Task<TUser> FindByEmailAsync(string email)
	{
		return await Users.FirstOrDefaultAsync(x => x.NormalizedEmail == NormalizeEmail(email));
	}

	#region Authentication

	private async Task AddUserToRolesAsync(TUser user, IEnumerable<Claim> roleClaims)
	{
		var userRoles = await GetRolesAsync(user);
		await AddToRolesAsync(user, roleClaims.Select(r => r.Value).Where(r => !userRoles.Contains(r)).ToArray());
	}

	private async Task StoreClaimsAsync(TUser user, List<Claim> claims, CancellationToken cancellationToken = default)
	{
		var currentClaims = await _dbContext.IdentityUserClaims.Where(c => c.UserId == user.Id).ToListAsync(cancellationToken);
		// remove current claims that have a match (type and issuer) in the new set
		var claimsToRemove = currentClaims.Where(cc => claims.Any(c => c.Type == cc.ClaimType && c.Issuer == cc.ClaimIssuer)).ToList();
		// add new claims that do not have a match in remaining current claims
		var claimsToKeep = currentClaims.Except(claimsToRemove);
		var claimsToAdd = claims.Where(c => !claimsToKeep.Any(cc => cc.ClaimType == c.Type && cc.ClaimIssuer == c.Issuer))
			.Select(c => new UserClaim
			{
				UserId = user.Id,
				ClaimType = c.Type,
				ClaimValue = c.Value,
				ClaimIssuer = c.Issuer,
				ClaimOriginalIssuer = c.OriginalIssuer,
				ClaimValueType = c.ValueType
			})
			.ToList();

		_dbContext.IdentityUserClaims.RemoveRange(claimsToRemove);
		_dbContext.IdentityUserClaims.AddRange(claimsToAdd);
		await _dbContext.SaveChangesAsync(cancellationToken);
	}

	public async Task<(TUser, List<Claim>)> FinishSignInAsync(TUser user, List<Claim> claims, UserLoginInfo userLoginInfo, string shardKey,
		CancellationToken cancellationToken = default)
	{
		if (user == null) throw new ArgumentNullException(nameof(user));
		if (string.IsNullOrWhiteSpace(shardKey)) throw new ArgumentNullException(nameof(shardKey));

		// Update User
		UpdateUserWithClaims(user, claims);
		var updatedUser = await CreateOrUpdateAsync(user, cancellationToken);

		// Store UserLoginInfo
		if (userLoginInfo != null)
		{
			if (await _dbContext.IdentityUserLogins.AllAsync(
					x => x.UserId == updatedUser.Id && x.LoginProvider != userLoginInfo.LoginProvider, cancellationToken))
			{
				var result = await AddLoginAsync(updatedUser, userLoginInfo);
				if (result == null)
					throw new Exception();
				if (!result.Succeeded)
					throw new ValidationException(result.Errors != null ? string.Join(",", result.Errors) : null);
			}
		}

		// Use Role Claims to create UserRoles
		var applicationRoleClaims = claims.Where(c => c.Type == ClaimTypes.Role && c.Issuer == "LOCAL AUTHORITY").ToList();
		await AddUserToRolesAsync(updatedUser, applicationRoleClaims);

		// Store Remaining Claims (excluding UserRole claims)
		var remainingClaims = claims.Except(applicationRoleClaims).ToList();
		await StoreClaimsAsync(updatedUser, remainingClaims, cancellationToken);

		// Setup Instructor Sandbox (if needed)
		if (applicationRoleClaims.Any(c => c.Value == BaseRole.Creator))
			await _instructorSandboxService.SetUpInstructorSandboxAsync(updatedUser, cancellationToken);

		// Add to Global Group (if needed)
		await _userSetupService.AddUserToGlobalGroupAsync(shardKey, updatedUser, cancellationToken);

		return (updatedUser, claims);
	}

	#endregion Authentication

	#region CRUD

	public virtual async Task<TUser> CreateUserAsync(TUser user, CancellationToken cancellationToken = default)
	{
		var result = await CreateAsync(user);
		if (!result.Succeeded)
			throw new Exception(string.Join(", ", result.Errors));
		return user;
	}

	public virtual async Task<TUser> CreateOrUpdateAsync(TUser user, CancellationToken cancellationToken = default)
	{
		// Find User by UserName
		var existingUser = await FindByNameAsync(user.UserName);

		// Add New User
		if (existingUser == null)
		{
			existingUser = await CreateUserAsync(user, cancellationToken);
			// no more updates required
			return existingUser;
		}

		// Update User Details
		await UpdateExistingUserAsync(existingUser, user, cancellationToken);

		return existingUser;
	}

	public virtual async Task UpdateExistingUserAsync(TUser existingUser, TUser user, CancellationToken cancellationToken = default)
	{
		// Update User Details
		if (existingUser.Uid != user.Uid && !string.IsNullOrWhiteSpace(user.Uid))
		{
			existingUser.Uid = user.Uid;
		}

		if (existingUser.EmployeeNumber != user.EmployeeNumber && !string.IsNullOrWhiteSpace(user.EmployeeNumber))
		{
			existingUser.EmployeeNumber = user.EmployeeNumber;
		}

		if (existingUser.FirstName != user.FirstName && !string.IsNullOrWhiteSpace(user.FirstName))
		{
			existingUser.FirstName = user.FirstName;
		}

		if (existingUser.LastName != user.LastName && !string.IsNullOrWhiteSpace(user.LastName))
		{
			existingUser.LastName = user.LastName;
		}

		if (existingUser.Email != user.Email && !string.IsNullOrWhiteSpace(user.Email))
		{
			existingUser.Email = user.Email;
		}

		await Store.UpdateAsync(existingUser, cancellationToken);
	}

	public void UpdateUserWithClaims(TUser user, List<Claim> claims)
	{
		var firstNameClaim = claims.FirstOrDefault(x => x.Type == ClaimTypes.GivenName);
		if (firstNameClaim?.Value != null)
			user.FirstName = firstNameClaim.Value.Split(' ')[0];

		var lastNameClaim = claims.FirstOrDefault(x => x.Type == ClaimTypes.Surname);
		if (lastNameClaim?.Value != null)
			user.LastName = lastNameClaim.Value;

		// CAS

		var loginClaim = claims.FirstOrDefault(c => c.Type == ScaffoldingClaimTypes.CasLogin);
		if (loginClaim?.Value != null)
			user.Uid = loginClaim.Value;

		var puidClaim = claims.FirstOrDefault(c => c.Type == ScaffoldingClaimTypes.CasPuid);
		if (puidClaim?.Value != null)
			user.EmployeeNumber = puidClaim.Value;

		// LTI - Brightspace

		// TODO - Handle other Purdue Campus Shards

		var brightspaceOrgDefinedIdClaim = claims.FirstOrDefault(c => c.Type == BrightspaceClaim.BrightspaceOrgDefinedId);
		if (brightspaceOrgDefinedIdClaim != null && user.Email.Contains("@purdue.edu") &&
			IsValueEmployeeNumber(brightspaceOrgDefinedIdClaim.Value, out var employeeNumber))
			user.EmployeeNumber = employeeNumber;

		var brightspaceUsernameClaim = claims.FirstOrDefault(c => c.Type == BrightspaceClaim.BrightspaceUsername);
		if (brightspaceUsernameClaim != null && user.Email.Contains("@purdue.edu"))
			user.Uid = ConvertBrightspaceUsernameToUid(brightspaceUsernameClaim.Value);
	}

	private static string ConvertBrightspaceUsernameToUid(string username)
	{
		return username
			.Replace("adm_", "")
			.Replace("support_", "");
	}

	private static bool IsValueEmployeeNumber(string claimValue, out string employeeNumber)
	{
		employeeNumber = null;
		var isNumeric = long.TryParse(claimValue, out _);
		if (!isNumeric) return false;
		employeeNumber = claimValue.Length < 10
			? claimValue.PadLeft(10, '0')
			: claimValue;
		return true;
	}

	public async Task AssertAndAddImpersonatorUserIdClaimIfNeededAsync(List<Claim> claims)
	{
		var brightspaceImpersonationOrgDefinedIdClaim =
			claims.SingleOrDefault(c => c.Type.Equals(BrightspaceClaim.BrightspaceImpersonationOrgDefinedId));

		if (brightspaceImpersonationOrgDefinedIdClaim == null)
		{
			// No claim provided, return early
			return;
		}

		var isEmployeeNumber =
			IsValueEmployeeNumber(brightspaceImpersonationOrgDefinedIdClaim.Value, out var employeeNumber);
		var uid = !isEmployeeNumber
			? ConvertBrightspaceUsernameToUid(brightspaceImpersonationOrgDefinedIdClaim.Value)
			: null;

		// TODO - Handle other Purdue Campus Shards

		var user = await _dbContext.Users.SingleOrDefaultAsync(u =>
			uid != null && uid != string.Empty && u.Uid.Equals(uid) ||
			employeeNumber != null && employeeNumber != string.Empty && u.EmployeeNumber.Equals(employeeNumber));

		if (user == null)
			throw new ForbiddenException(Strings.ImpersonatorNotFound);

		claims.Add(new Claim(ScaffoldingClaimTypes.ImpersonatorUserId, user.Id, ClaimValueTypes.String));
	}

	#endregion CRUD
}