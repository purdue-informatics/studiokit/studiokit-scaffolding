﻿using Azure.AI.OpenAI;
using Sentry;
using StudioKit.Cloud.Storage.Blob;
using StudioKit.Configuration;
using StudioKit.Encryption;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Mime;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services;

/// <inheritdoc cref="IOpenAIClientService"/>
public class OpenAIClientService : IOpenAIClientService
{
	private readonly OpenAIClient _client;

	private static readonly ConcurrentDictionary<string, LocalCacheItem> LocalCache = new();

	public OpenAIClientService(OpenAIClient client)
	{
		_client = client ?? throw new ArgumentNullException(nameof(client));
	}

	public async Task<string> GetChatCompletionsAsync(
		ChatCompletionsOptions chatCompletionsOptions,
		CancellationToken cancellationToken = default)
	{
		var transaction = SentrySdk.GetSpan()?.GetTransaction();
		var span = transaction?.StartChild("open-ai.get-chat-completions");
		try
		{
			var response = await _client.GetChatCompletionsAsync(chatCompletionsOptions, cancellationToken);
			var choice = response.Value.Choices[0];
			var content = choice.Message.Content;
			return content;
		}
		finally
		{
			span?.Finish();
		}
	}

	public async Task<(ICollection<string> Responses, int PromptTokens, int CompletionTokens)> GetToolCompletionsAsync(
		ChatCompletionsOptions chatCompletionsOptions,
		CancellationToken cancellationToken = default)
	{
		var transaction = SentrySdk.GetSpan()?.GetTransaction();
		var span = transaction?.StartChild("open-ai.get-tool-completions");
		try
		{
			var response = await _client.GetChatCompletionsAsync(chatCompletionsOptions, cancellationToken);
			var usage = response.Value.Usage;
			var choice = response.Value.Choices[0];
			if (choice.FinishReason != CompletionsFinishReason.ToolCalls)
			{
				throw new InvalidOperationException("FinishReason does not equal ToolCalls");
			}

			var arguments = new List<string>();
			foreach (var toolCall in choice.Message.ToolCalls)
			{
				if (toolCall is ChatCompletionsFunctionToolCall functionToolCall)
				{
					arguments.Add(functionToolCall.Arguments);
				}
			}

			return (arguments, usage.PromptTokens, usage.CompletionTokens);
		}
		finally
		{
			span?.Finish();
		}
	}

	/// <summary>
	/// Get the prompts to send to an LLM. For local, read from a file for easy editing of a generally large amount of text while running.
	/// For dev, read from a blob for relatively easy updating of the prompt without redeploying the app. For prod, read from a blob and
	/// cache for performance but updatability if needed (with a restart).
	/// </summary>
	/// <param name="promptFileNamePrefix">The prefix of the two prompt file names, e.g. `"Prefix"BusinessLogicPrompt.txt` and `"Prefix"Schema.json`</param>
	/// <param name="promptFilePath">The relative path to the prompt files inside the calling assembly, e.g. "Models/"</param>
	/// <param name="blobStorage"></param>
	/// <param name="shouldIncludeExamplePrompt">Whether an example should be returned, used for Variate example problems</param>
	/// <param name="useAbsolutePath">Use <see cref="promptFilePath"/> as an absolute path, not relative to the assembly location. Useful for LINQPad scripts</param>
	/// <param name="cancellationToken"></param>
	/// <returns>A tuple of the pieces of the prompt</returns>
	public static async Task<(string businessLogicPrompt, string schemaPrompt, string example)> GetPromptsAsync(
		string promptFileNamePrefix,
		string promptFilePath,
		IBlobStorage blobStorage,
		bool shouldIncludeExamplePrompt = false,
		bool useAbsolutePath = false,
		CancellationToken cancellationToken = default)
	{
		string businessLogicPrompt;
		string schemaPrompt;
		var examplePrompt = "";
		LocalCacheItem cacheItem;

		var (businessLogicPromptFileName, schemaFileName, exampleFileName) =
			GetFileNames(promptFileNamePrefix, shouldIncludeExamplePrompt);

		var tier = EncryptedConfigurationManager.GetSetting(BaseAppSetting.Tier);
		switch (tier)
		{
			case Tier.Local:
				// Read the prompts from files for easy development
				var (businessLogicPromptFilePath, schemaFilePath, exampleFilePath) =
					GetFilePaths(promptFilePath, useAbsolutePath, businessLogicPromptFileName, schemaFileName, exampleFileName);
				businessLogicPrompt = await File.ReadAllTextAsync(businessLogicPromptFilePath, cancellationToken);
				schemaPrompt = await File.ReadAllTextAsync(schemaFilePath, cancellationToken);
				if (shouldIncludeExamplePrompt)
					examplePrompt = await File.ReadAllTextAsync(exampleFilePath, cancellationToken);
				break;
			case Tier.Dev:
				// Update blobs on first run, but do not keep prompts in in-memory cache
				// cacheItem is still added to dictionary to track that blobs have been updated
				if (!LocalCache.TryGetValue(promptFileNamePrefix, out cacheItem))
				{
					cacheItem = await UpdateBlobsAndCacheAsync(blobStorage, promptFileNamePrefix, promptFilePath,
						shouldIncludeExamplePrompt: shouldIncludeExamplePrompt,
						cancellationToken: cancellationToken);
					LocalCache.TryAdd(promptFileNamePrefix, cacheItem);
				}

				// Read the prompts from blobs for easy updating
				var (businessLogicPromptBlobPath, schemaBlobPath, exampleBlobFilePath) =
					GetBlobPaths(businessLogicPromptFileName, schemaFileName, exampleFileName);
				businessLogicPrompt =
					Encoding.UTF8.GetString(await blobStorage.ReadAllBytesAsync(businessLogicPromptBlobPath, cancellationToken));
				schemaPrompt = Encoding.UTF8.GetString(await blobStorage.ReadAllBytesAsync(schemaBlobPath, cancellationToken));
				if (shouldIncludeExamplePrompt)
					examplePrompt =
						Encoding.UTF8.GetString(await blobStorage.ReadAllBytesAsync(exampleBlobFilePath, cancellationToken));
				break;
			case Tier.QA:
			case Tier.Prod:
				// Update blobs on first run, and keep prompts in in-memory cache
				if (!LocalCache.TryGetValue(promptFileNamePrefix, out cacheItem))
				{
					cacheItem = await UpdateBlobsAndCacheAsync(blobStorage, promptFileNamePrefix, promptFilePath, true,
						shouldIncludeExamplePrompt, cancellationToken);
					LocalCache.TryAdd(promptFileNamePrefix, cacheItem);
				}

				// Use the prompts from in-memory cache for performance
				businessLogicPrompt = cacheItem.BusinessLogicPrompt;
				schemaPrompt = cacheItem.SchemaPrompt;
				if (shouldIncludeExamplePrompt)
					examplePrompt = cacheItem.ExamplePrompt;
				break;
			default:
				throw new Exception($"Unknown tier: {tier}");
		}

		return (businessLogicPrompt, schemaPrompt, examplePrompt);
	}

	private static (string businessLogicPromptFileName, string schemaPromptFileName, string exampleFileName) GetFileNames(
		string promptFileNamePrefix, bool shouldIncludeExample = false)
	{
		return ($"{promptFileNamePrefix}BusinessLogicPrompt.txt", $"{promptFileNamePrefix}Schema.json",
			shouldIncludeExample ? $"{promptFileNamePrefix}Example.json" : "");
	}

	private static (string businessLogicPromptFilePath, string schemaPromptFilePath, string exampleFilePath) GetFilePaths(
		string promptFilePath,
		bool useAbsolutePath,
		string businessLogicPromptFileName, string schemaFileName, string exampleFileName = "")
	{
		string pathPrefix;
		if (useAbsolutePath)
		{
			pathPrefix = promptFilePath;
		}
		else
		{
			pathPrefix = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			Debug.Assert(pathPrefix != null, nameof(pathPrefix) + " != null");
		}

		var businessLogicPromptFilePath = Path.Combine(pathPrefix, $"{promptFilePath}{businessLogicPromptFileName}");
		var schemaFilePath = Path.Combine(pathPrefix, $"{promptFilePath}{schemaFileName}");

		var exampleFilePath = !string.IsNullOrWhiteSpace(exampleFileName)
			? Path.Combine(pathPrefix, $"{promptFilePath}{exampleFileName}")
			: "";
		return (businessLogicPromptFilePath, schemaFilePath, exampleFilePath);
	}

	private static (string businessLogicPromptBlobPath, string schemaPromptBlobPath, string exampleBlobFilePath) GetBlobPaths(
		string businessLogicPromptFileName, string schemaFileName, string exampleFileName = "")
	{
		return ($"fileartifacts/{businessLogicPromptFileName}", $"fileartifacts/{schemaFileName}",
			!string.IsNullOrWhiteSpace(exampleFileName)
				? $"fileartifacts/{exampleFileName}"
				: "");
	}

	private static async Task<LocalCacheItem> UpdateBlobsAndCacheAsync(
		IBlobStorage blobStorage,
		string promptFileNamePrefix,
		string promptFilePath,
		bool shouldCache = false,
		bool shouldIncludeExamplePrompt = false,
		CancellationToken cancellationToken = default)
	{
		var (businessLogicPromptFileName, schemaFileName, exampleFileName) = GetFileNames(promptFileNamePrefix, shouldIncludeExamplePrompt);
		var (businessLogicPromptFilePath, schemaFilePath, exampleFilePath) =
			GetFilePaths(promptFilePath, false, businessLogicPromptFileName, schemaFileName, exampleFileName);
		var (businessLogicPromptBlobPath, schemaBlobPath, exampleBlobFilePath) =
			GetBlobPaths(businessLogicPromptFileName, schemaFileName, exampleFileName);

		var cacheItem = new LocalCacheItem();

		// stream the prompt files to blobs
		var businessLogicReader = File.OpenText(businessLogicPromptFilePath);
		await blobStorage.UploadBlockBlobAsync(businessLogicReader.BaseStream, businessLogicPromptBlobPath, MediaTypeNames.Text.Plain,
			cancellationToken);

		var schemaReader = File.OpenText(schemaFilePath);
		await blobStorage.UploadBlockBlobAsync(schemaReader.BaseStream, schemaBlobPath, MediaTypeNames.Text.Plain,
			cancellationToken);

		// re-use the file streams to read the prompts into in-memory cache
		if (shouldCache)
		{
			schemaReader.BaseStream.Position = 0;
			cacheItem.SchemaPrompt = await schemaReader.ReadToEndAsync();

			businessLogicReader.BaseStream.Position = 0;
			cacheItem.BusinessLogicPrompt = await businessLogicReader.ReadToEndAsync();
		}

		// stream the example file to a blob, if it exists
		if (!string.IsNullOrWhiteSpace(exampleFilePath))
		{
			var exampleReader = File.OpenText(exampleFilePath);
			await blobStorage.UploadBlockBlobAsync(exampleReader.BaseStream, exampleBlobFilePath, MediaTypeNames.Text.Plain,
				cancellationToken);

			if (shouldCache)
			{
				exampleReader.BaseStream.Position = 0;
				cacheItem.ExamplePrompt = await exampleReader.ReadToEndAsync();
			}
		}

		return cacheItem;
	}

	private class LocalCacheItem
	{
		public string BusinessLogicPrompt { get; set; }
		public string SchemaPrompt { get; set; }
		public string ExamplePrompt { get; set; }
	}
}