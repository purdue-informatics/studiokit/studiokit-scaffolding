using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using StudioKit.Caching.Interfaces;
using StudioKit.Configuration;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Encryption;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.DataAccess;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Properties;
using StudioKit.Sharding;
using StudioKit.Sharding.Interfaces;
using System;
using System.Collections.Specialized;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace StudioKit.Scaffolding.BusinessLogic.Services;

public class LockDownBrowserService<TContext, TUser, TGroup, TConfiguration> : ILockDownBrowserService
	where TContext : BaseDbContext<TUser, TGroup, TConfiguration>
	where TUser : IdentityUser, IUser, new()
	where TGroup : BaseGroup, new()
	where TConfiguration : BaseConfiguration, new()
{
	private readonly TContext _dbContext;
	private readonly IAsyncCacheClient _cacheClient;
	private readonly IShardKeyProvider _shardKeyProvider;
	private readonly ILockDownBrowserConfiguration _lockDownBrowserConfiguration;
	private readonly ILockDownBrowserUtils _lockDownBrowserUtils;

	public LockDownBrowserService(
		TContext dbContext,
		IAsyncCacheClient cacheClient,
		IShardKeyProvider shardKeyProvider,
		ILockDownBrowserConfiguration lockDownBrowserConfiguration,
		ILockDownBrowserUtils lockDownBrowserUtils)
	{
		_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
		_cacheClient = cacheClient ?? throw new ArgumentNullException(nameof(cacheClient));
		_shardKeyProvider = shardKeyProvider ?? throw new ArgumentNullException(nameof(shardKeyProvider));
		_lockDownBrowserConfiguration =
			lockDownBrowserConfiguration ?? throw new ArgumentNullException(nameof(lockDownBrowserConfiguration));
		_lockDownBrowserUtils = lockDownBrowserUtils ?? throw new ArgumentNullException(nameof(lockDownBrowserUtils));
	}

	public string GetLaunchUrl(string userId, int? entityId, string redirectPath, string title)
	{
		var baseUrl = ShardDomainConfiguration.Instance.BaseUrlWithShardKey(_shardKeyProvider.GetShardKey());

		var queryParams = HttpUtility.ParseQueryString(string.Empty);
		queryParams.Add(nameof(userId), userId);
		if (entityId.HasValue)
		{
			queryParams.Add(nameof(entityId), entityId.ToString());
		}

		queryParams.Add(nameof(redirectPath), redirectPath);
		queryParams.Add(LockDownBrowserKey.PageTitle, title);
		queryParams.Add(LockDownBrowserKey.CheckApplicationBlockList, LockDownBrowserConstants.True);

		var data = $"<z><u>{baseUrl}{LockDownBrowserConstants.RestartPath}?{queryParams}</u></z>";

		return _lockDownBrowserUtils.GetLaunchUrl(data);
	}

	public string GetTestLaunchUrl(string userId)
	{
		return GetLaunchUrl(
			userId,
			null,
			$"{LockDownBrowserConstants.WebCheckPath}?{LockDownBrowserKey.EnableQuizNavigation}={LockDownBrowserConstants.True}",
			EncryptedConfigurationManager.GetSetting(BaseAppSetting.ApplicationName));
	}

	public async Task<NameValueCollection> HandleRestartAsync(
		string userId,
		int? entityId,
		string redirectPath,
		CancellationToken cancellationToken = default)
	{
		if (string.IsNullOrWhiteSpace(userId)) throw new ArgumentNullException(nameof(userId));
		if (string.IsNullOrWhiteSpace(redirectPath)) throw new ArgumentNullException(nameof(redirectPath));

		var user = await _dbContext.Users.SingleOrDefaultAsync(u => u.Id == userId, cancellationToken);
		if (user == null) throw new EntityNotFoundException();

		// validate entity exists
		if (entityId.HasValue)
		{
			await _dbContext.GetLockDownBrowserEntityAsync(entityId.Value, cancellationToken);
		}

		// create a Challenge object and cache it
		// to persist the data from the Restart => Challenge redirect
		var challenge = new LockDownBrowserChallenge
		{
			Id = Guid.NewGuid().ToString(),
			UserId = userId,
			EntityId = entityId,
			RedirectPath = redirectPath,
			Nonce = Guid.NewGuid().ToString()
		};
		var challengeKey = $"{LockDownBrowserConstants.ChallengeCachePrefix}{challenge.Id}";
		await _cacheClient.PutAsync(challengeKey, challenge, TimeSpan.FromSeconds(60));

		var queryParams = HttpUtility.ParseQueryString(string.Empty);
		queryParams.Add(LockDownBrowserKey.EnableCookies, LockDownBrowserConstants.True);
		queryParams.Add(LockDownBrowserKey.ChallengeValue, _lockDownBrowserUtils.GetChallengeValue(challenge));
		queryParams.Add(LockDownBrowserConstants.ChallengeIdKey, challenge.Id);

		return queryParams;
	}

	public async Task<(IUser user, int? entityId, string redirectPath, NameValueCollection queryParams)> HandleChallengeAsync(
		string challengeId,
		HttpRequest request,
		CancellationToken cancellationToken = default)
	{
		if (string.IsNullOrWhiteSpace(challengeId)) throw new ArgumentNullException(nameof(challengeId));

		// If the state does not exist in redis, this may by a csrf attempt
		var challengeKey = $"{LockDownBrowserConstants.ChallengeCachePrefix}{challengeId}";
		var cachedChallenge = await _cacheClient.GetAsync<LockDownBrowserChallenge>(challengeKey);
		if (cachedChallenge == null)
			throw new UnauthorizedException(Strings.LockDownBrowserChallengeNotCached);

		// If the nonce exists in redis, this is a replay attack
		var cachedNonceKey = $"{LockDownBrowserConstants.NonceCachePrefix}{cachedChallenge.Nonce}";
		var cachedNonce = await _cacheClient.GetAsync<string>(cachedNonceKey);
		if (!string.IsNullOrEmpty(cachedNonce))
			throw new UnauthorizedException(Strings.LockDownBrowserNonceCached);

		// Remove the challenge to prevent subsequent csrf attempts
		await _cacheClient.RemoveAsync(challengeKey);

		// Store the nonce to prevent replay attacks for 24 hours
		await _cacheClient.PutAsync(cachedNonceKey, LockDownBrowserConstants.ExistentialCacheValue, TimeSpan.FromHours(24));

		var expectedResponseValue = _lockDownBrowserUtils.GetChallengeResponseValue(cachedChallenge);
		var cookies = request.Cookies;

		var hasClientId = cookies.TryGetValue(LockDownBrowserKey.ClientIsLockDownBrowser, out var clientId);
		if (!hasClientId || clientId != LockDownBrowserConstants.True)
		{
			throw new ForbiddenException(Strings.LockDownBrowserClientIsNotLockDownBrowser);
		}

		var hasChallengeResponse = cookies.TryGetValue(LockDownBrowserKey.ChallengeResponseValue, out var challengeResponseValue);
		if (!hasChallengeResponse || challengeResponseValue != expectedResponseValue)
		{
			throw new ForbiddenException(Strings.LockDownBrowserBadChallengeResponse);
		}

		var user = await _dbContext.Users.SingleOrDefaultAsync(u => u.Id == cachedChallenge.UserId, cancellationToken);

		var queryParams = HttpUtility.ParseQueryString(string.Empty);
		queryParams.Add(LockDownBrowserKey.EnableCookies, LockDownBrowserConstants.True);
		queryParams.Add(LockDownBrowserKey.EnableQuizNavigation, LockDownBrowserConstants.True);

		if (cachedChallenge.EntityId.HasValue)
		{
			var entity = await _dbContext.GetLockDownBrowserEntityAsync(cachedChallenge.EntityId.Value, cancellationToken);
			queryParams.Add(entity.IsLockDownBrowserHighSecurityEnabled
					? LockDownBrowserKey.EnableSecurityLevelHigh
					: LockDownBrowserKey.EnableSecurityLevelMediumHigh,
				LockDownBrowserConstants.True);

			if (!string.IsNullOrWhiteSpace(entity.LockDownBrowserProctorExitPassword))
			{
				queryParams.Add(LockDownBrowserKey.ProctorExitPassword, entity.LockDownBrowserProctorExitPassword);
			}

			if (entity.IsLockDownBrowserPrintingEnabled)
			{
				queryParams.Add(LockDownBrowserKey.EnablePrinting, LockDownBrowserConstants.True);
			}

			if (entity.LockDownBrowserCalculatorTypeEnum != null)
			{
				queryParams.Add(LockDownBrowserKey.EnableCalculator,
					entity.LockDownBrowserCalculatorTypeEnum == LockDownBrowserCalculatorType.Standard
						? LockDownBrowserConstants.StandardCalculator
						: LockDownBrowserConstants.ScientificCalculator);
			}
		}
		else
		{
			queryParams.Add(LockDownBrowserKey.EnableSecurityLevelMediumHigh, LockDownBrowserConstants.True);
		}

		if (!string.IsNullOrWhiteSpace(_lockDownBrowserConfiguration.DomainAllowList))
			queryParams.Add(LockDownBrowserKey.DomainAllowList, _lockDownBrowserConfiguration.DomainAllowList);

		return (user, cachedChallenge.EntityId, cachedChallenge.RedirectPath, queryParams);
	}
}