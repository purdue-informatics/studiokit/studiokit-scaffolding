using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using StudioKit.Data;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using StudioKit.Scaffolding.DataAccess.Interfaces;
using StudioKit.Scaffolding.Models;
using StudioKit.Security.BusinessLogic.Models;
using StudioKit.Security.Entity.Identity.Models;
using StudioKit.Utilities.Extensions;
using System;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services.Authorization;

public class AuthorizationService<TContext> : IAuthorizationService
	where TContext : DbContext, IBaseDbContext
{
	private readonly TContext _dbContext;

	public AuthorizationService(TContext dbContext)
	{
		_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
	}

	/// <summary>
	/// Return true iff the principal has a <see cref="IdentityUserRole{TKey}"/> or <see cref="ServiceClientRole"/> with any of the given <paramref name="activities"/>
	/// This functions as authorization for "global" activities (i.e. activities not associated with a specific entity)
	/// </summary>
	/// <param name="principal">The current principal, containing roles and userId/clientId</param>
	/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
	/// <param name="activities">One or many <see cref="Activity"/> Ids</param>
	/// <returns>True if principal is authorized</returns>
	public async Task<bool> CanPerformActivitiesAsync(IPrincipal principal, CancellationToken cancellationToken = default,
		params string[] activities)
	{
		if (principal is SystemPrincipal)
			return true;

		var userIdOrServiceClientId = principal.Identity.GetUserId();
		return await _dbContext.IdentityUserRoles
			.Where(r => r.UserId.Equals(userIdOrServiceClientId))
			// Roles => RoleActivities, use a join
			.Join(_dbContext.RoleActivities.Where(ra => activities.Contains(ra.Activity.Name)),
				r => r.RoleId,
				ra => ra.RoleId,
				(r, ra) => ra.ActivityId)
			.Union(_dbContext.ServiceClientRoles
				.Where(r => r.ClientId.Equals(userIdOrServiceClientId))
				// Roles => RoleActivities, use a join
				.Join(_dbContext.RoleActivities.Where(ra => activities.Contains(ra.Activity.Name)),
					r => r.RoleId,
					ra => ra.RoleId,
					(r, ra) => ra.ActivityId))
			.AnyAsync(cancellationToken);
	}

	/// <summary>
	/// Return true iff <paramref name="principal"/> has a <see cref="GroupUserRole"/> entry and,
	/// traversing the relations from <paramref name="entity"/> to
	/// <see cref="BaseGroup"/>, the Group's Id matches the GroupUserRole's GroupId
	/// </summary>
	/// <typeparam name="T">ModelBase</typeparam>
	/// <param name="entity">ModelBase for which the principal is attempting to perform the activity</param>
	/// <param name="principal">The user trying to perform an activity</param>
	/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
	/// <param name="activities">The activities to be performed</param>
	/// <returns></returns>
	public async Task<bool> CanPerformActivitiesForEntityByGroupAsync<T>(IPrincipal principal, T entity,
		CancellationToken cancellationToken = default,
		params string[] activities) where T : ModelBase
	{
		if (principal is SystemPrincipal)
			return true;

		var userId = principal.Identity.GetUserId();
		return await _dbContext.GroupsQueryableGroupIds(userId, entity, activities).AnyAsync(cancellationToken);
	}

	/// <summary>
	/// Return true iff <paramref name="principal"/>
	/// has an <see cref="EntityUserRole"/> with any of the provided <paramref name="activities"/>
	/// for the given <paramref name="entity"/>.
	/// Note: if the caller needs to verify activity via a group-level activity,
	/// including Groups themselves, they should call <see cref="CanPerformActivitiesForEntityByGroupAsync{T}"/>
	/// </summary>
	/// <typeparam name="T">Type of entity, implementing <see cref="ModelBase"/></typeparam>
	/// <param name="principal">The current principal, containing userId</param>
	/// <param name="entity">The entity which possibly has EntityUserRoles for the user</param>
	/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
	/// <param name="activities">One or many <see cref="Activity"/>s</param>
	/// <returns></returns>
	public async Task<bool> CanPerformActivitiesForEntityAsync<T>(IPrincipal principal, T entity,
		CancellationToken cancellationToken = default,
		params string[] activities) where T : ModelBase, IUserRoleAssociatedEntity
	{
		if (principal is SystemPrincipal)
			return true;

		var userId = principal.Identity.GetUserId();
		return await _dbContext.EntityUserRolesQueryableEntityIds(userId, entity, activities)
			.AnyAsync(cancellationToken);
	}
}