﻿using Microsoft.EntityFrameworkCore;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.DataAccess.Interfaces;
using StudioKit.Scaffolding.Models;
using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services.Authorization;

public class GroupAuthorizationService<TContext> : AuthorizationService<TContext>, IGroupAuthorizationService
	where TContext : DbContext, IBaseDbContext
{
	private readonly TContext _dbContext;

	public GroupAuthorizationService(TContext dbContext) : base(dbContext)
	{
		_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
	}

	public async Task AssertCanReadAnyAsync(IPrincipal principal, CancellationToken cancellationToken = default,
		IDictionary<string, int> relationIds = null)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		var canReadAny = await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.GroupRead);
		if (!canReadAny)
			throw new ForbiddenException();
	}

	public async Task AssertCanReadAsync(int groupId, IPrincipal principal, CancellationToken cancellationToken = default,
		IDictionary<string, int> relationIds = null)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		var group = await _dbContext.FindEntityAsync<BaseGroup>(groupId, false, cancellationToken: cancellationToken);

		var canRead =
			await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.GroupRead) ||
			await CanPerformActivitiesForEntityByGroupAsync(principal, group, cancellationToken, BaseActivity.GroupRead);
		if (!canRead)
			throw new ForbiddenException();
	}

	public async Task AssertCanUpdateAsync(int groupId, IPrincipal principal, CancellationToken cancellationToken = default,
		IDictionary<string, int> relationIds = null)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		var group = await _dbContext.FindEntityAsync<BaseGroup>(groupId, false, cancellationToken: cancellationToken);

		var canUpdate =
			await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.GroupUpdate) ||
			await CanPerformActivitiesForEntityByGroupAsync(principal, group, cancellationToken, BaseActivity.GroupUpdate);
		if (!canUpdate)
			throw new ForbiddenException();
	}

	public async Task AssertCanCreateAsync(IPrincipal principal, CancellationToken cancellationToken = default,
		IDictionary<string, int> relationIds = null)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		var canCreate = await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.GroupCreate);
		if (!canCreate)
			throw new ForbiddenException();
	}

	public async Task AssertCanDeleteAsync(int groupId, IPrincipal principal, CancellationToken cancellationToken = default,
		IDictionary<string, int> relationIds = null)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		var group = await _dbContext.FindEntityAsync<BaseGroup>(groupId, false, cancellationToken: cancellationToken);

		var canDelete =
			await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.GroupDelete) ||
			await CanPerformActivitiesForEntityByGroupAsync(principal, group, cancellationToken, BaseActivity.GroupDelete);
		if (!canDelete)
			throw new ForbiddenException();
	}

	#region IEntityUserRoleAuthorizationService

	public async Task AssertCanReadEntityUserRolesAsync(int groupId, IPrincipal principal, CancellationToken cancellationToken = default)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		var group = await _dbContext.FindEntityAsync<BaseGroup>(groupId, false, cancellationToken: cancellationToken);

		var canReadGroupUserRole =
			await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.GroupUserRoleRead) ||
			await CanPerformActivitiesForEntityByGroupAsync(principal, group, cancellationToken,
				BaseActivity.GroupUserRoleRead);

		if (!canReadGroupUserRole)
			throw new ForbiddenException();
	}

	public async Task AssertCanModifyEntityUserRolesAsync(int groupId, IPrincipal principal, CancellationToken cancellationToken = default)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		var group = await _dbContext.FindEntityAsync<BaseGroup>(groupId, cancellationToken: cancellationToken);

		var canModifyGroupUserRole =
			await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.GroupUserRoleModify) ||
			await CanPerformActivitiesForEntityByGroupAsync(principal, group, cancellationToken,
				BaseActivity.GroupUserRoleModify);

		if (!canModifyGroupUserRole)
			throw new ForbiddenException();
	}

	public async Task AssertCanDeleteOwnEntityUserRoleAsync(int groupId, IPrincipal principal,
		CancellationToken cancellationToken = default)
	{
		// Groups have no special role for deleting own EntityUserRole, just check for modify instead
		await AssertCanModifyEntityUserRolesAsync(groupId, principal, cancellationToken);
	}

	#endregion IEntityUserRoleAuthorizationService

	public async Task AssertCanReadExternalGroupsAsync(int groupId, IPrincipal principal, CancellationToken cancellationToken = default)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		var group = await _dbContext.FindEntityAsync<BaseGroup>(groupId, false, cancellationToken: cancellationToken);

		var canReadExternalGroups =
			await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.ExternalGroupRead) ||
			await CanPerformActivitiesForEntityByGroupAsync(principal, group, cancellationToken, BaseActivity.ExternalGroupRead);
		if (!canReadExternalGroups)
			throw new ForbiddenException();
	}

	public async Task AssertCanConnectExternalGroupsAsync(int groupId, IPrincipal principal, CancellationToken cancellationToken = default)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		var group = await _dbContext.FindEntityAsync<BaseGroup>(groupId, cancellationToken: cancellationToken);

		var canConnect =
			await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.ExternalGroupConnectAny) ||
			await CanPerformActivitiesForEntityByGroupAsync(principal, group, cancellationToken, BaseActivity.ExternalGroupConnectOwn);
		if (!canConnect)
			throw new ForbiddenException();
	}

	public async Task AssertCanConnectAnyExternalGroupAsync(IPrincipal principal, CancellationToken cancellationToken = default)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		var canConnect = await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.ExternalGroupConnectAny);
		if (!canConnect)
			throw new ForbiddenException();
	}

	public async Task AssertCanConnectOwnExternalGroupAsync(IPrincipal principal, CancellationToken cancellationToken = default)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		var canConnect = await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.ExternalGroupConnectOwn);
		if (!canConnect)
			throw new ForbiddenException();
	}

	public async Task AssertCanSyncRosterAsync(int groupId, IPrincipal principal, CancellationToken cancellationToken = default)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		var group = await _dbContext.FindEntityAsync<BaseGroup>(groupId, cancellationToken: cancellationToken);

		var canSync =
			await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.GroupRosterSync) ||
			await CanPerformActivitiesForEntityByGroupAsync(principal, group, cancellationToken, BaseActivity.GroupRosterSync);
		if (!canSync)
			throw new ForbiddenException();
	}

	public async Task AssertCanSyncAllRostersAsync(IPrincipal principal, CancellationToken cancellationToken = default)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		if (!await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.GroupRosterSyncAll))
			throw new ForbiddenException();
	}
}