﻿using Microsoft.EntityFrameworkCore;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.DataAccess.Interfaces;
using System;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services.Authorization;

public class AppClientAuthorizationService<TContext> : AuthorizationService<TContext>, IAppClientAuthorizationService
	where TContext : DbContext, IBaseDbContext
{
	public AppClientAuthorizationService(TContext dbContext) : base(dbContext)
	{
	}

	public async Task AssertCanUpdateAsync(IPrincipal principal, CancellationToken cancellationToken = default)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		var canUpdate = await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.AppClientUpdate);
		if (!canUpdate)
			throw new ForbiddenException();
	}
}