﻿using Microsoft.EntityFrameworkCore;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.DataAccess.Interfaces;
using StudioKit.Utilities.Extensions;
using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services.Authorization;

public class UserAuthorizationService<TContext> : AuthorizationService<TContext>, IUserAuthorizationService
	where TContext : DbContext, IBaseDbContext
{
	public UserAuthorizationService(TContext dbContext) : base(dbContext)
	{
	}

	public async Task AssertCanReadAnyAsync(IPrincipal principal, CancellationToken cancellationToken = default,
		IDictionary<string, int> relationIds = null)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		var canReadAny = await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.UserReadAny);
		if (!canReadAny)
			throw new ForbiddenException();
	}

	public async Task AssertCanReadAsync(string userId, IPrincipal principal, CancellationToken cancellationToken = default,
		IDictionary<string, int> relationIds = null)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		var currentUserId = principal.Identity.GetUserId();
		var isCurrentUser = currentUserId == userId;
		var canReadAny = await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.UserReadAny);

		if (!canReadAny && !isCurrentUser)
			throw new ForbiddenException();
	}

	public Task AssertCanUpdateAsync(string userId, IPrincipal principal, CancellationToken cancellationToken = default,
		IDictionary<string, int> relationIds = null)
	{
		throw new NotImplementedException();
	}

	public Task AssertCanCreateAsync(IPrincipal principal, CancellationToken cancellationToken = default,
		IDictionary<string, int> relationIds = null)
	{
		throw new NotImplementedException();
	}

	public Task AssertCanDeleteAsync(string userId, IPrincipal principal, CancellationToken cancellationToken = default,
		IDictionary<string, int> relationIds = null)
	{
		throw new NotImplementedException();
	}

	public async Task AssertCanImpersonateAsync(string userId, IPrincipal principal, CancellationToken cancellationToken = default)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		var canImpersonateAny = await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.UserImpersonateAny);
		if (!canImpersonateAny)
			throw new ForbiddenException();
	}
}