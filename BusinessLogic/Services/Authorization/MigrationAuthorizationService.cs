﻿using Microsoft.EntityFrameworkCore;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.DataAccess.Interfaces;
using System;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Services.Authorization;

public class MigrationAuthorizationService<TContext> : AuthorizationService<TContext>, IMigrationAuthorizationService
	where TContext : DbContext, IBaseDbContext
{
	public MigrationAuthorizationService(TContext dbContext) : base(dbContext)
	{
	}

	public async Task AssertCanReadAsync(IPrincipal principal, CancellationToken cancellationToken = default)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		var canRead = await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.MigrationsRead);
		if (!canRead)
			throw new ForbiddenException();
	}

	public async Task AssertCanMigrateAsync(IPrincipal principal, CancellationToken cancellationToken = default)
	{
		if (principal == null) throw new ArgumentNullException(nameof(principal));

		var canRead = await CanPerformActivitiesAsync(principal, cancellationToken, BaseActivity.MigrationsModify);
		if (!canRead)
			throw new ForbiddenException();
	}
}