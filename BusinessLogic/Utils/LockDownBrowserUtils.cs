using FluentValidation;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.Models.Interfaces;
using StudioKit.Scaffolding.Properties;
using StudioKit.Utilities.Extensions;
using System;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace StudioKit.Scaffolding.BusinessLogic.Utils;

public class LockDownBrowserUtils : ILockDownBrowserUtils
{
	private readonly ILockDownBrowserConfiguration _configuration;

	public LockDownBrowserUtils(ILockDownBrowserConfiguration lockDownBrowserConfiguration)
	{
		_configuration = lockDownBrowserConfiguration ?? throw new ArgumentNullException(nameof(lockDownBrowserConfiguration));
	}

	/// <summary>
	/// Ensure that the LockDown Browser is in use when required
	/// </summary>
	/// <param name="claimedEntity">The entity attempting to be accessed</param>
	/// <param name="identity">The identity of the principal attempting to access the <paramref name="claimedEntity"/></param>
	/// <param name="isForResultsContext">True if the principal is attempting to access assessment results</param>
	/// <exception cref="ForbiddenException">Thrown if access could not be asserted</exception>
	public static void AssertLockDownBrowserAccess(ILockDownBrowserEntity claimedEntity, ClaimsIdentity identity,
		bool isForResultsContext = false)
	{
		if (claimedEntity == null) throw new ArgumentNullException(nameof(claimedEntity));
		if (identity == null) throw new ArgumentNullException(nameof(identity));

		// If LDB is not required at all or if it is required but not for results and this checking in a results context,
		// return without throwing any exception.
		if (!claimedEntity.IsLockDownBrowserRequired || (isForResultsContext && !claimedEntity.IsLockDownBrowserRequiredForResults))
			return;

		// Ensure the claim asserting they are using LDB exists and is set to "true".
		var clientIsLockDownBrowserClaim =
			identity.Claims.SingleOrDefault(c => c.Type.Equals(ScaffoldingClaimTypes.ClientIsLockDownBrowser));
		if (clientIsLockDownBrowserClaim == null)
			throw new ForbiddenException(Strings.LockDownBrowserClientIsNotLockDownBrowser);
		var clientIsLockDownBrowser = clientIsLockDownBrowserClaim.Value;
		if (!clientIsLockDownBrowser.Equals(LockDownBrowserConstants.True))
			throw new ForbiddenException(Strings.LockDownBrowserClientIsNotLockDownBrowser);

		// Ensure the claim asserting they have access to a specific entity is the entity being validated here.
		var entityIdClaim = identity.Claims.SingleOrDefault(c => c.Type.Equals(ScaffoldingClaimTypes.LockDownBrowserEntityId));
		if (entityIdClaim == null)
			throw new ForbiddenException(Strings.LockDownBrowserBadEntityId);
		var entityId = int.Parse(entityIdClaim.Value);
		if (entityId != claimedEntity.Id)
			throw new ForbiddenException(Strings.LockDownBrowserBadEntityId);
	}

	public string GetLaunchUrl(string data)
	{
		if (string.IsNullOrWhiteSpace(data)) throw new ArgumentNullException(nameof(data));

		var encryptedData = EncryptData(data.SpacePad16(), LockDownBrowserEncryptionKeyType.Launch);
		return $"ldb1:{_configuration.SecretIndex}:{_configuration.SecretVersion}:%5B{encryptedData}%5D";
	}

	public string GetChallengeValue(LockDownBrowserChallenge challenge)
	{
		if (challenge == null) throw new ArgumentNullException(nameof(challenge));

		var encryptedData = EncryptData(
			GetChallengeData(challenge),
			LockDownBrowserEncryptionKeyType.Challenge);
		return $"{_configuration.SecretIndex}:{_configuration.SecretVersion}:{encryptedData}";
	}

	public string GetChallengeResponseValue(LockDownBrowserChallenge challenge)
	{
		if (challenge == null) throw new ArgumentNullException(nameof(challenge));

		return EncryptData(GetChallengeData(challenge), LockDownBrowserEncryptionKeyType.ChallengeResponse);
	}

	public string EncryptProctorExitPassword(string password)
	{
		if (string.IsNullOrEmpty(password)) throw new ValidationException(Strings.LockDownBrowserProctorExitPasswordMinLength);
		// this is not technically required by LockDown Browser, but will allow us to decrypt the password and remove the encryption's space padding
		if (password != password.Trim()) throw new ValidationException(Strings.LockDownBrowserProctorExitPasswordMustTrimWhitespace);
		if (!ContainsValidCharacters(password)) throw new ValidationException(Strings.LockDownBrowserProctorExitPasswordInvalidCharacters);
		if (password.Length > LockDownBrowserConstants.MaxProctorExitPasswordLength)
			throw new ValidationException(Strings.LockDownBrowserProctorExitPasswordMaxLength);

		return EncryptData(password.SpacePad16(), LockDownBrowserEncryptionKeyType.ProctorExitPassword);
	}

	public string DecryptProctorExitPassword(string encryptedPassword)
	{
		if (string.IsNullOrWhiteSpace(encryptedPassword)) throw new ArgumentNullException(nameof(encryptedPassword));

		return DecryptData(encryptedPassword, LockDownBrowserEncryptionKeyType.ProctorExitPassword)
			// trim off all extra padding spaces
			.Trim();
	}

	public static string GetChallengeData(LockDownBrowserChallenge challenge)
	{
		return Utilities.Encryption.CreateMD5HashedString(challenge.ToString()).SpacePad16();
	}

	public static bool ContainsValidCharacters(string password)
	{
		// normal ASCII chars are 32-126
		return password.All(ch => ch >= 32 && ch <= 126);
	}

	#region Private Methods

	private string GetEncryptionKey(LockDownBrowserEncryptionKeyType keyType)
	{
		switch (keyType)
		{
			case LockDownBrowserEncryptionKeyType.Launch:
			case LockDownBrowserEncryptionKeyType.Challenge:
			case LockDownBrowserEncryptionKeyType.ProctorExitPassword:
				// secret 1 + secret 2 order is used for launch and challenge
				return $"{_configuration.Secret1}{_configuration.Secret2}";
			case LockDownBrowserEncryptionKeyType.ChallengeResponse:
				// secret 2 + secret 1 order is used for handshake response
				return $"{_configuration.Secret2}{_configuration.Secret1}";
			default:
				throw new ArgumentOutOfRangeException(nameof(keyType), keyType, null);
		}
	}

	private string EncryptData(string data, LockDownBrowserEncryptionKeyType keyType)
	{
		var encryptedDataBytes = Utilities.Encryption.EncryptStringToBytesAes(
			data,
			Encoding.ASCII.GetBytes(GetEncryptionKey(keyType)),
			Encoding.ASCII.GetBytes(_configuration.SecretIV));
		var encryptedDataString = Convert.ToBase64String(encryptedDataBytes);
		return encryptedDataString;
	}

	private string DecryptData(string encryptedData, LockDownBrowserEncryptionKeyType keyType)
	{
		var encryptedDataBytes = Convert.FromBase64String(encryptedData);
		var decryptedData = Utilities.Encryption.DecryptStringFromBytesAes(
			encryptedDataBytes,
			Encoding.ASCII.GetBytes(GetEncryptionKey(keyType)),
			Encoding.ASCII.GetBytes(_configuration.SecretIV));
		return decryptedData;
	}

	#endregion Private Methods
}