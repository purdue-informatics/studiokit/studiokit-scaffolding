﻿using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Encryption;
using StudioKit.Scaffolding.Models;
using StudioKit.Utilities.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StudioKit.Scaffolding.BusinessLogic.Utils;

/// <summary>
/// Utility class for constructing notification messages. General text construction and group-related text construction
/// </summary>
public static class NotificationTemplateConfigurationUtils
{
	#region General :salute: usage

	public static string PlainTextGreeting(string firstName)
	{
		return $"Hi {firstName},\r\n\r\n";
	}

	public static string PlainTextEnding(string baseUrl, string path = null, bool shouldShowLoginLink = true)
	{
		var applicationName = EncryptedConfigurationManager.GetSetting("applicationName");
		var ending = shouldShowLoginLink
			? $"Log in to {applicationName} at {baseUrl}{path ?? ""}.\r\n\r\nThanks\r\nThe {applicationName} Team\r\n\r\n\r\n"
			: $"Thanks\r\nThe {applicationName} Team\r\n\r\n\r\n";

		ending += "© 2020 Purdue University. An equal access, equal opportunity university.\r\n" +
				"Need help? Have trouble accessing this page because of a disability? Contact us at tlt@purdue.edu.\r\n" +
				$"Unsubscribe from our emails at {baseUrl}settings.";

		return ending;
	}

	public static string FormattedDateTime(DateTime dateTime)
	{
		var estDate = dateTime.ToEasternStandardTime();
		return "on " + estDate.ToString("dddd, MMMM d") + " at "
				+ estDate.ToString("h:mm tt ")
				+ (TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time").IsDaylightSavingTime(dateTime) ? "EDT" : "EST");
	}

	#endregion General :salute: usage

	#region Group Role message builders

	public static string PersonalGroupRolePlainTextBody(Func<string, string> plainTextGreeting, string firstName,
		string verb, string role, string groupName, IUser updatedByUser, string baseUrl, string path,
		Func<string, string, bool, string> plainTextEnding)
	{
		var body = plainTextGreeting(firstName);
		body +=
			$"You have been {verb} as {role} in course \"{groupName}\" by {updatedByUser.FirstName} {updatedByUser.LastName}.\r\n\r\n";
		if (verb == "added")
		{
			body += $"Log in now to view the course at {baseUrl}{path}.\r\n\r\n";
			body += plainTextEnding(baseUrl, null, false);
		}
		else
		{
			body += plainTextEnding(baseUrl, path, true);
		}

		return body;
	}

	public static string PersonalGroupRoleSubject(string verb, string role, string groupName)
	{
		return $"You Were {verb} as {role} in \"{groupName}\"";
	}

	public static string PersonalGroupRoleHtmlBody(string verb, string role, string groupName, IUser updatedByUser)
	{
		var body =
			$"<p>You have been {verb} as {role} in course &quot;{groupName}&quot; by {updatedByUser.FirstName} {updatedByUser.LastName}.</p>";
		if (verb == "added")
		{
			body += "<p>Log in now to view the course.</p>";
		}

		return body;
	}

	public static string GroupRolePlainTextBody(Func<string, string> plainTextGreeting, string firstName,
		IEnumerable<IUser> updatedUsers, string verb, string role, string groupName, IUser updatedByUser,
		List<ExternalGroup> removedExternalGroups, string baseUrl,
		string path, Func<string, string, bool, string> plainTextEnding)
	{
		var body = plainTextGreeting(firstName);
		var updatedUsersList = updatedUsers.ToList();
		if (updatedUsersList.Count == 1)
		{
			var updatedUser = updatedUsersList.First();
			body +=
				$"{updatedUser.FirstName} {updatedUser.LastName} has been {verb} as {role} in course \"{groupName}\" by {updatedByUser.FirstName} {updatedByUser.LastName}.\r\n\r\n";
		}
		else
		{
			body +=
				$"The following people have been {verb} as {role} in course \"{groupName}\" by {updatedByUser.FirstName} {updatedByUser.LastName}:\r\n";
			foreach (var updatedUser in updatedUsersList)
			{
				body += $"   - {updatedUser.FirstName} {updatedUser.LastName}\r\n";
			}

			body += "\r\n";
		}

		if (removedExternalGroups != null && removedExternalGroups.Any())
		{
			body += "This also removed the following course sections:\r\n";
			foreach (var removedExternalGroup in removedExternalGroups)
			{
				body += $"   - {removedExternalGroup.Name}\r\n";
			}

			body += "\r\n";
		}

		body += $"Log in now to view the course roster at {baseUrl}{path}.\r\n\r\n";
		body += plainTextEnding(baseUrl, null, false);

		return body;
	}

	public static string GroupRoleSubject(IEnumerable<IUser> updatedUsers, string verb, string role, string groupName)
	{
		var updatedUsersList = updatedUsers.ToList();
		if (updatedUsersList.Count > 1) return $"Multiple People Were {verb} as {role} in \"{groupName}\"";
		var updatedUser = updatedUsersList.First();
		return $"{updatedUser.FirstName} {updatedUser.LastName} Was {verb} as {role} in \"{groupName}\"";
	}

	public static string GroupRoleHtmlBody(IEnumerable<IUser> updatedUsers, string verb, string role, string groupName,
		IUser updatedByUser, List<ExternalGroup> removedExternalGroups)
	{
		string body;
		var updatedUsersList = updatedUsers.ToList();
		if (updatedUsersList.Count == 1)
		{
			var updatedUser = updatedUsersList.First();
			body =
				$"<p>{updatedUser.FirstName} {updatedUser.LastName} has been {verb} as {role} in course &quot;{groupName}&quot; by {updatedByUser.FirstName} {updatedByUser.LastName}.</p>";
		}
		else
		{
			body =
				$"<p>The following people have been {verb} as {role} in course &quot;{groupName}&quot; by {updatedByUser.FirstName} {updatedByUser.LastName}:</p><ul>";
			foreach (var updatedUser in updatedUsersList)
			{
				body += $"<li>{updatedUser.FirstName} {updatedUser.LastName}</li>";
			}

			body += "</ul>";
		}

		if (removedExternalGroups != null && removedExternalGroups.Any())
		{
			body += "<p>This also removed the following course sections:</p><ul>";
			foreach (var removedExternalGroup in removedExternalGroups)
			{
				body += $"<li>{removedExternalGroup.Name}</li>";
			}

			body += "</ul>";
		}

		body += "<p>Log in now to view the course roster.</p>";

		return body;
	}

	#endregion Group Role message builders

	#region External Group message builders

	public static string ExternalGroupSubject(string verb, string groupName, int externalGroupCount)
	{
		var words = externalGroupCount > 1 ? "Course Rosters Were" : "A Course Roster Was";
		return $"{words} {verb} in \"{groupName}\"";
	}

	public static string ExternalGroupPlainTextBody(Func<string, string> plainTextGreeting, string firstName,
		string verb, IReadOnlyCollection<ExternalGroup> externalGroups, string groupName, IUser updatedByUser,
		string baseUrl, string path, Func<string, string, bool, string> plainTextEnding)
	{
		var words = externalGroups.Count > 1 ? "Course rosters were" : "course roster was";

		var body = plainTextGreeting(firstName);
		body +=
			$"{(externalGroups.Count > 1 ? "" : "A")} {words} {verb} in \"{groupName}\" by {updatedByUser.FirstName} {updatedByUser.LastName}.\r\n\r\n";
		body += $"The following {words.ToLower()} {verb}:\r\n";
		foreach (var externalGroup in externalGroups)
		{
			body += $"   - {externalGroup.Name}\r\n";
		}

		body += "\r\n";
		body += plainTextEnding(baseUrl, path, true);
		return body;
	}

	public static string ExternalGroupHtmlBody(string verb, IReadOnlyCollection<ExternalGroup> externalGroups,
		string groupName, IUser updatedByUser)
	{
		var words = externalGroups.Count > 1 ? "Course rosters were" : "course roster was";
		var body =
			$"<p>{(externalGroups.Count > 1 ? "" : "A")} {words} {verb} in &quot;{groupName}&quot; by {updatedByUser.FirstName} {updatedByUser.LastName}.</p>";
		body += $"<p>The following {words.ToLower()} {verb}:</p><ul>";
		foreach (var externalGroup in externalGroups)
		{
			body += $"<li>{externalGroup.Name}</li>";
		}

		body += "</ul><p>Log in now to view the updates.</p>";
		return body;
	}

	#endregion External Group message builders
}