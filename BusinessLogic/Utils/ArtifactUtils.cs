using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Drawing.Pictures;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using FluentValidation;
using HtmlAgilityPack;
using Microsoft.EntityFrameworkCore;
using StudioKit.Cloud.Storage.Blob;
using StudioKit.Data;
using StudioKit.Data.Entity.Extensions;
using StudioKit.Data.Interfaces;
using StudioKit.Notification;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.DataAccess.Interfaces;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using StudioKit.Scaffolding.Properties;
using StudioKit.TransientFaultHandling.Http.Interfaces;
using StudioKit.Utilities.Extensions;
using StudioKit.Utilities.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using UglyToad.PdfPig;
using UglyToad.PdfPig.DocumentLayoutAnalysis.TextExtractor;
using HtmlDocument = HtmlAgilityPack.HtmlDocument;

namespace StudioKit.Scaffolding.BusinessLogic.Utils;

public static class ArtifactUtils
{
	public const string PlainTextContentType = "text/plain";
	public const string TextStorageLocation = "textartifacts/";
	public const string FileStorageLocation = "fileartifacts/";
	public const string GeneralTextArtifactFileName = "TextArtifact";
	public const string GeneralTextArtifactFileExtension = ".txt";
	public const string AllowedFileExtensionsPattern = @"^\.(pdf|txt|csv|doc|docx|xls|xlsx|ppt|pptx|jpg|jpeg|png|gif|bmp|mp4|mov|mp3)$";

	/// <summary>
	/// Create an <see cref="Artifact"/> for the given <paramref name="requestModel"/>.
	/// If not an <see cref="UrlArtifactBusinessModel"/>, uploads a BlockBlob in <see cref="IBlobStorage"/>.
	/// The blob is given a unique name (see <see cref="GenerateTextBlobPath"/> or <see cref="GenerateFileBlobPath"/>.
	/// </summary>
	/// <param name="requestModel">A request model to create an artifact</param>
	/// <param name="artifactStorage">The blob storage</param>
	/// <param name="userId">The user creating the artifact</param>
	/// <param name="cancellationToken">A cancellation token</param>
	/// <returns>The created <see cref="Artifact"/></returns>
	/// <exception cref="ArgumentNullException"></exception>
	/// <exception cref="InvalidEnumArgumentException"></exception>
	public static async Task<Artifact> CreateArtifactAsync(
		IArtifactBusinessModel requestModel,
		IBlobStorage artifactStorage,
		string userId,
		CancellationToken cancellationToken = default)
	{
		if (requestModel == null) throw new ArgumentNullException(nameof(requestModel));
		if (artifactStorage == null) throw new ArgumentNullException(nameof(artifactStorage));
		if (string.IsNullOrWhiteSpace(userId)) throw new ArgumentNullException(nameof(userId));

		Artifact artifact;
		switch (requestModel)
		{
			case TextArtifactBusinessModel t:
				var textBlobPath = GenerateTextBlobPath();
				var sanitizedText = ArtifactHtmlSanitizeSettings.Sanitize(t.Text);
				await using (var textStream = GenerateStreamFromString(sanitizedText))
				{
					await artifactStorage.UploadBlockBlobAsync(textStream, textBlobPath, PlainTextContentType, cancellationToken);
				}

				artifact = new TextArtifact
				{
					WordCount = t.WordCount,
					Url = artifactStorage.UrlForBlockBlob(textBlobPath),
					CreatedById = userId
				};
				break;

			case FileArtifactBusinessModel f:
				var filename = f.FileName;
				var fileBlobPath = GenerateFileBlobPath(filename);
				await using (var fileStream = f.File.OpenReadStream())
				{
					await artifactStorage.UploadBlockBlobAsync(fileStream, fileBlobPath, f.ContentType, cancellationToken);
				}

				artifact = new FileArtifact
				{
					FileName = filename,
					Url = artifactStorage.UrlForBlockBlob(fileBlobPath),
					CreatedById = userId
				};
				break;

			case UrlArtifactBusinessModel u:
				artifact = new UrlArtifact
				{
					Url = u.Url.Trim(),
					CreatedById = userId
				};
				break;

			default:
				throw new InvalidEnumArgumentException();
		}

		return artifact;
	}

	/// <summary>
	/// Create an <see cref="Artifact"/> using <see cref="CreateArtifactAsync"/>,
	/// then add the new artifact into the <paramref name="artifactsContainer"/>,
	/// and then save the <paramref name="dbContext"/>.
	/// </summary>
	/// <typeparam name="TEntity">An entity which contains a collection of many <see cref="TEntityArtifact"/>s</typeparam>
	/// <typeparam name="TEntityArtifact">A many-to-many entity which connects <see cref="TEntity"/> to <see cref="Artifact"/></typeparam>
	/// <param name="artifactsContainer">The instance of <see cref="TEntity"/> which will be connected to the new <see cref="Artifact"/></param>
	/// <param name="requestModel">A request model to create an artifact</param>
	/// <param name="artifactStorage">The blob storage</param>
	/// <param name="dbContext">The db context</param>
	/// <param name="userId">The user creating the artifact</param>
	/// <param name="cancellationToken">A cancellation token</param>
	/// <returns>The created and saved <see cref="Artifact"/></returns>
	/// <exception cref="ArgumentNullException"></exception>
	/// <exception cref="InvalidEnumArgumentException"></exception>
	public static async Task<Artifact> SaveArtifactAsync<TEntity, TEntityArtifact>(
		TEntity artifactsContainer,
		IArtifactBusinessModel requestModel,
		IBlobStorage artifactStorage,
		IBaseDbContext dbContext,
		string userId,
		CancellationToken cancellationToken = default)
		where TEntity : ModelBase, IArtifactsContainer<TEntityArtifact>
		where TEntityArtifact : ModelBase, IEntityArtifact, new()
	{
		if (artifactsContainer == null) throw new ArgumentNullException(nameof(artifactsContainer));

		var artifact = await CreateArtifactAsync(requestModel, artifactStorage, userId, cancellationToken);

		artifactsContainer.EntityArtifacts.Add(new TEntityArtifact
		{
			Artifact = artifact
		});
		await dbContext.SaveChangesAsync(cancellationToken);

		return artifact;
	}

	/// <summary>
	/// Update the Url of the <see cref="Artifact"/> using the <paramref name="requestModel"/> and save.
	/// If not an <see cref="UrlArtifactBusinessModel"/>, uploads a new BlockBlob in <see cref="IBlobStorage"/>.
	/// If <paramref name="shouldKeepExistingBlob"/> is false (the default), the existing BlockBlob is deleted after the new one is uploaded
	/// and saved.
	/// </summary>
	/// <param name="artifact">The artifact to update</param>
	/// <param name="requestModel">A request model to create a new artifact</param>
	/// <param name="artifactStorage">The blob storage</param>
	/// <param name="dbContext">The db context</param>
	/// <param name="cancellationToken">A cancellation token</param>
	/// <param name="shouldKeepExistingBlob">Whether or not to delete the existing blob after the new one is uploaded and saved</param>
	/// <returns>The updated and saved <see cref="Artifact"/></returns>
	/// <exception cref="ArgumentNullException"></exception>
	/// <exception cref="ValidationException"></exception>
	/// <exception cref="InvalidEnumArgumentException"></exception>
	public static async Task<Artifact> UpdateArtifactAsync(
		Artifact artifact,
		IArtifactBusinessModel requestModel,
		IBlobStorage artifactStorage,
		IBaseDbContext dbContext,
		CancellationToken cancellationToken = default,
		bool shouldKeepExistingBlob = false)
	{
		if (artifact == null) throw new ArgumentNullException(nameof(artifact));
		if (requestModel == null) throw new ArgumentNullException(nameof(requestModel));
		if (artifactStorage == null) throw new ArgumentNullException(nameof(artifactStorage));
		if (dbContext == null) throw new ArgumentNullException(nameof(dbContext));

		switch (requestModel)
		{
			case TextArtifactBusinessModel t:
				if (artifact is not TextArtifact textArtifact)
				{
					throw new ValidationException(Strings.UpdateArtifactTypeMismatch);
				}

				// Grab the existing blob path, for use later if needed
				var existingTextBlobPath = GetTextBlobPathFromUrl(textArtifact.Url);

				// Upload a new blob
				var textBlobPath = GenerateTextBlobPath();
				var sanitizedText = ArtifactHtmlSanitizeSettings.Sanitize(t.Text);
				await using (var textStream = GenerateStreamFromString(sanitizedText))
				{
					await artifactStorage.UploadBlockBlobAsync(textStream, textBlobPath, PlainTextContentType, cancellationToken);
				}

				// Update the artifact and save
				textArtifact.WordCount = t.WordCount;
				textArtifact.Url = artifactStorage.UrlForBlockBlob(textBlobPath);
				await dbContext.SaveChangesAsync(cancellationToken);

				// Delete the old blob, unless specified to keep. ONLY after the new blob is uploaded and its url is saved
				if (!shouldKeepExistingBlob)
				{
					await artifactStorage.DeleteBlockBlobAsync(existingTextBlobPath, cancellationToken);
				}

				return textArtifact;

			case FileArtifactBusinessModel f:
				if (artifact is not FileArtifact fileArtifact)
				{
					throw new ValidationException(Strings.UpdateArtifactTypeMismatch);
				}

				// Grab the existing blob path, for use later if needed
				var existingFileBlobPath = GetFileBlobPathFromUrl(fileArtifact.Url);

				// Upload a new blob
				var filename = f.FileName;
				var fileBlobPath = GenerateFileBlobPath(filename);
				await using (var fileStream = f.File.OpenReadStream())
				{
					await artifactStorage.UploadBlockBlobAsync(fileStream, fileBlobPath, f.ContentType, cancellationToken);
				}

				// Update the artifact and save
				fileArtifact.FileName = filename;
				fileArtifact.Url = artifactStorage.UrlForBlockBlob(fileBlobPath);
				await dbContext.SaveChangesAsync(cancellationToken);

				// Delete the old blob, unless specified to keep. ONLY after the new blob is uploaded and its url is saved
				if (!shouldKeepExistingBlob)
				{
					await artifactStorage.DeleteBlockBlobAsync(existingFileBlobPath, cancellationToken);
				}

				return fileArtifact;

			case UrlArtifactBusinessModel u:
				if (artifact is not UrlArtifact urlArtifact)
				{
					throw new ValidationException(Strings.UpdateArtifactTypeMismatch);
				}

				urlArtifact.Url = u.Url;
				await dbContext.SaveChangesAsync(cancellationToken);
				return urlArtifact;

			default:
				throw new InvalidEnumArgumentException();
		}
	}

	public static async Task UpdateArtifactContentsAsync(Artifact artifact, IArtifactBusinessModel requestModel,
		IBlobStorage artifactStorage, IBaseDbContext dbContext, CancellationToken cancellationToken = default)
	{
		ArgumentNullException.ThrowIfNull(artifact);
		ArgumentNullException.ThrowIfNull(requestModel);
		ArgumentNullException.ThrowIfNull(artifactStorage);
		ArgumentNullException.ThrowIfNull(dbContext);

		switch (requestModel)
		{
			case TextArtifactBusinessModel t:
				if (artifact is not TextArtifact textArtifact)
				{
					throw new ValidationException(Strings.UpdateArtifactTypeMismatch);
				}

				// Grab the existing blob path
				var textBlobPath = GetTextBlobPathFromUrl(textArtifact.Url);

				// Replace blob contents
				var sanitizedText = ArtifactHtmlSanitizeSettings.Sanitize(t.Text);
				await using (var textStream = GenerateStreamFromString(sanitizedText))
				{
					await artifactStorage.UploadBlockBlobAsync(textStream, textBlobPath, PlainTextContentType, cancellationToken);
				}

				// Update the artifact and save
				textArtifact.WordCount = t.WordCount;
				// We don't have a DateTimeProvider here in this static method, so this marks the entity as modified
				// so the LastUpdatedDate gets set to the current time, even if word count hasn't changed.
				((DbContext)dbContext).Entry(textArtifact).State = EntityState.Modified;
				await dbContext.SaveChangesAsync(cancellationToken);

				return;

			case FileArtifactBusinessModel f:
				if (artifact is not FileArtifact fileArtifact)
				{
					throw new ValidationException(Strings.UpdateArtifactTypeMismatch);
				}

				// Grab the existing blob path
				var fileBlobPath = GetFileBlobPathFromUrl(fileArtifact.Url);

				// Replace blob contents
				await using (var fileStream = f.File.OpenReadStream())
				{
					await artifactStorage.UploadBlockBlobAsync(fileStream, fileBlobPath, f.ContentType, cancellationToken);
				}

				// Update the artifact and save
				fileArtifact.FileName = f.FileName;
				// We don't have a DateTimeProvider here in this static method, so this marks the entity as modified
				// so the LastUpdatedDate gets set to the current time, even if the file name hasn't changed.
				((DbContext)dbContext).Entry(fileArtifact).State = EntityState.Modified;
				await dbContext.SaveChangesAsync(cancellationToken);

				return;

			// We aren't updating "contents" per se, but this is here for compatibility with the UpdateArtifactAsync
			case UrlArtifactBusinessModel u:
				if (artifact is not UrlArtifact urlArtifact)
				{
					throw new ValidationException(Strings.UpdateArtifactTypeMismatch);
				}

				urlArtifact.Url = u.Url;
				await dbContext.SaveChangesAsync(cancellationToken);
				return;

			default:
				throw new InvalidEnumArgumentException();
		}
	}

	/// <summary>
	/// Reads the BlockBlob from <see cref="IBlobStorage"/> for a <see cref="FileArtifact"/> to the given <see cref="Stream"/>.
	/// </summary>
	/// <param name="artifact">The artifact to read</param>
	/// <param name="stream">The target stream</param>
	/// <param name="artifactStorage">The blob storage</param>
	/// <param name="cancellationToken">A cancellation token</param>
	/// <returns></returns>
	/// <exception cref="ArgumentNullException"></exception>
	public static async Task StreamFileArtifactAsync(FileArtifact artifact, Stream stream, IBlobStorage artifactStorage,
		CancellationToken cancellationToken = default)
	{
		if (artifact == null)
			throw new ArgumentNullException(nameof(artifact));
		if (artifact.Url == null)
			throw new ArgumentException(Strings.ArtifactUrlRequired, nameof(artifact));
		if (stream == null)
			throw new ArgumentNullException(nameof(stream));
		if (artifactStorage == null)
			throw new ArgumentNullException(nameof(artifactStorage));
		var blobPath = GetFileBlobPathFromUrl(artifact.Url);
		await artifactStorage.WriteBlockBlobToStreamAsync(stream, blobPath, cancellationToken);
	}

	/// <summary>
	/// Asserts that the file extension in the <paramref name="businessModel"/> is allowed by <see cref="AllowedFileExtensionsPattern"/>.
	/// If not a <see cref="FileArtifactBusinessModel"/>, returns silently.
	/// </summary>
	/// <param name="businessModel">The business model to check</param>
	/// <exception cref="ArgumentNullException"></exception>
	/// <exception cref="ValidationException"></exception>
	public static void AssertAssignmentFileExtensionIsValid(IArtifactBusinessModel businessModel)
	{
		if (businessModel == null) throw new ArgumentNullException(nameof(businessModel));

		if (businessModel is not FileArtifactBusinessModel fileArtifactBusinessModel)
		{
			return;
		}

		var currentFileExtension = Path.GetExtension(fileArtifactBusinessModel.FileName)?.ToLowerInvariant();
		if (currentFileExtension == null || !Regex.IsMatch(currentFileExtension, AllowedFileExtensionsPattern))
		{
			throw new ValidationException(Strings.FileArtifactExtensionNotAllowed);
		}
	}

	/// <summary>
	/// Asserts that the file extension in the <paramref name="businessModel"/> is allowed by the given <paramref name="allowedFileExtensions"/>.
	/// If not a <see cref="FileArtifactBusinessModel"/>, returns silently.
	/// </summary>
	/// <param name="businessModel">The business model to check</param>
	/// <param name="allowedFileExtensions">The allowed file extensions in a comma separated list</param>
	/// <exception cref="ArgumentNullException"></exception>
	/// <exception cref="ValidationException"></exception>
	public static void AssertFileExtensionIsValid(IArtifactBusinessModel businessModel, string allowedFileExtensions)
	{
		if (businessModel == null) throw new ArgumentNullException(nameof(businessModel));
		if (string.IsNullOrWhiteSpace(allowedFileExtensions)) throw new ArgumentNullException(nameof(allowedFileExtensions));

		if (businessModel is not FileArtifactBusinessModel fileArtifactBusinessModel)
		{
			return;
		}

		var allowedFileExtensionParts = allowedFileExtensions.Split(',').ToList();
		var currentFileExtension = Path.GetExtension(fileArtifactBusinessModel.FileName)?.ToLowerInvariant();
		if (currentFileExtension == null || !allowedFileExtensionParts.Contains(currentFileExtension))
		{
			throw new ValidationException(Strings.FileArtifactExtensionNotAllowed);
		}
	}

	/// <summary>
	/// Copy the BlockBlob for the <paramref name="source"/> <see cref="Artifact"/>,
	/// update the Url of the <paramref name="destination"/>,
	/// and return the new blob's URI.
	/// </summary>
	/// <param name="source">The artifact from which to copy the blob</param>
	/// <param name="destination">The artifact to update with the copied blob's URI</param>
	/// <param name="artifactStorage">The blob storage</param>
	/// <param name="targetBlobStorage">The optional storage account that the new artifacts will be created in</param>
	/// <param name="targetCreatedById">
	/// The user id to be associated with the new blobs if <paramref name="targetBlobStorage"/>
	/// is provided
	/// </param>
	/// <param name="cancellationToken">A cancellation token</param>
	/// <returns>The new copies blob's URI, or null</returns>
	public static async Task<string> CopyBlobAsync(
		Artifact source,
		Artifact destination,
		IBlobStorage artifactStorage,
		IBlobStorage targetBlobStorage = null,
		string targetCreatedById = null,
		CancellationToken cancellationToken = default)
	{
		if (source == null) throw new ArgumentNullException(nameof(source));
		if (destination == null) throw new ArgumentNullException(nameof(destination));
		if (artifactStorage == null) throw new ArgumentNullException(nameof(artifactStorage));
		if (!destination.GetType().EntityTypeEquals(source.GetType())) throw new ArgumentException(Strings.ArtifactTypesMustMatch);
		if (targetBlobStorage != null && string.IsNullOrWhiteSpace(targetCreatedById))
			throw new ArgumentNullException(nameof(targetCreatedById));

		var sourcePath = "";
		var destinationPath = "";
		var shouldCopy = false;
		var contentType = "";
		switch (source)
		{
			case TextArtifact t:
				sourcePath = GetTextBlobPathFromUrl(t.Url);
				destinationPath = GenerateTextBlobPath();
				shouldCopy = true;
				contentType = PlainTextContentType;
				break;

			case FileArtifact f:
				sourcePath = GetFileBlobPathFromUrl(f.Url);
				destinationPath = GenerateFileBlobPath(f.FileName);
				shouldCopy = true;
				var mimeType = MimeTypes.GetMimeType(f.FileName);
				contentType = new MediaTypeHeaderValue(mimeType).ToString();
				break;
		}

		if (!shouldCopy) return null;
		if (targetBlobStorage == null)
		{
			await artifactStorage.CopyBlockBlobAsync(sourcePath, destinationPath, cancellationToken);
		}
		else
		{
			var sourceBlobBytes = await artifactStorage.ReadAllBytesAsync(sourcePath, cancellationToken);
			await using var stream = new MemoryStream(sourceBlobBytes);
			await targetBlobStorage.UploadBlockBlobAsync(stream, destinationPath, contentType, cancellationToken);
			destination.CreatedById = targetCreatedById;
		}

		var url = targetBlobStorage == null
			? artifactStorage.UrlForBlockBlob(destinationPath)
			: targetBlobStorage.UrlForBlockBlob(destinationPath);
		destination.Url = url;
		return url;
	}

	/// <summary>
	/// Delete a BlockBlob from <see cref="IBlobStorage"/> for the given <see cref="Artifact"/>.
	/// Throws an exception if <paramref name="artifact"/> is <see cref="UrlArtifact"/>.
	/// </summary>
	/// <param name="artifact">The artifact with the blob to delete</param>
	/// <param name="artifactStorage">The blob storage</param>
	/// <param name="cancellationToken">A cancellation token</param>
	/// <returns></returns>
	/// <exception cref="ArgumentNullException"></exception>
	/// <exception cref="NotSupportedException"></exception>
	/// <exception cref="InvalidEnumArgumentException"></exception>
	public static async Task DeleteBlobAsync(
		Artifact artifact,
		IBlobStorage artifactStorage,
		CancellationToken cancellationToken = default)
	{
		if (artifact == null) throw new ArgumentNullException(nameof(artifact));
		if (artifactStorage == null) throw new ArgumentNullException(nameof(artifactStorage));

		switch (artifact)
		{
			case TextArtifact t:
				var existingTextBlobPath = GetTextBlobPathFromUrl(t.Url);
				await artifactStorage.DeleteBlockBlobAsync(existingTextBlobPath, cancellationToken);
				break;

			case FileArtifact f:
				var existingFileBlobPath = GetFileBlobPathFromUrl(f.Url);
				await artifactStorage.DeleteBlockBlobAsync(existingFileBlobPath, cancellationToken);
				break;

			case UrlArtifact _:
				throw new NotSupportedException();
			default:
				throw new InvalidEnumArgumentException();
		}
	}

	/// <summary>
	/// <para>
	/// Return if a string contains any content, classified as:
	/// <list type="bullet">
	/// <item>text</item>
	/// <item>image tags</item>
	/// <item>iframe tags</item>
	/// </list>
	/// </para>
	/// All other html tags are stripped
	/// </summary>
	/// <param name="artifactText">A string of text, e.g. from a <see cref="TextArtifact"/></param>
	/// <returns>Whether the string contains any content</returns>
	public static bool IsTextNullOrEmpty(string artifactText)
	{
		if (string.IsNullOrWhiteSpace(artifactText)) return true;
		var htmlDoc = new HtmlDocument();
		htmlDoc.LoadHtml(artifactText);
		var innerText = htmlDoc.DocumentNode.InnerText;
		var images = htmlDoc.DocumentNode.SelectNodes("//img");
		var iframes = htmlDoc.DocumentNode.SelectNodes("//iframe");
		var variables = htmlDoc.DocumentNode.SelectNodes("//span[@class='variable' and @data-value]");
		var formulas = htmlDoc.DocumentNode.SelectNodes("//span[contains(@class, 'ql-formula') and @data-value]");
		// no content if no text, no images, no variables, no formulas, and no iframes
		return string.IsNullOrWhiteSpace(innerText) && images?.Any() != true && iframes?.Any() != true && variables?.Any() != true &&
				formulas?.Any() != true;
	}

	public static string GetFileBlobPathFromUrl(string url)
	{
		if (string.IsNullOrWhiteSpace(url)) throw new ArgumentNullException(nameof(url));
		var match = Regex.Match(url, $"({FileStorageLocation}.+$)");
		if (match.Groups.Count != 2 || string.IsNullOrEmpty(match.Groups[1].Value))
			throw new ArgumentException(Strings.ArtifactUrlInvalid, nameof(url));
		return match.Groups[1].Value;
	}

	public static string GetTextBlobPathFromUrl(string url)
	{
		if (string.IsNullOrWhiteSpace(url)) throw new ArgumentNullException(nameof(url));
		var match = Regex.Match(url, $"({TextStorageLocation}.+$)");
		if (match.Groups.Count != 2 || string.IsNullOrEmpty(match.Groups[1].Value))
			throw new ArgumentException(Strings.ArtifactUrlInvalid, nameof(url));
		return match.Groups[1].Value;
	}

	public static async Task<string> ExtractTextFromArtifactAsync(Func<IHttpClient> httpClientFactory, Artifact artifact,
		CancellationToken cancellationToken = default)
	{
		if (httpClientFactory == null) throw new ArgumentNullException(nameof(httpClientFactory));
		if (artifact == null) throw new ArgumentNullException(nameof(artifact));
		if (artifact is UrlArtifact) throw new ArgumentException(Strings.UrlArtifactNotSupported, nameof(artifact));

		var httpClient = httpClientFactory();
		var fileBytes = await httpClient.GetByteArrayAsync(new Uri(artifact.Url), cancellationToken);
		using var stream = new MemoryStream(fileBytes);
		return artifact switch
		{
			FileArtifact fileArtifact when fileArtifact.FileName.EndsWith(".docx") => ExtractTextFromDocx(stream),
			FileArtifact fileArtifact when fileArtifact.FileName.EndsWith(".pdf") => ExtractTextFromPdf(stream),
			TextArtifact => ExtractTextFromQuillHtml(stream),
			_ => throw new Exception(Strings.ArtifactExtractTextError)
		};
	}

	#region Private Methods

	private static string GenerateTextBlobPath()
	{
		return $"{TextStorageLocation}{GeneralTextArtifactFileName}-{Guid.NewGuid()}{GeneralTextArtifactFileExtension}";
	}

	private static string GenerateFileBlobPath(string fileName)
	{
		var fileExtension = Path.GetExtension(fileName);
		return $"{FileStorageLocation}{Guid.NewGuid()}{fileExtension}";
	}

	private static Stream GenerateStreamFromString(string content)
	{
		var stream = new MemoryStream();
		var writer = new StreamWriter(stream);
		writer.Write(content);
		writer.Flush();
		stream.Position = 0;
		return stream;
	}

	#region Docx

	private class AbstractNumTracking
	{
		public Dictionary<int, int> LevelTrackingDict { get; } = new();

		public int PreviousLevelIndex { get; set; } = -1;
	}

	private static string ExtractTextFromDocx(Stream stream)
	{
		var stringBuilder = new StringBuilder();
		using var document = WordprocessingDocument.Open(stream, false);
		if (document.MainDocumentPart == null) return null;
		var numberingDictionary = new Dictionary<int, AbstractNumTracking>();
		ExtractTextFromElement(stringBuilder, document.MainDocumentPart, numberingDictionary, document.MainDocumentPart.Document);
		return stringBuilder.ToString();
	}

	private static void ExtractTextFromElement(StringBuilder stringBuilder, MainDocumentPart mainDocumentPart,
		IDictionary<int, AbstractNumTracking> numberingDictionary, OpenXmlElement element)
	{
		if (element.HasChildren)
		{
			var p = element as Paragraph;
			var didAddNumbering = p != null && AddNumberingIfNeeded(stringBuilder, mainDocumentPart, numberingDictionary, p);
			if (p != null && !didAddNumbering)
			{
				var indentation = p.ParagraphProperties?.Indentation;
				if (indentation != null)
					AppendIndentation(indentation, stringBuilder);
			}

			foreach (var child in element.ChildElements)
			{
				ExtractTextFromElement(stringBuilder, mainDocumentPart, numberingDictionary, child);
			}

			switch (element)
			{
				case TableCell:
				case Paragraph:
					stringBuilder.Append(Environment.NewLine);
					break;
			}
		}
		else
			switch (element)
			{
				case Text text:
					stringBuilder.Append(text.InnerText);
					break;
				case Paragraph: // paragraph with no content
				case Break:
				case CarriageReturn:
					stringBuilder.Append(Environment.NewLine);
					break;
				case TabChar:
					stringBuilder.Append('\t');
					break;
				case NonVisualDrawingProperties p:
					if (p.Description != null)
						stringBuilder.Append($"image with alt text: {p.Description}");
					break;
				case VerticalTextAlignment va:
					if (va.Val == VerticalPositionValues.Superscript)
						stringBuilder.Append('^');
					else if (va.Val == VerticalPositionValues.Subscript)
						stringBuilder.Append('_');
					break;
			}
	}

	private static bool AddNumberingIfNeeded(StringBuilder stringBuilder, MainDocumentPart mainDocumentPart,
		IDictionary<int, AbstractNumTracking> numberingDictionary, Paragraph paragraph)
	{
		// only handle paragraph's with style "ListParagraph"
		if (!paragraph.IsListParagraph()) return false;

		// find the numbering definitions
		// Paragraph references => NumberingInstance => AbstractNum
		// AbstractNum defines a collection of Levels
		// each Level defines a different style / indent level
		var numberingProperties = paragraph.ParagraphProperties?.GetFirstChild<NumberingProperties>();
		var numberingId = numberingProperties?.NumberingId?.Val;
		var numberingLevel = numberingProperties?.NumberingLevelReference?.Val;
		var numberingDefinitionsPart = mainDocumentPart.NumberingDefinitionsPart;
		var numberingInstance =
			(NumberingInstance)numberingDefinitionsPart?.Numbering.FirstOrDefault(e =>
				e is NumberingInstance ni && ni.NumberID == numberingId);
		var abstractNum = (AbstractNum)numberingDefinitionsPart?.Numbering.FirstOrDefault(e =>
			e is AbstractNum an && an.AbstractNumberId == numberingInstance?.AbstractNumId?.Val);
		var level = (Level)abstractNum?.FirstOrDefault(e => e is Level l && l.LevelIndex == numberingLevel);

		// return if missing any level or required properties/children
		if (abstractNum?.AbstractNumberId == null || level?.LevelIndex?.Value == null || level.LevelText?.Val == null ||
			level.NumberingFormat?.Val == null) return false;

		// insert tab for each level of indentation. default to "0" for no indentation.
		var indentation = level.PreviousParagraphProperties?.GetFirstChild<Indentation>();
		AppendIndentation(indentation, stringBuilder);

		var levelIndexValue = level.LevelIndex.Value;
		var levelTextValue = level.LevelText.Val.Value;
		var numberingFormatValue = level.NumberingFormat.Val;
		var levelStartNumberingValue = level.StartNumberingValue?.Val?.Value ?? 1;

		// get the numbering definition + level running counts, for incrementing the numbering value between levels
		var abstractNumTracking = numberingDictionary.TryGetValue(abstractNum.AbstractNumberId.Value, out var existingAbstractNumDict)
			? existingAbstractNumDict
			: new AbstractNumTracking();
		var levelNumberingValue = abstractNumTracking.PreviousLevelIndex < level.LevelIndex
			// numbering is restarted when under a new parent nesting level (previous level is a lower index)
			? levelStartNumberingValue
			: abstractNumTracking.LevelTrackingDict.TryGetValue(level.LevelIndex, out var existingLevelNumberingValue)
				? existingLevelNumberingValue
				: levelStartNumberingValue;

		// append the value for the list item
		if (numberingFormatValue == NumberFormatValues.Bullet)
		{
			stringBuilder.Append("* ");
		}
		else
		{
			string levelChar;
			if (numberingFormatValue.Value == NumberFormatValues.UpperLetter)
			{
				levelChar = levelNumberingValue.OrdinalToAlphaLabel().ToUpper();
			}
			else if (numberingFormatValue.Value == NumberFormatValues.LowerLetter)
			{
				levelChar = levelNumberingValue.OrdinalToAlphaLabel();
			}
			else if (numberingFormatValue.Value == NumberFormatValues.UpperRoman)
			{
				levelChar = levelNumberingValue.OrdinalToRomanNumeralLabel().ToUpper();
			}
			else if (numberingFormatValue.Value == NumberFormatValues.LowerRoman)
			{
				levelChar = levelNumberingValue.OrdinalToRomanNumeralLabel();
			}
			else
			{
				// default to basic number / decimal format
				levelChar = levelNumberingValue.ToString();
			}

			stringBuilder.Append(levelTextValue?.Replace($"%{levelIndexValue + 1}", levelChar));
			stringBuilder.Append(' ');
		}

		levelNumberingValue++;
		// keep track of the numbering definition + level running counts, for incrementing the numbering value between levels
		abstractNumTracking.PreviousLevelIndex = levelIndexValue;
		abstractNumTracking.LevelTrackingDict[levelIndexValue] = levelNumberingValue;
		numberingDictionary[abstractNum.AbstractNumberId.Value] = abstractNumTracking;

		return true;
	}

	private static bool IsListParagraph(this Paragraph paragraph)
	{
		return paragraph.ParagraphProperties?.ParagraphStyleId?.Val == "ListParagraph";
	}

	private static void AppendIndentation(Indentation indentation, StringBuilder stringBuilder)
	{
		var indentValue = int.Parse((indentation?.Left?.HasValue == true ? indentation.Left.Value : "0") ?? "0");
		var indentLevel = indentValue / 360;
		for (var i = 1; i < indentLevel; i++)
		{
			stringBuilder.Append('\t');
		}
	}

	#endregion Docx

	#region Pdf

	private static string ExtractTextFromPdf(Stream stream)
	{
		var stringBuilder = new StringBuilder();
		using var document = PdfDocument.Open(stream);
		foreach (var page in document.GetPages())
		{
			// extract based on order in the underlying document with newlines and spaces.
			var text = ContentOrderTextExtractor.GetText(page, true);
			stringBuilder.Append(text);

			// extract based on grouping letters into words.
			// var otherText = string.Join(" ", page.GetWords());
			//stringBuilder.Append(otherText);

			// extract the raw text of the page's content stream.
			// var rawText = page.Text;
			//stringBuilder.Append(rawText);
		}

		return stringBuilder.ToString();
	}

	#endregion Pdf

	#region Html

	private static string ExtractTextFromQuillHtml(Stream stream)
	{
		using var reader = new StreamReader(stream);
		var htmlDoc = new HtmlDocument();
		htmlDoc.Load(stream);
		var stringBuilder = new StringBuilder();
		ExtractTextFromHtmlNode(stringBuilder, htmlDoc.DocumentNode);
		return stringBuilder.ToString();
	}

	private static int GetQuillIndentLevel(HtmlNode node)
	{
		return int.Parse(node.GetClasses().FirstOrDefault(c => c.StartsWith("ql-indent"))?.Replace("ql-indent-", "") ?? "0");
	}

	private static void ExtractTextFromHtmlNode(StringBuilder stringBuilder, HtmlNode node)
	{
		var indentLevel = GetQuillIndentLevel(node);
		// insert tabs for indent level, if any
		for (var i = 0; i < indentLevel; i++)
		{
			stringBuilder.Append('\t');
		}

		// list numbering
		if (node.Name == "li")
		{
			switch (node.ParentNode?.Name)
			{
				case "ul":
					stringBuilder.Append("* ");
					break;
				case "ol":
				{
					// get the correct index for the list item based on the previous items at the same indent level
					// quill puts all nested levels inside a single "ol" and nests using classes
					var listItemIndex = 0;
					var previousSibling = node.PreviousSibling;
					while (previousSibling != null)
					{
						var previousSiblingIndentLevel = GetQuillIndentLevel(previousSibling);
						// numbering is contained under the parent nesting level
						if (previousSiblingIndentLevel < indentLevel)
						{
							break;
						}

						if (previousSiblingIndentLevel == indentLevel)
						{
							listItemIndex++;
						}

						previousSibling = previousSibling.PreviousSibling;
					}

					// get the correct numbering for the indent level
					// quill alternates between numbers > lower letter > lower roman numeral, then repeats
					var listItemOrdinal = listItemIndex + 1;
					var dataValue = node.ParentNode.GetAttributeValue("data-value", "");
					var indentLevelMod = (indentLevel + (dataValue == "alpha" ? 1 : 0)) % 3;
					var listItemChar = indentLevelMod switch
					{
						1 => listItemOrdinal.OrdinalToAlphaLabel(),
						2 => listItemOrdinal.OrdinalToRomanNumeralLabel(),
						// default to number and when mod is "0"
						_ => listItemOrdinal.ToString()
					};
					stringBuilder.Append($"{listItemChar}. ");
					break;
				}
			}
		}

		switch (node.Name)
		{
			// do not recurse "ql-formula" or "variable" children, just append formula value
			case "span" when (node.HasClass("ql-formula") || node.HasClass("variable")):
				stringBuilder.Append(node.GetAttributeValue("data-value", ""));
				break;
			// append image alt text, if any
			case "img":
			{
				var altText = node.GetAttributeValue("alt", "");
				if (!string.IsNullOrWhiteSpace(altText))
				{
					stringBuilder.Append("image with alt text: ");
					stringBuilder.Append(altText);
				}

				break;
			}
			default:
			{
				if (node.HasChildNodes)
				{
					if (node.InnerText.Length > 0)
					{
						switch (node.Name)
						{
							case "sub":
								stringBuilder.Append('_');
								break;
							case "sup":
								stringBuilder.Append('^');
								break;
						}
					}

					foreach (var childNode in node.ChildNodes)
					{
						ExtractTextFromHtmlNode(stringBuilder, childNode);
					}
				}
				else
				{
					stringBuilder.Append(node.InnerText);
				}

				break;
			}
		}

		if (new[] { "ol", "ul", "li" }.Contains(node.Name))
		{
			stringBuilder.Append(Environment.NewLine);
		}

		// double line break
		if (new[] { "h1", "h2", "blockquote", "pre" }.Contains(node.Name))
		{
			stringBuilder.Append(Environment.NewLine);
			stringBuilder.Append(Environment.NewLine);
		}

		if (node.Name == "p")
		{
			stringBuilder.Append(Environment.NewLine);
			// double line break after paragraphs unless inside a table cell, or is the last child of a table cell
			if ((node.ParentNode?.Name != "td" && node.ParentNode?.Name != "th") || node.NextSibling == null)
			{
				stringBuilder.Append(Environment.NewLine);
			}
		}
	}

	#endregion Html

	#endregion Private Methods
}