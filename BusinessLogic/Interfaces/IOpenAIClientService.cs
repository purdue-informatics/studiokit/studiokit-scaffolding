﻿using Azure.AI.OpenAI;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces;

/// <summary>
/// Because <see cref="OpenAIClient"/> is not mockable due to methods returning values that are not instantiable,
/// this wrapper interface exists to allow for mocking of the OpenAIClient.
/// </summary>
public interface IOpenAIClientService
{
	public Task<string> GetChatCompletionsAsync(
		ChatCompletionsOptions chatCompletionsOptions,
		CancellationToken cancellationToken = default);

	public Task<(ICollection<string> Responses, int PromptTokens, int CompletionTokens)> GetToolCompletionsAsync(
		ChatCompletionsOptions chatCompletionsOptions,
		CancellationToken cancellationToken = default);
}