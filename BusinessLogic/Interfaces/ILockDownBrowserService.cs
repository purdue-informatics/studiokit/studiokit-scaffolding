using Microsoft.AspNetCore.Http;
using StudioKit.Data.Entity.Identity.Interfaces;
using System.Collections.Specialized;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces;

public interface ILockDownBrowserService
{
	string GetLaunchUrl(string userId, int? entityId, string redirectPath, string title);

	string GetTestLaunchUrl(string userId);

	Task<NameValueCollection> HandleRestartAsync(string userId, int? entityId, string redirectPath,
		CancellationToken cancellationToken = default);

	Task<(IUser user, int? entityId, string redirectPath, NameValueCollection queryParams)> HandleChallengeAsync(
		string challengeId,
		HttpRequest request,
		CancellationToken cancellationToken = default);
}