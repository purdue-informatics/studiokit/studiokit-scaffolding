﻿using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces;

public interface IInstructorSandboxService<in TUser>
{
	void SetUpInstructorSandbox(TUser user);

	Task SetUpInstructorSandboxAsync(TUser user, CancellationToken cancellationToken = default);
}