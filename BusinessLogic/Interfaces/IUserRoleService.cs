﻿using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using System.Collections.Generic;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces;

public interface IUserRoleService<TUser>
	where TUser : IUser
{
	Task<IEnumerable<UserRoleBusinessModel<TUser>>> GetUserRolesAsync(
		string roleName,
		IPrincipal principal,
		CancellationToken cancellationToken = default);

	Task<CreatedUserRolesBusinessModel<TUser>> CreateUserRolesAsync(
		string roleName,
		List<string> identifiers,
		IPrincipal principal,
		CancellationToken cancellationToken = default);

	Task DeleteUserRoleAsync(
		string userId,
		string roleName,
		IPrincipal principal,
		CancellationToken cancellationToken = default);
}