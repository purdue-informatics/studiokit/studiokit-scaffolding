﻿using StudioKit.Data.Entity.Identity.Interfaces;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces;

public interface IUserSetupService<in TUser> : IDisposable
	where TUser : IUser
{
	Task AddUserToGlobalGroupAsync(string shardKey, TUser user, CancellationToken cancellationToken = default);
}