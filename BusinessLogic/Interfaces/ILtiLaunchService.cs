﻿using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Models;
using System.Collections.Generic;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces;

public interface ILtiLaunchService
{
	Task<LtiLaunch> GetLtiLaunchAsync(int ltiLaunchId, IPrincipal principal, CancellationToken cancellationToken = default);

	Task<(List<ExternalGroup>, List<LtiLaunch>)> GetExternalGroupsToAddAsync(
		BaseGroup group,
		List<ExternalGroup> existingExternalGroups,
		IEnumerable<IExternalGroupEditBusinessModel> externalGroups,
		CancellationToken cancellationToken = default);

	Task<string> GetDeepLinkingResponseAsync(
		int ltiLaunchId,
		DeepLinkingResponseRequest deepLinkingResponseRequest,
		IPrincipal principal,
		CancellationToken cancellationToken = default);
}