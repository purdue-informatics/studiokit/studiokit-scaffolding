using System.Diagnostics.CodeAnalysis;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces;

public interface ILockDownBrowserConfiguration
{
	public string Secret1 { get; }

	public string Secret2 { get; }

	[SuppressMessage("ReSharper", "InconsistentNaming")]
	public string SecretIV { get; }

	public string SecretIndex { get; }

	public string SecretVersion { get; }

	public string DomainAllowList { get; }
}