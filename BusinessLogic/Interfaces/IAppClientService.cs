﻿using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Security.Models;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces;

public interface IAppClientService
{
	Task<AppClient> GetAsync(string clientId, CancellationToken cancellationToken = default);

	Task<AppClient> UpdateAsync(string clientId, AppClientUpdateBusinessModel businessModel, IPrincipal principal,
		CancellationToken cancellationToken = default);
}