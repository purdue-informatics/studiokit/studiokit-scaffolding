﻿using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;

public interface IUserAuthorizationService : IAuthorizationService<string>
{
	Task AssertCanImpersonateAsync(string userId, IPrincipal principal, CancellationToken cancellationToken = default);
}