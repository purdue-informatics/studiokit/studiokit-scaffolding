﻿namespace StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;

public interface IExternalProviderAuthorizationService : IAuthorizationService<int>
{
}