﻿using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;

public interface IExternalGroupAuthorizationService : IAuthorizationService
{
	Task AssertCanReadExternalGroupsAnyAsync(IPrincipal principal, CancellationToken cancellationToken = default);
}