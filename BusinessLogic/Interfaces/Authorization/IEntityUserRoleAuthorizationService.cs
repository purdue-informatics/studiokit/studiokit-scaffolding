﻿using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;

public interface IEntityUserRoleAuthorizationService : IAuthorizationService
{
	Task AssertCanReadEntityUserRolesAsync(int entityId, IPrincipal principal, CancellationToken cancellationToken = default);

	Task AssertCanModifyEntityUserRolesAsync(int entityId, IPrincipal principal, CancellationToken cancellationToken = default);

	Task AssertCanDeleteOwnEntityUserRoleAsync(int entityId, IPrincipal principal,
		CancellationToken cancellationToken = default);
}