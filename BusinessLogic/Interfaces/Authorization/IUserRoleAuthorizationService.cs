﻿using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;

public interface IUserRoleAuthorizationService : IAuthorizationService
{
	Task AssertCanReadAnyAsync(IPrincipal principal, CancellationToken cancellationToken = default);

	Task AssertCanCreateAsync(IPrincipal principal, CancellationToken cancellationToken = default);

	Task AssertCanDeleteAnyAsync(IPrincipal principal, CancellationToken cancellationToken = default);
}