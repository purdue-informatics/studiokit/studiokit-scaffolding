﻿namespace StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;

public interface ILtiLaunchAuthorizationService : IAuthorizationService<int>
{
}