﻿namespace StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;

public interface IIdentityProviderAuthorizationService : IAuthorizationService<int>
{
}