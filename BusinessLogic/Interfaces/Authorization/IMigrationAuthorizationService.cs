﻿using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;

public interface IMigrationAuthorizationService : IAuthorizationService
{
	Task AssertCanReadAsync(IPrincipal principal, CancellationToken cancellationToken = default);

	Task AssertCanMigrateAsync(IPrincipal principal, CancellationToken cancellationToken = default);
}