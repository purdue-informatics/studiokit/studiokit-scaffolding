﻿using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;

public interface IAppClientAuthorizationService : IAuthorizationService
{
	Task AssertCanUpdateAsync(IPrincipal principal, CancellationToken cancellationToken = default);
}