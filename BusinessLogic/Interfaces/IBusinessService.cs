﻿using System.Collections.Generic;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces;

/// <summary>
/// The interface that defines the required CRUD method implementations for all business logic services.
/// </summary>
/// <typeparam name="TKey">The type of unique key for the model (almost always int except .NET identity classes)</typeparam>
/// <typeparam name="TModel">The type of the business model</typeparam>
/// <typeparam name="TCreateModel">The type of the business model used for model creation. May be the same as TModel</typeparam>
/// <typeparam name="TUpdateModel">The type of the business model used for model updating. May be the same as TModel</typeparam>
public interface IBusinessService<in TKey, TModel, in TCreateModel, in TUpdateModel>
	where TModel : class
	where TCreateModel : class
	where TUpdateModel : class
{
	/// <summary>
	/// The method for retrieving all model objects
	/// </summary>
	/// <param name="principal">The user or system principal</param>
	/// <param name="cancellationToken"></param>
	/// <param name="relationIds">
	/// A dictionary, keyed by a string representation of the model name whose value is the
	/// model's id. This can be used to access/update related models in the service
	/// </param>
	/// <returns>An IEnumerable of <see cref="TModel"/> objects</returns>
	Task<IEnumerable<TModel>> GetAllAsync(IPrincipal principal,
		CancellationToken cancellationToken = default, IDictionary<string, int> relationIds = null);

	/// <summary>
	/// The method for retrieving a single model object
	/// </summary>
	/// <param name="entityId">The id of the model object to retrieve</param>
	/// <param name="principal">The user or system principal</param>
	/// <param name="cancellationToken"></param>
	/// <param name="relationIds">
	/// A dictionary, keyed by a string representation of the model name whose value is the
	/// model's id. This can be used to access/update related models in the service
	/// </param>
	/// <returns>An IEnumerable of the business model objects of type <see cref="TModel"/></returns>
	Task<TModel> GetAsync(TKey entityId, IPrincipal principal,
		CancellationToken cancellationToken = default, IDictionary<string, int> relationIds = null);

	/// <summary>
	/// The method for creating and persisting a new model object
	/// </summary>
	/// <param name="businessModel">
	/// The business model providing the properties needed to create and persist an instance
	/// of <see cref="TModel"/>
	/// </param>
	/// <param name="principal">The user or system principal</param>
	/// <param name="cancellationToken"></param>
	/// <param name="relationIds">
	/// A dictionary, keyed by a string representation of the model name whose value is the
	/// model's id. This can be used to access/update related models in the service
	/// </param>
	/// <returns>A business model object of type <see cref="TModel"/></returns>
	Task<TModel> CreateAsync(TCreateModel businessModel, IPrincipal principal,
		CancellationToken cancellationToken = default, IDictionary<string, int> relationIds = null);

	/// <summary>
	/// The method for updating a persisted model object
	/// </summary>
	/// <param name="entityId">The id of the model object to update</param>
	/// <param name="businessModel">
	/// The business model providing the properties needed to update an instance
	/// of <see cref="TModel"/>
	/// </param>
	/// <param name="principal">The user or system principal</param>
	/// <param name="cancellationToken"></param>
	/// <param name="relationIds">
	/// A dictionary, keyed by a string representation of the model name whose value is the
	/// model's id. This can be used to access/update related models in the service
	/// </param>
	/// <returns>A business model object of type <see cref="TModel"/></returns>
	Task<TModel> UpdateAsync(TKey entityId, TUpdateModel businessModel, IPrincipal principal,
		CancellationToken cancellationToken = default, IDictionary<string, int> relationIds = null);

	/// <summary>
	/// The method for deleting an existing, persisted model object
	/// </summary>
	/// <param name="entityId">The id of the model object to update</param>
	/// <param name="principal">The user or system principal</param>
	/// <param name="cancellationToken"></param>
	/// <param name="relationIds">
	/// A dictionary, keyed by a string representation of the model name whose value is the
	/// model's id. This can be used to access/update related models in the service
	/// </param>
	Task DeleteAsync(TKey entityId, IPrincipal principal,
		CancellationToken cancellationToken = default, IDictionary<string, int> relationIds = null);
}

/// <inheritdoc/>
/// <summary>
/// Inherited interface that defaults
/// the key type to integer
/// </summary>
/// <typeparam name="TModel">The type of the business model</typeparam>
/// <typeparam name="TCreateModel">The type of the business model used for model creation. May be the same as TModel</typeparam>
/// <typeparam name="TUpdateModel">The type of the business model used for model updating. May be the same as TModel</typeparam>
public interface IBusinessService<TModel, in TCreateModel, in TUpdateModel>
	: IBusinessService<int, TModel, TCreateModel, TUpdateModel>
	where TModel : class
	where TCreateModel : class
	where TUpdateModel : class
{
}