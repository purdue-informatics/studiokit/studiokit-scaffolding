﻿using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using System.Collections.Generic;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces;

public interface IEntityUserRoleService<TUser, TEntityUserRole>
	where TUser : IUser
	where TEntityUserRole : IEntityUserRole
{
	Task<IEnumerable<EntityUserRoleBusinessModel<TUser, TEntityUserRole>>> GetEntityUserRolesAsync(
		int entityId,
		IPrincipal principal,
		CancellationToken cancellationToken = default);

	Task<CreatedEntityUserRolesBusinessModel<TUser, TEntityUserRole>> CreateEntityUserRolesAsync(
		int entityId,
		List<string> identifiers,
		string roleName,
		IPrincipal principal,
		CancellationToken cancellationToken = default);

	Task<EntityUserRoleBusinessModel<TUser, TEntityUserRole>> UpdateEntityUserRoleAsync(
		int entityUserRoleId,
		string roleName,
		IPrincipal principal,
		CancellationToken cancellationToken = default);

	Task DeleteEntityUserRoleAsync(
		int entityUserRoleId,
		IPrincipal principal,
		CancellationToken cancellationToken = default);
}