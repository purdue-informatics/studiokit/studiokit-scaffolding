﻿using Microsoft.AspNetCore.Authentication;
using StudioKit.Data.Entity.Identity.Interfaces;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces;

public interface IOAuthService
{
	string GetAuthorizationCode(IUser user, List<Claim> claims);

	Task<Dictionary<string, string>> GetTokenAsync(Dictionary<string, string> formData, string userAgent, string ipAddress = null,
		CancellationToken cancellationToken = default);

	Task<AuthenticationTicket> ValidateTokenAsync(string token, CancellationToken cancellationToken = default);
}