﻿namespace StudioKit.Scaffolding.BusinessLogic.Interfaces;

public interface IViewModel<in TModel>
{
	IViewModel<TModel> MapFrom(TModel model);
}