﻿using StudioKit.Scaffolding.DataAccess.Interfaces;
using StudioKit.Scaffolding.Models;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces;

public interface IBaseConfigurationService<TConfiguration> : IConfigurationProvider<TConfiguration>
	where TConfiguration : BaseConfiguration, new()
{
}