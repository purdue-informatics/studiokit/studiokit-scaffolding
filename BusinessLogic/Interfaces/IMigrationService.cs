﻿using StudioKit.Sharding;
using System.Collections.Generic;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces;

public interface IMigrationService
{
	Task<List<MigrationStatus>> GetMigrationStatusesAsync(IPrincipal principal, CancellationToken cancellationToken = default);

	Task<List<MigrationStatus>> MigrateAsync(IPrincipal principal, string targetMigration = null,
		CancellationToken cancellationToken = default);

	Task SeedAsync(IPrincipal principal, CancellationToken cancellationToken = default);
}