﻿using Microsoft.AspNetCore.Identity;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.Scaffolding.Common;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces;

public interface IUserManager<TUser> where TUser : IUser
{
	#region Microsoft.AspNetCore.Identity

	Task<IList<string>> GetRolesAsync(TUser user);

	Task<bool> IsInRoleAsync(TUser user, string roleName);

	Task<IdentityResult> AddToRoleAsync(TUser user, string roleName);

	Task<IdentityResult> RemoveFromRoleAsync(TUser user, string roleName);

	Task<TUser> FindByNameAsync(string userName);

	Task<TUser> FindByIdAsync(string userId);

	Task<TUser> FindByEmailAsync(string email);

	Task<IdentityResult> AddLoginAsync(TUser user, UserLoginInfo login);

	Task<IdentityResult> AddClaimAsync(TUser user, Claim claim);

	Task<IdentityResult> RemoveClaimAsync(TUser user, Claim claim);

	Task<IList<Claim>> GetClaimsAsync(TUser user);

	#endregion Microsoft.AspNetCore.Identity

	#region StudioKit.Scaffolding

	/// <summary>
	/// Persists the given user, claims, and completes other setup logic. Then generates and returns an OAuth Code.
	/// </summary>
	/// <param name="user">The user to sign in. Should contain Email and/or Username.</param>
	/// <param name="claims">The claims for the user, if any.</param>
	/// <param name="userLoginInfo">The login info for the current sign in method.</param>
	/// <param name="shardKey">The shardKey for the db the user is signing into.</param>
	/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
	/// <returns>The updated user and a list of claims</returns>
	Task<(TUser, List<Claim>)> FinishSignInAsync(TUser user, List<Claim> claims, UserLoginInfo userLoginInfo, string shardKey,
		CancellationToken cancellationToken = default);

	Task<bool> CheckPasswordAsync(TUser user, string password);

	Task<IdentityResult> CreateAsync(TUser user, string password);

	Task<TUser> CreateUserAsync(TUser user, CancellationToken cancellationToken = default);

	Task<TUser> CreateOrUpdateAsync(TUser user, CancellationToken cancellationToken = default);

	Task UpdateExistingUserAsync(TUser existingUser, TUser user, CancellationToken cancellationToken = default);

	/// <summary>
	/// Using the provided <paramref name="claims"/>, checks for platform provided Impersonator identifier.
	/// If provided, assert that the impersonator exists and throw a <see cref="ForbiddenException"/> if they do not.
	/// If the user is found, add the <see cref="ScaffoldingClaimTypes.ImpersonatorUserId"/> claim to the claims list.
	/// </summary>
	/// <param name="claims">A list of claims</param>
	/// <exception cref="ForbiddenException"></exception>
	Task AssertAndAddImpersonatorUserIdClaimIfNeededAsync(List<Claim> claims);

	#endregion StudioKit.Scaffolding
}