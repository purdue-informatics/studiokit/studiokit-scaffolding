using StudioKit.Scaffolding.BusinessLogic.Models;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces;

public interface ILockDownBrowserUtils
{
	public string GetLaunchUrl(string data);

	public string GetChallengeValue(LockDownBrowserChallenge challenge);

	public string GetChallengeResponseValue(LockDownBrowserChallenge challenge);

	public string EncryptProctorExitPassword(string password);

	public string DecryptProctorExitPassword(string encryptedPassword);
}