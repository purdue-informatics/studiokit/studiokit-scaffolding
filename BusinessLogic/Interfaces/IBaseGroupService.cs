﻿using Microsoft.AspNetCore.Identity;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Models;
using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces;

public interface
	IBaseGroupService<TGroup, TGroupBusinessModel, in TGroupEditBusinessModel, TUser> : IBusinessService<TGroupBusinessModel,
		TGroupEditBusinessModel, TGroupEditBusinessModel>
	where TUser : IdentityUser, IUser
	where TGroup : BaseGroup, new()
	where TGroupBusinessModel : BaseGroupBusinessModel<TGroup, TUser>, new()
	where TGroupEditBusinessModel : BaseGroupEditBusinessModel<TGroup>, new()
{
	Task<IEnumerable<TGroupBusinessModel>> GetAsync(
		IPrincipal principal,
		string keywords = null,
		DateTime? date = null,
		bool queryAll = false,
		string userId = null,
		bool includeIfSoftDeleted = false,
		CancellationToken cancellationToken = default);

	Task<IEnumerable<ExternalGroupBusinessModel>> GetExternalGroupsAsync(int groupId, IPrincipal principal,
		CancellationToken cancellationToken = default);

	Task SyncRosterAsync(int groupId, IPrincipal principal, CancellationToken cancellationToken = default);

	Task SyncAllRostersAsync(IPrincipal principal, CancellationToken cancellationToken = default);

	Task<TGroupBusinessModel> CopyAsync(TGroupEditBusinessModel businessEditModel, int sourceGroupId, IPrincipal principal,
		CancellationToken cancellationToken = default);
}