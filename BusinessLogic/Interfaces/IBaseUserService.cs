﻿using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Common;
using System.Collections.Generic;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces;

public interface
	IBaseUserService<TUser, TUserBusinessModel, in TUserEditBusinessModel> : IBusinessService<string, TUserBusinessModel,
		TUserEditBusinessModel, TUserEditBusinessModel>
	where TUser : IUser, new()
	where TUserBusinessModel : BaseUserBusinessModel<TUser>, new()
	where TUserEditBusinessModel : BaseUserEditBusinessModel<TUser>, new()
{
	Task<IEnumerable<TUserBusinessModel>> GetAsync(IPrincipal principal, string keywords, CancellationToken cancellationToken = default);

	/// <summary>
	/// <para>Start impersonating a User with the given <paramref name="userId"/>.</para>
	/// Returns an OAuth Code containing a <see cref="ScaffoldingClaimTypes.ImpersonatorUserId"/> Claim,
	/// which the front-end can use to exchange for an AccessToken.
	/// </summary>
	/// <param name="userId">The Id of a User that will be impersonated.</param>
	/// <param name="principal">The current requester</param>
	/// <param name="cancellationToken"></param>
	/// <returns>An OAuth Code response</returns>
	Task<CodeResponse> StartImpersonationAsync(string userId, IPrincipal principal, CancellationToken cancellationToken = default);

	/// <summary>
	/// <para>Stop impersonating a User.</para>
	/// Finds the <see cref="ScaffoldingClaimTypes.ImpersonatorUserId"/> Claim on the <paramref name="principal"/>
	/// and returns an OAuth Code for the Impersonator, which the front-end can use to exchange for an AccessToken.
	/// </summary>
	/// <param name="principal">The current requester</param>
	/// <param name="cancellationToken"></param>
	/// <returns>An OAuth Code response</returns>
	Task<CodeResponse> StopImpersonationAsync(IPrincipal principal, CancellationToken cancellationToken = default);
}