﻿using StudioKit.Scaffolding.BusinessLogic.Models;

namespace StudioKit.Scaffolding.BusinessLogic.Interfaces;

public interface IExternalProviderService : IBusinessService<ExternalProvider.Models.ExternalProvider, ExternalProviderBusinessModel,
	ExternalProviderBusinessModel>
{
}