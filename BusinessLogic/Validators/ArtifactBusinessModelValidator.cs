﻿using FluentValidation;
using StudioKit.Scaffolding.BusinessLogic.Models;

namespace StudioKit.Scaffolding.BusinessLogic.Validators;

public class ArtifactBusinessModelValidator : BaseValidator<IArtifactBusinessModel>
{
	public ArtifactBusinessModelValidator()
	{
		RuleFor(x => x as TextArtifactBusinessModel)
			.SetValidator(new TextArtifactBusinessModelValidator());
		RuleFor(x => x as FileArtifactBusinessModel)
			.SetValidator(new FileArtifactBusinessModelValidator());
		RuleFor(x => x as UrlArtifactBusinessModel)
			.SetValidator(new UrlArtifactBusinessModelValidator());
	}
}

public class FileArtifactBusinessModelValidator : BaseValidator<FileArtifactBusinessModel>
{
	public FileArtifactBusinessModelValidator()
	{
		RuleFor(x => x.FileName).NotEmpty();
		RuleFor(x => x.ContentType).NotEmpty();
		RuleFor(x => x.File.Length).NotEmpty();
	}
}

public class TextArtifactBusinessModelValidator : BaseValidator<TextArtifactBusinessModel>
{
	public TextArtifactBusinessModelValidator()
	{
		RuleFor(x => x.Text).NotEmpty();
		RuleFor(x => x.WordCount).NotEmpty();
	}
}

public class UrlArtifactBusinessModelValidator : BaseValidator<UrlArtifactBusinessModel>
{
	public UrlArtifactBusinessModelValidator()
	{
		RuleFor(x => x.Url).NotEmpty();
	}
}