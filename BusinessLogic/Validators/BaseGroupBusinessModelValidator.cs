using FluentValidation;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Models;

namespace StudioKit.Scaffolding.BusinessLogic.Validators;

public class BaseGroupBusinessModelValidator<TGroup> : BaseValidator<BaseGroupEditBusinessModel<TGroup>>
	where TGroup : BaseGroup, new()

{
	public BaseGroupBusinessModelValidator()
	{
		RuleFor(x => x.Name).NotEmpty();
		When(x => x.ExternalTermId.HasValue, () =>
		{
			RuleFor(x => x.StartDate).Empty();
			RuleFor(x => x.EndDate).Empty();
		});
		When(x => !x.ExternalTermId.HasValue || x.StartDate.HasValue || x.EndDate.HasValue, () =>
		{
			RuleFor(x => x.ExternalTermId).Empty();
			RuleFor(x => x.StartDate).NotEmpty();
			RuleFor(x => x.EndDate).NotEmpty();
			RuleFor(x => x.EndDate).GreaterThan(x => x.StartDate);
		});
	}
}