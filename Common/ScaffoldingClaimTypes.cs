﻿namespace StudioKit.Scaffolding.Common;

public static class ScaffoldingClaimTypes
{
	public const string CasLogin = "StudioKit.Scaffolding.Cas.Login";
	public const string CasPuid = "StudioKit.Scaffolding.Cas.Puid";
	public const string CasI2A2Characteristics = "StudioKit.Cas.I2A2Characteristics";

	public const string ImpersonatorUserId = "StudioKit.Scaffolding.Impersonator.UserId";

	public const string LockDownBrowserEntityId = "StudioKit.Scaffolding.LockDownBrowser.EntityId";

	public const string ClientIsLockDownBrowser = "StudioKit.Scaffolding.LockDownBrowser.ClientIsLockDownBrowser";
}