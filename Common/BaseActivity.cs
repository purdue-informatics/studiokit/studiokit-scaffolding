﻿using StudioKit.Data.Entity.Identity;
using System.Collections.Generic;

namespace StudioKit.Scaffolding.Common;

public static class BaseActivity
{
	public const string UserRoleReadAny = "UserRoleReadAny";
	public const string UserRoleModifyAny = "UserRoleModifyAny";
	public const string UserReadAny = "UserReadAny";
	public const string UserUpdateAny = "UserUpdateAny";
	public const string UserImpersonateAny = "UserImpersonateAny";
	public const string GroupRead = "GroupRead";
	public const string GroupCreate = "GroupCreate";
	public const string GroupUpdate = "GroupUpdate";
	public const string GroupDelete = "GroupDelete";
	public const string GroupUserRoleRead = "GroupUserRoleRead";
	public const string GroupUserRoleModify = "GroupUserRoleModify";
	public const string ExternalProviderModify = "ExternalProviderModify";
	public const string ExternalGroupRead = "ExternalGroupRead";
	public const string ExternalGroupConnectAny = "ExternalGroupConnectAny";
	public const string ExternalGroupConnectOwn = "ExternalGroupConnectOwn";
	public const string LtiLaunchReadOwn = "LtiLaunchReadOwn";
	public const string LtiLaunchReadAny = "LtiLaunchReadAny";
	public const string GroupRosterSyncAll = "GroupRosterSyncAll";
	public const string GroupRosterSync = "GroupRosterSync";
	public const string ConfigurationModify = "ConfigurationModify";
	public const string IdentityProviderModify = "IdentityProviderModify";
	public const string MigrationsRead = "MigrationsRead";
	public const string MigrationsModify = "MigrationsModify";
	public const string AppClientUpdate = "AppClientUpdate";
	public const string GradePushDispatch = "GradePushDispatch";

	public static readonly Dictionary<string, List<string>> BaseActivitiesByRoleDictionary = new()
	{
		{
			BaseRole.SuperAdmin,
			new List<string>
			{
				UserRoleReadAny,
				UserRoleModifyAny,

				UserReadAny,
				UserUpdateAny,
				UserImpersonateAny,

				GroupRead,
				GroupCreate,
				GroupUpdate,
				GroupDelete,

				GroupUserRoleRead,
				GroupUserRoleModify,

				ExternalGroupRead,
				ExternalGroupConnectAny,
				ExternalGroupConnectOwn,

				GroupRosterSyncAll,
				GroupRosterSync,

				ExternalProviderModify,

				LtiLaunchReadAny,

				ConfigurationModify,

				IdentityProviderModify,

				MigrationsRead,
				MigrationsModify,
				AppClientUpdate,

				GradePushDispatch
			}
		},
		{
			BaseRole.DatabaseAdmin,
			new List<string>
			{
				MigrationsRead,
				MigrationsModify,
				AppClientUpdate
			}
		},
		{
			BaseRole.Admin,
			new List<string>
			{
				UserRoleReadAny,
				UserRoleModifyAny,

				UserReadAny,
				UserUpdateAny,
				UserImpersonateAny,

				GroupRead,
				GroupCreate,
				GroupUpdate,
				GroupDelete,

				GroupUserRoleRead,
				GroupUserRoleModify,

				ExternalGroupRead,
				ExternalGroupConnectAny,
				ExternalGroupConnectOwn,

				GroupRosterSyncAll,
				GroupRosterSync,

				ExternalProviderModify,

				LtiLaunchReadAny,

				ConfigurationModify,

				IdentityProviderModify,

				GradePushDispatch
			}
		},
		{
			BaseRole.Creator,
			new List<string>
			{
				GroupCreate,

				ExternalGroupRead,
				ExternalGroupConnectAny,
				ExternalGroupConnectOwn,

				LtiLaunchReadOwn
			}
		},
		{
			BaseRole.GroupOwner,
			new List<string>
			{
				GroupRead,
				GroupUpdate,
				GroupDelete,

				GroupUserRoleRead,
				GroupUserRoleModify,

				ExternalGroupRead,
				ExternalGroupConnectOwn,

				GroupRosterSync,
				GradePushDispatch
			}
		},
		{
			BaseRole.GroupGrader,
			new List<string>
			{
				GroupRead,
				GroupUserRoleRead,
				GradePushDispatch
			}
		},
		{
			BaseRole.GroupLearner,
			new List<string>
			{
				GroupRead
			}
		}
	};
}