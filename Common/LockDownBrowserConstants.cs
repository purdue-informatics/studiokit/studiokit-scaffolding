namespace StudioKit.Scaffolding.Common;

public static class LockDownBrowserConstants
{
	public const string RestartPath = "/api/ldb/restart";

	public const string ChallengePath = "/api/ldb/challenge";

	public const string ChallengeCachePrefix = "ldb:challenge:";

	public const string NonceCachePrefix = "ldb:nonce";

	public const string ChallengeIdKey = "challengeId";

	public const string ExistentialCacheValue = "exists";

	public const string True = "1";

	public const string StandardCalculator = "1";

	public const string ScientificCalculator = "2";

	public const int MaxProctorExitPasswordLength = 30;

	public const string WebLaunchPath = "/ldb-launch";

	public const string WebCheckPath = "/ldb-check";
}