﻿namespace StudioKit.Scaffolding.Common;

public static class ScaffoldingAppSetting
{
	public const string HeadlessCasBaseUrl = "HeadlessCasBaseUrl";

	public const string HeadlessCasHostKey = "HeadlessCasHostKey";

	public const string ArtifactTotpSecretKey = "ArtifactTotpSecretKey";

	public const string ArtifactTotpDurationSeconds = "ArtifactTotpDurationSeconds";

	public const string DataProtectionCertificateThumbprint = "DataProtectionCertificateThumbprint";

	public const string LockDownBrowserSecret1 = "LockDownBrowserSecret1";

	public const string LockDownBrowserSecret2 = "LockDownBrowserSecret2";

	public const string LockDownBrowserSecretIV = "LockDownBrowserSecretIV";

	public const string LockDownBrowserSecretIndex = "LockDownBrowserSecretIndex";

	public const string LockDownBrowserSecretVersion = "LockDownBrowserSecretVersion";

	public const string LockDownBrowserDomainAllowList = "LockDownBrowserDomainAllowList";

	public const string AzureDefaults = "AzureDefaults";

	public const string OpenAiApiKey = "OpenAiApiKey";

	public const string OpenAiEndpoint = "OpenAiEndpoint";

	public const string OpenAiDeploymentName = "OpenAiDeploymentName";
}