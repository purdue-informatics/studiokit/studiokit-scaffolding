using StudioKit.Configuration;
using StudioKit.Encryption;
using StudioKit.Sharding;
using System.Net;

namespace StudioKit.Scaffolding.Common;

public static class PurdueCas
{
	public const string LoginProvider = "https://sso.purdue.edu/idp/profile/cas";

	public static string LoginUrl => $"{LoginProvider}/login";

	public static string LogoutUrl => $"{LoginProvider}/logout";

	public static string ValidateUrl => $"{LoginProvider}/serviceValidate";

	public static string HeadlessCasBaseUrl => EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.HeadlessCasBaseUrl);

	public static string HeadlessCasHostKey => EncryptedConfigurationManager.GetSetting(ScaffoldingAppSetting.HeadlessCasHostKey);

	/// <summary>
	/// Returns the CAS service for the given shard.
	/// Uses the web URL returned by <see cref="ShardDomainConfiguration.BaseUrlWithShardKey"/>,
	/// and appends a trailing slash if needed.
	/// If running locally, uses the DEV web URL as the service.
	/// </summary>
	/// <param name="shardKey">The shard key</param>
	/// <returns></returns>
	public static string GetService(string shardKey)
	{
		var tier = EncryptedConfigurationManager.GetSetting(BaseAppSetting.Tier);
		// if running locally, don't use the local web url as the service, but instead use the dev web url
		var targetTier = tier switch
		{
			Tier.Local => Tier.Dev,
			// TODO - QA: temporarily use PROD service for QA CAS requests, until it is registered as a different service
			Tier.QA => Tier.Prod,
			_ => tier
		};
		// get the web url for the given tier, ignoring the endpoint override e.g. for staging, as that will not be registered as a CAS service
		var webUrl = ShardDomainConfiguration.Instance.BaseUrlWithShardKey(shardKey, true, targetTier, true);
		var service = AppendSlashIfNeeded(webUrl);
		return service;
	}

	/// <summary>
	/// Combines the given url, service, and optional ticket into a URL string.
	/// URL encodes and appends a trailing slash to service if needed.
	/// Assumes the ticket is already encoded.
	/// </summary>
	/// <param name="url"></param>
	/// <param name="service"></param>
	/// <param name="ticket"></param>
	/// <returns></returns>
	public static string ConstructUrl(string url, string service, string ticket = null)
	{
		var encodedService = WebUtility.UrlEncode(AppendSlashIfNeeded(service));
		return $"{url}?service={encodedService}{(ticket != null ? $"&ticket={ticket}" : "")}";
	}

	private static string AppendSlashIfNeeded(string service)
	{
		return $"{service}{(!service.EndsWith("/") ? "/" : "")}";
	}
}