namespace StudioKit.Scaffolding.Common;

public static class LockDownBrowserKey
{
	/// <summary>
	/// Query param added to the challenge url to tell LDB to set cookies in the current domain.
	/// </summary>
	public const string EnableCookies = "rldbsi";

	/// <summary>
	/// Cookie set by LDB to claim it is LDB.
	/// </summary>
	public const string ClientIsLockDownBrowser = "rldbci";

	/// <summary>
	/// Query param added to the challenge url.
	/// </summary>
	public const string ChallengeValue = "rldbacv";

	/// <summary>
	/// Cookie set by LDB in response to a <see cref="ChallengeValue"/> query param.
	/// </summary>
	public const string ChallengeResponseValue = "rldbarv";

	/// <summary>
	/// Cookie set by LDB containing the client build identifier.
	/// </summary>
	public const string BuildId = "rldbid";

	/// <summary>
	/// Cookie set by LDB containing the build date, if the client is on windows.
	/// </summary>
	public const string BuildDateWindows = "rldbbdw";

	/// <summary>
	/// Cookie set by LDB containing the build date, if the client is on mac.
	/// </summary>
	public const string BuildDateMac = "rldbbdm";

	/// <summary>
	/// Cookie set by LDB containing the build date, if the client is on iPad.
	/// </summary>
	public const string BuildDateIpad = "rldbbdi";

	/// <summary>
	/// Query param to set security to low. Blocks access to other applications, but does not restrict navigation to course tools like chat and to search boxes.
	/// </summary>
	public const string EnableSecurityLevelLow = "rldbsl";

	/// <summary>
	/// Query param to set security to medium.
	/// </summary>
	public const string EnableSecurityLevelMedium = "rldbsm";

	/// <summary>
	/// Query param to set security to medium-high. Mirrors the behavior of High security, with the exception that the user is able to close the browser during an exam
	/// with a confirmation prompt. This is the recommended security level for exams.
	/// </summary>
	public const string EnableSecurityLevelMediumHigh = "rldbsp";

	/// <summary>
	/// Query param to set security to high. Users are not allowed to close the page or exit the browser until the server signals the browser that the quiz has been completed.
	/// Refresh is allowed.
	/// </summary>
	public const string EnableSecurityLevelHigh = "rldbsh";

	/// <summary>
	/// Query param to set security to very high. Same as High, but Refresh button is blocked.
	/// </summary>
	public const string EnableSecurityLevelVeryHigh = "rldbsv";

	/// <summary>
	/// Query param to allow programs.
	/// </summary>
	public const string ApplicationAllowList = "rldbwl";

	/// <summary>
	/// Query param to block programs.
	/// </summary>
	public const string ApplicationBlockList = "rldbbl";

	/// <summary>
	/// Query param to have the browser check the block list right away.
	/// </summary>
	public const string CheckApplicationBlockList = "rldbpl";

	/// <summary>
	/// Query param to enable "quiz navigation".
	/// </summary>
	public const string EnableQuizNavigation = "rldbqn";

	/// <summary>
	/// Query param to enable the calculator, 1 = basic, 2 = scientific.
	/// </summary>
	public const string EnableCalculator = "rldbclc";

	/// <summary>
	/// Query param to enable printing.
	/// </summary>
	public const string EnablePrinting = "rldbprt";

	/// <summary>
	/// Cookie set by LDB
	/// </summary>
	public const string ApplicationSwitchCountWindows = "rldbswipe";

	/// <summary>
	/// Cookie set by LDB
	/// </summary>
	public const string ApplicationSwitchCountMac = "rldbswitch";

	/// <summary>
	/// Cookie set by LDB
	/// </summary>
	public const string SleepCountWindows = "rldbsleep";

	/// <summary>
	/// Query param to allow linking to domains.
	/// </summary>
	public const string DomainAllowList = "rldbwn";

	/// <summary>
	/// Query param to set the page title of the browser tab.
	/// </summary>
	public const string PageTitle = "rldbtxt";

	/// <summary>
	/// Query param to set the page title of the browser tab, for page and frame navigations.
	/// </summary>
	public const string AlternatePageTitle = "rldbtxt2";

	/// <summary>
	/// Query param to set a cleartext proctor exit password.
	/// </summary>
	public const string ProctorClearTextExitPassword = "rldbep";

	/// <summary>
	/// Query param to set an encrypted proctor exit password.
	/// </summary>
	public const string ProctorExitPassword = "rldbapw";

	/// <summary>
	/// Query param to tell LDB to close.
	/// </summary>
	public const string ExitBrowser = "rldbxb";
}