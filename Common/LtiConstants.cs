namespace StudioKit.Scaffolding.Common;

public static class LtiConstants
{
	public const string LaunchPath = "/api/lti/1p3/launch";

	public const string WebLaunchPath = "/lti-launch";

	public const string ErrorKey = "_ltierrormsg";
}