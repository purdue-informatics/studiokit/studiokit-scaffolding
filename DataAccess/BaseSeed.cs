using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using StudioKit.Data.Entity.Identity;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.DataAccess.Interfaces;
using StudioKit.Scaffolding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.DataAccess;

public static class BaseSeed
{
	public static async Task SeedDatabaseAsync(IBaseDbContext dbContext, CancellationToken cancellationToken = default)
	{
		await SeedBaseRolesAsync(dbContext, cancellationToken);
		await SeedBaseActivitiesAsync(dbContext, cancellationToken);
		await SeedBaseRoleActivitiesAsync(dbContext, cancellationToken);
	}

	public static async Task SeedBaseRolesAsync(IBaseDbContext dbContext, CancellationToken cancellationToken = default)
	{
		await CreateRolesAsync(dbContext, typeof(BaseRole), cancellationToken);
		await dbContext.SaveChangesAsync(cancellationToken);
	}

	public static async Task SeedBaseActivitiesAsync(IBaseDbContext dbContext, CancellationToken cancellationToken = default)
	{
		await CreateActivitiesAsync(dbContext, typeof(BaseActivity), cancellationToken);
		await dbContext.SaveChangesAsync(cancellationToken);
	}

	public static async Task SeedBaseRoleActivitiesAsync(IBaseDbContext dbContext, CancellationToken cancellationToken = default)
	{
		await AddOrRemoveRoleActivitiesAsync(dbContext, BaseActivity.BaseActivitiesByRoleDictionary, cancellationToken);
		await dbContext.SaveChangesAsync(cancellationToken);
	}

	public static async Task CreateRolesAsync(IBaseDbContext dbContext, Type roleType, CancellationToken cancellationToken = default)
	{
		var lookupNormalizer = new UpperInvariantLookupNormalizer();
		var existingRoleNames = await dbContext.Roles.Select(r => r.Name).ToListAsync(cancellationToken: cancellationToken);
		var rolesToAdd = GetNewNamesForType(roleType, existingRoleNames)
			.Select(roleName => new Role
			{
				Name = roleName,
				NormalizedName = lookupNormalizer.NormalizeName(roleName)
			})
			.ToList();
		if (!rolesToAdd.Any()) return;
		dbContext.Roles.AddRange(rolesToAdd);
	}

	public static async Task CreateActivitiesAsync(IBaseDbContext dbContext, Type activityType,
		CancellationToken cancellationToken = default)
	{
		var existingActivityNames = await dbContext.Activities.Select(a => a.Name).ToListAsync(cancellationToken: cancellationToken);
		var activitiesToAdd = GetNewNamesForType(activityType, existingActivityNames)
			.Select(activityName => new Activity
			{
				Name = activityName
			})
			.ToList();
		if (!activitiesToAdd.Any()) return;
		dbContext.Activities.AddRange(activitiesToAdd);
	}

	public static async Task DeleteOldActivitiesAsync(IBaseDbContext dbContext, CancellationToken cancellationToken = default,
		params Type[] activityTypes)
	{
		var existingActivityNames = await dbContext.Activities.ToListAsync(cancellationToken: cancellationToken);
		var activitiesToDelete = GetOldNamesForType(activityTypes, existingActivityNames).ToList();
		if (!activitiesToDelete.Any()) return;
		dbContext.Activities.RemoveRange(activitiesToDelete);
	}

	public static async Task AddOrRemoveRoleActivitiesAsync(IBaseDbContext dbContext,
		Dictionary<string, List<string>> activitiesByRoleDictionary, CancellationToken cancellationToken = default)
	{
		var existingRoles = await dbContext.Roles.ToListAsync(cancellationToken: cancellationToken);
		var existingActivities = await dbContext.Activities.ToListAsync(cancellationToken: cancellationToken);
		var existingRoleActivities = await dbContext.RoleActivities.ToListAsync(cancellationToken: cancellationToken);

		var newRoleActivities = activitiesByRoleDictionary
			.SelectMany(kvp =>
				kvp.Value.Select(activityName => new RoleActivity
				{
					RoleId = existingRoles.Single(r => r.Name.Equals(kvp.Key)).Id,
					ActivityId = existingActivities.Single(a => a.Name.Equals(activityName)).Id
				})
			)
			.ToList();

		var roleActivitiesToAdd = newRoleActivities
			.Where(newRoleActivity => !existingRoleActivities.Any(existingRoleActivity =>
				existingRoleActivity.ActivityId == newRoleActivity.ActivityId &&
				existingRoleActivity.RoleId == newRoleActivity.RoleId))
			.ToList();
		if (roleActivitiesToAdd.Any())
		{
			dbContext.RoleActivities.AddRange(roleActivitiesToAdd);
		}

		var roleActivitiesToRemove = existingRoleActivities
			.Where(existingRoleActivity => !newRoleActivities.Any(newRoleActivity =>
				newRoleActivity.ActivityId == existingRoleActivity.ActivityId &&
				newRoleActivity.RoleId == existingRoleActivity.RoleId))
			.ToList();
		if (roleActivitiesToRemove.Any())
		{
			dbContext.RoleActivities.RemoveRange(roleActivitiesToRemove);
		}
	}

	private static IEnumerable<string> GetNewNamesForType(IReflect type, ICollection<string> existingNames)
	{
		var typeNames = type
			.GetFields(BindingFlags.Public | BindingFlags.Static)
			.Where(f => f.IsLiteral && !f.IsInitOnly && f.FieldType == typeof(string))
			.Select(x => (string)x.GetRawConstantValue());
		return typeNames
			.Where(name => !existingNames.Contains(name))
			.ToList();
	}

	private static IEnumerable<Activity> GetOldNamesForType(IEnumerable<IReflect> types, IEnumerable<Activity> existingActivities)
	{
		var typeNames = new List<string>();
		foreach (var type in types)
		{
			var names = type
				.GetFields(BindingFlags.Public | BindingFlags.Static)
				.Where(f => f.IsLiteral && !f.IsInitOnly && f.FieldType == typeof(string))
				.Select(x => (string)x.GetRawConstantValue());
			typeNames.AddRange(names);
		}

		return existingActivities
			.Where(activity => !typeNames.Contains(activity.Name))
			.ToList();
	}
}