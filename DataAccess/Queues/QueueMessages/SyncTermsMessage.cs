﻿using StudioKit.Cloud.Storage.Queue;
using System.Runtime.Serialization;

namespace StudioKit.Scaffolding.DataAccess.Queues.QueueMessages;

[DataContract]
public class SyncTermsMessage : AzureQueueMessage
{
	[DataMember]
	public string ShardKey { get; set; }
}