﻿using Microsoft.Extensions.Logging;
using StudioKit.Cloud.Storage.Queue;
using StudioKit.ErrorHandling.Interfaces;
using StudioKit.Scaffolding.DataAccess.Queues.QueueMessages;

namespace StudioKit.Scaffolding.DataAccess.Queues.QueueManagers;

public class SyncGroupRosterQueueManager : AzureQueueManager<SyncGroupRosterMessage>
{
	public SyncGroupRosterQueueManager(ILogger<SyncGroupRosterQueueManager> logger, IErrorHandler errorHandler) : base(logger, errorHandler)
	{
	}
}