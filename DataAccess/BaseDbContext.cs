using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using StudioKit.Data;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Data.Interfaces;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ExternalProvider.DataAccess.Interfaces;
using StudioKit.ExternalProvider.Lti.DataAccess;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Models.Interfaces;
using StudioKit.ExternalProvider.UniTime.BusinessLogic.Utilities;
using StudioKit.ExternalProvider.UniTime.Models;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.DataAccess.Interfaces;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using StudioKit.Scaffolding.Properties;
using StudioKit.Security.BusinessLogic.Interfaces;
using StudioKit.Security.BusinessLogic.Services;
using StudioKit.Security.Entity.Identity.Models;
using StudioKit.Security.Models;
using StudioKit.Sharding;
using StudioKit.Sharding.Entity.Identity;
using StudioKit.Sharding.Entity.Identity.Models;
using StudioKit.Sharding.Interfaces;
using StudioKit.UserInfoService;
using StudioKit.Utilities;
using StudioKit.Utilities.Extensions;
using StudioKit.Utilities.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.DataAccess;

public class BaseDbContext<TUser, TGroup, TConfiguration> :
	UserIdentityShardDbContext<TUser, IdentityProvider, TConfiguration, License>,
	IRosterSyncDbContext<TUser, TGroup, GroupUserRole, ExternalTerm, ExternalGroup, ExternalGroupUser>,
	ITermSyncDbContext<ExternalTerm>,
	ILtiLaunchDbContext<TGroup, GroupUserRole, ExternalTerm, ExternalGroup, ExternalGroupUser>,
	IBaseDbContext
	where TUser : IdentityUser, IUser, new()
	where TGroup : BaseGroup, new()
	where TConfiguration : BaseConfiguration, new()
{
	protected readonly IDateTimeProvider DateTimeProvider;
	protected readonly IPrincipalProvider PrincipalProvider;
	protected ILoggerFactory LoggerFactory;

	public BaseDbContext() : this(ShardDomainConstants.PurdueShardKey, new DateTimeProvider(), new NullPrincipalProvider())
	{
	}

	public BaseDbContext(DbContextOptions options, IDateTimeProvider dateTimeProvider, IPrincipalProvider principalProvider) : base(options)
	{
		DateTimeProvider = dateTimeProvider;
		PrincipalProvider = principalProvider;
		SetSavingChanges();
	}

	public BaseDbContext(DbConnection existingConnection, IDateTimeProvider dateTimeProvider, IPrincipalProvider principalProvider) : base(
		existingConnection)
	{
		DateTimeProvider = dateTimeProvider;
		PrincipalProvider = principalProvider;
		SetSavingChanges();
	}

	public BaseDbContext(string nameOrConnectionString, IDateTimeProvider dateTimeProvider, IPrincipalProvider principalProvider) : base(
		nameOrConnectionString)
	{
		DateTimeProvider = dateTimeProvider;
		PrincipalProvider = principalProvider;
		SetSavingChanges();
	}

	public BaseDbContext(IShardKeyProvider shardKeyProvider, IDateTimeProvider dateTimeProvider, IPrincipalProvider principalProvider)
		: this(shardKeyProvider.GetShardKey(), dateTimeProvider, principalProvider)
	{
	}

	private void SetSavingChanges()
	{
		SavingChanges += (_, _) =>
		{
			var utcNow = DateTimeProvider.UtcNow;

			// Set the LastUpdatedById to either the Principal's UserId or the ImpersonatorUserId if the claim exists
			string lastUpdatedById = null;
			var principal = PrincipalProvider.GetPrincipal();
			if (principal != null)
			{
				lastUpdatedById = principal.Identity.GetUserId();

				if (principal.Identity is ClaimsIdentity claimsIdentity)
				{
					var impersonatorUserIdClaim = claimsIdentity.Claims
						.SingleOrDefault(c => c.Type.Equals(ScaffoldingClaimTypes.ImpersonatorUserId));
					if (impersonatorUserIdClaim != null)
						lastUpdatedById = impersonatorUserIdClaim.Value;
				}
			}

			// Set IAuditable entities' DateStored and DateLastUpdated properties set to the DateTimeProvider's UtcNow
			foreach (var entry in ChangeTracker.Entries<IAuditable>())
			{
				var entity = entry.Entity;
				switch (entry.State)
				{
					case EntityState.Added:
						entity.DateStored = utcNow;
						entity.DateLastUpdated = utcNow;
						if (principal != null)
							entity.LastUpdatedById = lastUpdatedById;
						break;

					case EntityState.Modified:
						entity.DateLastUpdated = utcNow;
						if (principal != null)
							entity.LastUpdatedById = lastUpdatedById;
						break;
				}
			}

			ChangeTracker.DetectChanges();
		};
	}

	protected override void OnModelCreating(ModelBuilder modelBuilder)
	{
		base.OnModelCreating(modelBuilder);

		// apply defaults to each type that applies
		foreach (var entityType in modelBuilder.Model.GetEntityTypes())
		{
			if (typeof(IAuditable).IsAssignableFrom(entityType.ClrType))
			{
				modelBuilder
					.Entity(entityType.ClrType)
					.Property<DateTime>(nameof(IAuditable.DateStored))
					.HasColumnType("datetime")
					.HasDefaultValueSql("(getutcdate())");
				modelBuilder
					.Entity(entityType.ClrType)
					.Property<DateTime>(nameof(IAuditable.DateLastUpdated))
					.HasColumnType("datetime")
					.HasDefaultValueSql("(getutcdate())");
			}

			// set a max length on Discriminators to allow them to be used in indexes
			var discriminator = entityType.FindProperty("Discriminator");
			if (discriminator != null)
			{
				modelBuilder
					.Entity(entityType.ClrType)
					.Property<string>("Discriminator")
					.HasMaxLength(128);
			}

			// datetime instead of datetime2
			foreach (var p in entityType.GetProperties()
						.Where(prop => prop.ClrType == typeof(DateTime) || prop.ClrType == typeof(DateTime?)))
			{
				modelBuilder
					.Entity(entityType.ClrType)
					.Property(p.Name)
					.HasColumnType("datetime");
			}
		}

		modelBuilder.Entity<Activity>(b =>
		{
			b.HasIndex(a => a.Name)
				.IsUnique();
		});

		modelBuilder.Entity<Artifact>(b =>
		{
			// cast IUser to TUser
			b.HasOne(a => (TUser)a.User)
				.WithMany()
				.HasForeignKey(d => d.CreatedById);

			b.HasIndex(e => e.IsDeleted);
		});

		modelBuilder.Entity<BaseGroup>(b =>
		{
			b.ToTable("Groups");

			b.Property(g => g.CreatedById).HasDefaultValueSql("('')");

			// cast IUser to TUser, prevent cascade delete
			b.HasOne(l => (TUser)l.User)
				.WithMany()
				.HasForeignKey(g => g.CreatedById)
				.OnDelete(DeleteBehavior.Restrict);

			b.HasIndex(g => g.IsDeleted);
		});

		modelBuilder.Entity<EntityUserRole>(b =>
		{
			b.Ignore(e => e.EntityId);

			// cast IUser to TUser
			b.HasOne(l => (TUser)l.User);

			b.Property("Discriminator").HasDefaultValueSql("('')");
		});

		modelBuilder.Entity<ExternalGroup>(b =>
		{
			// cast IUser to TUser, prevent cascade delete
			b.HasOne(l => (TUser)l.User)
				.WithMany()
				.OnDelete(DeleteBehavior.Restrict);

			b.HasIndex(e => new { e.GroupId, e.ExternalId, e.ExternalProviderId })
				.IsUnique();
		});

		modelBuilder.Entity<ExternalGroupUser>(b =>
		{
			// cast IUser to TUser
			b.HasOne(l => (TUser)l.User);

			b.HasIndex(e => new { e.ExternalGroupId, e.UserId });

			b.HasIndex(e => e.UserId)
				.IncludeProperties(e => e.ExternalGroupId);
		});

		modelBuilder.Entity<ExternalTerm>(b =>
		{
			b.HasIndex(e => new { e.ExternalProviderId, e.ExternalId });
		});

		modelBuilder.Entity<GroupUserRoleLog>(b =>
		{
			// cast IUser to TUser, define FK
			b.HasOne(l => (TUser)l.User)
				.WithMany()
				.HasForeignKey(e => e.UserId);
		});

		modelBuilder.Entity<LtiLaunch>(b =>
		{
			// cast IUser to TUser, define FK
			b.HasOne(l => (TUser)l.User)
				.WithMany()
				.HasForeignKey(l => l.CreatedById);
		});

		modelBuilder.Entity<Login>(b =>
		{
			// cast IUser to TUser
			b.HasOne(l => (TUser)l.User);
		});

		modelBuilder.Entity<RoleActivity>(b =>
		{
			b.HasIndex(e => new { e.RoleId, e.ActivityId })
				.IsUnique();
		});

		modelBuilder.Entity<ServiceClientRole>(b =>
		{
			// define compound key
			b.HasKey(e => new { e.ClientId, e.RoleId });
		});

		modelBuilder.Entity<UniTimeGroup>(b =>
		{
			// cast IExternalTerm to ExternalTerm, prevent cascade delete
			b.HasOne(l => (ExternalTerm)l.ExternalTerm)
				.WithMany(et => et.UniTimeGroups)
				.OnDelete(DeleteBehavior.Restrict);

			// cast IUser to TUser, allow cascade delete
			b.HasOne(l => (TUser)l.User)
				.WithMany()
				.HasForeignKey(g => g.CreatedById)
				.OnDelete(DeleteBehavior.Cascade);
		});

		// EFCore: ALL BELOW HERE is ONLY for Initial EFCore migration from EF6.
		// ---------------------------------------------------------------------------------------------------------------------------------------
		// Temporarily adding tweaks to make the model match the current DB Schema for initial migration

		//foreach (var entityType in modelBuilder.Model.GetEntityTypes())
		//{
		//	// UserId and RoleId lengths
		//	var userId = entityType.FindProperty("UserId");
		//	if (userId != null && !entityType.ClrType.IsSubclassOf(typeof(BaseNotificationLog<TGroup>))
		//						&& !entityType.ClrType.IsSubclassOf(typeof(UserSetting)))
		//	{
		//		modelBuilder
		//			.Entity(entityType.ClrType)
		//			.Property<string>("UserId")
		//			.HasMaxLength(128);
		//	}

		//	var createdById = entityType.FindProperty("CreatedById");
		//	if (createdById != null)
		//	{
		//		modelBuilder
		//			.Entity(entityType.ClrType)
		//			.Property<string>("CreatedById")
		//			.HasMaxLength(128);
		//	}

		//	var roleId = entityType.FindProperty("RoleId");
		//	if (roleId != null)
		//	{
		//		modelBuilder
		//			.Entity(entityType.ClrType)
		//			.Property<string>("RoleId")
		//			.HasMaxLength(128);
		//	}

		//	// primary key constraint names
		//	var id = entityType.FindDeclaredProperty("Id");
		//	if (id != null)
		//	{
		//		modelBuilder
		//			.Entity(entityType.ClrType)
		//			.HasKey("Id")
		//			.HasName($"PK_dbo.{entityType.GetTableName()}");
		//	}

		//	// foreign key constraint names
		//	foreach (var key in entityType.GetForeignKeys())
		//	{
		//		var constraintName = key.GetConstraintName();
		//		if (!constraintName.Contains("dbo."))
		//		{
		//			var parts = constraintName.Split("_");
		//			var translatedName = $"{string.Join("_dbo.", parts.SkipLast(1))}_{parts.Last()}";
		//			key.SetConstraintName(translatedName);
		//		}
		//	}
		//}

		//// AspNetCore.Identity Models

		//modelBuilder.Entity<Role>(b =>
		//{
		//	b.HasIndex(e => e.Name, "RoleNameIndex")
		//		.IsUnique();
		//});

		//modelBuilder.Entity<TUser>(b =>
		//{
		//	b.HasIndex(e => e.UserName, "UserNameIndex")
		//		.IsUnique();
		//});

		//modelBuilder.Entity<Role>(b =>
		//{
		//	b.Property(r => r.Id).HasMaxLength(128);
		//	b.Property(r => r.Name).IsRequired();
		//	b.Ignore(r => r.NormalizedName);
		//	b.Ignore(r => r.ConcurrencyStamp);
		//});

		//modelBuilder.Entity<UserClaim>(b =>
		//{
		//	b.HasIndex(e => e.UserId)
		//		.HasDatabaseName("IX_UserId");
		//});

		//modelBuilder.Entity<UserLogin>(b =>
		//{
		//	b.Property(r => r.LoginProvider).HasMaxLength(128);
		//	b.Property(r => r.ProviderKey).HasMaxLength(128);
		//	b.Ignore(l => l.ProviderDisplayName);

		//	b.HasKey(e => new { e.LoginProvider, e.ProviderKey, e.UserId })
		//		.HasName("PK_dbo.AspNetUserLogins");

		//	b.HasIndex(e => e.UserId)
		//		.HasDatabaseName("IX_UserId");
		//});

		//modelBuilder.Entity<UserRole>(b =>
		//{
		//	b.HasKey(e => new { e.UserId, e.RoleId })
		//		.HasName("PK_dbo.AspNetUserRoles");

		//	b.HasIndex(e => e.UserId)
		//		.HasDatabaseName("IX_UserId");
		//	b.HasIndex(e => e.RoleId)
		//		.HasDatabaseName("IX_RoleId");
		//});

		//modelBuilder.Ignore<IdentityRoleClaim<string>>();
		//modelBuilder.Ignore<IdentityUserToken<string>>();

		//modelBuilder.Entity<TUser>(b =>
		//{
		//	b.Property(u => u.Id).HasMaxLength(128);
		//	b.Property(u => u.UserName).IsRequired();
		//	b.Ignore(u => u.NormalizedEmail);
		//	b.Ignore(u => u.NormalizedUserName);
		//	b.Ignore(u => u.ConcurrencyStamp);
		//	b.Ignore(u => u.LockoutEnd);
		//});

		//// Old Index/Key/Constraint Names

		//modelBuilder
		//	.Entity<Activity>()
		//	.HasIndex(e => e.Name)
		//	.HasDatabaseName("IX_Name");

		//modelBuilder.Entity<Artifact>(b =>
		//{
		//	b.HasIndex(e => e.IsDeleted)
		//		.HasDatabaseName("IX_Artifact_IsDeleted");

		//	b.HasIndex(a => a.CreatedById)
		//		.HasDatabaseName("IX_CreatedById");
		//});

		//modelBuilder.Entity<BaseGroup>(b =>
		//{
		//	b.HasIndex(e => e.IsDeleted)
		//		.HasDatabaseName("IX_Group_isDeleted");
		//});

		//modelBuilder
		//	.Entity<Client>()
		//	.HasKey(e => e.ClientId)
		//	.HasName("PK_dbo.Clients");

		//modelBuilder
		//	.Entity<ServiceClientRole>()
		//	.HasKey(e => new { e.ClientId, e.RoleId })
		//	.HasName("PK_dbo.ServiceClientRoles");

		//modelBuilder.Entity<LicenseUser>(b =>
		//{
		//	b.HasKey(lu => new { lu.LicenseId, lu.UserId })
		//		.HasName("PK_dbo.LicenseUsers");

		//	b.HasIndex(e => e.LicenseId)
		//		.HasDatabaseName("IX_LicenseId");
		//	b.HasIndex(e => e.UserId)
		//		.HasDatabaseName("IX_UserId");
		//});

		//modelBuilder.Entity<LtiLaunch>(b =>
		//{
		//	b.HasIndex(e => e.CreatedById)
		//		.HasDatabaseName("IX_CreatedById");
		//	b.HasIndex(e => e.ExternalProviderId)
		//		.HasDatabaseName("IX_ExternalProviderId");
		//});

		//modelBuilder.Entity<ExternalGroup>(b =>
		//{
		//	b.HasIndex(eg => eg.UserId)
		//		.HasDatabaseName("IX_UserId");

		//	b.HasIndex(e => new { e.GroupId, e.ExternalId, e.ExternalProviderId })
		//		.IsUnique()
		//		.HasDatabaseName("IX_GroupId_ExternalId_ExternalProviderId");
		//});

		//modelBuilder.Entity<ExternalGroupUser>(b =>
		//{
		//	b.HasIndex(e => new { e.ExternalGroupId, e.UserId })
		//		.HasDatabaseName("IX_ExternalGroupId_UserId");

		//	b.HasIndex(e => e.UserId)
		//		.IncludeProperties(e => e.ExternalGroupId)
		//		.HasDatabaseName("nci_wi_ExternalGroupUsers_F262FC54CB66CCA39309F6BF9A84136C");
		//});

		//modelBuilder.Entity<ExternalTerm>(b =>
		//{
		//	b.HasIndex(e => new { e.ExternalProviderId, e.ExternalId })
		//		.HasDatabaseName("IX_ExternalProviderId_ExternalId");
		//});

		//modelBuilder
		//	.Entity<Login>()
		//	.HasIndex(eg => eg.UserId)
		//	.HasDatabaseName("IX_UserId");

		//modelBuilder.Entity<BaseGroup>(b =>
		//{
		//	b.HasIndex(e => e.CreatedById)
		//		.HasDatabaseName("IX_CreatedById");
		//	b.HasIndex(e => e.ExternalTermId)
		//		.HasDatabaseName("IX_ExternalTermId");
		//	b.HasIndex(e => e.IsDeleted)
		//		.HasDatabaseName("IX_Group_isDeleted");
		//});

		//modelBuilder.Entity<GroupUserRoleLog>(b =>
		//{
		//	b.HasIndex(e => e.UserId)
		//		.HasDatabaseName("IX_UserId");
		//	b.HasIndex(e => e.GroupId)
		//		.HasDatabaseName("IX_GroupId");
		//	b.HasIndex(e => e.RoleId)
		//		.HasDatabaseName("IX_RoleId");
		//});

		//modelBuilder.Entity<RoleActivity>(b =>
		//{
		//	b.HasIndex(e => new { e.RoleId, e.ActivityId })
		//		.IsUnique()
		//		.HasDatabaseName("IX_RoleId_ActivityId");
		//});

		//modelBuilder.Entity<ServiceClientRole>(b =>
		//{
		//	b.HasIndex(e => e.ClientId)
		//		.HasDatabaseName("IX_ClientId");
		//	b.HasIndex(e => e.RoleId)
		//		.HasDatabaseName("IX_RoleId");
		//});

		//modelBuilder.Entity<UniTimeGroup>(b =>
		//{
		//	b.HasIndex(e => e.CreatedById)
		//		.HasDatabaseName("IX_CreatedById");
		//	b.HasIndex(e => e.ExternalProviderId)
		//		.HasDatabaseName("IX_ExternalProviderId");
		//	b.HasIndex(e => e.ExternalTermId)
		//		.HasDatabaseName("IX_ExternalTermId");
		//});
	}

	protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
	{
		base.OnConfiguring(optionsBuilder);
		optionsBuilder.UseLoggerFactory(LoggerFactory);
	}

	#region StudioKit.Data.Entity.Identity

	public virtual DbSet<Login> Logins { get; set; }

	#endregion StudioKit.Data.Entity.Identity

	#region StudioKit.Security.Entity + StudioKit.Security.Entity.Identity

	public virtual DbSet<RefreshToken> RefreshTokens { get; set; }

	public virtual DbSet<Client> Clients { get; set; }

	public virtual DbSet<AppClient> AppClients { get; set; }

	public virtual DbSet<IdentityServiceClient> ServiceClients { get; set; }

	public virtual DbSet<ServiceClientRole> ServiceClientRoles { get; set; }

	#endregion StudioKit.Security.Entity + StudioKit.Security.Entity.Identity

	#region StudioKit.ExternalProvider

	public virtual DbSet<ExternalProvider.Models.ExternalProvider> ExternalProviders { get; set; }

	public virtual DbSet<UniTimeExternalProvider> UniTimeExternalProviders { get; set; }

	public virtual DbSet<LtiExternalProvider> LtiExternalProviders { get; set; }

	public virtual DbSet<ExternalGroup> ExternalGroups { get; set; }

	public virtual DbSet<ExternalGroupUser> ExternalGroupUsers { get; set; }

	public virtual DbSet<LtiLaunch> LtiLaunches { get; set; }

	public virtual DbSet<UniTimeGroup> UniTimeGroups { get; set; }

	#endregion StudioKit.ExternalProvider

	#region ILtiLaunchDbContext

	public virtual void AddExternalGradable(IExternalGradable externalGradable)
	{
		throw new NotImplementedException();
	}

	public virtual Task<IExternalGradable> GetExternalGradableOrDefaultAsync(int gradableId, int? externalGroupId = null,
		string externalId = null, CancellationToken cancellationToken = default)
	{
		throw new NotImplementedException();
	}

	public virtual Task<IGradable> GetGradableAsync(int gradableId, CancellationToken cancellationToken = default)
	{
		throw new NotImplementedException();
	}

	#endregion ILtiLaunchDbContext

	#region Scaffolding

	public virtual DbSet<ExternalTerm> ExternalTerms { get; set; }

	public virtual DbSet<TGroup> Groups { get; set; }

	public virtual DbSet<EntityUserRole> EntityUserRoles { get; set; }

	public virtual DbSet<GroupUserRole> GroupUserRoles { get; set; }

	public virtual DbSet<GroupUserRoleLog> GroupUserRoleLogs { get; set; }

	public virtual DbSet<Activity> Activities { get; set; }

	public virtual DbSet<RoleActivity> RoleActivities { get; set; }

	public virtual DbSet<Artifact> Artifacts { get; set; }

	public virtual DbSet<TextArtifact> TextArtifacts { get; set; }

	public virtual DbSet<UrlArtifact> UrlArtifacts { get; set; }

	public virtual DbSet<FileArtifact> FileArtifacts { get; set; }

	public virtual Func<int, IQueryable<BaseGroup>> GroupsQueryableFunc<T>() where T : ModelBase
	{
		throw new NotImplementedException();
	}

	public IQueryable<int> GroupsQueryableGroupIds<T>(string userId, T entity, params string[] activities)
		where T : ModelBase
	{
		return GroupsQueryableFunc<T>()(entity.Id)
			.SelectMany(g => g.GroupUserRoles)
			.Where(r => r.UserId.Equals(userId))
			.Join(RoleActivities.Where(ra => activities.Contains(ra.Activity.Name)),
				gur => gur.RoleId,
				ra => ra.RoleId,
				(eur, ra) => eur.GroupId)
			.Distinct();
	}

	public async Task<(List<string>, List<string>)> UserEntityRolesAndActivitiesAsync<TEntity, TEntityUserRole>(string userId,
		TEntity entity, CancellationToken cancellationToken = default)
		where TEntity : ModelBase
		where TEntityUserRole : EntityUserRole
	{
		var entityUserRoleIds = (await EntityUserRoles.Where(eur => eur.UserId == userId && eur is TEntityUserRole)
				.ToListAsync(cancellationToken))
			.Where(eur => eur.EntityId == entity.Id)
			.Select(eur => eur.RoleId)
			.ToList();
		var roleNamesAndActivities = await RoleActivities.Where(ra => entityUserRoleIds.Contains(ra.RoleId)).Select(ra => new
		{
			RoleName = ra.Role.Name,
			ActivityName = ra.Activity.Name
		}).ToListAsync(cancellationToken);
		return (roleNamesAndActivities.Select(ra => ra.RoleName).Distinct().ToList(),
			roleNamesAndActivities.Select(o => o.ActivityName).Distinct().ToList());
	}

	public async Task<Dictionary<int, (List<string>, List<string>)>> UserEntityRolesAndActivitiesAsync<TEntity, TEntityUserRole>(
		string userId,
		IQueryable<TEntity> entities, CancellationToken cancellationToken = default)
		where TEntity : ModelBase
		where TEntityUserRole : EntityUserRole
	{
		// load the entity ids
		var entityIds = await entities.Select(e => e.Id).ToListAsync(cancellationToken);

		// load the entity roles for the user
		var entityUserRoles = (await EntityUserRoles
				.Where(eur => eur.UserId == userId && eur is TEntityUserRole)
				.ToListAsync(cancellationToken))
			.Where(eur => entityIds.Contains(eur.EntityId))
			.ToList();

		// for the roles the user has, load the corresponding activities
		var roleIds = entityUserRoles
			.Select(o => o.RoleId)
			.Distinct()
			.ToList();
		var roleNamesAndActivities = (await RoleActivities
				.Where(ra => roleIds.Contains(ra.RoleId))
				.Select(ra => new
				{
					ra.RoleId,
					RoleName = ra.Role.Name,
					ActivityName = ra.Activity.Name,
				})
				.ToListAsync(cancellationToken))
			.GroupBy(ra => new
			{
				Id = ra.RoleId,
				ra.RoleName
			})
			.Select(g => new
			{
				Role = g.Key,
				ActivityNames = g.Select(o => o.ActivityName)
			});

		// join user's entity roles and activities, group distinctly by entity
		var rolesAndActivitiesByEntity = entityUserRoles
			.Join(roleNamesAndActivities,
				eur => eur.RoleId,
				rna => rna.Role.Id,
				(eur, rna) => new
				{
					eur.EntityId,
					rna.Role.RoleName,
					rna.ActivityNames
				})
			.GroupBy(eur => eur.EntityId)
			.ToDictionary(
				g => g.Key,
				g => (g.Select(ar => ar.RoleName).Distinct().ToList(), g.SelectMany(ar => ar.ActivityNames).Distinct().ToList())
			);

		// return entityIds in a dictionary paired with a tuple of roles and activities (if any)
		return entityIds
			.ToDictionary(
				entityId => entityId,
				entityId => !rolesAndActivitiesByEntity.ContainsKey(entityId)
					? (new List<string>(), new List<string>())
					: rolesAndActivitiesByEntity[entityId]);
	}

	#region EntityUserRolesQueryables

	public virtual IQueryable<TUser> GetUsersMatchingKeywordsQueryable(IQueryable<TUser> users, List<string> keywordList)
	{
		return keywordList.Aggregate(
			users,
			(current, k) =>
				current.Where(u =>
					u.Id.ToLower().Contains(k) ||
					u.UserName.ToLower().Contains(k) ||
					u.Email.ToLower().Contains(k) ||
					u.FirstName.ToLower().Contains(k) ||
					u.LastName.ToLower().Contains(k) ||
					u.EmployeeNumber.ToLower().Contains(k) ||
					u.Uid.ToLower().Contains(k)
				)
		);
	}

	public virtual Func<int, string, IQueryable<EntityIdAndRoleId>> EntityUserRoleEntityIdsQueryableFunc<T>()
		where T : ModelBase, IUserRoleAssociatedEntity
	{
		throw new NotImplementedException();
	}

	public IQueryable<int> EntityUserRolesQueryableEntityIds<T>(string userId, T entity, params string[] activities)
		where T : ModelBase, IUserRoleAssociatedEntity
	{
		return EntityUserRoleEntityIdsQueryableFunc<T>()(entity.Id, userId)
			.Join(
				RoleActivities.Where(ra => activities.Contains(ra.Activity.Name)),
				eur => eur.RoleId,
				ra => ra.RoleId,
				(eur, ra) => eur.EntityId)
			.Distinct();
	}

	#endregion EntityUserRolesQueryables

	public async Task<TEntity> FindEntityAsync<TEntity>(int entityKey,
		bool throwIfSoftDeleted = true,
		string notFoundMessage = null,
		CancellationToken cancellationToken = default)
		where TEntity : ModelBase
	{
		var entity = await Set<TEntity>().FindAsync(new object[] { entityKey }, cancellationToken);
		if (entity == null || entity is IDelible delibleEntity && delibleEntity.IsDeleted && throwIfSoftDeleted)
			throw new EntityNotFoundException(notFoundMessage ??
											string.Format(Strings.EntityNotFoundById, typeof(TEntity).Name, entityKey));
		return entity;
	}

	public virtual Task<ILockDownBrowserEntity> GetLockDownBrowserEntityAsync(int entityId,
		CancellationToken cancellationToken = default)
	{
		throw new NotImplementedException();
	}

	#endregion Scaffolding

	#region Repository Business Logic

	public IQueryable<TGroup> GetUserGroupsQueryable(bool queryAll, string userId, bool includeIfSoftDeleted = false)
	{
		if (queryAll)
			return Groups.Where(g => includeIfSoftDeleted || !g.IsDeleted);

		return GroupUserRoles
			.Where(r => r.UserId.Equals(userId))
			.Join(RoleActivities.Where(ra => ra.Activity.Name == BaseActivity.GroupRead),
				gur => gur.RoleId,
				ra => ra.RoleId,
				(eur, ra) => eur.GroupId)
			.Join(Groups.Where(g => includeIfSoftDeleted || !g.IsDeleted),
				groupId => groupId,
				g => g.Id, (groupId, g) => g)
			.Distinct();
	}

	public virtual async Task DeleteGroupAsync(TGroup group, CancellationToken cancellationToken = default)
	{
		group.IsDeleted = true;
		await SaveChangesAsync(cancellationToken);
	}

	public virtual Task OnSyncRosterCompleteAsync(int? groupId, CancellationToken cancellationToken = default)
	{
		return Task.FromResult(0);
	}

	/// <summary>
	/// Get or create a list of <see cref="TUser"/>s. Uses the given <see cref="identifiers"/> as each User's UserName and Email on creation.
	/// </summary>
	/// <param name="identifiers">A list of scoped identifiers, e.g. email address or ePPN.</param>
	/// <param name="configuration"><see cref="TConfiguration"/> used to validate domains in scoped identifiers.</param>
	/// <param name="shardKey">The shard key of the shard database.</param>
	/// <param name="userFactory">A factory that can create a new <see cref="TUser"/>.</param>
	/// <param name="userInfoService">The service needed to call the User Information Service from Purdue.</param>
	/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
	/// <returns>A <see cref="UserSearchResult{TUser}"/> containing which user identifiers were found or created, and which were invalid.</returns>
	public async Task<UserSearchResult<TUser>> GetOrCreateUsersByIdentifiersAsync(
		List<string> identifiers,
		TConfiguration configuration,
		string shardKey,
		IUserFactory<TUser> userFactory,
		IUserInfoService userInfoService = null,
		CancellationToken cancellationToken = default)
	{
		if (shardKey.Equals(ShardDomainConstants.PurdueShardKey) && userInfoService != null)
		{
			var (userInfoUsers, invalidIdentifiers) = userInfoService.GetUserInfoUsersByIdentifiers(userFactory, identifiers);
			var result = await GetOrCreateUsersAsync(userInfoUsers, configuration, cancellationToken);
			result.InvalidIdentifiers = result.InvalidIdentifiers.Union(invalidIdentifiers).ToList();
			return result;
		}

		var users = identifiers
			.Select(identifier => userFactory.CreateUser(identifier, identifier))
			.ToList();

		return await GetOrCreateUsersAsync(users, configuration, cancellationToken);
	}

	/// <summary>
	/// Get or create a list of <see cref="TUser"/>s. UserName or Email and both are required in order for new Users to be created.
	/// </summary>
	/// <param name="users">A list of <see cref="TUser"/>s to get or create. Are matched on UserName.</param>
	/// <param name="configuration"><see cref="TConfiguration"/> used to validate domains in scoped identifiers.</param>
	/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
	/// <returns>A <see cref="UserSearchResult{TUser}"/> containing which user identifiers were found or created, and which were invalid.</returns>
	public async Task<UserSearchResult<TUser>> GetOrCreateUsersAsync(List<TUser> users, TConfiguration configuration,
		CancellationToken cancellationToken = default)
	{
		var result = new UserSearchResult<TUser>
		{
			AllowedDomains = configuration.AllowedDomains
		};

		// valid UserName/Email = scoped variable (email or ePPN), e.g. alias@domain.edu
		var validUsers = users
			.Where(u =>
				u.NormalizedUserName.HasValidDomainScope()
				&& configuration.ValidateAllowedDomains(u.NormalizedUserName)
				&& u.NormalizedEmail.HasValidDomainScope()
				&& configuration.ValidateAllowedDomains(u.NormalizedEmail))
			.ToList();
		var invalidUsers = users
			.Where(u =>
				!u.NormalizedUserName.HasValidDomainScope()
				|| !configuration.ValidateAllowedDomains(u.NormalizedUserName)
				|| !u.NormalizedEmail.HasValidDomainScope()
				|| !configuration.ValidateAllowedDomains(u.NormalizedEmail))
			.ToList();
		// return non-normalized UserNames to preserve user inputs
		result.InvalidIdentifiers = invalidUsers.Select(u => u.UserName).ToList();

		// Load existing users
		var existingUsers = await GetUsersByUserNameAsync(validUsers, cancellationToken);

		// Find users with no match
		var newUsers = validUsers
			.Where(u => existingUsers.All(e => e.NormalizedUserName != u.NormalizedUserName))
			.ToList();

		// Update any user if firstname, lastname or puid is empty
		await UpdateUsersWithUsersAsync(existingUsers, validUsers, cancellationToken);

		// No new users need to be created
		if (!newUsers.Any())
		{
			result.Users = existingUsers;
			return result;
		}

		// Create users that were not found
		foreach (var newUser in newUsers)
		{
			newUser.SecurityStamp = Guid.NewGuid().ToString();
			Users.Add(newUser);
		}

		await SaveChangesAsync(cancellationToken);

		// Add to result
		existingUsers.AddRange(newUsers);
		result.Users = existingUsers;

		return result;
	}

	/// <summary>
	/// Load Users by UserName
	/// </summary>
	/// <param name="validUsers"></param>
	/// <param name="cancellationToken"></param>
	/// <returns></returns>
	public async Task<List<TUser>> GetUsersByUserNameAsync(IEnumerable<TUser> validUsers, CancellationToken cancellationToken = default)
	{
		var validUserNames = validUsers.Select(u => u.NormalizedUserName).ToList();

		return await Users
			.Where(u => validUserNames.Contains(u.NormalizedUserName))
			.ToListAsync(cancellationToken);
	}

	public async Task UpdateUsersWithUsersAsync(IEnumerable<TUser> existingUsers, List<TUser> newUsers,
		CancellationToken cancellationToken = default)
	{
		// update any user if firstname, lastname, puid, alias is empty
		var usersToUpdate = existingUsers
			.Where(e =>
				(e.FirstName == null || e.LastName == null || e.EmployeeNumber == null || e.Uid == null) &&
				newUsers.Any(n => n.NormalizedUserName.Equals(e.NormalizedUserName)))
			.ToList();

		if (usersToUpdate.Any())
		{
			foreach (var userToUpdate in usersToUpdate)
			{
				var user = newUsers.SingleOrDefault(v => v.NormalizedUserName.Equals(userToUpdate.NormalizedUserName));
				if (user == null) continue;
				userToUpdate.FirstName = user.FirstName;
				userToUpdate.LastName = user.LastName;
				userToUpdate.EmployeeNumber = user.EmployeeNumber;
				userToUpdate.Uid = user.Uid;
				userToUpdate.Email = user.Email;
			}

			await SaveChangesAsync(cancellationToken);
		}
	}

	public async Task<IEnumerable<TUser>> GetOrCreateUsersAsync(IEnumerable<TUser> users, CancellationToken cancellationToken = default)
	{
		var newUsers = users.ToList();

		// load (or create) new users from the DB. also updates missing user data
		var configuration = await GetConfigurationAsync(cancellationToken);
		var getOrCreateResult = newUsers.Any()
			? await GetOrCreateUsersAsync(newUsers, configuration, cancellationToken)
			: new UserSearchResult<TUser>();

		return getOrCreateResult.Users
			.Distinct()
			.ToList();
	}

	#endregion Repository Business Logic
}