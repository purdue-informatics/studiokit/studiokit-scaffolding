using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.Models;
using StudioKit.Sharding.Entity.Identity;
using StudioKit.Sharding.Entity.Identity.Interfaces;
using StudioKit.Sharding.Entity.Identity.Models;
using StudioKit.Sharding.Utils;
using System;

namespace StudioKit.Scaffolding.DataAccess;

public class ScaffoldingShardManager<TContext, TConfiguration, TLicense, TUser, TIdentityProvider>
	: UserIdentityShardManager<TContext, TConfiguration, TLicense, TUser, TIdentityProvider>
	where TContext : DbContext, IUserIdentityShardDbContext<TUser, TIdentityProvider, TConfiguration, TLicense>, new()
	where TConfiguration : BaseConfiguration, new()
	where TLicense : License, new()
	where TUser : IdentityUser, IUser
	where TIdentityProvider : IdentityProvider, new()
{
	public ScaffoldingShardManager(ILoggerFactory loggerFactory,
		ILogger<ScaffoldingShardManager<TContext, TConfiguration, TLicense, TUser, TIdentityProvider>> logger) : base(loggerFactory, logger)
	{
	}

	protected override void ConsoleUpdateConfiguration(TConfiguration configuration)
	{
		base.ConsoleUpdateConfiguration(configuration);

		Console.Write("Add AllowedDomains as a comma separated string (or leave blank to skip): ");
		var response = Console.ReadLine();
		if (!string.IsNullOrWhiteSpace(response))
		{
			configuration.AllowedDomains = response;
		}

		Console.Write("Enable Caliper Analytics? (Y/N or leave blank to skip): ");
		response = Console.ReadLine();
		if (!string.IsNullOrWhiteSpace(response) && response.ToUpperInvariant().Equals("Y"))
		{
			configuration.CaliperEnabled = true;

			Console.Write("Set Caliper EventStore Hostname: ");
			response = Console.ReadLine();
			if (!string.IsNullOrWhiteSpace(response))
			{
				configuration.CaliperEventStoreHostname = response;
			}

			Console.Write("Set Caliper Person Namespace (e.g. https://purdue.edu/user/): ");
			response = Console.ReadLine();
			if (!string.IsNullOrWhiteSpace(response))
			{
				configuration.CaliperPersonNamespace = response;
			}
		}

		Console.Write("Should the Instructor Sandbox be enabled? (Y/N or leave blank to skip): ");
		response = Console.ReadLine();
		if (!string.IsNullOrWhiteSpace(response) && response.ToUpperInvariant().Equals("Y"))
		{
			configuration.IsInstructorSandboxEnabled = true;

			var intResponse = ConsoleUtils.ReadIntegerInput("Set the Demo Expiration Day Limit (leave blank to skip): ", true);
			configuration.DemoExpirationDayLimit = intResponse;
		}
	}

	protected override void ConsoleUpdateIdentityProviders(TContext context)
	{
		base.ConsoleUpdateIdentityProviders(context);

		Console.Write("Add Purdue CAS IdentityProvider (Y/N or leave blank to skip): ");
		var response = Console.ReadLine();
		if (!string.IsNullOrWhiteSpace(response) && response.ToUpperInvariant().Equals("Y"))
		{
			var service = PurdueCas.GetService(Subdomain);
			var casLoginUrl = PurdueCas.ConstructUrl(PurdueCas.LoginUrl, service);
			var casLogoutUrl = PurdueCas.ConstructUrl(PurdueCas.LogoutUrl, service);
			context.IdentityProviders.Add(
				new TIdentityProvider
				{
					Name = "Purdue CAS",
					LoginUrl = casLoginUrl,
					LogoutUrl = casLogoutUrl,
					MobileLoginUrl = casLoginUrl,
					Text = "Log in with your Purdue Career Account"
				});

			ConsoleUtils.WriteDetailedInfo("Added Purdue CAS IdentityProvider");
		}
	}
}