using Microsoft.EntityFrameworkCore;
using StudioKit.Data;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.ExternalProvider.UniTime.Models;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using StudioKit.Security.Entity.Identity.Interfaces;
using StudioKit.Security.Entity.Identity.Models;
using StudioKit.Security.Models;
using StudioKit.Sharding.Entity.Identity.Models;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.DataAccess.Interfaces;

public class EntityIdAndRoleId
{
	public int EntityId { get; set; }

	public string RoleId { get; set; }
}

public interface IBaseDbContext : IIdentityClientDbContext<AppClient, IdentityServiceClient, ServiceClientRole, RefreshToken>
{
	Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);

	int SaveChanges();

	DbSet<Role> Roles { get; set; }

	DbSet<UserRole> IdentityUserRoles { get; set; }

	DbSet<IdentityProvider> IdentityProviders { get; set; }

	DbSet<Login> Logins { get; set; }

	DbSet<ExternalTerm> ExternalTerms { get; set; }

	DbSet<Activity> Activities { get; set; }

	DbSet<RoleActivity> RoleActivities { get; set; }

	DbSet<UniTimeGroup> UniTimeGroups { get; set; }

	#region Licenses

	DbSet<License> Licenses { get; set; }

	License GetLicense();

	Task<License> GetLicenseAsync(CancellationToken cancellationToken = default);

	#endregion Licenses

	#region EntityUserRoles

	DbSet<EntityUserRole> EntityUserRoles { get; set; }

	DbSet<GroupUserRole> GroupUserRoles { get; set; }

	DbSet<GroupUserRoleLog> GroupUserRoleLogs { get; set; }

	/// <summary>
	/// Returns a function used to generate a queryable of <see cref="BaseGroup"/>, representing all groups connected to the entity of type <see cref="T"/> with the Id passed to said function.
	/// </summary>
	/// <typeparam name="T">Type of entity, implementing <see cref="ModelBase"/></typeparam>
	/// <returns></returns>
	Func<int, IQueryable<BaseGroup>> GroupsQueryableFunc<T>() where T : ModelBase;

	/// <summary>
	/// Determine which groups, if any, grant a given user access to perform a set of <paramref name="activities"/> on <paramref name="entity"/>.
	/// </summary>
	/// <typeparam name="T">Type of entity, implementing <see cref="ModelBase"/></typeparam>
	/// <param name="userId">The Id of a User</param>
	/// <param name="entity">The Entity being acted upon</param>
	/// <param name="activities">The actions to be performed</param>
	/// <returns>
	/// A queryable of Ids from <see cref="BaseGroup"/> which have granted the given user
	/// access to perform <paramref name="activities"/> on <paramref name="entity"/>.
	/// </returns>
	IQueryable<int> GroupsQueryableGroupIds<T>(string userId, T entity, params string[] activities)
		where T : ModelBase;

	/// <summary>
	/// Returns a function used to generate a queryable of <see cref="EntityIdAndRoleId"/>,
	/// representing the entity of type and role pairs for the given user, from <see cref="EntityUserRole"/>s.
	/// </summary>
	/// <typeparam name="T">Type of entity</typeparam>
	/// <returns></returns>
	Func<int, string, IQueryable<EntityIdAndRoleId>> EntityUserRoleEntityIdsQueryableFunc<T>()
		where T : ModelBase, IUserRoleAssociatedEntity;

	/// <summary>
	/// Determine if a user has access to perform one or many <paramref name="activities"/>
	/// on <paramref name="entity"/>.
	/// </summary>
	/// <typeparam name="T">Type of entity, implementing <see cref="ModelBase"/></typeparam>
	/// <param name="userId">The Id of a User</param>
	/// <param name="entity">The Entity being acted upon</param>
	/// <param name="activities">The actions to be performed</param>
	/// <returns>
	/// A queryable which denotes if the given user has
	/// access to perform one or many <paramref name="activities"/> on <paramref name="entity"/>,
	/// e.g. it will be empty if they cannot, and have results if they can
	/// </returns>
	IQueryable<int> EntityUserRolesQueryableEntityIds<T>(string userId, T entity, params string[] activities)
		where T : ModelBase, IUserRoleAssociatedEntity;

	#endregion EntityUserRoles

	#region Artifacts

	DbSet<Artifact> Artifacts { get; set; }

	#endregion Artifacts

	Task<TEntity> FindEntityAsync<TEntity>(int entityKey,
		bool throwIfSoftDeleted = true,
		string notFoundMessage = null,
		CancellationToken cancellationToken = default)
		where TEntity : ModelBase;

	/// <summary>
	/// Returns entity after checking for null/deleted entity, assignment/assessment, and group
	/// </summary>
	Task<ILockDownBrowserEntity> GetLockDownBrowserEntityAsync(int entityId, CancellationToken cancellationToken);
}