﻿using StudioKit.Caliper.Interfaces;
using StudioKit.Security.BusinessLogic.Interfaces;
using StudioKit.Sharding.Interfaces;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.DataAccess.Interfaces;

public interface IConfigurationProvider<TConfiguration> : ICaliperConfigurationProvider, ICertificateConfigurationProvider
	where TConfiguration : IBaseShardConfiguration, ICaliperConfiguration
{
	Task<TConfiguration> GetConfigurationAsync(CancellationToken cancellationToken = default);
}