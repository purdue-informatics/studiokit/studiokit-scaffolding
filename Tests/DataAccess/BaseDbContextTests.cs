using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSTestExtensions;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.Tests.TestData.DataAccess;
using StudioKit.Scaffolding.Tests.TestData.Models;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Tests.DataAccess;

[TestClass]
public class BaseDbContextTests : BaseTest
{
	private static readonly ScaffoldingTestDependencies Deps = new(ScaffoldingTestDependencies.StartDate);
	private TestBaseDbContext _dbContext;
	private IDbContextTransaction _transaction;
	private TestAuditableEntity _auditableEntity;

	[ClassInitialize]
	public static async Task BeforeAll(TestContext testContext)
	{
		// seed common data that will persist across tests
		await using var dbContext = Deps.PersistentDbContextFactory();
		await Deps.SeedRolesAndUsersAsync(dbContext);
		await Deps.SeedExternalProvidersAsync(dbContext);

		var auditableEntity = new TestAuditableEntity
		{
			Name = "Added"
		};
		dbContext.TestAuditableEntities.Add(auditableEntity);
		await dbContext.SaveChangesAsync();
	}

	[TestInitialize]
	public async Task BeforeEach()
	{
		// reset deps, _dbContext, transaction
		Deps.Reset(ScaffoldingTestDependencies.StartDate);
		(_dbContext, _transaction) = Deps.PersistentDbContextAndTransactionFactory();

		_auditableEntity = await _dbContext.TestAuditableEntities.SingleAsync();
	}

	[TestCleanup]
	public void AfterEach()
	{
		_transaction.Rollback();
		_transaction.Dispose();
		_dbContext.Dispose();
	}

	[ClassCleanup]
	public static void AfterAll()
	{
		Deps.Dispose();
	}

	#region SetSavingChanges

	#region Added

	[TestMethod]
	public async Task SetSavingChanges_Added_ShouldSetAuditableDateProperties()
	{
		var saveDate = Deps.DateTime;

		var auditableEntity = new TestAuditableEntity
		{
			Name = "Added"
		};
		_dbContext.TestAuditableEntities.Add(auditableEntity);
		await _dbContext.SaveChangesAsync();

		_dbContext.ChangeTracker.Clear();
		auditableEntity = await _dbContext.TestAuditableEntities.SingleAsync(a => a.Id.Equals(auditableEntity.Id));
		Assert.AreEqual("Added", auditableEntity.Name);
		Assert.AreEqual(saveDate, auditableEntity.DateStored);
		Assert.AreEqual(saveDate, auditableEntity.DateLastUpdated);
		Assert.IsNull(auditableEntity.LastUpdatedById);
	}

	[TestMethod]
	public async Task SetSavingChanges_Added_ShouldSetAuditableDateAndPrincipalProperties()
	{
		var saveDate = Deps.DateTime;

		await using var dbContext = Deps.PersistentDbContextFactory(Deps.InstructorPrincipalProvider);
		var auditableEntity = new TestAuditableEntity
		{
			Name = "Added"
		};
		dbContext.TestAuditableEntities.Add(auditableEntity);
		await dbContext.SaveChangesAsync();

		_dbContext.ChangeTracker.Clear();
		auditableEntity = await _dbContext.TestAuditableEntities.SingleAsync(a => a.Id.Equals(auditableEntity.Id));
		Assert.AreEqual("Added", auditableEntity.Name);
		Assert.AreEqual(saveDate, auditableEntity.DateStored);
		Assert.AreEqual(saveDate, auditableEntity.DateLastUpdated);
		Assert.AreEqual(Deps.InstructorId, auditableEntity.LastUpdatedById);
	}

	[TestMethod]
	public async Task SetSavingChanges_Added_ShouldSetAuditableDateAndPrincipalProperties_WithImpersonator()
	{
		var saveDate = Deps.DateTime;

		var (_, impersonatedPrincipalProvider) = ScaffoldingTestDependencies.GeneratePrincipalAndProvider(
			Deps.InstructorId,
			new List<Claim>
			{
				new(ScaffoldingClaimTypes.ImpersonatorUserId, Deps.SuperAdminId, ClaimValueTypes.String)
			});
		await using var dbContext = Deps.PersistentDbContextFactory(impersonatedPrincipalProvider);
		var auditableEntity = new TestAuditableEntity
		{
			Name = "Added"
		};
		dbContext.TestAuditableEntities.Add(auditableEntity);
		await dbContext.SaveChangesAsync();

		_dbContext.ChangeTracker.Clear();
		auditableEntity = await _dbContext.TestAuditableEntities.SingleAsync(a => a.Id.Equals(auditableEntity.Id));
		Assert.AreEqual("Added", auditableEntity.Name);
		Assert.AreEqual(saveDate, auditableEntity.DateStored);
		Assert.AreEqual(saveDate, auditableEntity.DateLastUpdated);
		Assert.AreEqual(Deps.SuperAdminId, auditableEntity.LastUpdatedById);
	}

	[TestMethod]
	public async Task SetSavingChanges_Added_ShouldNotAllowAuditableDatesToBeOverridden()
	{
		var saveDate = Deps.DateTime;
		var overrideDate = ScaffoldingTestDependencies.StartDate.AddMinutes(-1);
		Assert.AreNotEqual(saveDate, overrideDate);

		var auditableEntity = new TestAuditableEntity
		{
			Name = "Added",
			DateStored = overrideDate,
			DateLastUpdated = overrideDate
		};
		_dbContext.TestAuditableEntities.Add(auditableEntity);
		await _dbContext.SaveChangesAsync();

		_dbContext.ChangeTracker.Clear();
		auditableEntity = await _dbContext.TestAuditableEntities.SingleAsync(a => a.Id.Equals(auditableEntity.Id));
		Assert.AreEqual("Added", auditableEntity.Name);
		Assert.AreEqual(saveDate, auditableEntity.DateStored);
		Assert.AreEqual(saveDate, auditableEntity.DateLastUpdated);
		Assert.IsNull(auditableEntity.LastUpdatedById);
	}

	#endregion Added

	#region Modified

	[TestMethod]
	public async Task SetSavingChanges_Modified_ShouldSetAuditableDateProperties()
	{
		var dateStored = Deps.DateTime;

		var saveDate = ScaffoldingTestDependencies.StartDate.AddMinutes(1);
		Deps.SetDate(saveDate);

		_auditableEntity.Name = "Modified";
		await _dbContext.SaveChangesAsync();

		_dbContext.ChangeTracker.Clear();
		_auditableEntity = await _dbContext.TestAuditableEntities.SingleAsync();
		Assert.AreEqual("Modified", _auditableEntity.Name);
		Assert.AreEqual(dateStored, _auditableEntity.DateStored);
		Assert.AreEqual(saveDate, _auditableEntity.DateLastUpdated);
	}

	[TestMethod]
	public async Task SetSavingChanges_Modified_ShouldSetAuditableDateAndPrincipalProperties()
	{
		var dateStored = Deps.DateTime;

		await using var dbContext = Deps.PersistentDbContextFactory(Deps.InstructorPrincipalProvider);
		var saveDate = ScaffoldingTestDependencies.StartDate.AddMinutes(1);
		Deps.SetDate(saveDate);

		_auditableEntity = await dbContext.TestAuditableEntities.SingleAsync();
		_auditableEntity.Name = "Modified";
		await dbContext.SaveChangesAsync();

		dbContext.ChangeTracker.Clear();
		_auditableEntity = await dbContext.TestAuditableEntities.SingleAsync();
		Assert.AreEqual("Modified", _auditableEntity.Name);
		Assert.AreEqual(dateStored, _auditableEntity.DateStored);
		Assert.AreEqual(saveDate, _auditableEntity.DateLastUpdated);
		Assert.AreEqual(Deps.InstructorId, _auditableEntity.LastUpdatedById);
	}

	[TestMethod]
	public async Task SetSavingChanges_Modified_ShouldSetAuditableDateAndPrincipalProperties_WithImpersonator()
	{
		var dateStored = Deps.DateTime;

		var (_, impersonatedPrincipalProvider) = ScaffoldingTestDependencies.GeneratePrincipalAndProvider(
			Deps.InstructorId,
			new List<Claim>
			{
				new(ScaffoldingClaimTypes.ImpersonatorUserId, Deps.SuperAdminId, ClaimValueTypes.String)
			});
		await using var dbContext = Deps.PersistentDbContextFactory(impersonatedPrincipalProvider);
		var saveDate = ScaffoldingTestDependencies.StartDate.AddMinutes(1);
		Deps.SetDate(saveDate);

		_auditableEntity = await dbContext.TestAuditableEntities.SingleAsync();
		_auditableEntity.Name = "Modified";
		await dbContext.SaveChangesAsync();

		dbContext.ChangeTracker.Clear();
		_auditableEntity = await dbContext.TestAuditableEntities.SingleAsync();
		Assert.AreEqual("Modified", _auditableEntity.Name);
		Assert.AreEqual(dateStored, _auditableEntity.DateStored);
		Assert.AreEqual(saveDate, _auditableEntity.DateLastUpdated);
		Assert.AreEqual(Deps.SuperAdminId, _auditableEntity.LastUpdatedById);
	}

	[TestMethod]
	public async Task SetSavingChanges_Modified_ShouldNotAllowAuditableDateToBeOverridden()
	{
		var dateStored = Deps.DateTime;

		var saveDate = ScaffoldingTestDependencies.StartDate.AddMinutes(2);
		Deps.SetDate(saveDate);

		var overrideDate = ScaffoldingTestDependencies.StartDate.AddMinutes(1);
		_auditableEntity.DateLastUpdated = overrideDate;
		_auditableEntity.Name = "Modified";
		await _dbContext.SaveChangesAsync();

		_dbContext.ChangeTracker.Clear();
		_auditableEntity = await _dbContext.TestAuditableEntities.SingleAsync();
		Assert.AreEqual("Modified", _auditableEntity.Name);
		Assert.AreEqual(dateStored, _auditableEntity.DateStored);
		Assert.AreEqual(saveDate, _auditableEntity.DateLastUpdated);
		Assert.AreNotEqual(overrideDate, _auditableEntity.DateLastUpdated);
	}

	#endregion Modified

	#endregion SetSavingChanges
}