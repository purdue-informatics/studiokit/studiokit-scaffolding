﻿using StudioKit.Data.Entity.Extensions;
using StudioKit.Scaffolding.BusinessLogic.Models;
using System.Collections.Generic;

namespace StudioKit.Scaffolding.Tests.EqualityComparers;

public class ExternalGroupBusinessModelEqualityComparer : IEqualityComparer<ExternalGroupBusinessModel>
{
	public bool Equals(ExternalGroupBusinessModel x, ExternalGroupBusinessModel y)
	{
		if (ReferenceEquals(x, y)) return true;
		if (x is null) return false;
		if (y is null) return false;
		if (!x.GetType().EntityTypeEquals(y.GetType())) return false;
		return x.GroupId == y.GroupId &&
				string.Equals(x.ExternalId, y.ExternalId) &&
				x.ExternalProviderId == y.ExternalProviderId &&
				string.Equals(x.Name, y.Name) &&
				string.Equals(x.Description, y.Description) &&
				string.Equals(x.RosterUrl, y.RosterUrl) &&
				string.Equals(x.GradesUrl, y.GradesUrl) &&
				x.IsAutoGradePushEnabled == y.IsAutoGradePushEnabled &&
				string.Equals(x.UserId, y.UserId);
	}

	public int GetHashCode(ExternalGroupBusinessModel obj)
	{
		unchecked
		{
			var hashCode = obj.GroupId.GetHashCode();
			hashCode = (hashCode * 397) ^ obj.ExternalProviderId.GetHashCode();
			hashCode = (hashCode * 397) ^ (obj.ExternalId != null ? obj.ExternalId.GetHashCode() : 0);
			hashCode = (hashCode * 397) ^ (obj.Name != null ? obj.Name.GetHashCode() : 0);
			hashCode = (hashCode * 397) ^ (obj.Description != null ? obj.Description.GetHashCode() : 0);
			hashCode = (hashCode * 397) ^ (obj.RosterUrl != null ? obj.RosterUrl.GetHashCode() : 0);
			hashCode = (hashCode * 397) ^ (obj.GradesUrl != null ? obj.GradesUrl.GetHashCode() : 0);
			hashCode = (hashCode * 397) ^ (obj.IsAutoGradePushEnabled != null ? obj.IsAutoGradePushEnabled.GetHashCode() : 0);
			hashCode = (hashCode * 397) ^ (obj.UserId != null ? obj.UserId.GetHashCode() : 0);
			return hashCode;
		}
	}
}