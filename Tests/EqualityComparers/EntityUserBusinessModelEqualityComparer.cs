﻿using StudioKit.Data.Entity.Extensions;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using System.Collections.Generic;

namespace StudioKit.Scaffolding.Tests.EqualityComparers;

public class EntityUserBusinessModelEqualityComparer<TUser> : IEqualityComparer<EntityUserBusinessModel<TUser>> where TUser : IUser
{
	public bool Equals(EntityUserBusinessModel<TUser> x, EntityUserBusinessModel<TUser> y)
	{
		if (x == y) return true;
		if (x is null) return false;
		if (y is null) return false;
		if (!x.GetType().EntityTypeEquals(y.GetType())) return false;
		return x.EntityId == y.EntityId &&
				string.Equals(x.User.UserName, y.User.UserName) &&
				string.Equals(x.User.Id, y.User.Id) &&
				string.Equals(x.User.FirstName, y.User.FirstName) &&
				string.Equals(x.User.LastName, y.User.LastName) &&
				string.Equals(x.User.Email, y.User.Email) &&
				string.Equals(x.User.Uid, y.User.Uid);
	}

	public int GetHashCode(EntityUserBusinessModel<TUser> obj)
	{
		unchecked
		{
			var hashCode = obj.EntityId;
			hashCode = (hashCode * 397) ^ (obj.User.UserName != null ? obj.User.UserName.GetHashCode() : 0);
			hashCode = (hashCode * 397) ^ (obj.User.Id != null ? obj.User.Id.GetHashCode() : 0);
			hashCode = (hashCode * 397) ^ (obj.User.FirstName != null ? obj.User.FirstName.GetHashCode() : 0);
			hashCode = (hashCode * 397) ^ (obj.User.LastName != null ? obj.User.LastName.GetHashCode() : 0);
			hashCode = (hashCode * 397) ^ (obj.User.Email != null ? obj.User.Email.GetHashCode() : 0);
			hashCode = (hashCode * 397) ^ (obj.User.Uid != null ? obj.User.Uid.GetHashCode() : 0);
			return hashCode;
		}
	}
}