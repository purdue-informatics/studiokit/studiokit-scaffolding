﻿using StudioKit.Data.Entity.Extensions;
using StudioKit.Scaffolding.Models;
using System.Collections.Generic;

namespace StudioKit.Scaffolding.Tests.EqualityComparers;

public class BaseGroupEqualityComparer : IEqualityComparer<BaseGroup>
{
	public bool Equals(BaseGroup x, BaseGroup y)
	{
		if (ReferenceEquals(x, y)) return true;
		if (x is null) return false;
		if (y is null) return false;
		if (!x.GetType().EntityTypeEquals(y.GetType())) return false;
		return x.IsDeleted == y.IsDeleted &&
				string.Equals(x.Name, y.Name) &&
				x.ExternalTermId == y.ExternalTermId &&
				x.StartDate == y.StartDate &&
				x.EndDate == y.EndDate;
	}

	public int GetHashCode(BaseGroup obj)
	{
		unchecked
		{
			var hashCode = obj.IsDeleted.GetHashCode();
			hashCode = (hashCode * 397) ^ (obj.Name != null ? obj.Name.GetHashCode() : 0);
			hashCode = (hashCode * 397) ^ (obj.ExternalTermId != null ? obj.ExternalTermId.GetHashCode() : 0);
			hashCode = (hashCode * 397) ^ (obj.StartDate != null ? obj.StartDate.GetHashCode() : 0);
			hashCode = (hashCode * 397) ^ (obj.EndDate != null ? obj.EndDate.GetHashCode() : 0);
			return hashCode;
		}
	}
}