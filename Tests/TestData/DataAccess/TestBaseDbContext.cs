using Microsoft.EntityFrameworkCore;
using StudioKit.Scaffolding.DataAccess;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using StudioKit.Scaffolding.Tests.TestData.Models;
using StudioKit.Security.BusinessLogic.Interfaces;
using StudioKit.Utilities.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Tests.TestData.DataAccess;

public class TestBaseDbContext : BaseDbContext<TestBaseUser, TestBaseGroup, BaseConfiguration>
{
	public TestBaseDbContext()
	{
	}

	public TestBaseDbContext(DbContextOptions options, IDateTimeProvider dateTimeProvider, IPrincipalProvider principalProvider) :
		base(options, dateTimeProvider, principalProvider)
	{
	}

	public TestBaseDbContext(DbConnection existingConnection, IDateTimeProvider dateTimeProvider, IPrincipalProvider principalProvider) :
		base(existingConnection, dateTimeProvider, principalProvider)
	{
	}

	protected override void OnModelCreating(ModelBuilder modelBuilder)
	{
		base.OnModelCreating(modelBuilder);

		modelBuilder.Entity<TestBaseUser>(b =>
		{
			b.HasIndex(e => e.Uid);
			b.HasIndex(e => e.EmployeeNumber);
		});
	}

	public override Func<int, IQueryable<BaseGroup>> GroupsQueryableFunc<T>()
	{
		var groupsQueryableFuncDictionary = new Dictionary<Type, Func<int, IQueryable<BaseGroup>>>
		{
			{ typeof(BaseGroup), GroupGroupsQueryableFunc },
			{ typeof(TestBaseGroup), GroupGroupsQueryableFunc }
		};
		return groupsQueryableFuncDictionary[typeof(T)];
	}

	public Func<int, IQueryable<BaseGroup>> GroupGroupsQueryableFunc =>
		id =>
			Groups
				.Where(g => g.Id.Equals(id));


	#region LockDown Browser

	public virtual DbSet<TestLockDownBrowserEntity> TestLockDownBrowserEntities { get; set; }

	public override async Task<ILockDownBrowserEntity> GetLockDownBrowserEntityAsync(int entityId,
		CancellationToken cancellationToken = default)
	{
		var entity = await FindEntityAsync<TestLockDownBrowserEntity>(
			entityId,
			throwIfSoftDeleted: false,
			cancellationToken: cancellationToken);
		return entity;
	}

	#endregion LockDown Browser

	public virtual DbSet<TestAuditableEntity> TestAuditableEntities { get; set; }

	public virtual DbSet<TestEntityArtifact> TestEntityArtifacts { get; set; }

	public virtual DbSet<TestEntityArtifactsContainer> TestEntityArtifactsContainers { get; set; }
}