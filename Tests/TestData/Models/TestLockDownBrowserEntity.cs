using StudioKit.Data;
using StudioKit.Data.Interfaces;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using StudioKit.Utilities.Extensions;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.Scaffolding.Tests.TestData.Models;

public class TestLockDownBrowserEntity : ModelBase, ILockDownBrowserEntity, IDelible
{
	public bool IsDeleted { get; set; }

	public bool IsLockDownBrowserRequired { get; set; }

	public bool IsLockDownBrowserRequiredForResults { get; set; }

	public bool IsLockDownBrowserHighSecurityEnabled { get; set; }

	public string LockDownBrowserProctorExitPassword { get; set; }

	public string LockDownBrowserCalculatorType { get; set; }

	[NotMapped]
	public LockDownBrowserCalculatorType? LockDownBrowserCalculatorTypeEnum
	{
		get => LockDownBrowserCalculatorType.ToEnum<LockDownBrowserCalculatorType>();
		set => LockDownBrowserCalculatorType = value.ToString();
	}

	public bool IsLockDownBrowserPrintingEnabled { get; set; }
}