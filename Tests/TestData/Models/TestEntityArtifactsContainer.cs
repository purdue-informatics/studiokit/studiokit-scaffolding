using StudioKit.Data;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace StudioKit.Scaffolding.Tests.TestData.Models;

public class TestEntityArtifactsContainer : ModelBase, IArtifactsContainer<TestEntityArtifact>
{
	public ICollection<TestEntityArtifact> EntityArtifacts { get; set; }

	[NotMapped]
	public IEnumerable<Artifact> Artifacts => EntityArtifacts.Select(ea => ea.Artifact);
}