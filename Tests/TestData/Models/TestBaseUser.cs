using Microsoft.AspNetCore.Identity;
using StudioKit.Data.Entity.Identity;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Models.Interfaces;
using StudioKit.Sharding.Entity.Identity.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace StudioKit.Scaffolding.Tests.TestData.Models;

public class TestBaseUser : IdentityUser, IUser
{
	public TestBaseUser()
	{
		Id = Guid.NewGuid().ToString();
	}

	#region IUser

	public string FirstName { get; set; }

	public string LastName { get; set; }

	public string FullName => $"{FirstName} {LastName}";

	[NotMapped]
	public string CareerAccountAlias
	{
		get => Uid;
		set => Uid = value;
	}

	[NotMapped]
	public string Puid
	{
		get => EmployeeNumber;
		set => EmployeeNumber = value;
	}

	[StringLength(128)]
	public string Uid { get; set; }

	[StringLength(128)]
	public string EmployeeNumber { get; set; }

	public DateTime DateStored { get; set; }

	public DateTime DateLastUpdated { get; set; }

	public string LastUpdatedById { get; set; }

	public ICollection<ExternalGroup> ExternalGroups { get; set; }

	public ICollection<ExternalGroupUser> ExternalGroupUsers { get; set; }

	public ICollection<EntityUserRole> EntityUserRoles { get; set; }

	[NotMapped]
	public IEnumerable<GroupUserRole> GroupUserRoles => EntityUserRoles.OfType<GroupUserRole>();

	public ICollection<LicenseUser> LicenseUsers { get; set; }

	public ICollection<Login> UserLogins { get; set; }

	[NotMapped]
	public IEnumerable<BaseGroup> Groups
		=>
			EntityUserRoles.OfType<GroupUserRole>().Where(m => m.Role.Name.Equals(BaseRole.GroupLearner))
				.Select(m => m.Group);

	[NotMapped]
	public IEnumerable<BaseGroup> InstructorGroups
		=>
			EntityUserRoles.OfType<GroupUserRole>().Where(m => m.Role.Name.Equals(BaseRole.GroupOwner))
				.Select(m => m.Group);

	public ICollection<Artifact> Artifacts { get; set; }

	#endregion IUser
}