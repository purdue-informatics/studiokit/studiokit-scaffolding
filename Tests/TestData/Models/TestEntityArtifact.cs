using StudioKit.Data;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Models.Interfaces;

namespace StudioKit.Scaffolding.Tests.TestData.Models;

public class TestEntityArtifact : ModelBase, IEntityArtifact
{
	public Artifact Artifact { get; set; }
}