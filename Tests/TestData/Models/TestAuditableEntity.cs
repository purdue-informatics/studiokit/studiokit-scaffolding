using StudioKit.Data;

namespace StudioKit.Scaffolding.Tests.TestData.Models;

public class TestAuditableEntity : ModelBase
{
	public string Name { get; set; }
}