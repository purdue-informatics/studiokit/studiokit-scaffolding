﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Services;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Tests.TestData.DataAccess;
using StudioKit.Scaffolding.Tests.TestData.Models;
using System;
using System.Collections.Generic;

namespace StudioKit.Scaffolding.Tests.TestData.BusinessLogic.Services;

public class TestUserManager : UserManager<TestBaseDbContext, TestBaseUser, TestBaseGroup, BaseConfiguration>
{
	public TestUserManager(IUserStore<TestBaseUser> store,
		IOptions<IdentityOptions> optionsAccessor,
		IPasswordHasher<TestBaseUser> passwordHasher,
		IEnumerable<IUserValidator<TestBaseUser>> userValidators,
		IEnumerable<IPasswordValidator<TestBaseUser>> passwordValidators,
		ILookupNormalizer keyNormalizer,
		IdentityErrorDescriber errors,
		IServiceProvider services,
		ILogger<TestUserManager> logger,
		TestBaseDbContext dbContext,
		IInstructorSandboxService<TestBaseUser> instructorSandboxService,
		IUserSetupService<TestBaseUser> userSetupService)
		: base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger,
			dbContext, instructorSandboxService, userSetupService)
	{
	}
}