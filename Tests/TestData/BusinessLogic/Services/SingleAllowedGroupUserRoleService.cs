﻿using StudioKit.Cloud.Storage.Queue;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Notification.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces.Authorization;
using StudioKit.Scaffolding.BusinessLogic.Services;
using StudioKit.Scaffolding.DataAccess.Queues.QueueMessages;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Tests.TestData.DataAccess;
using StudioKit.Scaffolding.Tests.TestData.Models;
using StudioKit.Sharding.Interfaces;
using StudioKit.UserInfoService;
using StudioKit.Utilities.Interfaces;

namespace StudioKit.Scaffolding.Tests.TestData.BusinessLogic.Services;

public class SingleAllowedGroupUserRoleService : BaseGroupUserRoleService<TestBaseDbContext, TestBaseUser, TestBaseGroup,
	BaseConfiguration>
{
	public SingleAllowedGroupUserRoleService(TestBaseDbContext dbContext, IShardKeyProvider shardKeyProvider,
		IUserInfoService userInfoService, IEntityUserRoleAuthorizationService authorizationService,
		RoleManager roleManager, IQueueManager<SyncGroupRosterMessage> syncGroupRosterQueueManager,
		IDateTimeProvider dateTimeProvider, IUserFactory<TestBaseUser> userFactory,
		INotificationService<TestBaseGroup> notificationService = null) : base(
		dbContext, shardKeyProvider, userInfoService, authorizationService, roleManager,
		syncGroupRosterQueueManager, dateTimeProvider, userFactory, notificationService)
	{
	}

	protected override bool AreMultipleRolesAllowed()
	{
		return false;
	}
}