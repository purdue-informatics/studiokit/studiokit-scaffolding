using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("StudioKit.Scaffolding.Tests")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("StudioKit.Scaffolding.Tests")]
[assembly: AssemblyCopyright("Copyright © 2019 Purdue University")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("03d564da-78fd-4aa3-bc8f-97e2bcfc1596")]

// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]