﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MSTestExtensions;
using StudioKit.Cloud.Storage.Blob;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.BusinessLogic.Utils;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Properties;
using StudioKit.Scaffolding.Tests.TestData.DataAccess;
using StudioKit.Scaffolding.Tests.TestData.Models;
using StudioKit.Tests;
using StudioKit.TransientFaultHandling.Http.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Tests.BusinessLogic.Utils;

[TestClass]
public class ArtifactUtilsTests : BaseTest
{
	private static readonly ScaffoldingTestDependencies Deps = new(ScaffoldingTestDependencies.StartDate);
	private TestBaseDbContext _dbContext;
	private IDbContextTransaction _transaction;

	private const string StorageHost = "https://127.0.0.1/";

	private const string TextArtifactBlobPath = $"{ArtifactUtils.TextStorageLocation}TextArtifact-1.txt";
	private const string TextArtifactUrl = $"{StorageHost}{TextArtifactBlobPath}";
	private TextArtifact _textArtifact;
	private const string TextArtifact2Url = $"{StorageHost}{ArtifactUtils.TextStorageLocation}TextArtifact-2.txt";
	private TextArtifact _textArtifact2;
	private TextArtifactBusinessModel _textArtifactRequestModel;

	private const string FileNameDocx = "cat.docx";
	private const string FileArtifactDocxBlobPath = $"{ArtifactUtils.FileStorageLocation}FileArtifact-1.docx";
	private const string FileArtifactDocxUrl = $"{StorageHost}{FileArtifactDocxBlobPath}";
	private FileArtifact _fileArtifactDocx;

	private const string FileNameDocx2 = "cat2.docx";
	private const string FileArtifactDocx2BlobPath = $"{ArtifactUtils.FileStorageLocation}FileArtifact-2.docx";
	private const string FileArtifactDocx2Url = $"{StorageHost}{FileArtifactDocx2BlobPath}";
	private FileArtifact _fileArtifactDocx2;

	private const string FileNamePdf = "doge.pdf";
	private const string FileArtifactPdfBlobPath = $"{ArtifactUtils.FileStorageLocation}FileArtifact-3.pdf";
	private const string FileArtifactPdfUrl = $"{StorageHost}{FileArtifactPdfBlobPath}";
	private FileArtifact _fileArtifactPdf;
	private FileArtifactBusinessModel _fileArtifactRequestModel;

	private const string UrlArtifactUrl = "https://dummy.data/old-1";
	private UrlArtifact _urlArtifact;
	private const string UrlArtifact2Url = "https://dummy.data/old-2";
	private UrlArtifact _urlArtifact2;
	private UrlArtifactBusinessModel _urlArtifactRequestModel;

	private string _uploadedPath;
	private string _deletedPath;
	private Mock<IBlobStorage> _blobStorageMock;
	private IBlobStorage _blobStorage;

	[ClassInitialize]
	public static async Task BeforeAll(TestContext testContext)
	{
		// seed common data that will persist across tests
		await using var dbContext = Deps.PersistentDbContextFactory();
		await Deps.SeedRolesAndUsersAsync(dbContext);
		await Deps.SeedExternalProvidersAsync(dbContext);
	}

	[TestInitialize]
	public async Task BeforeEach()
	{
		Deps.Reset(ScaffoldingTestDependencies.StartDate);
		(_dbContext, _transaction) = Deps.PersistentDbContextAndTransactionFactory();

		// Artifacts

		_textArtifact = new TextArtifact
		{
			Url = TextArtifactUrl,
			WordCount = 3,
			CreatedById = Deps.InstructorId
		};
		_dbContext.Artifacts.Add(_textArtifact);

		_textArtifact2 = new TextArtifact
		{
			Url = TextArtifact2Url,
			WordCount = 5,
			CreatedById = Deps.InstructorId
		};
		_dbContext.Artifacts.Add(_textArtifact2);

		_fileArtifactDocx = new FileArtifact
		{
			FileName = FileNameDocx,
			Url = FileArtifactDocxUrl,
			CreatedById = Deps.InstructorId
		};
		_dbContext.Artifacts.Add(_fileArtifactDocx);

		_fileArtifactDocx2 = new FileArtifact
		{
			FileName = FileNameDocx2,
			Url = FileArtifactDocx2Url,
			CreatedById = Deps.InstructorId
		};
		_dbContext.Artifacts.Add(_fileArtifactDocx2);

		_fileArtifactPdf = new FileArtifact
		{
			FileName = FileNamePdf,
			Url = FileArtifactPdfUrl,
			CreatedById = Deps.InstructorId
		};
		_dbContext.Artifacts.Add(_fileArtifactPdf);

		_urlArtifact = new UrlArtifact
		{
			Url = UrlArtifactUrl,
			CreatedById = Deps.InstructorId
		};
		_dbContext.Artifacts.Add(_urlArtifact);

		_urlArtifact2 = new UrlArtifact
		{
			Url = UrlArtifact2Url,
			CreatedById = Deps.InstructorId
		};
		_dbContext.Artifacts.Add(_urlArtifact2);

		await _dbContext.SaveChangesAsync();

		_textArtifactRequestModel = new TextArtifactBusinessModel
		{
			Text = "<p>Hello World!</p>",
			WordCount = 2
		};

		_fileArtifactRequestModel = new FileArtifactBusinessModel
		{
			FileName = "cat3.docx",
			File = Deps.GenerateFormFile()
		};

		_urlArtifactRequestModel = new UrlArtifactBusinessModel
		{
			Url = "https://dummy.data/new"
		};

		// IBlobStorage

		_uploadedPath = null;
		_deletedPath = null;

		_blobStorageMock = GetBlobStorageMock();
		_blobStorage = _blobStorageMock.Object;

		_dbContext.ChangeTracker.Clear();
	}

	[TestCleanup]
	public void AfterEach()
	{
		_transaction.Rollback();
		_transaction.Dispose();
		_dbContext.Dispose();
	}

	[ClassCleanup]
	public static void AfterAll()
	{
		Deps.Dispose();
	}

	#region CreateArtifactAsync

	[TestMethod]
	public void CreateArtifactAsync_ShouldThrowWithNullArguments()
	{
		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await ArtifactUtils.CreateArtifactAsync(null, _blobStorage, Deps.InstructorId);
		}), ExceptionUtils.ArgumentNullExceptionMessage("requestModel"));

		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await ArtifactUtils.CreateArtifactAsync(_fileArtifactRequestModel, null, Deps.InstructorId);
		}), ExceptionUtils.ArgumentNullExceptionMessage("artifactStorage"));

		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await ArtifactUtils.CreateArtifactAsync(_fileArtifactRequestModel, _blobStorage, null);
		}), ExceptionUtils.ArgumentNullExceptionMessage("userId"));
	}

	[TestMethod]
	public async Task CreateArtifactAsync_ShouldCreateTextArtifact()
	{
		var artifact = await ArtifactUtils.CreateArtifactAsync(_textArtifactRequestModel, _blobStorage, Deps.InstructorId);
		Assert.IsNotNull(artifact);
		var textArtifact = artifact as TextArtifact;
		Debug.Assert(textArtifact != null, nameof(textArtifact) + " != null");
		Assert.AreEqual(_textArtifactRequestModel.WordCount, textArtifact.WordCount);
		Assert.IsTrue(textArtifact.Url.StartsWith($"{StorageHost}{ArtifactUtils.TextStorageLocation}"));
		Assert.AreEqual(Deps.InstructorId, textArtifact.CreatedById);
	}

	[TestMethod]
	public async Task CreateArtifactAsync_ShouldCreateFileArtifact()
	{
		var artifact = await ArtifactUtils.CreateArtifactAsync(_fileArtifactRequestModel, _blobStorage, Deps.InstructorId);
		Assert.IsNotNull(artifact);
		var fileArtifact = artifact as FileArtifact;
		Debug.Assert(fileArtifact != null, nameof(fileArtifact) + " != null");
		Assert.AreEqual(_fileArtifactRequestModel.FileName, fileArtifact.FileName);
		Assert.IsTrue(fileArtifact.Url.StartsWith($"{StorageHost}{ArtifactUtils.FileStorageLocation}"));
		Assert.AreEqual(Deps.InstructorId, fileArtifact.CreatedById);
	}

	[TestMethod]
	public async Task CreateArtifactAsync_ShouldCreateUrlArtifact()
	{
		var artifact = await ArtifactUtils.CreateArtifactAsync(_urlArtifactRequestModel, _blobStorage, Deps.InstructorId);
		Assert.IsNotNull(artifact);
		var urlArtifact = artifact as UrlArtifact;
		Debug.Assert(urlArtifact != null, nameof(urlArtifact) + " != null");
		Assert.AreEqual(_urlArtifactRequestModel.Url, urlArtifact.Url);
		Assert.AreEqual(Deps.InstructorId, urlArtifact.CreatedById);
	}

	#endregion CreateArtifactAsync

	#region SaveArtifactAsync

	[TestMethod]
	public void SaveArtifactAsync_ShouldThrowWithNullArguments()
	{
		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await ArtifactUtils.SaveArtifactAsync<TestEntityArtifactsContainer, TestEntityArtifact>(null, _textArtifactRequestModel,
				_blobStorage, _dbContext, Deps.InstructorId);
		}), ExceptionUtils.ArgumentNullExceptionMessage("artifactsContainer"));
	}

	[TestMethod]
	public async Task SaveArtifactAsync_ShouldSave()
	{
		var artifactsContainer = new TestEntityArtifactsContainer
		{
			EntityArtifacts = new List<TestEntityArtifact>()
		};
		_dbContext.TestEntityArtifactsContainers.Add(artifactsContainer);
		await _dbContext.SaveChangesAsync();

		var artifact = await ArtifactUtils.SaveArtifactAsync<TestEntityArtifactsContainer, TestEntityArtifact>(artifactsContainer,
			_textArtifactRequestModel, _blobStorage, _dbContext, Deps.InstructorId);

		_dbContext.ChangeTracker.Clear();

		var savedArtifactsContainer = await _dbContext.TestEntityArtifactsContainers
			.Include(c => c.EntityArtifacts)
			.ThenInclude(ea => ea.Artifact)
			.SingleAsync(c => c.Id.Equals(artifactsContainer.Id));

		Assert.AreEqual(artifact.Id, savedArtifactsContainer.Artifacts.Single().Id);
	}

	#endregion SaveArtifactAsync

	#region UpdateArtifactAsync

	[TestMethod]
	public void UpdateArtifactAsync_ShouldThrowWithNullArguments()
	{
		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await ArtifactUtils.UpdateArtifactAsync(null, _textArtifactRequestModel, _blobStorage, _dbContext);
		}), ExceptionUtils.ArgumentNullExceptionMessage("artifact"));

		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await ArtifactUtils.UpdateArtifactAsync(_textArtifact, null, _blobStorage, _dbContext);
		}), ExceptionUtils.ArgumentNullExceptionMessage("requestModel"));

		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await ArtifactUtils.UpdateArtifactAsync(_textArtifact, _textArtifactRequestModel, null, _dbContext);
		}), ExceptionUtils.ArgumentNullExceptionMessage("artifactStorage"));

		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await ArtifactUtils.UpdateArtifactAsync(_textArtifact, _textArtifactRequestModel, _blobStorage, null);
		}), ExceptionUtils.ArgumentNullExceptionMessage("dbContext"));
	}

	[TestMethod]
	public void UpdateArtifactAsync_ShouldThrowIfRequestModelDoesNotMatchArtifactType()
	{
		Assert.ThrowsAsync<ValidationException>(Task.Run(async () =>
		{
			await ArtifactUtils.UpdateArtifactAsync(_textArtifact, _urlArtifactRequestModel, _blobStorage, _dbContext);
		}), Strings.UpdateArtifactTypeMismatch);

		Assert.ThrowsAsync<ValidationException>(Task.Run(async () =>
		{
			await ArtifactUtils.UpdateArtifactAsync(_fileArtifactDocx, _urlArtifactRequestModel, _blobStorage, _dbContext);
		}), Strings.UpdateArtifactTypeMismatch);

		Assert.ThrowsAsync<ValidationException>(Task.Run(async () =>
		{
			await ArtifactUtils.UpdateArtifactAsync(_urlArtifact, _textArtifactRequestModel, _blobStorage, _dbContext);
		}), Strings.UpdateArtifactTypeMismatch);
	}

	[TestMethod]
	public async Task UpdateArtifactAsync_ShouldUploadNewBlobAndDeleteExistingBlob_TextArtifact()
	{
		await ArtifactUtils.UpdateArtifactAsync(_textArtifact, _textArtifactRequestModel, _blobStorage, _dbContext);

		Assert.AreNotEqual(TextArtifactUrl, _textArtifact.Url);
		Assert.AreEqual(_textArtifactRequestModel.WordCount, _textArtifact.WordCount);
		Assert.AreEqual(ArtifactUtils.GetTextBlobPathFromUrl(_textArtifact.Url), _uploadedPath);
		Assert.AreEqual(TextArtifactBlobPath, _deletedPath);
	}

	[TestMethod]
	public async Task UpdateArtifactAsync_ShouldUploadNewBlobButNotDeleteExistingBlobWithArg_TextArtifact()
	{
		await ArtifactUtils.UpdateArtifactAsync(_textArtifact, _textArtifactRequestModel, _blobStorage, _dbContext,
			shouldKeepExistingBlob: true);

		Assert.AreNotEqual(TextArtifactUrl, _textArtifact.Url);
		Assert.AreEqual(_textArtifactRequestModel.WordCount, _textArtifact.WordCount);
		Assert.AreEqual(ArtifactUtils.GetTextBlobPathFromUrl(_textArtifact.Url), _uploadedPath);
		Assert.IsNull(_deletedPath);
	}

	[TestMethod]
	public void UpdateArtifactAsync_ShouldNotDeleteExistingBlobIfUploadFails_TextArtifact()
	{
		// create failing mock
		var blobStorageMock = GetBlobStorageMock();
		blobStorageMock.Setup(s => s.UploadBlockBlobAsync(
				It.IsAny<Stream>(),
				It.IsAny<string>(),
				It.IsAny<string>(),
				It.IsAny<CancellationToken>()))
			.Throws<OutOfMemoryException>();

		Assert.ThrowsAsync<OutOfMemoryException>(
			Task.Run(async () =>
			{
				await ArtifactUtils.UpdateArtifactAsync(_textArtifact, _textArtifactRequestModel, blobStorageMock.Object, _dbContext);
			}));

		Assert.AreEqual(TextArtifactUrl, _textArtifact.Url);
		Assert.AreNotEqual(_textArtifactRequestModel.WordCount, _textArtifact.WordCount);
		Assert.IsNull(_uploadedPath);
		Assert.IsNull(_deletedPath);
	}

	[TestMethod]
	public async Task UpdateArtifactAsync_ShouldUploadNewBlobAndDeleteExistingBlob_FileArtifact()
	{
		await ArtifactUtils.UpdateArtifactAsync(_fileArtifactDocx, _fileArtifactRequestModel, _blobStorage, _dbContext);

		Assert.AreNotEqual(FileArtifactDocxUrl, _fileArtifactDocx.Url);
		Assert.AreEqual(_fileArtifactRequestModel.FileName, _fileArtifactDocx.FileName);
		Assert.AreEqual(ArtifactUtils.GetFileBlobPathFromUrl(_fileArtifactDocx.Url), _uploadedPath);
		Assert.AreEqual(FileArtifactDocxBlobPath, _deletedPath);
	}

	[TestMethod]
	public async Task UpdateArtifactAsync_ShouldUploadNewBlobButNotDeleteExistingBlobWithArg_FileArtifact()
	{
		await ArtifactUtils.UpdateArtifactAsync(_fileArtifactDocx, _fileArtifactRequestModel, _blobStorage, _dbContext,
			shouldKeepExistingBlob: true);

		Assert.AreNotEqual(FileArtifactDocxUrl, _fileArtifactDocx.Url);
		Assert.AreEqual(_fileArtifactRequestModel.FileName, _fileArtifactDocx.FileName);
		Assert.AreEqual(ArtifactUtils.GetFileBlobPathFromUrl(_fileArtifactDocx.Url), _uploadedPath);
		Assert.IsNull(_deletedPath);
	}

	[TestMethod]
	public void UpdateArtifactAsync_ShouldNotDeleteExistingBlobIfUploadFails_FileArtifact()
	{
		// create failing mock
		var blobStorageMock = GetBlobStorageMock();
		blobStorageMock.Setup(s => s.UploadBlockBlobAsync(
				It.IsAny<Stream>(),
				It.IsAny<string>(),
				It.IsAny<string>(),
				It.IsAny<CancellationToken>()))
			.Throws<OutOfMemoryException>();

		Assert.ThrowsAsync<OutOfMemoryException>(
			Task.Run(async () =>
			{
				await ArtifactUtils.UpdateArtifactAsync(_fileArtifactDocx, _fileArtifactRequestModel, blobStorageMock.Object, _dbContext);
			}));

		Assert.AreEqual(FileArtifactDocxUrl, _fileArtifactDocx.Url);
		Assert.AreNotEqual(_fileArtifactRequestModel.FileName, _fileArtifactDocx.FileName);
		Assert.IsNull(_uploadedPath);
		Assert.IsNull(_deletedPath);
	}

	[TestMethod]
	public async Task UpdateArtifactAsync_ShouldUpdateUrlArtifact()
	{
		await ArtifactUtils.UpdateArtifactAsync(_urlArtifact, _urlArtifactRequestModel, _blobStorage, _dbContext,
			shouldKeepExistingBlob: true);

		Assert.AreNotEqual(UrlArtifactUrl, _urlArtifact.Url);
		Assert.AreEqual(_urlArtifactRequestModel.Url, _urlArtifact.Url);
		Assert.IsNull(_uploadedPath);
		Assert.IsNull(_deletedPath);
	}

	#endregion UpdateArtifactAsync

	#region UpdateArtifactContentsAsync

	[TestMethod]
	public void UpdateArtifactContentsAsync_ShouldThrowWithNullArguments()
	{
		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await ArtifactUtils.UpdateArtifactContentsAsync(null, _textArtifactRequestModel, _blobStorage, _dbContext);
		}), ExceptionUtils.ArgumentNullExceptionMessage("artifact"));

		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await ArtifactUtils.UpdateArtifactContentsAsync(_textArtifact, null, _blobStorage, _dbContext);
		}), ExceptionUtils.ArgumentNullExceptionMessage("requestModel"));

		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await ArtifactUtils.UpdateArtifactContentsAsync(_textArtifact, _textArtifactRequestModel, null, _dbContext);
		}), ExceptionUtils.ArgumentNullExceptionMessage("artifactStorage"));

		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await ArtifactUtils.UpdateArtifactContentsAsync(_textArtifact, _textArtifactRequestModel, _blobStorage, null);
		}), ExceptionUtils.ArgumentNullExceptionMessage("dbContext"));
	}

	[TestMethod]
	public void UpdateArtifactContentsAsync_ShouldThrowIfRequestModelDoesNotMatchArtifactType()
	{
		Assert.ThrowsAsync<ValidationException>(Task.Run(async () =>
		{
			await ArtifactUtils.UpdateArtifactContentsAsync(_textArtifact, _urlArtifactRequestModel, _blobStorage, _dbContext);
		}), Strings.UpdateArtifactTypeMismatch);

		Assert.ThrowsAsync<ValidationException>(Task.Run(async () =>
		{
			await ArtifactUtils.UpdateArtifactContentsAsync(_fileArtifactDocx, _urlArtifactRequestModel, _blobStorage, _dbContext);
		}), Strings.UpdateArtifactTypeMismatch);

		Assert.ThrowsAsync<ValidationException>(Task.Run(async () =>
		{
			await ArtifactUtils.UpdateArtifactContentsAsync(_urlArtifact, _textArtifactRequestModel, _blobStorage, _dbContext);
		}), Strings.UpdateArtifactTypeMismatch);
	}

	[TestMethod]
	public async Task UpdateArtifactContentsAsync_ShouldUpdateBlob_TextArtifact()
	{
		_textArtifact.DateLastUpdated = Deps.DateTimeProvider.UtcNow.AddMinutes(-1);
		await _dbContext.SaveChangesAsync();

		await ArtifactUtils.UpdateArtifactContentsAsync(_textArtifact, _textArtifactRequestModel, _blobStorage, _dbContext);

		Assert.AreEqual(TextArtifactUrl, _textArtifact.Url);
		Assert.AreEqual(_textArtifactRequestModel.WordCount, _textArtifact.WordCount);
		Assert.AreEqual(ArtifactUtils.GetTextBlobPathFromUrl(_textArtifact.Url), _uploadedPath);
		Assert.IsNull(_deletedPath);
		Assert.AreEqual(_textArtifact.DateLastUpdated, Deps.DateTimeProvider.UtcNow);
	}

	[TestMethod]
	public void UpdateArtifactContentsAsync_ShouldNotDeleteExistingBlobIfUploadFails_TextArtifact()
	{
		// create failing mock
		var blobStorageMock = GetBlobStorageMock();
		blobStorageMock.Setup(s => s.UploadBlockBlobAsync(
				It.IsAny<Stream>(),
				It.IsAny<string>(),
				It.IsAny<string>(),
				It.IsAny<CancellationToken>()))
			.Throws<OutOfMemoryException>();

		Assert.ThrowsAsync<OutOfMemoryException>(
			Task.Run(async () =>
			{
				await ArtifactUtils.UpdateArtifactContentsAsync(_textArtifact, _textArtifactRequestModel, blobStorageMock.Object,
					_dbContext);
			}));

		Assert.AreEqual(TextArtifactUrl, _textArtifact.Url);
		Assert.AreNotEqual(_textArtifactRequestModel.WordCount, _textArtifact.WordCount);
		Assert.IsNull(_uploadedPath);
		Assert.IsNull(_deletedPath);
	}

	[TestMethod]
	public async Task UpdateArtifactContentsAsync_ShouldUpdateBlob_FileArtifact()
	{
		_fileArtifactDocx.DateLastUpdated = Deps.DateTimeProvider.UtcNow.AddMinutes(-1);
		await _dbContext.SaveChangesAsync();

		await ArtifactUtils.UpdateArtifactContentsAsync(_fileArtifactDocx, _fileArtifactRequestModel, _blobStorage, _dbContext);

		Assert.AreEqual(FileArtifactDocxUrl, _fileArtifactDocx.Url);
		Assert.AreEqual(_fileArtifactRequestModel.FileName, _fileArtifactDocx.FileName);
		Assert.AreEqual(ArtifactUtils.GetFileBlobPathFromUrl(_fileArtifactDocx.Url), _uploadedPath);
		Assert.IsNull(_deletedPath);
		Assert.AreEqual(_fileArtifactDocx.DateLastUpdated, Deps.DateTimeProvider.UtcNow);
	}

	[TestMethod]
	public void UpdateArtifactContentsAsync_ShouldNotDeleteExistingBlobIfUploadFails_FileArtifact()
	{
		// create failing mock
		var blobStorageMock = GetBlobStorageMock();
		blobStorageMock.Setup(s => s.UploadBlockBlobAsync(
				It.IsAny<Stream>(),
				It.IsAny<string>(),
				It.IsAny<string>(),
				It.IsAny<CancellationToken>()))
			.Throws<OutOfMemoryException>();

		Assert.ThrowsAsync<OutOfMemoryException>(
			Task.Run(async () =>
			{
				await ArtifactUtils.UpdateArtifactContentsAsync(_fileArtifactDocx, _fileArtifactRequestModel, blobStorageMock.Object,
					_dbContext);
			}));

		Assert.AreEqual(FileArtifactDocxUrl, _fileArtifactDocx.Url);
		Assert.AreNotEqual(_fileArtifactRequestModel.FileName, _fileArtifactDocx.FileName);
		Assert.IsNull(_uploadedPath);
		Assert.IsNull(_deletedPath);
	}

	[TestMethod]
	public async Task UpdateArtifactContentsAsync_ShouldUpdateUrlArtifact()
	{
		await ArtifactUtils.UpdateArtifactContentsAsync(_urlArtifact, _urlArtifactRequestModel, _blobStorage, _dbContext);

		Assert.AreNotEqual(UrlArtifactUrl, _urlArtifact.Url);
		Assert.AreEqual(_urlArtifactRequestModel.Url, _urlArtifact.Url);
		Assert.IsNull(_uploadedPath);
		Assert.IsNull(_deletedPath);
	}

	#endregion UpdateArtifactContentsAsync

	#region StreamFileArtifactAsync

	[TestMethod]
	public void StreamFileArtifactAsync_ShouldThrowWithNullArguments()
	{
		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await ArtifactUtils.StreamFileArtifactAsync(null, new MemoryStream(), _blobStorage);
		}), ExceptionUtils.ArgumentNullExceptionMessage("artifact"));

		Assert.ThrowsAsync<ArgumentException>(Task.Run(async () =>
		{
			_fileArtifactPdf.Url = null;
			await ArtifactUtils.StreamFileArtifactAsync(_fileArtifactPdf, new MemoryStream(), _blobStorage);
		}), ExceptionUtils.ArgumentExceptionMessage(Strings.ArtifactUrlRequired, "artifact"));

		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await ArtifactUtils.StreamFileArtifactAsync(_fileArtifactDocx, null, _blobStorage);
		}), ExceptionUtils.ArgumentNullExceptionMessage("stream"));

		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await ArtifactUtils.StreamFileArtifactAsync(_fileArtifactDocx, new MemoryStream(), null);
		}), ExceptionUtils.ArgumentNullExceptionMessage("artifactStorage"));
	}

	#endregion StreamFileArtifactAsync

	#region AssertAssignmentFileExtensionIsValid

	[TestMethod]
	public void AssertAssignmentFileExtensionIsValid_ShouldThrowWithNullArguments()
	{
		Assert.Throws<ArgumentNullException>(() =>
		{
			ArtifactUtils.AssertAssignmentFileExtensionIsValid(null);
		}, ExceptionUtils.ArgumentNullExceptionMessage("businessModel"));
	}

	[TestMethod]
	public void AssertAssignmentFileExtensionIsValid_ShouldNotThrowIfNotFileArtifact()
	{
		ArtifactUtils.AssertAssignmentFileExtensionIsValid(_textArtifactRequestModel);
	}

	[TestMethod]
	public void AssertAssignmentFileExtensionIsValid_ShouldThrowWithNoExtension()
	{
		Assert.Throws<ValidationException>(() =>
		{
			_fileArtifactRequestModel.FileName = null;
			ArtifactUtils.AssertAssignmentFileExtensionIsValid(_fileArtifactRequestModel);
		}, Strings.FileArtifactExtensionNotAllowed);
	}

	[TestMethod]
	public void AssertAssignmentFileExtensionIsValid_ShouldThrowWithInvalidExtension()
	{
		Assert.Throws<ValidationException>(() =>
		{
			_fileArtifactRequestModel.FileName = "dog.exe";
			ArtifactUtils.AssertAssignmentFileExtensionIsValid(_fileArtifactRequestModel);
		}, Strings.FileArtifactExtensionNotAllowed);
	}

	[TestMethod]
	public void AssertAssignmentFileExtensionIsValid_ShouldNotThrowWithValidExtension()
	{
		ArtifactUtils.AssertAssignmentFileExtensionIsValid(_fileArtifactRequestModel);
	}

	#endregion AssertAssignmentFileExtensionIsValid

	#region AssertFileExtensionIsValid

	[TestMethod]
	public void AssertFileExtensionIsValid_ShouldThrowWithNullArguments()
	{
		Assert.Throws<ArgumentNullException>(() =>
		{
			ArtifactUtils.AssertFileExtensionIsValid(null, ".pdf,.docx");
		}, ExceptionUtils.ArgumentNullExceptionMessage("businessModel"));

		Assert.Throws<ArgumentNullException>(() =>
		{
			ArtifactUtils.AssertFileExtensionIsValid(_fileArtifactRequestModel, null);
		}, ExceptionUtils.ArgumentNullExceptionMessage("allowedFileExtensions"));
	}

	[TestMethod]
	public void AssertFileExtensionIsValid_ShouldNotThrowIfNotFileArtifact()
	{
		ArtifactUtils.AssertFileExtensionIsValid(_urlArtifactRequestModel, ".pdf,.docx");
	}

	[TestMethod]
	public void AssertFileExtensionIsValid_ShouldThrowWithNoExtension()
	{
		Assert.Throws<ValidationException>(() =>
		{
			_fileArtifactRequestModel.FileName = null;
			ArtifactUtils.AssertFileExtensionIsValid(_fileArtifactRequestModel, ".pdf,.docx");
		}, Strings.FileArtifactExtensionNotAllowed);
	}

	[TestMethod]
	public void AssertFileExtensionIsValid_ShouldThrowWithInvalidExtension()
	{
		Assert.Throws<ValidationException>(() =>
		{
			ArtifactUtils.AssertFileExtensionIsValid(_fileArtifactRequestModel, ".mov");
		}, Strings.FileArtifactExtensionNotAllowed);
	}

	[TestMethod]
	public void AssertFileExtensionIsValid_ShouldNotThrowWithValidExtension()
	{
		ArtifactUtils.AssertFileExtensionIsValid(_fileArtifactRequestModel, ".docx");
	}

	#endregion AssertFileExtensionIsValid

	#region CopyBlobAsync

	[TestMethod]
	public void CloneAsync_ShouldThrowWithNullArguments()
	{
		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await ArtifactUtils.CopyBlobAsync(null, _fileArtifactDocx2, _blobStorage);
		}), ExceptionUtils.ArgumentNullExceptionMessage("source"));

		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await ArtifactUtils.CopyBlobAsync(_fileArtifactDocx, null, _blobStorage);
		}), ExceptionUtils.ArgumentNullExceptionMessage("destination"));

		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await ArtifactUtils.CopyBlobAsync(_fileArtifactDocx, _fileArtifactDocx2, null);
		}), ExceptionUtils.ArgumentNullExceptionMessage("artifactStorage"));

		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await ArtifactUtils.CopyBlobAsync(_fileArtifactDocx, _fileArtifactDocx2, _blobStorage, _blobStorage);
		}), ExceptionUtils.ArgumentNullExceptionMessage("targetCreatedById"));
	}

	[TestMethod]
	public void CloneAsync_ShouldThrowIfSourceAndDestinationArtifactTypesDoNotMatch()
	{
		Assert.ThrowsAsync<ArgumentException>(Task.Run(async () =>
		{
			await ArtifactUtils.CopyBlobAsync(_textArtifact, _fileArtifactDocx, _blobStorage);
		}), ExceptionUtils.ArgumentExceptionMessage(Strings.ArtifactTypesMustMatch));
	}

	[TestMethod]
	public async Task CloneAsync_ShouldReturnFalseIfNotTextOrFileArtifact()
	{
		var result = await ArtifactUtils.CopyBlobAsync(_urlArtifact, _urlArtifact2, _blobStorage);
		Assert.IsNull(result);
	}

	[TestMethod]
	public async Task CloneAsync_ShouldCloneTextArtifact()
	{
		await ArtifactUtils.CopyBlobAsync(_textArtifact, _textArtifact2, _blobStorage);
		_blobStorageMock.Verify(s => s.CopyBlockBlobAsync(It.IsAny<string>(), It.IsAny<string>(),
			It.IsAny<CancellationToken>()), Times.Once);
	}

	[TestMethod]
	public async Task CloneAsync_ShouldCloneFileArtifact()
	{
		await ArtifactUtils.CopyBlobAsync(_fileArtifactDocx, _fileArtifactDocx2, _blobStorage);
		_blobStorageMock.Verify(s => s.CopyBlockBlobAsync(It.IsAny<string>(), It.IsAny<string>(),
			It.IsAny<CancellationToken>()), Times.Once);
	}

	[TestMethod]
	public async Task CloneAsync_ShouldCloneTextArtifact_ToTargetBlobStorage()
	{
		var mockBlobStorage = new Mock<IBlobStorage>();
		mockBlobStorage.Setup(s =>
			s.UploadBlockBlobAsync(It.IsAny<Stream>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<CancellationToken>()));
		const string url = "https://test-uri/";
		mockBlobStorage.Setup(s => s.UrlForBlockBlob(It.IsAny<string>()))
			.Returns(url);
		const string targetCreatedById = "test-user-id";
		await ArtifactUtils.CopyBlobAsync(_textArtifact, _textArtifact2, _blobStorage, mockBlobStorage.Object, targetCreatedById);
		mockBlobStorage.Verify(s => s.UploadBlockBlobAsync(It.IsAny<Stream>(), It.IsAny<string>(), It.IsAny<string>(),
			It.IsAny<CancellationToken>()), Times.Once);
		Assert.AreEqual(targetCreatedById, _textArtifact2.CreatedById);
		Assert.AreEqual(url, _textArtifact2.Url);
	}

	[TestMethod]
	public async Task CloneAsync_ShouldSucceedCloningToTargetBlobStorage_FileArtifact()
	{
		var mockBlobStorage = new Mock<IBlobStorage>();
		mockBlobStorage.Setup(s =>
			s.UploadBlockBlobAsync(It.IsAny<Stream>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<CancellationToken>()));
		const string url = "https://test-uri/";
		mockBlobStorage.Setup(s => s.UrlForBlockBlob(It.IsAny<string>()))
			.Returns(url);
		const string targetCreatedById = "test-user-id";
		await ArtifactUtils.CopyBlobAsync(_fileArtifactDocx, _fileArtifactDocx2, _blobStorage, mockBlobStorage.Object, targetCreatedById);
		mockBlobStorage.Verify(s => s.UploadBlockBlobAsync(It.IsAny<Stream>(), It.IsAny<string>(), It.IsAny<string>(),
			It.IsAny<CancellationToken>()), Times.Once);
		Assert.AreEqual(targetCreatedById, _fileArtifactDocx2.CreatedById);
		Assert.AreEqual(url, _fileArtifactDocx2.Url);
	}

	#endregion CopyBlobAsync

	#region DeleteBlobAsync

	[TestMethod]
	public void DeleteBlobAsync_ShouldThrowWithNullArguments()
	{
		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await ArtifactUtils.DeleteBlobAsync(null, _blobStorage);
		}), ExceptionUtils.ArgumentNullExceptionMessage("artifact"));

		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await ArtifactUtils.DeleteBlobAsync(_fileArtifactDocx, null);
		}), ExceptionUtils.ArgumentNullExceptionMessage("artifactStorage"));
	}

	[TestMethod]
	public async Task DeleteBlobAsync_ShouldDeleteTextArtifact()
	{
		await ArtifactUtils.DeleteBlobAsync(_textArtifact, _blobStorage);
		Assert.AreEqual(TextArtifactBlobPath, _deletedPath);
	}

	[TestMethod]
	public async Task DeleteBlobAsync_ShouldDeleteFileArtifact()
	{
		await ArtifactUtils.DeleteBlobAsync(_fileArtifactDocx, _blobStorage);
		Assert.AreEqual(FileArtifactDocxBlobPath, _deletedPath);
	}

	[TestMethod]
	public void DeleteBlobAsync_ShouldThrowIfUrlArtifact()
	{
		Assert.ThrowsAsync<NotSupportedException>(Task.Run(async () =>
		{
			await ArtifactUtils.DeleteBlobAsync(_urlArtifact, _blobStorage);
		}));
	}

	#endregion DeleteBlobAsync

	#region IsTextNullOrEmpty

	/// <summary>
	/// Tuples for testing <see cref="ArtifactUtils.IsTextNullOrEmpty"/>, containing the expected result and the example text.
	/// </summary>
	private readonly List<(bool, string)> _isTextNullOrEmptyExamples = new()
	{
		(true, null),
		(true, ""),
		(true, " "),
		(true, "<p><br></p>"),
		(true, "<p></p>"),
		(true, "<p> </p>"),
		(true, "<ol><li></li></ol>"),
		(true, "<ul><li><sub></sub></li></ul>"),
		(true, "<p><span class=\"ql-size-huge\"></span></p>"),
		(false, "<p>word</p>"),
		(false, "<p class=\"ql-indent-1\">indented</p>"),
		(false, "<p><em>italic</em></p>"),
		(false, "<p><strong>bold</strong></p>"),
		(false, "<ul><li>item</li></ul>"),
		(false, "<ol><li>number</li></ol>"),
		(false, "<p><span class=\"ql-size-huge\">wumbo</span></p>"),
		(false, "<p><img src=\"url\" /></p>"),
		(false,
			"<iframe class=\"ql-video\" frameborder=\"0\" allowfullscreen=\"true\" src=\"https://www.youtube.com/embed/X492gdfNZBc\"></iframe>"),
		(false,
			"<p><span class=\"variable\" data-value=\"a\">﻿<span contenteditable=\"false\"><span class=\"sr-only\">the variable, A. </span><span aria-hidden=\"true\" data-original-content=\"\variable{a}\" data-original-mathstyle=\"displaystyle\"><span class=\"ML__mathlive\"><span class=\"ML__strut\" style=\"height:0.43em;\"></span><span class=\"ML__base\"><span class=\"pill red-pill-outline ML__variable\"><span class=\"ML__mathit\">a</span></span></span></span></span></span>﻿</span></p>"),
		(false,
			"<p><span class=\"ql-formula\" data-value=\"1+2\" style=\"font-size: 1.1rem;\">﻿<span contenteditable=\"false\"><span class=\"sr-only\">1 plus 2</span><span aria-hidden=\"true\" data-original-content=\"1+2\" data-original-mathstyle=\"displaystyle\"><span class=\"ML__mathlive\"><span class=\"ML__strut\" style=\"height:0.64em;\"></span><span class=\"ML__strut--bottom\" style=\"height:0.72em;vertical-align:-0.09em;\"></span><span class=\"ML__base\"><span class=\"ML__mathrm\">1</span><span class=\"ML__mathrm\" style=\"margin-left:0.22em;\">+</span><span class=\"ML__mathrm\" style=\"margin-left:0.22em;\">2</span></span></span></span></span>﻿</span>"),
		(false, "<p><span class=\"variable\" data-value=\"a\"></span></p>"),
		(false, "<p><span class=\"ql-formula\" data-value=\"1+2\" style=\"font-size: 1.1rem;\"></span></p>"),
		(false, "<p><span class=\"ql-formula plain-text\" data-value=\"-1\"></span></p>")
	};

	[TestMethod]
	public void IsTextNullOrEmpty_ShouldReturnExpectedResults()
	{
		foreach (var (expected, text) in _isTextNullOrEmptyExamples)
		{
			var actual = ArtifactUtils.IsTextNullOrEmpty(text);
			Assert.AreEqual(expected, actual);
		}
	}

	#endregion IsTextNullOrEmpty

	#region GetFileBlobPathFromUrl

	[TestMethod]
	public void GetFileBlobPathFromUrl_ShouldThrowWithNullParameters()
	{
		Assert.Throws<ArgumentNullException>(() =>
		{
			ArtifactUtils.GetFileBlobPathFromUrl(null);
		}, ExceptionUtils.ArgumentNullExceptionMessage("url"));

		Assert.Throws<ArgumentNullException>(() =>
		{
			ArtifactUtils.GetFileBlobPathFromUrl("");
		}, ExceptionUtils.ArgumentNullExceptionMessage("url"));
	}

	[TestMethod]
	public void GetFileBlobPathFromUrl_ShouldThrowIfArtifactUrlIsInvalid()
	{
		Assert.Throws<ArgumentException>(() =>
		{
			ArtifactUtils.GetFileBlobPathFromUrl(TextArtifactUrl);
		}, ExceptionUtils.ArgumentExceptionMessage(Strings.ArtifactUrlInvalid, "url"));

		Assert.Throws<ArgumentException>(() =>
		{
			ArtifactUtils.GetFileBlobPathFromUrl($"{StorageHost}{ArtifactUtils.FileStorageLocation}");
		}, ExceptionUtils.ArgumentExceptionMessage(Strings.ArtifactUrlInvalid, "url"));
	}

	[TestMethod]
	public void GetFileBlobPathFromUrl_ShouldSucceed()
	{
		var blobPath = ArtifactUtils.GetFileBlobPathFromUrl(FileArtifactDocxUrl);
		Assert.AreEqual(FileArtifactDocxBlobPath, blobPath);
	}

	#endregion GetFileBlobPathFromUrl

	#region GetTextBlobPathFromUrl

	[TestMethod]
	public void GetTextBlobPathFromUrl_ShouldThrowWithNullParameters()
	{
		Assert.Throws<ArgumentNullException>(() =>
		{
			ArtifactUtils.GetTextBlobPathFromUrl(null);
		}, ExceptionUtils.ArgumentNullExceptionMessage("url"));

		Assert.Throws<ArgumentNullException>(() =>
		{
			ArtifactUtils.GetTextBlobPathFromUrl("");
		}, ExceptionUtils.ArgumentNullExceptionMessage("url"));
	}

	[TestMethod]
	public void GetTextBlobPathFromUrl_ShouldThrowIfArtifactUrlIsInvalid()
	{
		Assert.Throws<ArgumentException>(() =>
		{
			ArtifactUtils.GetTextBlobPathFromUrl(FileArtifactDocxUrl);
		}, ExceptionUtils.ArgumentExceptionMessage(Strings.ArtifactUrlInvalid, "url"));

		Assert.Throws<ArgumentException>(() =>
		{
			ArtifactUtils.GetTextBlobPathFromUrl($"{StorageHost}{ArtifactUtils.TextStorageLocation}");
		}, ExceptionUtils.ArgumentExceptionMessage(Strings.ArtifactUrlInvalid, "url"));
	}

	[TestMethod]
	public void GetTextBlobPathFromUrl_ShouldSucceed()
	{
		var blobPath = ArtifactUtils.GetTextBlobPathFromUrl(TextArtifactUrl);
		Assert.AreEqual(TextArtifactBlobPath, blobPath);
	}

	#endregion GetTextBlobPathFromUrl

	#region ExtractTextFromArtifactAsync

	[TestMethod]
	public void ExtractTextFromArtifactAsync_ShouldThrowWithNullArguments()
	{
		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await ArtifactUtils.ExtractTextFromArtifactAsync(null, _fileArtifactDocx);
		}), ExceptionUtils.ArgumentNullExceptionMessage("httpClientFactory"));

		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await ArtifactUtils.ExtractTextFromArtifactAsync(() => new Mock<IHttpClient>().Object, null);
		}), ExceptionUtils.ArgumentNullExceptionMessage("artifact"));
	}

	[TestMethod]
	public void ExtractTextFromArtifactAsync_ShouldThrowIfUrlArtifact()
	{
		Assert.ThrowsAsync<ArgumentException>(Task.Run(async () =>
		{
			await ArtifactUtils.ExtractTextFromArtifactAsync(() => new Mock<IHttpClient>().Object, _urlArtifact);
		}), ExceptionUtils.ArgumentExceptionMessage(Strings.UrlArtifactNotSupported, "artifact"));
	}

	[TestMethod]
	public void ExtractTextFromArtifactAsync_ShouldThrowIfInvalidFileExtension()
	{
		Assert.ThrowsAsync<Exception>(Task.Run(async () =>
		{
			_fileArtifactDocx.FileName = "cat.png";
			await ArtifactUtils.ExtractTextFromArtifactAsync(() => new Mock<IHttpClient>().Object, _fileArtifactDocx);
		}), Strings.ArtifactExtractTextError);
	}

	[TestMethod]
	public async Task ExtractTextFromArtifactAsync_ShouldExtractFromDocxFileArtifact()
	{
		var assemblyLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
		Debug.Assert(assemblyLocation != null, nameof(assemblyLocation) + " != null");
		var fileBytes = await File.ReadAllBytesAsync(Path.Combine(assemblyLocation, "Files/extract-text-test.docx"));

		// NOTE: the indentation and line breaks do not match the other file types
		// but do resemble the contents of the document
		const string fileArtifactDocxExtractedText = @"heading 1
heading 2
paragraph text with bold italic underline base_sub base^sup strike-through custom font large font size red color highlighted

align center

align right

image with alt text: variate.png



	1. level one, 1
	2. level one, 2

	* bullet one
			* bullet nested
	* bullet two

	a. level one, a
	b. level one, b

	A. level one, A
	B. level one, B

1) level one, 1
	a) level two, a
		i) level three, i
			(1) level four, 1

	i. level one, i
	ii. level one, ii

	I. level one, I
	II. level one, II

indented paragraph
	indented level-two paragraph
													indented level-eight paragraph

Vibin’

cell 1.1

cell 1.2

cell 2.1

cell 2.2

	* bullet one
	* bullet two

cell line one
cell line two


";

		var httpClientMock = new Mock<IHttpClient>();
		httpClientMock.Setup(c =>
				c.GetByteArrayAsync(
					It.Is<Uri>(uri => uri.AbsoluteUri == FileArtifactDocxUrl),
					It.IsAny<CancellationToken>(),
					null,
					null))
			.ReturnsAsync(fileBytes);

		var text = await ArtifactUtils.ExtractTextFromArtifactAsync(() => httpClientMock.Object, _fileArtifactDocx);
		Assert.AreEqual(fileArtifactDocxExtractedText, text);
	}

	[TestMethod]
	public async Task ExtractTextFromArtifactAsync_ShouldExtractFromPdfFileArtifact()
	{
		var assemblyLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
		Debug.Assert(assemblyLocation != null, nameof(assemblyLocation) + " != null");
		var fileBytes = await File.ReadAllBytesAsync(Path.Combine(assemblyLocation, "Files/extract-text-test.pdf"));

		// NOTE the expected text result contains some trailing spaces which Rider keeps removing in auto-formatting, so it now lives in a separate file
		// we have less control over the PDF output versus the two other methods
		var fileArtifactPdfExtractedText =
			await File.ReadAllTextAsync(Path.Combine(assemblyLocation, "Files/extract-text-test-pdf-expected.txt"));

		var httpClientMock = new Mock<IHttpClient>();
		httpClientMock.Setup(c =>
				c.GetByteArrayAsync(
					It.Is<Uri>(uri => uri.AbsoluteUri == FileArtifactPdfUrl),
					It.IsAny<CancellationToken>(),
					null,
					null))
			.ReturnsAsync(fileBytes);

		var text = await ArtifactUtils.ExtractTextFromArtifactAsync(() => httpClientMock.Object, _fileArtifactPdf);
		Assert.AreEqual(fileArtifactPdfExtractedText, text);
	}

	[TestMethod]
	public async Task ExtractTextFromArtifactAsync_ShouldExtractFromTextArtifact()
	{
		// quill html does not contain any line breaks between tags
		const string textArtifactHtml =
			@"<h1>heading 1</h1><h2>heading 2</h2><p>paragraph text with <strong>bold</strong> <em>italic</em> <u>underline</u> base<sub>sub</sub> base<sup>sup</sup> base<sub></sub> <s>strike-through</s> <span class=""ql-font-serif"">custom font</span> <span class=""ql-size-large"">large font size</span></p><p class=""ql-align-center"">align center</p><p class=""ql-align-right"">align right</p><p><img alt=""variate.png"" src=""..."" height=""auto"" width=""auto"" /></p><p><img alt="""" src=""..."" height=""auto"" width=""auto"" /></p><ol><li>level one, 1</li><li>level one, 2</li></ol><ul><li>bullet one</li><li class=""ql-indent-1"">bullet nested</li><li>bullet two</li></ul><ol data-value=""alpha""><li>level one, a</li><li class=""ql-indent-1"">level two, i</li><li>level one, b</li></ol><ol><li>level one, 1</li><li class=""ql-indent-1"">level two, a</li><li class=""ql-indent-2"">level three, i</li><li class=""ql-indent-3"">level four, 1</li></ol><p><span class=""ql-formula"" data-value=""x=1""></span></p><p class=""ql-indent-1"">indented level-one paragraph</p><p class=""ql-indent-2"">indented level-two paragraph</p><p class=""ql-indent-8"">indented level-eight paragraph</p><iframe class=""ql-video"" frameborder=""0"" allowfullscreen=""true"" src=""https://www.youtube.com/embed/qMQ-y9dHE2k?showinfo=0""></iframe><p><br /></p><p><a href=""https://www.youtube.com/watch?v=qMQ-y9dHE2k"" rel=""noopener noreferrer"" target=""_blank"">Vibin'</a></p><div data-value=""table-l4ch"" class=""table-container""><table data-value=""table-l4ch""><tbody data-value=""table-l4ch""><tr data-value=""table-l4ch|tr-ri95""><td data-value=""table-l4ch|tr-ri95|td-ci8w""><p>cell 1.1</p></td><td data-value=""table-l4ch|tr-ri95|td-bko4""><p>cell 1.2</p></td></tr><tr data-value=""table-l4ch|tr-yq6g""><td data-value=""table-l4ch|tr-yq6g|td-55am""><p>cell 2.1</p></td><td data-value=""table-l4ch|tr-yq6g|td-xix8""><p>cell 2.2</p></td></tr><tr data-value=""table-l4ch|tr-sk9s""><td data-value=""table-l4ch|tr-sk9s|td-3jod""><ul><li>bullet one</li><li>bullet two</li></ul></td><td data-value=""table-l4ch|tr-sk9s|td-jl7q""><p>cell line one</p><p>cell line two</p></td></tr></tbody></table></div><p><span class=""variable"" data-value=""variableName""></span></p><blockquote>block quote</blockquote><pre class=""ql-syntax"" spellcheck=""false"">code block</pre>";

		// NOTE: the indentation and line breaks do not match the other file types
		// but do resemble the contents of quill
		const string textArtifactExtractedText = @"heading 1

heading 2

paragraph text with bold italic underline base_sub base^sup base strike-through custom font large font size

align center

align right

image with alt text: variate.png



1. level one, 1
2. level one, 2

* bullet one
	* bullet nested
* bullet two

a. level one, a
	i. level two, i
b. level one, b

1. level one, 1
	a. level two, a
		i. level three, i
			1. level four, 1

x=1

	indented level-one paragraph

		indented level-two paragraph

								indented level-eight paragraph



Vibin'

cell 1.1

cell 1.2

cell 2.1

cell 2.2

* bullet one
* bullet two

cell line one
cell line two

variableName

block quote

code block

";

		var httpClientMock = new Mock<IHttpClient>();
		httpClientMock.Setup(c =>
				c.GetByteArrayAsync(
					It.Is<Uri>(uri => uri.AbsoluteUri == TextArtifactUrl),
					It.IsAny<CancellationToken>(),
					null,
					null))
			.ReturnsAsync(Encoding.UTF8.GetBytes(textArtifactHtml));

		var text = await ArtifactUtils.ExtractTextFromArtifactAsync(() => httpClientMock.Object, _textArtifact);
		Assert.AreEqual(textArtifactExtractedText, text);
	}

	#endregion ExtractTextFromArtifactAsync

	#region Private Methods

	private Mock<IBlobStorage> GetBlobStorageMock()
	{
		var blobStorageMock = new Mock<IBlobStorage>();
		blobStorageMock.Setup(s => s.UrlForBlockBlob(It.IsAny<string>()))
			.Returns<string>(path => $"{StorageHost}{path}");

		blobStorageMock.Setup(s => s.UploadBlockBlobAsync(
				It.IsAny<Stream>(),
				It.IsAny<string>(),
				It.IsAny<string>(),
				It.IsAny<CancellationToken>()))
			.Callback((Stream _, string blobPath, string _, CancellationToken _) =>
			{
				_uploadedPath = blobPath;
			})
			.Returns(Task.CompletedTask);

		blobStorageMock.Setup(s => s.DeleteBlockBlobAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()))
			.Callback((string blobPath, CancellationToken _) =>
			{
				_deletedPath = blobPath;
			})
			.Returns(Task.CompletedTask);

		return blobStorageMock;
	}

	#endregion Private Methods
}