using FluentValidation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MSTestExtensions;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.BusinessLogic.Utils;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.Models.Interfaces;
using StudioKit.Scaffolding.Properties;
using StudioKit.Tests;
using StudioKit.Utilities.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Tests.BusinessLogic.Utils;

[TestClass]
public class LockDownBrowserUtilsTests : BaseTest
{
	private ILockDownBrowserEntity _entity;
	private ClaimsIdentity _identity;
	private List<Claim> _claims;
	private const int EntityId = 1;
	private ILockDownBrowserConfiguration _configuration;
	private LockDownBrowserUtils _utils;

	[TestInitialize]
	public void BeforeEach()
	{
		_entity = Mock.Of<ILockDownBrowserEntity>(e =>
			e.IsLockDownBrowserRequired == true &&
			e.IsLockDownBrowserRequiredForResults == true &&
			e.Id == EntityId);

		_claims = new List<Claim>();
		var identityMock = new Mock<ClaimsIdentity>();
		identityMock.Setup(i => i.AddClaim(It.IsAny<Claim>())).Callback<Claim>(claim => { _claims.Add(claim); });
		identityMock.SetupGet(i => i.Claims).Returns(_claims);
		_identity = identityMock.Object;

		var mockConfiguration = new Mock<ILockDownBrowserConfiguration>();
		mockConfiguration.Setup(c => c.Secret1).Returns("xxxxxxxxxxxxxxxx");
		mockConfiguration.Setup(c => c.Secret2).Returns("yyyyyyyyyyyyyyyy");
		mockConfiguration.Setup(c => c.SecretIV).Returns("zzzzzzzzzzzzzzzz");
		mockConfiguration.Setup(c => c.SecretIndex).Returns("aa");
		mockConfiguration.Setup(c => c.SecretVersion).Returns("1");
		_configuration = mockConfiguration.Object;

		_utils = new LockDownBrowserUtils(_configuration);
	}

	#region AssertLockDownBrowserAccess

	[TestMethod]
	public void AssertLockDownBrowserAccess_ShouldThrowWithNullEntity()
	{
		Assert.Throws<ArgumentNullException>(() =>
		{
			LockDownBrowserUtils.AssertLockDownBrowserAccess(null, _identity);
		}, ExceptionUtils.ArgumentNullExceptionMessage("claimedEntity"));
	}

	[TestMethod]
	public void AssertLockDownBrowserAccess_ShouldThrowWithNullIdentity()
	{
		Assert.Throws<ArgumentNullException>(() =>
		{
			LockDownBrowserUtils.AssertLockDownBrowserAccess(_entity, null);
		}, ExceptionUtils.ArgumentNullExceptionMessage("identity"));
	}

	[TestMethod]
	public void AssertLockDownBrowserAccess_ShouldNotThrowIfLockDownBrowserNotRequired()
	{
		_entity.IsLockDownBrowserRequired = false;
		LockDownBrowserUtils.AssertLockDownBrowserAccess(_entity, _identity);
	}

	[TestMethod]
	public void AssertLockDownBrowserAccess_ShouldNotThrowIfLockDownBrowserNotRequiredForResults()
	{
		_entity.IsLockDownBrowserRequiredForResults = false;
		LockDownBrowserUtils.AssertLockDownBrowserAccess(_entity, _identity, true);
	}

	[TestMethod]
	public void AssertLockDownBrowserAccess_ShouldThrowIfLockDownBrowserRequiredAndNoClaims()
	{
		Assert.Throws<ForbiddenException>(() =>
		{
			LockDownBrowserUtils.AssertLockDownBrowserAccess(_entity, _identity);
		}, Strings.LockDownBrowserClientIsNotLockDownBrowser);
	}

	[TestMethod]
	public void AssertLockDownBrowserAccess_ShouldThrowIfLockDownBrowserRequiredAndWrongClaimValue()
	{
		_identity.AddClaim(new Claim(ScaffoldingClaimTypes.ClientIsLockDownBrowser, "foobar"));
		Assert.Throws<ForbiddenException>(() =>
		{
			LockDownBrowserUtils.AssertLockDownBrowserAccess(_entity, _identity);
		}, Strings.LockDownBrowserClientIsNotLockDownBrowser);
	}

	[TestMethod]
	public void AssertLockDownBrowserAccess_ShouldThrowIfLockDownBrowserRequiredAndNoEntityClaim()
	{
		_identity.AddClaim(new Claim(ScaffoldingClaimTypes.ClientIsLockDownBrowser, LockDownBrowserConstants.True));
		Assert.Throws<ForbiddenException>(() =>
		{
			LockDownBrowserUtils.AssertLockDownBrowserAccess(_entity, _identity);
		}, Strings.LockDownBrowserBadEntityId);
	}

	[TestMethod]
	public void AssertLockDownBrowserAccess_ShouldThrowIfLockDownBrowserRequiredAndWrongEntityClaim()
	{
		_identity.AddClaim(new Claim(ScaffoldingClaimTypes.ClientIsLockDownBrowser, LockDownBrowserConstants.True));
		_identity.AddClaim(new Claim(ScaffoldingClaimTypes.LockDownBrowserEntityId, "-1"));
		Assert.Throws<ForbiddenException>(() =>
		{
			LockDownBrowserUtils.AssertLockDownBrowserAccess(_entity, _identity);
		}, Strings.LockDownBrowserBadEntityId);
	}

	[TestMethod]
	public void AssertLockDownBrowserAccess_ShouldSucceedWithCorrectClaims()
	{
		_identity.AddClaim(new Claim(ScaffoldingClaimTypes.ClientIsLockDownBrowser, LockDownBrowserConstants.True));
		_identity.AddClaim(new Claim(ScaffoldingClaimTypes.LockDownBrowserEntityId, EntityId.ToString()));
		LockDownBrowserUtils.AssertLockDownBrowserAccess(_entity, _identity);
	}

	#endregion AssertLockDownBrowserAccess

	#region GetLaunchUrl

	[TestMethod]
	public void GetLaunchUrl_ShouldThrowWithNullData()
	{
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(() => { _utils.GetLaunchUrl(null); }),
			ExceptionUtils.ArgumentNullExceptionMessage("data"));
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(() => { _utils.GetLaunchUrl(""); }),
			ExceptionUtils.ArgumentNullExceptionMessage("data"));
	}

	[TestMethod]
	public void GetLaunchUrl_ShouldSucceed()
	{
		const string data =
			$"<z><u>https://example.com/restart?userId=abc&entityId=1&redirectPath=%2fquiz%2f1&{LockDownBrowserKey.PageTitle}=Custom+Title</u></z>";
		var launchUrl = _utils.GetLaunchUrl(data);

		Assert.IsNotNull(launchUrl);
		Assert.IsTrue(launchUrl.StartsWith($"ldb1:{_configuration.SecretIndex}:{_configuration.SecretVersion}"));

		var encryptedData = launchUrl.Split(":").Last().Replace("%5B", "").Replace("%5D", "");
		var decryptedData = DecryptData(encryptedData, $"{_configuration.Secret1}{_configuration.Secret2}");

		Assert.AreEqual(
			data.SpacePad16(),
			decryptedData,
			"LaunchUrl should be encrypted with secret1+secret2");
	}

	#endregion GetLaunchUrl

	#region GetChallengeValue

	[TestMethod]
	public void GetChallengeValue_ShouldThrowWithNullChallenge()
	{
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(() => { _utils.GetChallengeValue(null); }),
			ExceptionUtils.ArgumentNullExceptionMessage("challenge"));
	}

	[TestMethod]
	public void GetChallengeValue_ShouldSucceed()
	{
		var challenge = new LockDownBrowserChallenge
		{
			Id = "abc",
			UserId = "xyz",
			EntityId = 1,
			Nonce = "efg",
			RedirectPath = "/quiz/1"
		};
		var challengeValue = _utils.GetChallengeValue(challenge);

		Assert.IsNotNull(challengeValue);
		Assert.IsTrue(challengeValue.StartsWith($"{_configuration.SecretIndex}:{_configuration.SecretVersion}:"));

		var encryptedData = challengeValue.Split(":").Last();
		var decryptedData = DecryptData(encryptedData, $"{_configuration.Secret1}{_configuration.Secret2}");
		var expectedData = LockDownBrowserUtils.GetChallengeData(challenge);

		Assert.AreEqual(
			expectedData,
			decryptedData,
			"ChallengeValue should be encrypted with secret1+secret2");
	}

	#endregion GetChallengeValue

	#region GetChallengeResponseValue

	[TestMethod]
	public void GetChallengeResponseValue_ShouldThrowWithNullChallenge()
	{
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(() => { _utils.GetChallengeResponseValue(null); }),
			ExceptionUtils.ArgumentNullExceptionMessage("challenge"));
	}

	[TestMethod]
	public void GetChallengeResponseValue_ShouldSucceed()
	{
		var challenge = new LockDownBrowserChallenge
		{
			Id = "abc",
			UserId = "xyz",
			EntityId = 1,
			Nonce = "efg",
			RedirectPath = "/quiz/1"
		};
		var challengeResponseValue = _utils.GetChallengeResponseValue(challenge);

		Assert.IsNotNull(challengeResponseValue);

		var decryptedData = DecryptData(challengeResponseValue, $"{_configuration.Secret2}{_configuration.Secret1}");
		var expectedData = LockDownBrowserUtils.GetChallengeData(challenge);

		Assert.AreEqual(
			expectedData,
			decryptedData,
			"ChallengeResponseValue should be encrypted with secret2+secret1");
	}

	#endregion GetChallengeResponseValue

	#region EncryptProctorExitPassword

	[TestMethod]
	public void EncryptProctorExitPassword_ShouldThrowIfPasswordIsNullOrEmpty()
	{
		Assert.ThrowsAsync<ValidationException>(
			Task.Run(() => { _utils.EncryptProctorExitPassword(null); }),
			Strings.LockDownBrowserProctorExitPasswordMinLength);
		Assert.ThrowsAsync<ValidationException>(
			Task.Run(() => { _utils.EncryptProctorExitPassword(""); }),
			Strings.LockDownBrowserProctorExitPasswordMinLength);
	}

	[TestMethod]
	public void EncryptProctorExitPassword_ShouldThrowIfPasswordHasLeadingOrTrailingWhitespace()
	{
		Assert.ThrowsAsync<ValidationException>(
			Task.Run(() => { _utils.EncryptProctorExitPassword(" apples "); }),
			Strings.LockDownBrowserProctorExitPasswordMustTrimWhitespace);
	}

	[TestMethod]
	public void EncryptProctorExitPassword_ShouldThrowIfPasswordContainsNonAsciiChars()
	{
		Assert.ThrowsAsync<ValidationException>(
			Task.Run(() => { _utils.EncryptProctorExitPassword("∆"); }),
			Strings.LockDownBrowserProctorExitPasswordInvalidCharacters);
	}

	[TestMethod]
	public void EncryptProctorExitPassword_ShouldThrowIfPasswordIsLongerThanMaxLength()
	{
		Assert.ThrowsAsync<ValidationException>(
			Task.Run(() => { _utils.EncryptProctorExitPassword("abcdefghijklmnopqrstuvwxyz12345"); }),
			Strings.LockDownBrowserProctorExitPasswordMaxLength);
	}

	[TestMethod]
	public void EncryptProctorExitPassword_ShouldSucceed()
	{
		const string password = "abcdefghijklmnopqrstuvwxyz1234";
		var encryptedPassword = _utils.EncryptProctorExitPassword(password);
		var decryptedPassword = DecryptData(encryptedPassword, $"{_configuration.Secret1}{_configuration.Secret2}");

		Assert.AreEqual(
			password,
			decryptedPassword.Trim(),
			"ProctorExitPassword should be encrypted with secret1+secret2");
	}

	#endregion EncryptProctorExitPassword

	#region DecryptProctorExitPassword

	[TestMethod]
	public void DecryptProctorExitPassword_ShouldThrowIfEncryptedPasswordIsNullOrWhitespace()
	{
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(() => { _utils.DecryptProctorExitPassword(null); }),
			ExceptionUtils.ArgumentNullExceptionMessage("encryptedPassword"));
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(() => { _utils.DecryptProctorExitPassword(""); }),
			ExceptionUtils.ArgumentNullExceptionMessage("encryptedPassword"));
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(() => { _utils.DecryptProctorExitPassword(" "); }),
			ExceptionUtils.ArgumentNullExceptionMessage("encryptedPassword"));
	}

	[TestMethod]
	public void DecryptProctorExitPassword_ShouldSucceed()
	{
		const string password = "abcdefghijklmnopqrstuvwxyz1234";
		var encryptedPassword = _utils.EncryptProctorExitPassword(password);
		var decryptedPassword = _utils.DecryptProctorExitPassword(encryptedPassword);

		Assert.AreEqual(password, decryptedPassword);
	}

	#endregion DecryptProctorExitPassword

	#region Private Methods

	private string DecryptData(string encryptedData, string key)
	{
		var encryptedDataBytes = Convert.FromBase64String(encryptedData);
		var decryptedData = Utilities.Encryption.DecryptStringFromBytesAes(
			encryptedDataBytes,
			Encoding.ASCII.GetBytes(key),
			Encoding.ASCII.GetBytes(_configuration.SecretIV));
		return decryptedData;
	}

	#endregion Private Methods
}