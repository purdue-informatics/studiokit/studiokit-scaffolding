﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MSTestExtensions;
using StudioKit.Data.Entity.Identity;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ExternalProvider.Models.Interfaces;
using StudioKit.Notification.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Services.Authorization;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Properties;
using StudioKit.Scaffolding.Tests.TestData.BusinessLogic.Services;
using StudioKit.Scaffolding.Tests.TestData.DataAccess;
using StudioKit.Scaffolding.Tests.TestData.Models;
using StudioKit.Tests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Tests.BusinessLogic.Services;

[TestClass]
public class BaseGroupUserRoleServiceTests : BaseTest
{
	private static readonly ScaffoldingTestDependencies Deps = new(ScaffoldingTestDependencies.StartDate);
	private TestBaseDbContext _dbContext;
	private IDbContextTransaction _transaction;

	[ClassInitialize]
	public static async Task BeforeAll(TestContext testContext)
	{
		// seed common data that will persist across tests
		await using var dbContext = Deps.PersistentDbContextFactory();
		await Deps.SeedRolesAndUsersAsync(dbContext);
		await Deps.SeedExternalProvidersAsync(dbContext);
	}

	[TestInitialize]
	public void BeforeEach()
	{
		// reset deps, _dbContext, transaction
		Deps.Reset(ScaffoldingTestDependencies.StartDate);
		(_dbContext, _transaction) = Deps.PersistentDbContextAndTransactionFactory();
	}

	[TestCleanup]
	public void AfterEach()
	{
		_transaction.Rollback();
		_transaction.Dispose();
		_dbContext.Dispose();
	}

	[ClassCleanup]
	public static void AfterAll()
	{
		Deps.Dispose();
	}

	#region GetEntityUserRolesAsync

	[TestMethod]
	public async Task GetEntityUserRolesAsync_ShouldThrowWithNullArguments()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);
		var service = Deps.GetGroupUserRoleService(_dbContext);

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await service.GetEntityUserRolesAsync(group.Id, null);
			}), ExceptionUtils.ArgumentNullExceptionMessage("principal"));
	}

	[TestMethod]
	public async Task GetEntityUserRolesAsync_ShouldReturnAllGroupUsers()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);
		var service = Deps.GetGroupUserRoleService(_dbContext);

		var entityUserRoles = await service.GetEntityUserRolesAsync(group.Id, Deps.InstructorPrincipal);
		Assert.AreEqual(3, entityUserRoles.Count());
	}

	#endregion GetEntityUserRolesAsync

	#region CreateEntityUserRolesAsync

	[TestMethod]
	public async Task CreateEntityUserRolesAsync_ShouldThrowWithNullArguments()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);
		var service = Deps.GetGroupUserRoleService(_dbContext);

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await service.CreateEntityUserRolesAsync(group.Id, null, null, null);
			}), ExceptionUtils.ArgumentNullExceptionMessage("principal"));
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await service.CreateEntityUserRolesAsync(group.Id, null, null, Deps.InstructorPrincipal);
			}), ExceptionUtils.ArgumentNullExceptionMessage("identifiers"));
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await service.CreateEntityUserRolesAsync(group.Id, new List<string>(), null, Deps.InstructorPrincipal);
			}), ExceptionUtils.ArgumentNullExceptionMessage("identifiers"));
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await service.CreateEntityUserRolesAsync(group.Id, new List<string> { Deps.StudentId }, null, Deps.InstructorPrincipal);
			}), ExceptionUtils.ArgumentNullExceptionMessage("roleName"));
	}

	[TestMethod]
	public async Task CreateEntityUserRolesAsync_ShouldThrowWithInvalidGroupRole()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);
		var service = Deps.GetGroupUserRoleService(_dbContext);

		Assert.ThrowsAsync<ArgumentOutOfRangeException>(
			Task.Run(async () =>
			{
				await service.CreateEntityUserRolesAsync(
					group.Id,
					new List<string> { Deps.StudentId },
					BaseRole.Admin,
					Deps.InstructorPrincipal);
			}),
			$"{string.Format(Strings.EntityRoleNotValid, BaseRole.Admin)} (Parameter 'roleName')");
	}

	[TestMethod]
	public async Task CreateEntityUserRolesAsync_ShouldThrowIfGroupRoleDoesNotExist()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);

		var role = await _dbContext.Roles.SingleAsync(r => r.Name == BaseRole.GroupGrader);
		_dbContext.Roles.Remove(role);
		await _dbContext.SaveChangesAsync();

		var service = Deps.GetGroupUserRoleService(_dbContext);

		Assert.ThrowsAsync<EntityNotFoundException>(
			Task.Run(async () =>
			{
				await service.CreateEntityUserRolesAsync(
					group.Id,
					new List<string> { Deps.StudentId },
					BaseRole.GroupGrader,
					Deps.InstructorPrincipal);
			}),
			string.Format(Strings.RoleNotFound, BaseRole.GroupGrader));
	}

	[TestMethod]
	public async Task CreateEntityUserRolesAsync_ShouldAddNewGroupUser()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);
		var service = Deps.GetGroupUserRoleService(_dbContext);
		var studentGroupUserRole = group.GroupUserRoles.Single(gur => gur.UserId.Equals(Deps.StudentId));

		await service.DeleteEntityUserRoleAsync(studentGroupUserRole.Id, Deps.InstructorPrincipal);

		var results = await service.CreateEntityUserRolesAsync(
			group.Id,
			new List<string> { $"{Deps.StudentId}@localhost.com" },
			BaseRole.GroupLearner,
			Deps.InstructorPrincipal);

		Assert.AreEqual(1, results.AddedUserRoles.Count(), "AddedUserRoles");
		Assert.AreEqual(0, results.ExistingUserRoles.Count(), "ExistingUserRoles");
		Assert.AreEqual(0, results.InvalidIdentifiers.Count(), "InvalidIdentifiers");
		Assert.AreEqual(0, results.InvalidDomainIdentifiers.Count(), "InvalidDomainIdentifiers");

		var groupUserRole = await _dbContext.GroupUserRoles
			.Where(gur => gur.GroupId.Equals(group.Id) && gur.UserId.Equals(Deps.StudentId))
			.SingleOrDefaultAsync();
		Assert.IsNotNull(groupUserRole, "Has GroupUserRole");
		Assert.AreEqual(BaseRole.GroupLearner, groupUserRole?.Role.Name, "Has correct Role");
	}

	[TestMethod]
	public async Task CreateEntityUserRolesAsync_ShouldAddExistingUserToSecondRole()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);
		var service = Deps.GetGroupUserRoleService(_dbContext);

		var results = await service.CreateEntityUserRolesAsync(
			group.Id,
			new List<string> { $"{Deps.StudentId}@localhost.com" },
			BaseRole.GroupGrader,
			Deps.InstructorPrincipal);

		Assert.AreEqual(1, results.AddedUserRoles.Count(), "AddedUserRoles");
		Assert.AreEqual(0, results.ExistingUserRoles.Count(), "ExistingUserRoles");
		Assert.AreEqual(0, results.InvalidIdentifiers.Count(), "InvalidIdentifiers");
		Assert.AreEqual(0, results.InvalidDomainIdentifiers.Count(), "InvalidDomainIdentifiers");

		var groupUserRoles = await _dbContext.GroupUserRoles
			.Where(gur => gur.GroupId.Equals(group.Id) && gur.UserId.Equals(Deps.StudentId))
			.ToListAsync();
		Assert.AreEqual(2, groupUserRoles.Count, "GroupUserRoles");
		Assert.IsTrue(groupUserRoles.Any(gur => gur.Role.Name.Equals(BaseRole.GroupLearner)), "Has original Role");
		Assert.IsTrue(groupUserRoles.Any(gur => gur.Role.Name.Equals(BaseRole.GroupGrader)), "Has new Role");
	}

	[TestMethod]
	public async Task CreateEntityUserRolesAsync_ShouldNotAddExistingGroupUsersToSameRole()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);
		var service = Deps.GetGroupUserRoleService(_dbContext);

		var results = await service.CreateEntityUserRolesAsync(
			group.Id,
			new List<string> { $"{Deps.StudentId}@localhost.com" },
			BaseRole.GroupLearner,
			Deps.InstructorPrincipal);

		Assert.AreEqual(0, results.AddedUserRoles.Count(), "AddedUserRoles");
		Assert.AreEqual(1, results.ExistingUserRoles.Count(), "ExistingUserRoles");
		Assert.AreEqual(0, results.InvalidIdentifiers.Count(), "InvalidIdentifiers");
		Assert.AreEqual(0, results.InvalidDomainIdentifiers.Count(), "InvalidDomainIdentifiers");

		var groupUserRole = await _dbContext.GroupUserRoles
			.Where(gur => gur.GroupId.Equals(group.Id) && gur.UserId.Equals(Deps.StudentId))
			.SingleOrDefaultAsync();
		Assert.IsNotNull(groupUserRole, "Has GroupUserRole");
		Assert.AreEqual(BaseRole.GroupLearner, groupUserRole?.Role.Name, "Has correct Role");
	}

	[TestMethod]
	public async Task CreateEntityUserRolesAsync_ShouldThrowIfAddingLearnerAndGroupIsEnded()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);
		var studentGroupUserRole = group.GroupUserRoles.Single(gur => gur.UserId.Equals(Deps.StudentId));

		var service = Deps.GetGroupUserRoleService(_dbContext);
		// remove student from global seed
		await service.DeleteEntityUserRoleAsync(studentGroupUserRole.Id, Deps.InstructorPrincipal);

		group = await _dbContext.Groups.SingleAsync(g => g.Id == group.Id);
		// make group ended
		group.EndDate = Deps.DateTime.AddSeconds(-1);
		await _dbContext.SaveChangesAsync();

		_dbContext.ChangeTracker.Clear();

		Assert.ThrowsAsync<ForbiddenException>(
			Task.Run(async () =>
			{
				await service.CreateEntityUserRolesAsync(
					group.Id,
					new List<string> { $"{Deps.StudentId}@localhost.com" },
					BaseRole.GroupLearner,
					Deps.InstructorPrincipal);
			}), Strings.EndedGroupCannotAddLearner);
	}

	[TestMethod]
	public async Task CreateEntityUserRolesAsync_ShouldBlockAddingUsersToSecondRoleIfMultipleRolesNotAllowed()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);
		var service = new SingleAllowedGroupUserRoleService(
			_dbContext,
			Deps.ShardKeyProvider,
			Deps.UserInfoService,
			new GroupAuthorizationService<TestBaseDbContext>(_dbContext),
			Deps.GetRoleManager(_dbContext),
			Deps.SyncGroupRosterQueueManager,
			Deps.DateTimeProvider,
			Deps.UserFactory,
			new Mock<INotificationService<TestBaseGroup>>().Object);

		var results = await service.CreateEntityUserRolesAsync(
			group.Id,
			new List<string> { $"{Deps.StudentId}@localhost.com" },
			BaseRole.GroupGrader,
			Deps.InstructorPrincipal);

		Assert.AreEqual(0, results.AddedUserRoles.Count(), "AddedUserRoles");
		Assert.AreEqual(0, results.ExistingUserRoles.Count(), "ExistingUserRoles");
		Assert.AreEqual(1, results.BlockedUserRoles.Count(), "BlockedUserRoles");
		Assert.AreEqual(0, results.InvalidIdentifiers.Count(), "InvalidIdentifiers");
		Assert.AreEqual(0, results.InvalidDomainIdentifiers.Count(), "InvalidDomainIdentifiers");
	}

	#endregion CreateEntityUserRolesAsync

	#region UpdateEntityUserRoleAsync

	[TestMethod]
	public void UpdateEntityUserRoleAsync_ShouldThrowWithNullArguments()
	{
		var service = Deps.GetGroupUserRoleService(_dbContext);

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await service.UpdateEntityUserRoleAsync(1, BaseRole.GroupLearner, null);
			}), ExceptionUtils.ArgumentNullExceptionMessage("principal"));
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await service.UpdateEntityUserRoleAsync(1, null, Deps.InstructorPrincipal);
			}), ExceptionUtils.ArgumentNullExceptionMessage("roleName"));
	}

	[TestMethod]
	public async Task UpdateEntityUserRoleAsync_ShouldThrowIfGroupRoleDoesNotExist()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);

		var role = await _dbContext.Roles.SingleAsync(r => r.Name == BaseRole.GroupGrader);
		_dbContext.Roles.Remove(role);
		await _dbContext.SaveChangesAsync();

		var instructorUserRole = group.GroupUserRoles.Single(gur => gur.UserId.Equals(Deps.InstructorId));
		var service = Deps.GetGroupUserRoleService(_dbContext);

		Assert.ThrowsAsync<EntityNotFoundException>(
			Task.Run(async () =>
			{
				await service.UpdateEntityUserRoleAsync(
					instructorUserRole.Id,
					BaseRole.GroupGrader,
					Deps.InstructorPrincipal);
			}),
			string.Format(Strings.RoleNotFound, BaseRole.GroupGrader));
	}

	[TestMethod]
	public async Task UpdateEntityUserRoleAsync_ShouldThrowWithoutCorrectActivity()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);
		var service = Deps.GetGroupUserRoleService(_dbContext);
		var studentUserRole = group.GroupUserRoles.Single(gur => gur.UserId.Equals(Deps.StudentId));

		Assert.ThrowsAsync<ForbiddenException>(
			Task.Run(async () =>
			{
				await service.UpdateEntityUserRoleAsync(
					studentUserRole.Id,
					BaseRole.GroupOwner,
					Deps.StudentPrincipal);
			}));
	}

	[TestMethod]
	public async Task UpdateEntityUserRolesAsync_ShouldThrowIfUpdatingToARoleTheUserAlreadyHas()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);
		var service = Deps.GetGroupUserRoleService(_dbContext);
		var studentUserRole = group.GroupUserRoles.Single(gur => gur.UserId.Equals(Deps.StudentId));

		// add student as a grader as well
		await service.CreateEntityUserRolesAsync(
			group.Id,
			new List<string> { $"{Deps.StudentId}@localhost.com" },
			BaseRole.GroupGrader,
			Deps.InstructorPrincipal);

		Assert.ThrowsAsync<ForbiddenException>(
			Task.Run(async () =>
			{
				// try to update the student GUR to a grader
				await service.UpdateEntityUserRoleAsync(
					studentUserRole.Id,
					BaseRole.GroupGrader,
					Deps.InstructorPrincipal);
			}), string.Format(Strings.EntityUserRoleCannotUpdateAlreadyExists, BaseRole.GroupGrader, Deps.StudentId));
	}

	[TestMethod]
	public async Task UpdateEntityUserRolesAsync_ShouldThrowIfUpdatingLastOwner()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);
		var service = Deps.GetGroupUserRoleService(_dbContext);
		var ownerUserRole = group.GroupUserRoles.Single(gur => gur.UserId.Equals(Deps.InstructorId));

		Assert.ThrowsAsync<ForbiddenException>(
			Task.Run(async () =>
			{
				await service.UpdateEntityUserRoleAsync(
					ownerUserRole.Id,
					BaseRole.GroupGrader,
					Deps.InstructorPrincipal);
			}), Strings.CannotRemoveLastEntityOwner);
	}

	[TestMethod]
	public async Task UpdateEntityUserRoleAsync_ShouldUpdateEntityUserRole()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);
		var service = Deps.GetGroupUserRoleService(_dbContext);
		var studentUserRole = group.GroupUserRoles.Single(gur => gur.UserId.Equals(Deps.StudentId));

		var updatedGroupUserRole = await service.UpdateEntityUserRoleAsync(studentUserRole.Id, BaseRole.GroupGrader,
			Deps.InstructorPrincipal);
		Assert.AreEqual(studentUserRole.Id, updatedGroupUserRole.EntityUserRole.Id);
		Assert.AreEqual(BaseRole.GroupGrader, updatedGroupUserRole.Role);
	}

	#endregion UpdateEntityUserRoleAsync

	#region DeleteEntityUserRoleAsync

	[TestMethod]
	public void DeleteEntityUserRoleAsync_ShouldThrowWithNullArguments()
	{
		var service = Deps.GetGroupUserRoleService(_dbContext);

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await service.DeleteEntityUserRoleAsync(1, null);
			}), ExceptionUtils.ArgumentNullExceptionMessage("principal"));
	}

	[TestMethod]
	public void DeleteEntityUserRoleAsync_ShouldThrowIfEntityUserRoleDoesNotExist()
	{
		var service = Deps.GetGroupUserRoleService(_dbContext);

		Assert.ThrowsAsync<EntityNotFoundException>(
			Task.Run(async () =>
			{
				await service.DeleteEntityUserRoleAsync(1234, Deps.InstructorPrincipal);
			}), string.Format(Strings.EntityNotFoundById, nameof(GroupUserRole), 1234));
	}

	[TestMethod]
	public async Task DeleteEntityUserRoleAsync_ShouldThrowIfDeletingOnlyOwner()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);
		var service = Deps.GetGroupUserRoleService(_dbContext);
		var instructorGroupUserRole = group.GroupUserRoles.Single(gur => gur.UserId.Equals(Deps.InstructorId));

		Assert.ThrowsAsync<ForbiddenException>(
			Task.Run(async () =>
			{
				await service.DeleteEntityUserRoleAsync(instructorGroupUserRole.Id, Deps.InstructorPrincipal);
			}), Strings.CannotRemoveLastEntityOwner);
	}

	[TestMethod]
	public async Task DeleteEntityUserRoleAsync_ShouldRemoveGroupUser()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);
		var service = Deps.GetGroupUserRoleService(_dbContext);
		var studentGroupUserRole = group.GroupUserRoles.Single(gur => gur.UserId.Equals(Deps.StudentId));

		await service.DeleteEntityUserRoleAsync(studentGroupUserRole.Id, Deps.InstructorPrincipal);

		var groupUserRole = await _dbContext.GroupUserRoles
			.Where(gur => gur.GroupId.Equals(group.Id) && gur.UserId.Equals(Deps.StudentId))
			.SingleOrDefaultAsync();
		Assert.IsNull(groupUserRole);
	}

	[TestMethod]
	public async Task DeleteEntityUserRoleAsync_ShouldRemoveOwnGroupUser()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);
		await SeedSecondOwnerForGroupAsync(_dbContext, group.Id);
		var service = Deps.GetGroupUserRoleService(_dbContext);
		var ownerGroupUserRole = group.GroupUserRoles.Single(gur => gur.UserId.Equals(Deps.InstructorId));

		await service.DeleteEntityUserRoleAsync(ownerGroupUserRole.Id, Deps.InstructorPrincipal);

		var groupUserRole = await _dbContext.GroupUserRoles
			.Where(gur => gur.GroupId.Equals(group.Id) && gur.UserId.Equals(Deps.InstructorId))
			.SingleOrDefaultAsync();
		Assert.IsNull(groupUserRole);
	}

	[TestMethod]
	public async Task DeleteEntityUserRoleAsync_ShouldRemoveGroupOwnerAndTheirUniTimeExternalGroups()
	{
		var (group, _) = await Deps.SeedGroupWithTermAndUniTimeExternalGroupAsync(_dbContext);
		await SeedSecondOwnerForGroupAsync(_dbContext, group.Id);
		var service = Deps.GetGroupUserRoleService(_dbContext);
		var instructorGroupUserRole = group.GroupUserRoles.Single(gur => gur.UserId.Equals(Deps.InstructorId));

		await service.DeleteEntityUserRoleAsync(instructorGroupUserRole.Id, Deps.AdminPrincipal);

		var groupUserRole = await _dbContext.GroupUserRoles
			.Where(gur => gur.GroupId.Equals(group.Id) && gur.UserId.Equals(Deps.InstructorId))
			.SingleOrDefaultAsync();
		Assert.IsNull(groupUserRole);

		var hasExternalGroups = await _dbContext.ExternalGroups.AnyAsync(eg => eg.GroupId.Equals(group.Id));
		Assert.IsFalse(hasExternalGroups);
	}

	[TestMethod]
	public async Task DeleteEntityUserRoleAsync_ShouldRemoveGroupOwnerButNotTheirLtiExternalGroups()
	{
		var (group, _) = await Deps.SeedGroupWithLtiExternalGroupAsync(_dbContext);
		await SeedSecondOwnerForGroupAsync(_dbContext, group.Id);
		var service = Deps.GetGroupUserRoleService(_dbContext);
		var instructorGroupUserRole = group.GroupUserRoles.Single(gur => gur.UserId.Equals(Deps.InstructorId));

		await service.DeleteEntityUserRoleAsync(instructorGroupUserRole.Id, Deps.AdminPrincipal);

		var groupUserRole = await _dbContext.GroupUserRoles
			.Where(gur => gur.GroupId.Equals(group.Id) && gur.UserId.Equals(Deps.InstructorId))
			.SingleOrDefaultAsync();
		Assert.IsNull(groupUserRole);

		var hasExternalGroups = await _dbContext.ExternalGroups.AnyAsync(eg => eg.GroupId.Equals(group.Id));
		Assert.IsTrue(hasExternalGroups);
	}

	[TestMethod]
	public async Task DeleteEntityUserRoleAsync_ShouldThrowIfRemovingUserAndGroupIsEnded()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);
		await SeedSecondOwnerForGroupAsync(_dbContext, group.Id);
		group.EndDate = Deps.DateTime.AddSeconds(-1);
		await _dbContext.SaveChangesAsync();

		var studentGroupUserRole = group.GroupUserRoles.Single(gur => gur.UserId.Equals(Deps.StudentId));
		var graderGroupUserRole = group.GroupUserRoles.Single(gur => gur.UserId.Equals(Deps.GraderId));
		var instructorGroupUserRole = group.GroupUserRoles.Single(gur => gur.UserId.Equals(Deps.InstructorId));

		var service = Deps.GetGroupUserRoleService(_dbContext);

		Assert.ThrowsAsync<ForbiddenException>(
			Task.Run(async () =>
			{
				await service.DeleteEntityUserRoleAsync(instructorGroupUserRole.Id, Deps.AdminPrincipal);
			}), Strings.EndedGroupCannotRemoveUser);

		Assert.ThrowsAsync<ForbiddenException>(
			Task.Run(async () =>
			{
				await service.DeleteEntityUserRoleAsync(graderGroupUserRole.Id, Deps.AdminPrincipal);
			}), Strings.EndedGroupCannotRemoveUser);

		Assert.ThrowsAsync<ForbiddenException>(
			Task.Run(async () =>
			{
				await service.DeleteEntityUserRoleAsync(studentGroupUserRole.Id, Deps.AdminPrincipal);
			}), Strings.EndedGroupCannotRemoveUser);
	}

	#endregion DeleteEntityUserRoleAsync

	/// <summary>
	/// Tests deleting owners need to have a second user with the owner role to prevent throwing an error.
	/// </summary>
	/// <param name="dbContext"></param>
	/// <param name="groupId"></param>
	private static async Task SeedSecondOwnerForGroupAsync(TestBaseDbContext dbContext, int groupId)
	{
		var userManager = Deps.GetUserManager(dbContext);
		var instructor2Id = Guid.NewGuid().ToString();
		var instructor2 = Deps.GenerateUser(instructor2Id, "Instructor2", "TestUser");
		dbContext.Users.Add(instructor2);
		await dbContext.SaveChangesAsync();
		await userManager.AddToRoleAsync(instructor2, BaseRole.GroupOwner);
		var groupOwnerRole = await dbContext.Roles.SingleAsync(r => r.Name.Equals(BaseRole.GroupOwner));

		var instructor2Role = new GroupUserRole
		{
			UserId = instructor2Id,
			RoleId = groupOwnerRole.Id,
			GroupId = groupId
		};
		var groupUserRoleLog = new GroupUserRoleLog
		{
			UserId = instructor2Id,
			RoleId = groupOwnerRole.Id,
			GroupId = groupId,
			Type = GroupUserRoleLogType.Added
		};
		dbContext.GroupUserRoles.Add(instructor2Role);
		dbContext.GroupUserRoleLogs.Add(groupUserRoleLog);
		await dbContext.SaveChangesAsync();
	}
}