﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MSTestExtensions;
using StudioKit.Data.Entity.Identity;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.UniTime.Models;
using StudioKit.Notification.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.DataAccess.Queues.QueueMessages;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Properties;
using StudioKit.Scaffolding.Tests.EqualityComparers;
using StudioKit.Scaffolding.Tests.TestData.DataAccess;
using StudioKit.Scaffolding.Tests.TestData.Models;
using StudioKit.Tests;
using StudioKit.Utilities.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Tests.BusinessLogic.Services;

[TestClass]
public class BaseGroupServiceTests : BaseTest
{
	private static readonly ScaffoldingTestDependencies Deps = new(ScaffoldingTestDependencies.StartDate);
	private TestBaseDbContext _dbContext;
	private IDbContextTransaction _transaction;

	[ClassInitialize]
	public static async Task BeforeAll(TestContext testContext)
	{
		// seed common data that will persist across tests
		await using var dbContext = Deps.PersistentDbContextFactory();
		await Deps.SeedRolesAndUsersAsync(dbContext);
		await Deps.SeedExternalProvidersAsync(dbContext);
	}

	[TestInitialize]
	public void BeforeEach()
	{
		Deps.Reset(ScaffoldingTestDependencies.StartDate);
		(_dbContext, _transaction) = Deps.PersistentDbContextAndTransactionFactory();
	}

	[TestCleanup]
	public void AfterEach()
	{
		_transaction.Rollback();
		_transaction.Dispose();
		_dbContext.Dispose();
	}

	[ClassCleanup]
	public static void AfterAll()
	{
		Deps.Dispose();
	}

	#region GetAsync

	/// <summary>
	/// Create two groups, one deleted, one not. When calling GetAsync with includeDeleted = false, ensure
	/// only the non-deleted group is returned
	/// </summary>
	/// <returns></returns>
	[TestMethod]
	public async Task GetAsync_ShouldNotIncludeDeleted()
	{
		var group1 = await Deps.SeedGroupWithDatesAsync(_dbContext);
		group1.Name = "foo one";
		var group2 = await Deps.SeedGroupWithDatesAsync(_dbContext);
		group2.Name = "foo two";
		group2.IsDeleted = true;
		await _dbContext.SaveChangesAsync();

		var service = Deps.GetBaseGroupService(_dbContext);

		var groups = await service.GetAsync(Deps.InstructorPrincipal, "foo");

		Assert.AreEqual(1, groups.Count());
	}

	/// <summary>
	/// Create two groups, one deleted, one not. When calling GetAsync with includeDeleted = false, ensure
	/// both groups are returned
	/// </summary>
	/// <returns></returns>
	[TestMethod]
	public async Task GetAsync_ShouldIncludeDeleted()
	{
		var group1 = await Deps.SeedGroupWithDatesAsync(_dbContext);
		group1.Name = "foo one";
		var group2 = await Deps.SeedGroupWithDatesAsync(_dbContext);
		group2.Name = "foo two";
		group2.IsDeleted = true;
		await _dbContext.SaveChangesAsync();

		var service = Deps.GetBaseGroupService(_dbContext);

		var groups = await service.GetAsync(Deps.InstructorPrincipal, "foo", null, false, null, true);

		Assert.AreEqual(2, groups.Count());
	}

	[TestMethod]
	public async Task GetAsync_ShouldIncludeCorrectRolesActivitiesAndOwners()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);

		var service = Deps.GetBaseGroupService(_dbContext);

		var groupBusinessModel = await service.GetAsync(group.Id, Deps.InstructorPrincipal);
		Assert.IsTrue(groupBusinessModel.Roles.SequenceEqual(new List<string> { BaseRole.GroupOwner }));
		var groupOwnerRoleId = (await _dbContext.Roles.SingleAsync(r => r.Name.Equals(BaseRole.GroupOwner))).Id;
		Assert.IsTrue(groupBusinessModel.Activities
			.SequenceEqual(_dbContext.RoleActivities.Where(ra => ra.RoleId == groupOwnerRoleId).Select(ra => ra.Activity.Name)));
		Assert.IsTrue(groupBusinessModel.Owners.SequenceEqual(new List<EntityUserBusinessModel<TestBaseUser>>
		{
			new(Deps.Instructor, groupBusinessModel.Group.Id)
		}, new EntityUserBusinessModelEqualityComparer<TestBaseUser>()));
	}

	/// <summary>
	/// Ensure the return value from GetAsync includes members for Roles and Activities and that they contain
	/// the seeded values for the rubric owner role. Simple regression test. Does not exercise every get, create, update method
	/// </summary>
	/// <returns></returns>
	[TestMethod]
	public async Task GetAsync_ShouldIncludeCorrectRolesActivitiesAndOwnersIfQuerying()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);
		group.Name = "foo one";
		await _dbContext.SaveChangesAsync();

		var service = Deps.GetBaseGroupService(_dbContext);

		var groupBusinessModel = (await service.GetAsync(Deps.InstructorPrincipal, "foo")).First();
		Assert.IsTrue(groupBusinessModel.Roles.SequenceEqual(new List<string> { BaseRole.GroupOwner }));
		var groupOwnerRoleId = (await _dbContext.Roles.SingleAsync(r => r.Name.Equals(BaseRole.GroupOwner))).Id;
		Assert.IsTrue(groupBusinessModel.Activities
			.SequenceEqual(_dbContext.RoleActivities.Where(ra => ra.RoleId == groupOwnerRoleId).Select(ra => ra.Activity.Name)));
		Assert.IsTrue(groupBusinessModel.Owners.SequenceEqual(new List<EntityUserBusinessModel<TestBaseUser>>
		{
			new(Deps.Instructor, groupBusinessModel.Group.Id)
		}, new EntityUserBusinessModelEqualityComparer<TestBaseUser>()));
	}

	[TestMethod]
	public async Task GetAsync_ShouldReturnOthersGroupsIfQueryingAll()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);
		group.Name = "foo one";
		await _dbContext.SaveChangesAsync();

		var service = Deps.GetBaseGroupService(_dbContext);

		var groupBusinessModel = (await service.GetAsync(Deps.AdminPrincipal, "foo", null, true)).First();
		Assert.AreEqual(0, groupBusinessModel.Roles.Count());
		Assert.AreEqual(0, groupBusinessModel.Activities.Count());
		Assert.IsTrue(groupBusinessModel.Owners.SequenceEqual(new List<EntityUserBusinessModel<TestBaseUser>>
		{
			new(Deps.Instructor, groupBusinessModel.Group.Id)
		}, new EntityUserBusinessModelEqualityComparer<TestBaseUser>()));
	}

	[TestMethod]
	public async Task GetAsync_ShouldFindByUser()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);
		group.Name = "foo one";
		await _dbContext.SaveChangesAsync();

		var service = Deps.GetBaseGroupService(_dbContext);

		var groupBusinessModels = (await service.GetAsync(Deps.InstructorPrincipal,
				Deps.Instructor.FirstName,
				null,
				false,
				null,
				true))
			.ToList();
		Assert.AreEqual(1, groupBusinessModels.Count);
		Assert.AreEqual(group.Id, groupBusinessModels.Single().Group.Id);
	}

	#endregion GetAsync

	#region CreateAsync

	/// <summary>
	/// Ensure a group can be created.
	/// </summary>
	/// <returns></returns>
	[TestMethod]
	public async Task CreateAsync_ShouldCreateGroupWithDates()
	{
		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = "New-Group-Created-With-Dates",
			StartDate = new DateTime(2018, 1, 1),
			EndDate = new DateTime(2019, 1, 1)
		};

		var service = Deps.GetBaseGroupService(_dbContext);
		var businessModel = await service.CreateAsync(groupEditBusinessModel, Deps.InstructorPrincipal);

		Assert.IsTrue(new BaseGroupEqualityComparer().Equals(businessModel.Group, new TestBaseGroup
		{
			Name = "New-Group-Created-With-Dates",
			StartDate = new DateTime(2018, 1, 1),
			EndDate = new DateTime(2019, 1, 1)
		}));
	}

	[TestMethod]
	public async Task CreateAsync_ShouldCreateGroupWithTerm()
	{
		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = "New-Group-Created-With-Term-Without-ExternalGroups",
			ExternalTermId = Deps.UniTimeExternalTerm1.Id
		};

		var service = Deps.GetBaseGroupService(_dbContext);
		var businessModel = await service.CreateAsync(groupEditBusinessModel, Deps.InstructorPrincipal);

		Assert.IsTrue(new BaseGroupEqualityComparer().Equals(businessModel.Group, new TestBaseGroup
		{
			Name = "New-Group-Created-With-Term-Without-ExternalGroups",
			ExternalTermId = Deps.UniTimeExternalTerm1.Id
		}));
	}

	[TestMethod]
	public async Task CreateAsync_ShouldNotifyUsersForNewExternalGroups()
	{
		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = "New-Group-With-External-Groups",
			ExternalTermId = Deps.UniTimeExternalTerm1.Id,
			ExternalGroups = new List<UniTimeGroupExternalGroupEditBusinessModel>
			{
				new()
				{
					Id = Deps.UniTimeGroup.Id,
					UserId = Deps.InstructorPrincipal.Identity.GetUserId()
				}
			}
		};

		var notificationServiceMock = new Mock<INotificationService<TestBaseGroup>>();
		var service = Deps.GetBaseGroupService(_dbContext, notificationService: notificationServiceMock.Object);
		var businessModel = await service.CreateAsync(groupEditBusinessModel, Deps.InstructorPrincipal);
		notificationServiceMock.Verify(s => s.NotifyGroupAsync(businessModel.Group.Id,
				BaseNotificationEvent.ExternalGroupAdded,
				It.IsAny<IPrincipal>(),
				It.IsAny<Func<ExternalGroupNotificationTemplateData<TestBaseGroup>,
					Task<ExternalGroupNotificationTemplateData<TestBaseGroup>>>>(),
				null,
				It.IsAny<CancellationToken>(), true),
			Times.Once);
		notificationServiceMock.Verify(s => s.NotifyGroupAsync(businessModel.Group.Id,
				BaseNotificationEvent.ExternalGroupRemoved,
				It.IsAny<IPrincipal>(),
				It.IsAny<Func<ExternalGroupNotificationTemplateData<TestBaseGroup>,
					Task<ExternalGroupNotificationTemplateData<TestBaseGroup>>>>(),
				null,
				It.IsAny<CancellationToken>(), true),
			Times.Never);
	}

	[TestMethod]
	public void CreateAsync_ShouldThrowEmptyExternalTermAndDatesValidationException()
	{
		var service = Deps.GetBaseGroupService(_dbContext);

		Assert.ThrowsAsync<ValidationException>(
			Task.Run(async () =>
			{
				await service.CreateAsync(new BaseGroupEditBusinessModel<TestBaseGroup> { Name = "test" }, Deps.InstructorPrincipal);
			}));
	}

	[TestMethod]
	public void CreateAsync_ShouldThrowEmptyEndDatesValidationException()
	{
		var service = Deps.GetBaseGroupService(_dbContext);

		Assert.ThrowsAsync<ValidationException>(
			Task.Run(async () =>
			{
				await service.CreateAsync(
					new BaseGroupEditBusinessModel<TestBaseGroup>
					{
						Name = "test",
						StartDate = new DateTime(2018, 1, 1)
					}, Deps.InstructorPrincipal);
			}));
	}

	[TestMethod]
	public void CreateAsync_ShouldThrowEmptyStartDatesValidationException()
	{
		var service = Deps.GetBaseGroupService(_dbContext);

		Assert.ThrowsAsync<ValidationException>(
			Task.Run(async () =>
			{
				await service.CreateAsync(
					new BaseGroupEditBusinessModel<TestBaseGroup> { Name = "test", EndDate = new DateTime(2018, 1, 1) },
					Deps.InstructorPrincipal);
			}));
	}

	[TestMethod]
	public void CreateAsync_ShouldThrowStartDateGreaterThanEndDateValidationException()
	{
		var service = Deps.GetBaseGroupService(_dbContext);

		Assert.ThrowsAsync<ValidationException>(
			Task.Run(async () =>
			{
				await service.CreateAsync(
					new BaseGroupEditBusinessModel<TestBaseGroup>
					{
						Name = "test",
						EndDate = new DateTime(2017, 1, 1),
						StartDate = new DateTime(2018, 1, 1)
					}, Deps.InstructorPrincipal);
			}));
	}

	[TestMethod]
	public void CreateAsync_ShouldThrowNullBusinessModelException()
	{
		var service = Deps.GetBaseGroupService(_dbContext);

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await service.CreateAsync(null, Deps.InstructorPrincipal);
			}), ExceptionUtils.ArgumentNullExceptionMessage("businessModel"));
	}

	[TestMethod]
	public void CreateAsync_ShouldThrowNullPrincipalException()
	{
		var service = Deps.GetBaseGroupService(_dbContext);

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await service.CreateAsync(new BaseGroupEditBusinessModel<TestBaseGroup>(), null);
			}), ExceptionUtils.ArgumentNullExceptionMessage("principal"));
	}

	[TestMethod]
	public void CreateAsync_ShouldThrowUserCannotCreateException()
	{
		var service = Deps.GetBaseGroupService(_dbContext);

		Assert.ThrowsAsync<ForbiddenException>(
			Task.Run(async () =>
			{
				await service.CreateAsync(new BaseGroupEditBusinessModel<TestBaseGroup>(), Deps.StudentPrincipal);
			}));
	}

	[TestMethod]
	public void CreateAsync_ShouldThrowEmptyBusinessModelException()
	{
		var service = Deps.GetBaseGroupService(_dbContext);

		Assert.ThrowsAsync<ValidationException>(
			Task.Run(async () =>
			{
				await service.CreateAsync(new BaseGroupEditBusinessModel<TestBaseGroup>(), Deps.InstructorPrincipal);
			}));
	}

	[TestMethod]
	public async Task CreateAsync_ShouldCreateWithLtiExternalGroup()
	{
		var originalGroupCount = await _dbContext.Groups.CountAsync();

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = "New-Group-Created-With-LtiLaunch",
			StartDate = new DateTime(2018, 1, 1),
			EndDate = new DateTime(2019, 1, 1),
			ExternalGroups = new List<IExternalGroupEditBusinessModel>
			{
				new LtiLaunchExternalGroupEditBusinessModel
				{
					Id = Deps.LtiLaunch.Id,
					UserId = Deps.InstructorId,
					IsAutoGradePushEnabled = true
				}
			}
		};

		var service = Deps.GetBaseGroupService(_dbContext);
		var businessModel = await service.CreateAsync(groupEditBusinessModel, Deps.InstructorPrincipal);

		Assert.IsTrue(new BaseGroupEqualityComparer().Equals(businessModel.Group, new TestBaseGroup
		{
			Name = "New-Group-Created-With-LtiLaunch",
			StartDate = new DateTime(2018, 1, 1),
			EndDate = new DateTime(2019, 1, 1)
		}));

		Assert.IsTrue(businessModel.ExternalGroups.SequenceEqual(new List<ExternalGroupBusinessModel>
		{
			new()
			{
				GroupId = businessModel.Group.Id,
				ExternalProviderId = Deps.LtiExternalProvider.Id,
				ExternalId = Deps.LtiLaunch.ExternalId,
				UserId = Deps.InstructorId,
				Name = Deps.LtiLaunch.Name,
				Description = Deps.LtiLaunch.Description,
				RosterUrl = Deps.LtiLaunch.RosterUrl,
				GradesUrl = Deps.LtiLaunch.GradesUrl,
				IsAutoGradePushEnabled = true
			}
		}, new ExternalGroupBusinessModelEqualityComparer()));

		var groupCount = await _dbContext.Groups.CountAsync();

		Assert.AreEqual(originalGroupCount + 1, groupCount, "Group should be created");
	}

	[TestMethod]
	public async Task CreateAsync_ShouldThrowAndNotCreateWithInvalidLtiExternalGroup()
	{
		var originalGroupCount = await _dbContext.Groups.CountAsync();

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = "New-Group-Created-With-LtiLaunch",
			StartDate = new DateTime(2018, 1, 1),
			EndDate = new DateTime(2019, 1, 1),
			ExternalGroups = new List<IExternalGroupEditBusinessModel>
			{
				new LtiLaunchExternalGroupEditBusinessModel
				{
					Id = Deps.LtiLaunch.Id,
					UserId = Deps.InstructorId,
					// setting this to null causes an exception
					IsAutoGradePushEnabled = null
				}
			}
		};

		var service = Deps.GetBaseGroupService(_dbContext);
		Assert.ThrowsAsync<ValidationException>(
			Task.Run(async () =>
			{
				await service.CreateAsync(groupEditBusinessModel, Deps.InstructorPrincipal);
			}), string.Format(Strings.ExternalGroupsInvalidAutoGradePushEnabled, nameof(LtiLaunch)));

		var groupCount = await _dbContext.Groups.CountAsync();

		Assert.AreEqual(originalGroupCount, groupCount, "Group should not be created when exception is thrown");
	}

	#endregion CreateAsync

	#region CopyAsync

	[TestMethod]
	public async Task CopyAsync_ShouldCopyNewGroupWithSameOwnersAndGraders()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);
		var service = Deps.GetBaseGroupService(_dbContext);

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = "New-Group-Created",
			StartDate = new DateTime(2018, 1, 1),
			EndDate = new DateTime(2019, 1, 1)
		};

		var businessModel = await service.CopyAsync(groupEditBusinessModel, group.Id, Deps.InstructorPrincipal);

		Assert.AreEqual(group.GroupUserRoles.Count(gur => !gur.Role.Name.Equals(BaseRole.GroupLearner)),
			businessModel.Group.GroupUserRoles.Count(gur => !gur.Role.Name.Equals(BaseRole.GroupLearner)));
		Assert.IsTrue(businessModel.Group.GroupUserRoles.Any(gur =>
			gur.UserId.Equals(Deps.GraderId) && gur.Role.Name.Equals(BaseRole.GroupGrader)));
		Assert.IsTrue(businessModel.Group.GroupUserRoles.Any(gur =>
			gur.UserId.Equals(Deps.InstructorId) && gur.Role.Name.Equals(BaseRole.GroupOwner)));
	}

	[TestMethod]
	public async Task CopyAsync_ShouldThrowExceptionWhenStudentOrGraderPrincipal()
	{
		var group1 = await Deps.SeedGroupWithDatesAsync(_dbContext);

		var service = Deps.GetBaseGroupService(_dbContext);

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = "New-Group-Created",
			StartDate = new DateTime(2018, 1, 1),
			EndDate = new DateTime(2019, 1, 1)
		};
		await service.CreateAsync(groupEditBusinessModel, Deps.InstructorPrincipal);

		Assert.ThrowsAsync<ForbiddenException>(
			Task.Run(async () =>
			{
				await service.CopyAsync(groupEditBusinessModel, group1.Id, Deps.GraderPrincipal);
			}));

		Assert.ThrowsAsync<ForbiddenException>(
			Task.Run(async () =>
			{
				await service.CopyAsync(groupEditBusinessModel, group1.Id, Deps.StudentPrincipal);
			}));
	}

	#endregion CopyAsync

	#region UpdateAsync

	[TestMethod]
	public async Task UpdateAsync_ShouldUpdateGroupFromDatesToTerm()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);

		var service = Deps.GetBaseGroupService(_dbContext);

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = "UpdatedGroupName",
			ExternalTermId = Deps.UniTimeExternalTerm1.Id
		};

		var updatedBusinessModel = await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.InstructorPrincipal);

		Assert.IsTrue(new BaseGroupEqualityComparer().Equals(updatedBusinessModel.Group, new TestBaseGroup
		{
			Name = "UpdatedGroupName",
			ExternalTermId = Deps.UniTimeExternalTerm1.Id
		}));
	}

	[TestMethod]
	public async Task UpdateAsync_ShouldUpdateGroupFromTermToDates()
	{
		var group = await Deps.SeedGroupWithTermAsync(_dbContext);

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = "UpdatedGroupName",
			StartDate = new DateTime(2018, 1, 1),
			EndDate = new DateTime(2019, 1, 1)
		};
		var service = Deps.GetBaseGroupService(_dbContext);
		var updatedBusinessModel = await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.InstructorPrincipal);

		Assert.IsTrue(new BaseGroupEqualityComparer().Equals(updatedBusinessModel.Group, new TestBaseGroup
		{
			Name = "UpdatedGroupName",
			StartDate = new DateTime(2018, 1, 1),
			EndDate = new DateTime(2019, 1, 1)
		}));
	}

	[TestMethod]
	public void UpdateAsync_ShouldThrowNullPrincipalException()
	{
		var service = Deps.GetBaseGroupService(_dbContext);

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await service.UpdateAsync(1, new BaseGroupEditBusinessModel<TestBaseGroup>(), null);
			}), ExceptionUtils.ArgumentNullExceptionMessage("principal"));
	}

	[TestMethod]
	public void UpdateAsync_ShouldThrowNullBusinessModelException()
	{
		var service = Deps.GetBaseGroupService(_dbContext);

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await service.UpdateAsync(1, null, Deps.InstructorPrincipal);
			}), ExceptionUtils.ArgumentNullExceptionMessage("businessModel"));
	}

	[TestMethod]
	public async Task UpdateAsync_ShouldRemoveUniTimeExternalGroupsWhenRemovingTerm()
	{
		var (group, _) = await Deps.SeedGroupWithTermAndUniTimeExternalGroupAsync(_dbContext);

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = "UpdatedGroupName",
			StartDate = new DateTime(2018, 1, 1),
			EndDate = new DateTime(2019, 1, 1)
		};

		var didCallSyncRoster = false;
		// re-mock the queue manager with a callback that we can use to ensure roster sync happens
		var syncGroupRosterQueueManager =
			ScaffoldingTestDependencies.GenerateQueueManager<SyncGroupRosterMessage>(() => didCallSyncRoster = true);
		var notificationServiceMock = new Mock<INotificationService<TestBaseGroup>>();
		var service = Deps.GetBaseGroupService(_dbContext, syncGroupRosterQueueManager, notificationServiceMock.Object);
		var updatedBusinessModel = await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.InstructorPrincipal);

		Assert.IsTrue(new BaseGroupEqualityComparer().Equals(updatedBusinessModel.Group, new TestBaseGroup
		{
			Name = "UpdatedGroupName",
			StartDate = new DateTime(2018, 1, 1),
			EndDate = new DateTime(2019, 1, 1)
		}));

		Assert.AreEqual(0, updatedBusinessModel.ExternalGroups.Count());

		Assert.IsTrue(didCallSyncRoster);

		notificationServiceMock.Verify(s => s.NotifyGroupAsync(updatedBusinessModel.Group.Id,
				BaseNotificationEvent.ExternalGroupAdded,
				It.IsAny<IPrincipal>(),
				It.IsAny<Func<ExternalGroupNotificationTemplateData<TestBaseGroup>,
					Task<ExternalGroupNotificationTemplateData<TestBaseGroup>>>>(),
				null,
				It.IsAny<CancellationToken>(), true),
			Times.Never);
		notificationServiceMock.Verify(s => s.NotifyGroupAsync(updatedBusinessModel.Group.Id,
				BaseNotificationEvent.ExternalGroupRemoved,
				It.IsAny<IPrincipal>(),
				It.IsAny<Func<ExternalGroupNotificationTemplateData<TestBaseGroup>,
					Task<ExternalGroupNotificationTemplateData<TestBaseGroup>>>>(),
				null,
				It.IsAny<CancellationToken>(), true),
			Times.Once);
	}

	[TestMethod]
	public async Task UpdateAsync_ShouldRemoveUniTimeExternalGroupsWhenChangingTermAndAddNewExternalGroups()
	{
		var (group, _) = await Deps.SeedGroupWithTermAndUniTimeExternalGroupAsync(_dbContext);

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = group.Name,
			ExternalTermId = Deps.UniTimeExternalTerm2.Id,
			ExternalGroups = new List<IExternalGroupEditBusinessModel>
			{
				new UniTimeGroupExternalGroupEditBusinessModel
				{
					Id = Deps.UniTimeGroup2.Id,
					UserId = Deps.InstructorId
				}
			}
		};

		var didCallSyncRoster = false;
		// re-mock the queue manager with a callback that we can use to ensure roster sync happens
		var syncGroupRosterQueueManager =
			ScaffoldingTestDependencies.GenerateQueueManager<SyncGroupRosterMessage>(() => didCallSyncRoster = true);
		var service = Deps.GetBaseGroupService(_dbContext, syncGroupRosterQueueManager);
		var updatedBusinessModel = await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.InstructorPrincipal);

		Assert.IsTrue(new BaseGroupEqualityComparer().Equals(updatedBusinessModel.Group, new TestBaseGroup
		{
			Name = group.Name,
			ExternalTermId = Deps.UniTimeExternalTerm2.Id
		}));

		Assert.IsTrue(updatedBusinessModel.ExternalGroups.SequenceEqual(new List<ExternalGroupBusinessModel>
		{
			new()
			{
				GroupId = group.Id,
				ExternalProviderId = Deps.UniTimeExternalProvider.Id,
				ExternalId = Deps.UniTimeGroup2.ExternalId,
				UserId = Deps.InstructorId,
				Name = Deps.UniTimeGroup2.Name,
				Description = Deps.UniTimeGroup2.Description,
				RosterUrl = null
			}
		}, new ExternalGroupBusinessModelEqualityComparer()));

		Assert.IsTrue(didCallSyncRoster);
	}

	[TestMethod]
	public async Task UpdateAsync_ShouldRemoveUniTimeExternalGroupsWhenChangingTerm()
	{
		var (group, _) = await Deps.SeedGroupWithTermAndUniTimeExternalGroupAsync(_dbContext);

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = "UpdatedGroupName",
			ExternalTermId = Deps.UniTimeExternalTerm2.Id
		};

		var didCallSyncRoster = false;
		// re-mock the queue manager with a callback that we can use to ensure roster sync happens
		var syncGroupRosterQueueManager =
			ScaffoldingTestDependencies.GenerateQueueManager<SyncGroupRosterMessage>(() => didCallSyncRoster = true);
		var service = Deps.GetBaseGroupService(_dbContext, syncGroupRosterQueueManager);
		var updatedBusinessModel = await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.InstructorPrincipal);

		Assert.IsTrue(new BaseGroupEqualityComparer().Equals(updatedBusinessModel.Group, new TestBaseGroup
		{
			Name = "UpdatedGroupName",
			ExternalTermId = Deps.UniTimeExternalTerm2.Id
		}));

		Assert.AreEqual(0, updatedBusinessModel.ExternalGroups.Count());

		Assert.IsTrue(didCallSyncRoster);
	}

	[TestMethod]
	public async Task UpdateAsync_ShouldSucceedForInstructorIfNoExternalGroupsAdded()
	{
		var group = await Deps.SeedGroupWithTermAsync(_dbContext);

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = group.Name,
			ExternalTermId = group.ExternalTermId,
			ExternalGroups = new List<IExternalGroupEditBusinessModel>()
		};

		var service = Deps.GetBaseGroupService(_dbContext);
		var updatedGroupBusinessModel = await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.InstructorPrincipal);

		Assert.AreEqual(0, updatedGroupBusinessModel.ExternalGroups.Count());
	}

	[TestMethod]
	public async Task UpdateAsync_ShouldCreateExternalGroupWithUniTimeGroup()
	{
		var group = await Deps.SeedGroupWithTermAsync(_dbContext);

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = group.Name,
			ExternalTermId = group.ExternalTermId,
			ExternalGroups = new List<IExternalGroupEditBusinessModel>
			{
				new UniTimeGroupExternalGroupEditBusinessModel
				{
					Id = Deps.UniTimeGroup.Id,
					UserId = Deps.InstructorId
				}
			}
		};

		var didCallSyncRoster = false;
		// re-mock the queue manager with a callback that we can use to ensure roster sync happens
		var syncGroupRosterQueueManager =
			ScaffoldingTestDependencies.GenerateQueueManager<SyncGroupRosterMessage>(() => didCallSyncRoster = true);
		var notificationServiceMock = new Mock<INotificationService<TestBaseGroup>>();
		var service = Deps.GetBaseGroupService(_dbContext, syncGroupRosterQueueManager, notificationServiceMock.Object);
		var updatedGroupBusinessModel = await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.InstructorPrincipal);

		Assert.IsTrue(updatedGroupBusinessModel.ExternalGroups.SequenceEqual(new List<ExternalGroupBusinessModel>
		{
			new()
			{
				GroupId = group.Id,
				ExternalProviderId = Deps.UniTimeExternalProvider.Id,
				ExternalId = Deps.UniTimeGroup.ExternalId,
				UserId = Deps.InstructorId,
				Name = Deps.UniTimeGroup.Name,
				Description = Deps.UniTimeGroup.Description,
				RosterUrl = null
			}
		}, new ExternalGroupBusinessModelEqualityComparer()));

		Assert.IsTrue(didCallSyncRoster);
		notificationServiceMock.Verify(s => s.NotifyGroupAsync(updatedGroupBusinessModel.Group.Id,
				BaseNotificationEvent.ExternalGroupAdded,
				It.IsAny<IPrincipal>(),
				It.IsAny<Func<ExternalGroupNotificationTemplateData<TestBaseGroup>,
					Task<ExternalGroupNotificationTemplateData<TestBaseGroup>>>>(),
				null,
				It.IsAny<CancellationToken>(), true),
			Times.Once);
		notificationServiceMock.Verify(s => s.NotifyGroupAsync(updatedGroupBusinessModel.Group.Id,
				BaseNotificationEvent.ExternalGroupRemoved,
				It.IsAny<IPrincipal>(),
				It.IsAny<Func<ExternalGroupNotificationTemplateData<TestBaseGroup>,
					Task<ExternalGroupNotificationTemplateData<TestBaseGroup>>>>(),
				null,
				It.IsAny<CancellationToken>(), true),
			Times.Never);
	}

	[TestMethod]
	public async Task UpdateAsync_ShouldRemoveOwnExistingExternalGroup()
	{
		var group = await Deps.SeedGroupWithTermAsync(_dbContext);
		_dbContext.ExternalGroups.Add(new ExternalGroup
		{
			GroupId = group.Id,
			ExternalProviderId = Deps.UniTimeExternalProvider.Id,
			ExternalId = Deps.UniTimeGroup.ExternalId,
			UserId = Deps.InstructorId,
			Name = Deps.UniTimeGroup.Name,
			Description = Deps.UniTimeGroup.Description,
			RosterUrl = null
		});
		var otherExternalGroup = new ExternalGroup
		{
			GroupId = group.Id,
			ExternalProviderId = Deps.UniTimeExternalProvider.Id,
			ExternalId = "555",
			UserId = Deps.AdminId,
			Name = "Other One",
			Description = "Other One",
			RosterUrl = null
		};
		_dbContext.ExternalGroups.Add(otherExternalGroup);
		await _dbContext.SaveChangesAsync();

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = group.Name,
			ExternalTermId = group.ExternalTermId,
			ExternalGroups = new List<IExternalGroupEditBusinessModel>
			{
				new ExternalGroupEditBusinessModel
				{
					Id = otherExternalGroup.Id,
					UserId = Deps.AdminId
				}
			}
		};

		var didCallSyncRoster = false;
		// re-mock the queue manager with a callback that we can use to ensure roster sync happens
		var syncGroupRosterQueueManager =
			ScaffoldingTestDependencies.GenerateQueueManager<SyncGroupRosterMessage>(() => didCallSyncRoster = true);
		var service = Deps.GetBaseGroupService(_dbContext, syncGroupRosterQueueManager);
		var updatedGroup = await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.InstructorPrincipal);

		Assert.AreEqual(1, updatedGroup.ExternalGroups.Count());
		Assert.IsTrue(updatedGroup.ExternalGroups.SequenceEqual(new List<ExternalGroupBusinessModel>
		{
			new()
			{
				GroupId = otherExternalGroup.GroupId,
				ExternalProviderId = otherExternalGroup.ExternalProviderId,
				ExternalId = otherExternalGroup.ExternalId,
				UserId = otherExternalGroup.UserId,
				Name = otherExternalGroup.Name,
				Description = otherExternalGroup.Description,
				RosterUrl = otherExternalGroup.RosterUrl
			}
		}, new ExternalGroupBusinessModelEqualityComparer()));

		Assert.IsTrue(didCallSyncRoster);
	}

	[TestMethod]
	public async Task UpdateAsync_ShouldRemoveOthersExistingExternalGroupsIfAdmin()
	{
		var group = await Deps.SeedGroupWithTermAsync(_dbContext);
		_dbContext.ExternalGroups.Add(new ExternalGroup
		{
			GroupId = group.Id,
			ExternalProviderId = Deps.UniTimeExternalProvider.Id,
			ExternalId = Deps.UniTimeGroup.ExternalId,
			UserId = Deps.InstructorId,
			Name = Deps.UniTimeGroup.Name,
			Description = Deps.UniTimeGroup.Description,
			RosterUrl = null
		});
		_dbContext.ExternalGroups.Add(new ExternalGroup
		{
			GroupId = group.Id,
			ExternalProviderId = Deps.UniTimeExternalProvider.Id,
			ExternalId = "555",
			UserId = Deps.AdminId,
			Name = "Other One",
			Description = "Other One",
			RosterUrl = null
		});
		await _dbContext.SaveChangesAsync();

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = group.Name,
			ExternalTermId = group.ExternalTermId,
			ExternalGroups = new List<IExternalGroupEditBusinessModel>()
		};

		var didCallSyncRoster = false;
		// re-mock the queue manager with a callback that we can use to ensure roster sync happens
		var syncGroupRosterQueueManager =
			ScaffoldingTestDependencies.GenerateQueueManager<SyncGroupRosterMessage>(() => didCallSyncRoster = true);
		var service = Deps.GetBaseGroupService(_dbContext, syncGroupRosterQueueManager);
		var updatedGroup = await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.AdminPrincipal);

		Assert.AreEqual(0, updatedGroup.ExternalGroups.Count());

		Assert.IsTrue(didCallSyncRoster);
	}

	[TestMethod]
	public async Task UpdateAsync_ShouldRemoveOrRetainExistingExternalGroupsWhileAdding()
	{
		var group = await Deps.SeedGroupWithTermAsync(_dbContext);

		var existingExternalGroups = new List<ExternalGroup>
		{
			new()
			{
				GroupId = group.Id,
				ExternalProviderId = Deps.UniTimeExternalProvider.Id,
				ExternalId = "555",
				UserId = Deps.InstructorId,
				Name = "One to Remove",
				Description = "One to Remove",
				RosterUrl = null
			},
			new()
			{
				GroupId = group.Id,
				ExternalProviderId = Deps.UniTimeExternalProvider.Id,
				ExternalId = "666",
				UserId = Deps.InstructorId,
				Name = "One to Keep",
				Description = "One to Keep",
				RosterUrl = null
			}
		};
		_dbContext.ExternalGroups.AddRange(existingExternalGroups);
		await _dbContext.SaveChangesAsync();

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = group.Name,
			ExternalTermId = group.ExternalTermId,
			ExternalGroups = new List<IExternalGroupEditBusinessModel>
			{
				// new
				new UniTimeGroupExternalGroupEditBusinessModel
				{
					Id = Deps.UniTimeGroup.Id,
					UserId = Deps.InstructorId
				},
				// keep
				new ExternalGroupEditBusinessModel
				{
					Id = existingExternalGroups[1].Id,
					UserId = Deps.InstructorId
				}
			}
		};

		var didCallSyncRoster = false;
		// re-mock the queue manager with a callback that we can use to ensure roster sync happens
		var syncGroupRosterQueueManager =
			ScaffoldingTestDependencies.GenerateQueueManager<SyncGroupRosterMessage>(() => didCallSyncRoster = true);

		var notificationServiceMock = new Mock<INotificationService<TestBaseGroup>>();
		var service = Deps.GetBaseGroupService(_dbContext, syncGroupRosterQueueManager, notificationServiceMock.Object);
		var updatedGroup = await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.InstructorPrincipal);

		Assert.AreEqual(2, updatedGroup.ExternalGroups.Count());
		Assert.IsTrue(updatedGroup.ExternalGroups.OrderBy(e => e.Id).SequenceEqual(new List<ExternalGroupBusinessModel>
		{
			new()
			{
				GroupId = group.Id,
				ExternalProviderId = Deps.UniTimeExternalProvider.Id,
				ExternalId = "666",
				UserId = Deps.InstructorId,
				Name = "One to Keep",
				Description = "One to Keep",
				RosterUrl = null
			},
			new()
			{
				GroupId = group.Id,
				ExternalProviderId = Deps.UniTimeExternalProvider.Id,
				ExternalId = Deps.UniTimeGroup.ExternalId,
				UserId = Deps.InstructorId,
				Name = Deps.UniTimeGroup.Name,
				Description = Deps.UniTimeGroup.Description,
				RosterUrl = null
			}
		}, new ExternalGroupBusinessModelEqualityComparer()));

		Assert.IsTrue(didCallSyncRoster);
		notificationServiceMock.Verify(s => s.NotifyGroupAsync(updatedGroup.Group.Id,
				BaseNotificationEvent.ExternalGroupAdded,
				It.IsAny<IPrincipal>(),
				It.IsAny<Func<ExternalGroupNotificationTemplateData<TestBaseGroup>,
					Task<ExternalGroupNotificationTemplateData<TestBaseGroup>>>>(),
				null,
				It.IsAny<CancellationToken>(), true),
			Times.Once);
		notificationServiceMock.Verify(s => s.NotifyGroupAsync(updatedGroup.Group.Id,
				BaseNotificationEvent.ExternalGroupRemoved,
				It.IsAny<IPrincipal>(),
				It.IsAny<Func<ExternalGroupNotificationTemplateData<TestBaseGroup>,
					Task<ExternalGroupNotificationTemplateData<TestBaseGroup>>>>(),
				null,
				It.IsAny<CancellationToken>(), true),
			Times.Once);
	}

	[TestMethod]
	public async Task UpdateAsync_ShouldThrowIfAutoGradePushShouldHaveValueWhileUpdatingExistingExternalGroup()
	{
		var group = await Deps.SeedGroupWithTermAsync(_dbContext);

		var existingExternalGroup = new ExternalGroup
		{
			GroupId = group.Id,
			ExternalProviderId = Deps.LtiExternalProvider.Id,
			ExternalId = Deps.LtiLaunch.ExternalId,
			UserId = Deps.InstructorId,
			Name = Deps.LtiLaunch.Name,
			Description = Deps.LtiLaunch.Description,
			RosterUrl = Deps.LtiLaunch.RosterUrl,
			GradesUrl = Deps.LtiLaunch.GradesUrl,
			IsAutoGradePushEnabled = true
		};
		_dbContext.ExternalGroups.Add(existingExternalGroup);
		await _dbContext.SaveChangesAsync();

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = group.Name,
			ExternalTermId = group.ExternalTermId,
			ExternalGroups = new List<IExternalGroupEditBusinessModel>
			{
				new ExternalGroupEditBusinessModel
				{
					Id = existingExternalGroup.Id,
					UserId = Deps.InstructorId,
					IsAutoGradePushEnabled = null // invalid, should have value
				}
			}
		};

		var service = Deps.GetBaseGroupService(_dbContext);

		Assert.ThrowsAsync<ValidationException>(
			Task.Run(async () =>
			{
				await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.InstructorPrincipal);
			}), Strings.ExternalGroupsUpdateInvalidAutoGradePushEnabled);
	}

	[TestMethod]
	public async Task UpdateAsync_ShouldThrowIfAutoGradePushShouldNotHaveValueWhileUpdatingExistingExternalGroup()
	{
		Deps.LtiExternalProvider.GradePushEnabled = false;
		var group = await Deps.SeedGroupWithTermAsync(_dbContext);

		var existingExternalGroup = new ExternalGroup
		{
			GroupId = group.Id,
			ExternalProviderId = Deps.LtiExternalProvider.Id,
			ExternalId = Deps.LtiLaunch.ExternalId,
			UserId = Deps.InstructorId,
			Name = Deps.LtiLaunch.Name,
			Description = Deps.LtiLaunch.Description,
			RosterUrl = Deps.LtiLaunch.RosterUrl,
			GradesUrl = null,
			IsAutoGradePushEnabled = null
		};
		_dbContext.ExternalGroups.Add(existingExternalGroup);
		await _dbContext.SaveChangesAsync();

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = group.Name,
			ExternalTermId = group.ExternalTermId,
			ExternalGroups = new List<IExternalGroupEditBusinessModel>
			{
				new ExternalGroupEditBusinessModel
				{
					Id = existingExternalGroup.Id,
					UserId = Deps.InstructorId,
					IsAutoGradePushEnabled = true // invalid, should not have value
				}
			}
		};

		var service = Deps.GetBaseGroupService(_dbContext);

		Assert.ThrowsAsync<ValidationException>(
			Task.Run(async () =>
			{
				await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.InstructorPrincipal);
			}), Strings.ExternalGroupsUpdateInvalidAutoGradePushEnabled);
	}

	[TestMethod]
	public async Task UpdateAsync_ShouldUpdateOwnExistingExternalGroup()
	{
		var group = await Deps.SeedGroupWithTermAsync(_dbContext);
		var ownExternalGroup = new ExternalGroup
		{
			GroupId = group.Id,
			ExternalProviderId = Deps.LtiExternalProvider.Id,
			ExternalId = Deps.LtiLaunch.ExternalId,
			UserId = Deps.InstructorId,
			Name = Deps.LtiLaunch.Name,
			Description = Deps.LtiLaunch.Description,
			RosterUrl = Deps.LtiLaunch.RosterUrl,
			GradesUrl = Deps.LtiLaunch.GradesUrl,
			IsAutoGradePushEnabled = false
		};
		_dbContext.ExternalGroups.Add(ownExternalGroup);
		var otherExternalGroup = new ExternalGroup
		{
			GroupId = group.Id,
			ExternalProviderId = Deps.UniTimeExternalProvider.Id,
			ExternalId = "555",
			UserId = Deps.AdminId,
			Name = "Other One",
			Description = "Other One",
			RosterUrl = null,
			GradesUrl = null,
			IsAutoGradePushEnabled = null
		};
		_dbContext.ExternalGroups.Add(otherExternalGroup);
		await _dbContext.SaveChangesAsync();

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = group.Name,
			ExternalTermId = group.ExternalTermId,
			ExternalGroups = new List<IExternalGroupEditBusinessModel>
			{
				// edit own
				new ExternalGroupEditBusinessModel
				{
					Id = ownExternalGroup.Id,
					UserId = Deps.InstructorId,
					IsAutoGradePushEnabled = true
				},
				// keep other the same
				new ExternalGroupEditBusinessModel
				{
					Id = otherExternalGroup.Id,
					UserId = Deps.AdminId,
					IsAutoGradePushEnabled = null
				}
			}
		};

		var didCallSyncRoster = false;
		// re-mock the queue manager with a callback that we can use to ensure roster sync happens
		var syncGroupRosterQueueManager =
			ScaffoldingTestDependencies.GenerateQueueManager<SyncGroupRosterMessage>(() => didCallSyncRoster = true);

		var service = Deps.GetBaseGroupService(_dbContext, syncGroupRosterQueueManager);
		var updatedGroup = await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.InstructorPrincipal);

		Assert.AreEqual(2, updatedGroup.ExternalGroups.Count());
		Assert.IsTrue(updatedGroup.ExternalGroups.Single(eg => eg.Id == ownExternalGroup.Id)
			.IsAutoGradePushEnabled == true);
		Assert.IsNull(updatedGroup.ExternalGroups.Single(eg => eg.Id == otherExternalGroup.Id)
			.IsAutoGradePushEnabled);
		// shouldn't sync roster if not adding/removing external groups
		Assert.IsFalse(didCallSyncRoster);
	}

	[TestMethod]
	public async Task UpdateAsync_ShouldUpdateOthersExistingExternalGroup()
	{
		var group = await Deps.SeedGroupWithTermAsync(_dbContext);
		var otherExternalGroup = new ExternalGroup
		{
			GroupId = group.Id,
			ExternalProviderId = Deps.LtiExternalProvider.Id,
			ExternalId = Deps.LtiLaunch.ExternalId,
			UserId = Deps.AdminId, // other user
			Name = Deps.LtiLaunch.Name,
			Description = Deps.LtiLaunch.Description,
			RosterUrl = Deps.LtiLaunch.RosterUrl,
			GradesUrl = Deps.LtiLaunch.GradesUrl,
			IsAutoGradePushEnabled = false
		};
		_dbContext.ExternalGroups.Add(otherExternalGroup);
		await _dbContext.SaveChangesAsync();

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = group.Name,
			ExternalTermId = group.ExternalTermId,
			ExternalGroups = new List<IExternalGroupEditBusinessModel>
			{
				// edit others
				new ExternalGroupEditBusinessModel
				{
					Id = otherExternalGroup.Id,
					UserId = Deps.AdminId,
					IsAutoGradePushEnabled = true
				}
			}
		};

		var didCallSyncRoster = false;
		// re-mock the queue manager with a callback that we can use to ensure roster sync happens
		var syncGroupRosterQueueManager =
			ScaffoldingTestDependencies.GenerateQueueManager<SyncGroupRosterMessage>(() => didCallSyncRoster = true);

		var service = Deps.GetBaseGroupService(_dbContext, syncGroupRosterQueueManager);
		var updatedGroup = await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.InstructorPrincipal);

		Assert.AreEqual(1, updatedGroup.ExternalGroups.Count());
		Assert.IsTrue(updatedGroup.ExternalGroups.SequenceEqual(new List<ExternalGroupBusinessModel>
		{
			new()
			{
				GroupId = otherExternalGroup.GroupId,
				ExternalProviderId = otherExternalGroup.ExternalProviderId,
				ExternalId = otherExternalGroup.ExternalId,
				UserId = otherExternalGroup.UserId,
				Name = otherExternalGroup.Name,
				Description = otherExternalGroup.Description,
				RosterUrl = otherExternalGroup.RosterUrl,
				GradesUrl = otherExternalGroup.GradesUrl,
				// changed value

				IsAutoGradePushEnabled = true
			}
		}, new ExternalGroupBusinessModelEqualityComparer()));

		// shouldn't sync roster if not adding/removing external groups
		Assert.IsFalse(didCallSyncRoster);
	}

	[TestMethod]
	public async Task UpdateAsync_ShouldThrowIfExternalGroupExistsForUniTimeGroup()
	{
		var group = await Deps.SeedGroupWithTermAsync(_dbContext);
		_dbContext.ExternalGroups.Add(new ExternalGroup
		{
			GroupId = group.Id,
			ExternalProviderId = Deps.UniTimeExternalProvider.Id,
			ExternalId = Deps.UniTimeGroup.ExternalId,
			UserId = Deps.InstructorId,
			Name = Deps.UniTimeGroup.Name,
			Description = Deps.UniTimeGroup.Description,
			RosterUrl = null
		});
		await _dbContext.SaveChangesAsync();

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = group.Name,
			ExternalTermId = group.ExternalTermId,
			ExternalGroups = new List<IExternalGroupEditBusinessModel>
			{
				new UniTimeGroupExternalGroupEditBusinessModel
				{
					Id = Deps.UniTimeGroup.Id,
					UserId = Deps.InstructorId
				}
			}
		};

		var service = Deps.GetBaseGroupService(_dbContext);

		Assert.ThrowsAsync<ValidationException>(
			Task.Run(async () =>
			{
				await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.InstructorPrincipal);
			}), string.Format(Strings.ExternalGroupAlreadyExists, nameof(UniTimeGroup)));
	}

	[TestMethod]
	public async Task UpdateAsync_ShouldThrowIfUserIdsAreInvalidForUniTimeGroup()
	{
		// Note: called as an Admin, otherwise other UserIds are not allowed.
		var group = await Deps.SeedGroupWithTermAsync(_dbContext);

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = group.Name,
			ExternalTermId = group.ExternalTermId,
			ExternalGroups = new List<IExternalGroupEditBusinessModel>
			{
				new UniTimeGroupExternalGroupEditBusinessModel
				{
					Id = Deps.UniTimeGroup.Id,
					UserId = Deps.AdminId // wrong userId
				}
			}
		};

		var service = Deps.GetBaseGroupService(_dbContext);

		Assert.ThrowsAsync<ValidationException>(
			Task.Run(async () =>
			{
				await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.AdminPrincipal);
			}), string.Format(Strings.ExternalGroupsInvalidUserIds, nameof(UniTimeGroup)));
	}

	[TestMethod]
	public async Task UpdateAsync_ShouldThrowIfExternalTermIdIsInvalidForUniTimeGroup()
	{
		var group = await Deps.SeedGroupWithTermAsync(_dbContext);
		// change the term so that it does not match the group
		Deps.UniTimeGroup.ExternalTermId = Deps.UniTimeExternalTerm2.Id;
		await _dbContext.SaveChangesAsync();

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = group.Name,
			ExternalTermId = group.ExternalTermId,
			ExternalGroups = new List<IExternalGroupEditBusinessModel>
			{
				new UniTimeGroupExternalGroupEditBusinessModel
				{
					Id = Deps.UniTimeGroup.Id,
					UserId = Deps.InstructorId
				}
			}
		};

		var service = Deps.GetBaseGroupService(_dbContext);

		Assert.ThrowsAsync<ValidationException>(
			Task.Run(async () =>
			{
				await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.InstructorPrincipal);
			}), string.Format(Strings.ExternalGroupsInvalidExternalTermId, nameof(UniTimeGroup)));
	}

	[TestMethod]
	public async Task UpdateAsync_ShouldCreateExternalGroupWithLtiLaunch()
	{
		var group = await Deps.SeedGroupWithTermAsync(_dbContext);

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = group.Name,
			ExternalTermId = group.ExternalTermId,
			ExternalGroups = new List<IExternalGroupEditBusinessModel>
			{
				new LtiLaunchExternalGroupEditBusinessModel
				{
					Id = Deps.LtiLaunch.Id,
					UserId = Deps.InstructorId,
					IsAutoGradePushEnabled = true
				}
			}
		};

		var didCallSyncRoster = false;
		// re-mock the queue manager with a callback that we can use to ensure roster sync happens
		var syncGroupRosterQueueManager =
			ScaffoldingTestDependencies.GenerateQueueManager<SyncGroupRosterMessage>(() => didCallSyncRoster = true);

		var service = Deps.GetBaseGroupService(_dbContext, syncGroupRosterQueueManager);
		var updatedGroup = await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.InstructorPrincipal);
		var ltiLaunches = await _dbContext.LtiLaunches.ToListAsync();

		Assert.IsTrue(updatedGroup.ExternalGroups.SequenceEqual(new List<ExternalGroupBusinessModel>
		{
			new()
			{
				GroupId = group.Id,
				ExternalProviderId = Deps.LtiExternalProvider.Id,
				ExternalId = Deps.LtiLaunch.ExternalId,
				UserId = Deps.InstructorId,
				Name = Deps.LtiLaunch.Name,
				Description = Deps.LtiLaunch.Description,
				RosterUrl = Deps.LtiLaunch.RosterUrl,
				GradesUrl = Deps.LtiLaunch.GradesUrl,
				IsAutoGradePushEnabled = true
			}
		}, new ExternalGroupBusinessModelEqualityComparer()));

		Assert.AreEqual(1, ltiLaunches.Count);

		Assert.IsTrue(didCallSyncRoster);
	}

	[TestMethod]
	public async Task UpdateAsync_ShouldThrowIfExternalGroupExistsForLtiLaunch()
	{
		var group = await Deps.SeedGroupWithTermAsync(_dbContext);
		_dbContext.ExternalGroups.Add(new ExternalGroup
		{
			GroupId = group.Id,
			ExternalProviderId = Deps.LtiExternalProvider.Id,
			ExternalId = Deps.LtiLaunch.ExternalId,
			UserId = Deps.InstructorId,
			Name = Deps.LtiLaunch.Name,
			Description = Deps.LtiLaunch.Description,
			RosterUrl = Deps.LtiLaunch.RosterUrl
		});
		await _dbContext.SaveChangesAsync();

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = group.Name,
			ExternalTermId = group.ExternalTermId,
			ExternalGroups = new List<IExternalGroupEditBusinessModel>
			{
				new LtiLaunchExternalGroupEditBusinessModel
				{
					Id = Deps.LtiLaunch.Id,
					UserId = Deps.InstructorId
				}
			}
		};

		var service = Deps.GetBaseGroupService(_dbContext);

		Assert.ThrowsAsync<ValidationException>(
			Task.Run(async () =>
			{
				await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.InstructorPrincipal);
			}), string.Format(Strings.ExternalGroupAlreadyExists, nameof(LtiLaunch)));
	}

	[TestMethod]
	public async Task UpdateAsync_ShouldThrowIfUserIdsAreInvalidForLtiLaunch()
	{
		// Note: called as an Admin, otherwise other UserIds are not allowed.

		var group = await Deps.SeedGroupWithTermAsync(_dbContext);

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = group.Name,
			ExternalTermId = group.ExternalTermId,
			ExternalGroups = new List<IExternalGroupEditBusinessModel>
			{
				new LtiLaunchExternalGroupEditBusinessModel
				{
					Id = Deps.LtiLaunch.Id,
					UserId = Deps.AdminId // invalid
				}
			}
		};

		var service = Deps.GetBaseGroupService(_dbContext);

		Assert.ThrowsAsync<ValidationException>(
			Task.Run(async () =>
			{
				await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.AdminPrincipal);
			}), string.Format(Strings.ExternalGroupsInvalidUserIds, nameof(LtiLaunch)));
	}

	[TestMethod]
	public async Task UpdateAsync_ShouldThrowIfTryingToRemoveLtiExternalGroup()
	{
		var group = await Deps.SeedGroupWithTermAsync(_dbContext);

		var existingExternalGroups = new List<ExternalGroup>
		{
			new()
			{
				GroupId = group.Id,
				ExternalProviderId = Deps.LtiExternalProvider.Id,
				ExternalId = "666",
				UserId = Deps.AdminId,
				Name = "One to Keep",
				Description = "One to Keep",
				RosterUrl = null
			}
		};
		_dbContext.ExternalGroups.AddRange(existingExternalGroups);
		await _dbContext.SaveChangesAsync();

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = group.Name,
			ExternalTermId = group.ExternalTermId,
			ExternalGroups = new List<IExternalGroupEditBusinessModel>()
		};

		var service = Deps.GetBaseGroupService(_dbContext);

		Assert.ThrowsAsync<ForbiddenException>(
			Task.Run(async () =>
			{
				await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.AdminPrincipal);
			}), Strings.ExternalGroupsCannotRemoveIfLtiExternalProvider);
	}

	[TestMethod]
	public async Task UpdateAsync_ShouldThrowIfAutoGradePushShouldHaveValueForLtiLaunch()
	{
		var group = await Deps.SeedGroupWithTermAsync(_dbContext);

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = group.Name,
			ExternalTermId = group.ExternalTermId,
			ExternalGroups = new List<IExternalGroupEditBusinessModel>
			{
				new LtiLaunchExternalGroupEditBusinessModel
				{
					Id = Deps.LtiLaunch.Id,
					UserId = Deps.InstructorId,
					IsAutoGradePushEnabled = null // invalid, should have value
				}
			}
		};

		var service = Deps.GetBaseGroupService(_dbContext);

		Assert.ThrowsAsync<ValidationException>(
			Task.Run(async () =>
			{
				await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.InstructorPrincipal);
			}), string.Format(Strings.ExternalGroupsInvalidAutoGradePushEnabled, nameof(LtiLaunch)));
	}

	[TestMethod]
	public async Task UpdateAsync_ShouldThrowIfAutoGradePushShouldNotHaveValueForLtiLaunch()
	{
		Deps.LtiExternalProvider.GradePushEnabled = false;
		Deps.LtiLaunch.GradesUrl = null;
		var group = await Deps.SeedGroupWithTermAsync(_dbContext);

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = group.Name,
			ExternalTermId = group.ExternalTermId,
			ExternalGroups = new List<IExternalGroupEditBusinessModel>
			{
				new LtiLaunchExternalGroupEditBusinessModel
				{
					Id = Deps.LtiLaunch.Id,
					UserId = Deps.InstructorId,
					IsAutoGradePushEnabled = true // invalid, should not have value
				}
			}
		};

		var service = Deps.GetBaseGroupService(_dbContext);

		Assert.ThrowsAsync<ValidationException>(
			Task.Run(async () =>
			{
				await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.InstructorPrincipal);
			}), string.Format(Strings.ExternalGroupsInvalidAutoGradePushEnabled, nameof(LtiLaunch)));
	}

	/// <summary>
	/// For a group that has been soft-deleted, restore and ensure it is
	/// no longer soft-deleted.
	/// </summary>
	/// <returns></returns>
	[TestMethod]
	public async Task UpdateAsync_ShouldRestoreGroup()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);
		group.IsDeleted = true;
		await _dbContext.SaveChangesAsync();

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = group.Name,
			IsDeleted = false,
			StartDate = group.StartDate,
			EndDate = group.EndDate
		};

		var service = Deps.GetBaseGroupService(_dbContext);
		var updatedBusinessModel = await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.InstructorPrincipal);
		Assert.IsFalse(updatedBusinessModel.Group.IsDeleted);
	}

	//
	/// <summary>
	/// Ensure an exception is thrown trying to update properties other than
	/// IsDeleted for a deleted group
	/// </summary>
	/// <returns></returns>
	[TestMethod]
	public async Task UpdateAsync_ShouldThrowIfUpdatingDeletedGroup()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);
		group.IsDeleted = true;
		await _dbContext.SaveChangesAsync();

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = group.Name,
			StartDate = Deps.DateTimeProvider.UtcNow.AddDays(1),
			EndDate = Deps.DateTimeProvider.UtcNow.AddDays(10)
		};

		var service = Deps.GetBaseGroupService(_dbContext);
		Assert.ThrowsAsync<ForbiddenException>(
			Task.Run(async () =>
			{
				await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.AdminPrincipal);
			}), string.Format(Strings.GroupCannotUpdateDeleted, group.Id));
	}

	/// <summary>
	/// Deleting an already deleted group should be idempotent. Delete and
	/// ensure nothing has changed.
	/// </summary>
	/// <returns></returns>
	[TestMethod]
	public async Task UpdateAsync_ShouldNotUpdateIfDeletingDeletedGroup()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);
		group.IsDeleted = true;
		await _dbContext.SaveChangesAsync();

		var prior = new TestBaseGroup
		{
			Name = group.Name,
			IsDeleted = group.IsDeleted,
			StartDate = group.StartDate,
			EndDate = group.EndDate,
			ExternalTermId = group.ExternalTermId
		};

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = group.Name,
			IsDeleted = true,
			StartDate = group.StartDate,
			EndDate = group.EndDate
		};
		var service = Deps.GetBaseGroupService(_dbContext);
		await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.AdminPrincipal);

		Assert.IsTrue(new BaseGroupEqualityComparer().Equals(prior, group));
	}

	/// <summary>
	/// Deletion has to happen through the update method, but any other updates should be prevented.
	/// </summary>
	/// <returns></returns>
	[TestMethod]
	public async Task UpdateAsync_ShouldPreventOtherUpdatesDuringDeletion()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);
		var originalName = group.Name;

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = "TryingToDelete",
			IsDeleted = true,
			StartDate = group.StartDate,
			EndDate = group.EndDate
		};
		var service = Deps.GetBaseGroupService(_dbContext);
		await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.AdminPrincipal);
		Assert.IsTrue(group.Name.Equals(originalName));
	}

	// ensure roster sync is initiated when group is restored
	/// <summary>
	/// When a group is restored, roster sync should be initiated (if external groups are associated). Restore
	/// a group and ensure that roster sync is kicked off
	/// </summary>
	/// <returns></returns>
	[TestMethod]
	public async Task UpdateAsync_ShouldRosterSyncAfterGroupRestoreWithExternalGroups()
	{
		var (group, externalGroup) = await Deps.SeedGroupWithTermAndUniTimeExternalGroupAsync(_dbContext);
		group.IsDeleted = true;
		await _dbContext.SaveChangesAsync();

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = group.Name,
			IsDeleted = false,
			ExternalTermId = group.ExternalTermId,
			ExternalGroups = new List<IExternalGroupEditBusinessModel>
			{
				new ExternalGroupEditBusinessModel
				{
					Id = externalGroup.Id,
					UserId = Deps.InstructorId,
					IsAutoGradePushEnabled = externalGroup.IsAutoGradePushEnabled
				}
			}
		};
		var didCallSyncRoster = false;
		// re-mock the queue manager with a callback that we can use to ensure roster sync happens
		var syncGroupRosterQueueManager =
			ScaffoldingTestDependencies.GenerateQueueManager<SyncGroupRosterMessage>(() => didCallSyncRoster = true);

		var service = Deps.GetBaseGroupService(_dbContext, syncGroupRosterQueueManager);
		await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.AdminPrincipal);
		Assert.IsTrue(didCallSyncRoster);
	}

	// ensure roster sync is initiated when group is restored
	/// <summary>
	/// When a group is restored, roster sync should not be initiated (if external groups are not associated). Restore
	/// a group and ensure that roster sync is not kicked off
	/// </summary>
	/// <returns></returns>
	[TestMethod]
	public async Task UpdateAsync_ShouldNotRosterSyncAfterGroupRestoreWithoutExternalGroups()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);
		group.IsDeleted = true;
		await _dbContext.SaveChangesAsync();

		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = group.Name,
			IsDeleted = false,
			StartDate = group.StartDate,
			EndDate = group.EndDate
		};
		var didCallSyncRoster = false;
		// re-mock the queue manager with a callback that we can use to ensure roster sync happens
		var syncGroupRosterQueueManager =
			ScaffoldingTestDependencies.GenerateQueueManager<SyncGroupRosterMessage>(() => didCallSyncRoster = true);

		var service = Deps.GetBaseGroupService(_dbContext, syncGroupRosterQueueManager);
		await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.AdminPrincipal);
		Assert.IsFalse(didCallSyncRoster);
	}

	/// <summary>
	/// Ensure group is soft deleted after calling the delete method
	/// </summary>
	/// <returns></returns>
	[TestMethod]
	public async Task UpdateAsync_ShouldDeleteGroup()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);
		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = group.Name,
			IsDeleted = true,
			StartDate = group.StartDate,
			EndDate = group.EndDate
		};
		var service = Deps.GetBaseGroupService(_dbContext);
		var updatedBusinessModel = await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.AdminPrincipal);
		Assert.IsTrue(updatedBusinessModel.Group.IsDeleted);
	}

	[TestMethod]
	public async Task UpdateAsync_ShouldThrowIfTryingToRemoveExternalGroupsAndTermIsEnded()
	{
		var (group, _) = await Deps.SeedGroupWithTermAndUniTimeExternalGroupAsync(_dbContext);
		// make term ended
		group.ExternalTerm.EndDate = Deps.DateTime.AddSeconds(-1);
		await _dbContext.SaveChangesAsync();

		// remove external groups
		var groupEditBusinessModel = new BaseGroupEditBusinessModel<TestBaseGroup>
		{
			Name = group.Name,
			ExternalTermId = group.ExternalTermId,
			ExternalGroups = new List<IExternalGroupEditBusinessModel>()
		};

		var service = Deps.GetBaseGroupService(_dbContext);

		Assert.ThrowsAsync<ForbiddenException>(
			Task.Run(async () =>
			{
				await service.UpdateAsync(group.Id, groupEditBusinessModel, Deps.AdminPrincipal);
			}), Strings.EndedGroupCannotChangeExternalGroups);
	}

	#endregion UpdateAsync

	#region DeleteAsync

	/// <summary>
	/// When deleting a group, ensure exception is throw if insufficient permissions
	/// </summary>
	/// <returns></returns>
	[TestMethod]
	public async Task DeleteAsync_ShouldThrowNullPrincipalException()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);
		var service = Deps.GetBaseGroupService(_dbContext);
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await service.DeleteAsync(group.Id, null);
			}), ExceptionUtils.ArgumentNullExceptionMessage("principal"));
	}

	/// <summary>
	/// Deleting a group is not implemented because all soft delete actions should go through update
	/// </summary>
	/// <returns></returns>
	[TestMethod]
	public async Task DeleteAsync_ShouldThrowNotImplementedException()
	{
		var group = await Deps.SeedGroupWithDatesAsync(_dbContext);
		var service = Deps.GetBaseGroupService(_dbContext);
		Assert.ThrowsAsync<NotImplementedException>(
			Task.Run(async () =>
			{
				await service.DeleteAsync(group.Id, Deps.InstructorPrincipal);
			}));
	}

	#endregion DeleteAsync
}