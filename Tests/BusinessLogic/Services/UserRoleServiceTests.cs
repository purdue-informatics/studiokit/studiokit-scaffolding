﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSTestExtensions;
using StudioKit.Data.Entity.Identity;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.Scaffolding.Tests.TestData.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Tests.BusinessLogic.Services;

[TestClass]
public class UserRoleServiceTests : BaseTest
{
	private static readonly ScaffoldingTestDependencies Deps = new(ScaffoldingTestDependencies.StartDate);
	private TestBaseDbContext _dbContext;
	private IDbContextTransaction _transaction;

	[ClassInitialize]
	public static async Task BeforeAll(TestContext testContext)
	{
		// seed common data that will persist across tests
		await using var dbContext = Deps.PersistentDbContextFactory();
		await Deps.SeedRolesAndUsersAsync(dbContext);
		await Deps.SeedExternalProvidersAsync(dbContext);
	}

	[TestInitialize]
	public void BeforeEach()
	{
		Deps.Reset(ScaffoldingTestDependencies.StartDate);
		(_dbContext, _transaction) = Deps.PersistentDbContextAndTransactionFactory(Deps.AdminPrincipalProvider);
	}

	[TestCleanup]
	public void AfterEach()
	{
		_transaction.Rollback();
		_transaction.Dispose();
		_dbContext.Dispose();
	}

	[ClassCleanup]
	public static void AfterAll()
	{
		Deps.Dispose();
	}

	#region GetUserRolesAsync

	[TestMethod]
	public void GetUserRolesAsync_ShouldThrowWithNullArguments()
	{
		var service = Deps.GetUserRoleService(_dbContext);
		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await service.GetUserRolesAsync(null, Deps.StudentPrincipal);
		}));
		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await service.GetUserRolesAsync(BaseRole.Admin, null);
		}));
		Assert.ThrowsAsync<EntityNotFoundException>(Task.Run(async () =>
		{
			await service.GetUserRolesAsync("Foobar", Deps.AdminPrincipal);
		}));
	}

	[TestMethod]
	public async Task GetUserRolesAsync_ShouldReturnAllUserRoles()
	{
		var service = Deps.GetUserRoleService(_dbContext);
		var result = (await service.GetUserRolesAsync(BaseRole.Admin, Deps.AdminPrincipal)).ToList();

		Assert.AreEqual(result.Count, 2, "Two users have admin role");
		Assert.IsTrue(result.Select(r => r.User.Id).OrderBy(r => r)
			.SequenceEqual(new List<string> { Deps.SuperAdminId, Deps.AdminId }.OrderBy(l => l)), "The correct users have admin role");

		result = (await service.GetUserRolesAsync(BaseRole.SuperAdmin, Deps.SuperAdminPrincipal)).ToList();
		Assert.AreEqual(result.Count, 1, "One user has super admin role");
		Assert.IsTrue(result.Select(r => r.User.Id).OrderBy(r => r)
			.SequenceEqual(new List<string> { Deps.SuperAdminId }.OrderBy(l => l)), "The correct user has super admin role");

		result = (await service.GetUserRolesAsync(BaseRole.Creator, Deps.SuperAdminPrincipal)).ToList();
		Assert.AreEqual(result.Count, 1, "One user has the creator role");
		Assert.IsTrue(result.Select(r => r.User.Id).OrderBy(r => r)
			.SequenceEqual(new List<string> { Deps.InstructorId }.OrderBy(l => l)), "The correct user has creator role");
	}

	[TestMethod]
	public void GetUserRolesAsync_ShouldThrowWithoutAccessToQueryRoles()
	{
		var service = Deps.GetUserRoleService(_dbContext);
		Assert.ThrowsAsync<ForbiddenException>(Task.Run(async () =>
		{
			await service.GetUserRolesAsync(BaseRole.Admin, Deps.StudentPrincipal);
		}));
	}

	#endregion GetUserRolesAsync

	#region CreateUserRolesAsync

	[TestMethod]
	public void CreateUserRoleAsync_ShouldThrowWithNullArguments()
	{
		var service = Deps.GetUserRoleService(_dbContext);
		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await service.CreateUserRolesAsync(null, new List<string> { Deps.Student.UserName },
				Deps.AdminPrincipal);
		}));
		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await service.CreateUserRolesAsync(BaseRole.Admin, null, Deps.AdminPrincipal);
		}));
		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await service.CreateUserRolesAsync(BaseRole.Admin, new List<string>(), Deps.AdminPrincipal);
		}));
		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await service.CreateUserRolesAsync(BaseRole.Admin, new List<string> { Deps.Student.UserName }, null);
		}));
	}

	[TestMethod]
	public void CreateUserRoleAsync_ShouldThrowWithInvalidRole()
	{
		var service = Deps.GetUserRoleService(_dbContext);
		Assert.ThrowsAsync<EntityNotFoundException>(Task.Run(async () =>
		{
			await service.CreateUserRolesAsync("foobar", new List<string> { Deps.Student.UserName },
				Deps.AdminPrincipal);
		}));
	}

	[TestMethod]
	public async Task CreateUserRoleAsync_ShouldAddNewRole()
	{
		var service = Deps.GetUserRoleService(_dbContext);
		var result = await service.CreateUserRolesAsync(BaseRole.Admin, new List<string> { Deps.Student.UserName }, Deps.AdminPrincipal);
		Assert.AreEqual(1, result.AddedUserRoles.Count(), "AddedUserRoles");
		Assert.AreEqual(0, result.ExistingUserRoles.Count(), "ExistingUserRoles");
		Assert.AreEqual(0, result.InvalidIdentifiers.Count(), "InvalidIdentifiers");
		Assert.AreEqual(0, result.InvalidDomainIdentifiers.Count(), "InvalidDomainIdentifiers");

		var adminRoleId = (await _dbContext.Roles.SingleAsync(r => r.Name.Equals(BaseRole.Admin))).Id;
		var userRole = await _dbContext.IdentityUserRoles
			.Where(ur => ur.RoleId.Equals(adminRoleId) && ur.UserId.Equals(Deps.StudentId))
			.SingleOrDefaultAsync();
		Assert.IsNotNull(userRole, "Has role");
		Assert.AreEqual(adminRoleId, userRole?.RoleId, "Has correct role");
	}

	[TestMethod]
	public async Task CreateUserRoleAsync_ShouldAddExistingUserToSecondRole()
	{
		var service = Deps.GetUserRoleService(_dbContext);
		var result = await service.CreateUserRolesAsync(BaseRole.SuperAdmin, new List<string> { Deps.Admin.UserName },
			Deps.AdminPrincipal);
		Assert.AreEqual(1, result.AddedUserRoles.Count(), "AddedUserRoles");
		Assert.AreEqual(0, result.ExistingUserRoles.Count(), "ExistingUserRoles");
		Assert.AreEqual(0, result.InvalidIdentifiers.Count(), "InvalidIdentifiers");
		Assert.AreEqual(0, result.InvalidDomainIdentifiers.Count(), "InvalidDomainIdentifiers");

		var superAdminRoleId = (await _dbContext.Roles.SingleAsync(r => r.Name.Equals(BaseRole.SuperAdmin))).Id;
		var userRole = await _dbContext.IdentityUserRoles
			.Where(ur => ur.RoleId.Equals(superAdminRoleId) && ur.UserId.Equals(Deps.AdminId))
			.SingleOrDefaultAsync();
		Assert.IsNotNull(userRole, "Has role");
		Assert.AreEqual(superAdminRoleId, userRole?.RoleId, "Has correct role");
	}

	[TestMethod]
	public async Task CreateUserRoleAsync_ShouldNotAddExistingUsersToSameRole()
	{
		var service = Deps.GetUserRoleService(_dbContext);
		var result = await service.CreateUserRolesAsync(BaseRole.Admin, new List<string> { Deps.Admin.UserName }, Deps.AdminPrincipal);
		Assert.AreEqual(0, result.AddedUserRoles.Count(), "AddedUserRoles");
		Assert.AreEqual(1, result.ExistingUserRoles.Count(), "ExistingUserRoles");
		Assert.AreEqual(0, result.InvalidIdentifiers.Count(), "InvalidIdentifiers");
		Assert.AreEqual(0, result.InvalidDomainIdentifiers.Count(), "InvalidDomainIdentifiers");

		var adminRoleId = (await _dbContext.Roles.SingleAsync(r => r.Name.Equals(BaseRole.Admin))).Id;
		var userRole = await _dbContext.IdentityUserRoles
			.Where(ur => ur.RoleId.Equals(adminRoleId) && ur.UserId.Equals(Deps.AdminId))
			.SingleOrDefaultAsync();
		Assert.IsNotNull(userRole, "Has role");
		Assert.AreEqual(adminRoleId, userRole?.RoleId, "Has correct role");
	}

	#endregion CreateUserRolesAsync

	#region DeleteUserRoleAsync

	[TestMethod]
	public void DeleteUserRoleAsync_ShouldThrowWithNullOrBadArguments()
	{
		var service = Deps.GetUserRoleService(_dbContext);
		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await service.DeleteUserRoleAsync(null, BaseRole.Admin, Deps.AdminPrincipal);
		}));
		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await service.DeleteUserRoleAsync(Deps.AdminId, null, Deps.AdminPrincipal);
		}));
		Assert.ThrowsAsync<ArgumentNullException>(Task.Run(async () =>
		{
			await service.DeleteUserRoleAsync(Deps.AdminId, BaseRole.Admin, null);
		}));
		Assert.ThrowsAsync<EntityNotFoundException>(Task.Run(async () =>
		{
			await service.DeleteUserRoleAsync(Deps.AdminId, "foobar", Deps.AdminPrincipal);
		}));
	}

	[TestMethod]
	public async Task DeleteUserRoleAsync_ShouldDeleteUserRole()
	{
		var service = Deps.GetUserRoleService(_dbContext);
		await service.DeleteUserRoleAsync(Deps.AdminId, BaseRole.Admin, Deps.AdminPrincipal);
		var adminRoleId = (await _dbContext.Roles.SingleAsync(r => r.Name.Equals(BaseRole.Admin))).Id;
		var userRole = await _dbContext.IdentityUserRoles
			.Where(ur => ur.RoleId.Equals(adminRoleId) && ur.UserId.Equals(Deps.AdminId))
			.SingleOrDefaultAsync();
		Assert.IsNull(userRole, "Does not have role");
	}

	[TestMethod]
	public async Task DeleteUserRoleAsync_ShouldBeIdempotentWithoutExistingRole()
	{
		var service = Deps.GetUserRoleService(_dbContext);
		await service.DeleteUserRoleAsync(Deps.StudentId, BaseRole.Admin, Deps.AdminPrincipal);
		var adminRoleId = (await _dbContext.Roles.SingleAsync(r => r.Name.Equals(BaseRole.Admin))).Id;
		var userRole = await _dbContext.IdentityUserRoles
			.Where(ur => ur.RoleId.Equals(adminRoleId) && ur.UserId.Equals(Deps.StudentId)).SingleOrDefaultAsync();
		Assert.IsNull(userRole, "Does not have role");
	}

	#endregion DeleteUserRoleAsync
}