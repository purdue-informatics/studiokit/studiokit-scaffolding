using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MSTestExtensions;
using StudioKit.Caching.Interfaces;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.BusinessLogic.Services;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Properties;
using StudioKit.Scaffolding.Tests.TestData.DataAccess;
using StudioKit.Scaffolding.Tests.TestData.Models;
using StudioKit.Sharding;
using StudioKit.Tests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Tests.BusinessLogic.Services;

[TestClass]
public class LockDownBrowserServiceTests : BaseTest
{
	private static readonly ScaffoldingTestDependencies Deps = new(ScaffoldingTestDependencies.StartDate);
	private TestBaseDbContext _dbContext;
	private IDbContextTransaction _transaction;
	private IAsyncCacheClient _cacheClient;
	private readonly Dictionary<string, object> _cache = new();
	private Mock<ILockDownBrowserConfiguration> _mockConfiguration;
	private Mock<ILockDownBrowserUtils> _mockLockDownBrowserUtils;
	private TestLockDownBrowserEntity _entity;
	private const string ChallengeValue = "Just a fake challenge";
	private const string ChallengeResponseValue = "Just a fake challenge response";
	private const string ChallengeId = "abc";
	private const string ChallengeCacheKey = $"{LockDownBrowserConstants.ChallengeCachePrefix}{ChallengeId}";
	private const string Nonce = "foo";
	private LockDownBrowserChallenge _challenge;

	[ClassInitialize]
	public static async Task BeforeAll(TestContext testContext)
	{
		// seed common data that will persist across tests
		await using var dbContext = Deps.PersistentDbContextFactory();
		await Deps.SeedRolesAndUsersAsync(dbContext);
		await Deps.SeedExternalProvidersAsync(dbContext);
		dbContext.TestLockDownBrowserEntities.Add(new TestLockDownBrowserEntity());
		await dbContext.SaveChangesAsync();
	}

	[TestInitialize]
	public void BeforeEach()
	{
		ShardDomainConfiguration.Instance = new ShardDomainConfiguration("example.com");

		Deps.Reset(ScaffoldingTestDependencies.StartDate);
		(_dbContext, _transaction) = Deps.PersistentDbContextAndTransactionFactory();

		_mockConfiguration = new Mock<ILockDownBrowserConfiguration>();
		_mockConfiguration.Setup(c => c.Secret1).Returns("xxxxxxxxxxxxxxxx");
		_mockConfiguration.Setup(c => c.Secret2).Returns("yyyyyyyyyyyyyyyy");
		_mockConfiguration.Setup(c => c.SecretIV).Returns("zzzzzzzzzzzzzzzz");
		_mockConfiguration.Setup(c => c.SecretIndex).Returns("aa");
		_mockConfiguration.Setup(c => c.SecretVersion).Returns("1");
		_mockConfiguration.Setup(c => c.SecretVersion).Returns("1");

		_mockLockDownBrowserUtils = new Mock<ILockDownBrowserUtils>();
		_mockLockDownBrowserUtils.Setup(u => u.GetLaunchUrl(It.IsAny<string>())).Returns<string>((data) => data);
		_mockLockDownBrowserUtils.Setup(u => u.GetChallengeValue(It.IsAny<LockDownBrowserChallenge>())).Returns(ChallengeValue);
		_mockLockDownBrowserUtils.Setup(u => u.GetChallengeResponseValue(It.IsAny<LockDownBrowserChallenge>()))
			.Returns(ChallengeResponseValue);

		var mockCacheClient = new Mock<IAsyncCacheClient>();
		mockCacheClient.Setup(c => c.PutAsync(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<TimeSpan>()))
			.Callback<string, object, TimeSpan>((k, v, _) => _cache[k] = v)
			.ReturnsAsync(true);
		mockCacheClient.Setup(c => c.GetAsync<string>(It.IsAny<string>()))
			.ReturnsAsync((string k) => _cache.ContainsKey(k) ? (string)_cache[k] : null);
		mockCacheClient.Setup(c => c.GetAsync<LockDownBrowserChallenge>(It.IsAny<string>()))
			.ReturnsAsync((string k) => _cache.ContainsKey(k) ? (LockDownBrowserChallenge)_cache[k] : null);
		mockCacheClient.Setup(c => c.RemoveAsync(It.IsAny<string>()))
			.Callback<string>(k => _cache.Remove(k))
			.ReturnsAsync(true);
		_cacheClient = mockCacheClient.Object;

		_entity = _dbContext.TestLockDownBrowserEntities.First();

		_challenge = CreateChallenge(_entity.Id, $"/quiz/{_entity.Id}");
	}

	[TestCleanup]
	public void AfterEach()
	{
		_transaction.Rollback();
		_transaction.Dispose();
		_dbContext.Dispose();
	}

	[ClassCleanup]
	public static void AfterAll()
	{
		Deps.Dispose();
	}

	#region GetLaunchUrl

	[TestMethod]
	public void GetLaunchUrl_ShouldSucceedWithEntityId()
	{
		var service = GetService(_dbContext);
		var launchUrl = service.GetLaunchUrl("abc", 1, "/quiz/1", "Custom Title");
		Assert.IsNotNull(launchUrl);

		// the utils are mocked, actual encryption is not tested here
		Assert.AreEqual(
			$"<z><u>https://test-shard.example.com/api/ldb/restart?userId=abc&entityId=1&redirectPath=%2fquiz%2f1&{LockDownBrowserKey.PageTitle}=Custom+Title&{LockDownBrowserKey.CheckApplicationBlockList}={LockDownBrowserConstants.True}</u></z>",
			launchUrl);
	}

	[TestMethod]
	public void GetLaunchUrl_ShouldSucceedWithoutEntityId()
	{
		var service = GetService(_dbContext);
		var launchUrl = service.GetLaunchUrl("abc", null, "/test", "Custom Title");
		Assert.IsNotNull(launchUrl);

		// the utils are mocked, actual encryption is not tested here
		Assert.AreEqual(
			$"<z><u>https://test-shard.example.com/api/ldb/restart?userId=abc&redirectPath=%2ftest&{LockDownBrowserKey.PageTitle}=Custom+Title&{LockDownBrowserKey.CheckApplicationBlockList}={LockDownBrowserConstants.True}</u></z>",
			launchUrl);
	}

	#endregion GetLaunchUrl

	#region HandleRestartAsync

	[TestMethod]
	public void HandleRestartAsync_ShouldThrowWithNullArguments()
	{
		var service = GetService(_dbContext);

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () => { await service.HandleRestartAsync(null, 1, "/"); }),
			ExceptionUtils.ArgumentNullExceptionMessage("userId"));

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () => { await service.HandleRestartAsync("abc", 1, null); }),
			ExceptionUtils.ArgumentNullExceptionMessage("redirectPath"));
	}

	[TestMethod]
	public void HandleRestartAsync_ShouldThrowIfUserDoesNotExist()
	{
		var service = GetService(_dbContext);
		Assert.ThrowsAsync<EntityNotFoundException>(
			Task.Run(async () => { await service.HandleRestartAsync("abc", 1, "/"); }));
	}

	[TestMethod]
	public void HandleRestartAsync_ShouldThrowIfEntityDoesNotExist()
	{
		var service = GetService(_dbContext);
		Assert.ThrowsAsync<EntityNotFoundException>(
			Task.Run(async () => { await service.HandleRestartAsync(Deps.StudentId, 999, "/"); }));
	}

	[TestMethod]
	public async Task HandleRestartAsync_ShouldSucceedWithEntityId()
	{
		var service = GetService(_dbContext);
		var queryParams = await service.HandleRestartAsync(Deps.StudentId, _entity.Id, "/");

		// returns the correct query params
		Assert.IsTrue(queryParams.AllKeys.Contains(LockDownBrowserKey.EnableCookies));
		Assert.AreEqual(LockDownBrowserConstants.True, queryParams[LockDownBrowserKey.EnableCookies]);
		Assert.IsTrue(queryParams.AllKeys.Contains(LockDownBrowserKey.ChallengeValue));
		Assert.AreEqual(ChallengeValue, queryParams[LockDownBrowserKey.ChallengeValue]);
		Assert.IsTrue(queryParams.AllKeys.Contains(LockDownBrowserConstants.ChallengeIdKey));

		var challengeId = queryParams[LockDownBrowserConstants.ChallengeIdKey];

		// inserts challenge object into cache
		Assert.AreEqual(1, _cache.Keys.Count);
		var key = _cache.Keys.First();
		Assert.AreEqual($"{LockDownBrowserConstants.ChallengeCachePrefix}{challengeId}", key);
	}

	[TestMethod]
	public async Task HandleRestartAsync_ShouldSucceedWithoutEntityId()
	{
		_challenge = CreateChallenge(null, $"/test");

		var service = GetService(_dbContext);
		var queryParams = await service.HandleRestartAsync(Deps.StudentId, null, "/");

		// returns the correct query params
		Assert.IsTrue(queryParams.AllKeys.Contains(LockDownBrowserKey.EnableCookies));
		Assert.AreEqual(LockDownBrowserConstants.True, queryParams[LockDownBrowserKey.EnableCookies]);
		Assert.IsTrue(queryParams.AllKeys.Contains(LockDownBrowserKey.ChallengeValue));
		Assert.AreEqual(ChallengeValue, queryParams[LockDownBrowserKey.ChallengeValue]);
		Assert.IsTrue(queryParams.AllKeys.Contains(LockDownBrowserConstants.ChallengeIdKey));

		var challengeId = queryParams[LockDownBrowserConstants.ChallengeIdKey];

		// inserts challenge object into cache
		Assert.AreEqual(1, _cache.Keys.Count);
		var key = _cache.Keys.First();
		Assert.AreEqual($"{LockDownBrowserConstants.ChallengeCachePrefix}{challengeId}", key);
	}

	#endregion HandleRestartAsync

	#region HandleChallengeAsync

	[TestMethod]
	public void HandleChallengeAsync_ShouldThrowWithNullArguments()
	{
		var service = GetService(_dbContext);

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () => { await service.HandleChallengeAsync(null, new Mock<HttpRequest>().Object); }),
			ExceptionUtils.ArgumentNullExceptionMessage("challengeId"));
	}

	[TestMethod]
	public void HandleChallengeAsync_ShouldThrowIfChallengeDoesNotExistInCache()
	{
		var service = GetService(_dbContext);

		Assert.ThrowsAsync<UnauthorizedException>(
			Task.Run(async () => { await service.HandleChallengeAsync("abc", new Mock<HttpRequest>().Object); }),
			Strings.LockDownBrowserChallengeNotCached);
	}

	[TestMethod]
	public void HandleChallengeAsync_ShouldThrowIfNonceAlreadyExistsInCache()
	{
		_cache.Add(ChallengeCacheKey, _challenge);

		const string nonceCacheKey = $"{LockDownBrowserConstants.NonceCachePrefix}{Nonce}";
		_cache.Add(nonceCacheKey, LockDownBrowserConstants.ExistentialCacheValue);

		var service = GetService(_dbContext);

		Assert.ThrowsAsync<UnauthorizedException>(
			Task.Run(async () => { await service.HandleChallengeAsync(ChallengeId, new Mock<HttpRequest>().Object); }),
			Strings.LockDownBrowserNonceCached);
	}

	[TestMethod]
	public void HandleChallengeAsync_ShouldThrowIfTheClientIsNotLockDownBrowser()
	{
		_cache.Add(ChallengeCacheKey, _challenge);

		var expectedClientIsLockDownBrowserValue = "2";
		var requestCookiesMock = new Mock<IRequestCookieCollection>();
		requestCookiesMock.Setup(c => c.TryGetValue(LockDownBrowserKey.ClientIsLockDownBrowser, out expectedClientIsLockDownBrowserValue))
			.Returns(true);
		var httpRequestMock = new Mock<HttpRequest>();
		httpRequestMock.Setup(r => r.Cookies).Returns(requestCookiesMock.Object);

		var service = GetService(_dbContext);

		Assert.ThrowsAsync<ForbiddenException>(
			Task.Run(async () => { await service.HandleChallengeAsync(ChallengeId, httpRequestMock.Object); }),
			Strings.LockDownBrowserClientIsNotLockDownBrowser);
	}

	[TestMethod]
	public void HandleChallengeAsync_ShouldThrowIfTheChallengeResponseValueIsBad()
	{
		_cache.Add(ChallengeCacheKey, _challenge);

		var expectedClientIsLockDownBrowserValue = LockDownBrowserConstants.True;
		var expectedChallengeResponseValue = "BAD";

		var requestCookiesMock = new Mock<IRequestCookieCollection>();
		requestCookiesMock.Setup(c => c.TryGetValue(LockDownBrowserKey.ClientIsLockDownBrowser, out expectedClientIsLockDownBrowserValue))
			.Returns(true);
		requestCookiesMock.Setup(c => c.TryGetValue(LockDownBrowserKey.ChallengeResponseValue, out expectedChallengeResponseValue))
			.Returns(true);
		var httpRequestMock = new Mock<HttpRequest>();
		httpRequestMock.Setup(r => r.Cookies).Returns(requestCookiesMock.Object);

		var service = GetService(_dbContext);
		Assert.ThrowsAsync<ForbiddenException>(
			Task.Run(async () => { await service.HandleChallengeAsync(ChallengeId, httpRequestMock.Object); }),
			Strings.LockDownBrowserBadChallengeResponse);
	}

	[TestMethod]
	public async Task HandleChallengeAsync_ShouldSucceedWithEntityId()
	{
		_cache.Add(ChallengeCacheKey, _challenge);

		var expectedClientIsLockDownBrowserValue = LockDownBrowserConstants.True;
		var expectedChallengeResponseValue = ChallengeResponseValue;

		var requestCookiesMock = new Mock<IRequestCookieCollection>();
		requestCookiesMock.Setup(c => c.TryGetValue(LockDownBrowserKey.ClientIsLockDownBrowser, out expectedClientIsLockDownBrowserValue))
			.Returns(true);
		requestCookiesMock.Setup(c => c.TryGetValue(LockDownBrowserKey.ChallengeResponseValue, out expectedChallengeResponseValue))
			.Returns(true);
		var httpRequestMock = new Mock<HttpRequest>();
		httpRequestMock.Setup(r => r.Cookies).Returns(requestCookiesMock.Object);

		var service = GetService(_dbContext);
		var (user, entityId, redirectPath, queryParams) = await service.HandleChallengeAsync(ChallengeId, httpRequestMock.Object);

		Assert.AreEqual(_challenge.UserId, user.Id);
		Assert.AreEqual(_challenge.EntityId, entityId);
		Assert.AreEqual(_challenge.RedirectPath, redirectPath);

		Assert.AreEqual(3, queryParams.AllKeys.Length);
		Assert.IsTrue(queryParams.AllKeys.Contains(LockDownBrowserKey.EnableCookies));
		Assert.AreEqual(LockDownBrowserConstants.True, queryParams[LockDownBrowserKey.EnableCookies]);
		Assert.IsTrue(queryParams.AllKeys.Contains(LockDownBrowserKey.EnableQuizNavigation));
		Assert.AreEqual(LockDownBrowserConstants.True, queryParams[LockDownBrowserKey.EnableQuizNavigation]);
		Assert.IsTrue(queryParams.AllKeys.Contains(LockDownBrowserKey.EnableSecurityLevelMediumHigh));
		Assert.AreEqual(LockDownBrowserConstants.True, queryParams[LockDownBrowserKey.EnableSecurityLevelMediumHigh]);
	}

	[TestMethod]
	public async Task HandleChallengeAsync_ShouldSucceedWithEntitySettings()
	{
		_entity.IsLockDownBrowserHighSecurityEnabled = true;
		_entity.LockDownBrowserCalculatorTypeEnum = LockDownBrowserCalculatorType.Scientific;
		_entity.IsLockDownBrowserPrintingEnabled = true;
		_entity.LockDownBrowserProctorExitPassword = "[encrypted password]";
		await _dbContext.SaveChangesAsync();

		_cache.Add(ChallengeCacheKey, _challenge);

		var expectedClientIsLockDownBrowserValue = LockDownBrowserConstants.True;
		var expectedChallengeResponseValue = ChallengeResponseValue;

		var requestCookiesMock = new Mock<IRequestCookieCollection>();
		requestCookiesMock.Setup(c => c.TryGetValue(LockDownBrowserKey.ClientIsLockDownBrowser, out expectedClientIsLockDownBrowserValue))
			.Returns(true);
		requestCookiesMock.Setup(c => c.TryGetValue(LockDownBrowserKey.ChallengeResponseValue, out expectedChallengeResponseValue))
			.Returns(true);
		var httpRequestMock = new Mock<HttpRequest>();
		httpRequestMock.Setup(r => r.Cookies).Returns(requestCookiesMock.Object);

		var service = GetService(_dbContext);
		var (user, entityId, redirectPath, queryParams) = await service.HandleChallengeAsync(ChallengeId, httpRequestMock.Object);

		Assert.AreEqual(_challenge.UserId, user.Id);
		Assert.AreEqual(_challenge.EntityId, entityId);
		Assert.AreEqual(_challenge.RedirectPath, redirectPath);

		Assert.AreEqual(6, queryParams.AllKeys.Length);
		Assert.IsTrue(queryParams.AllKeys.Contains(LockDownBrowserKey.EnableCookies));
		Assert.AreEqual(LockDownBrowserConstants.True, queryParams[LockDownBrowserKey.EnableCookies]);
		Assert.IsTrue(queryParams.AllKeys.Contains(LockDownBrowserKey.EnableQuizNavigation));
		Assert.AreEqual(LockDownBrowserConstants.True, queryParams[LockDownBrowserKey.EnableQuizNavigation]);
		Assert.IsTrue(queryParams.AllKeys.Contains(LockDownBrowserKey.EnableSecurityLevelHigh));
		Assert.AreEqual(LockDownBrowserConstants.True, queryParams[LockDownBrowserKey.EnableSecurityLevelHigh]);
		Assert.IsTrue(queryParams.AllKeys.Contains(LockDownBrowserKey.ProctorExitPassword));
		Assert.AreEqual(_entity.LockDownBrowserProctorExitPassword, queryParams[LockDownBrowserKey.ProctorExitPassword]);
		Assert.IsTrue(queryParams.AllKeys.Contains(LockDownBrowserKey.EnableCalculator));
		Assert.AreEqual(LockDownBrowserConstants.ScientificCalculator, queryParams[LockDownBrowserKey.EnableCalculator]);
		Assert.IsTrue(queryParams.AllKeys.Contains(LockDownBrowserKey.EnablePrinting));
		Assert.AreEqual(LockDownBrowserConstants.True, queryParams[LockDownBrowserKey.EnablePrinting]);
	}

	[TestMethod]
	public async Task HandleChallengeAsync_ShouldSucceedWithoutEntityId()
	{
		_challenge = CreateChallenge(null, $"/test");
		_cache.Add(ChallengeCacheKey, _challenge);

		var expectedClientIsLockDownBrowserValue = LockDownBrowserConstants.True;
		var expectedChallengeResponseValue = ChallengeResponseValue;

		var requestCookiesMock = new Mock<IRequestCookieCollection>();
		requestCookiesMock.Setup(c => c.TryGetValue(LockDownBrowserKey.ClientIsLockDownBrowser, out expectedClientIsLockDownBrowserValue))
			.Returns(true);
		requestCookiesMock.Setup(c => c.TryGetValue(LockDownBrowserKey.ChallengeResponseValue, out expectedChallengeResponseValue))
			.Returns(true);
		var httpRequestMock = new Mock<HttpRequest>();
		httpRequestMock.Setup(r => r.Cookies).Returns(requestCookiesMock.Object);

		var service = GetService(_dbContext);
		var (user, entityId, redirectPath, queryParams) = await service.HandleChallengeAsync(ChallengeId, httpRequestMock.Object);

		Assert.AreEqual(_challenge.UserId, user.Id);
		Assert.IsNull(entityId);
		Assert.AreEqual(_challenge.RedirectPath, redirectPath);

		Assert.AreEqual(3, queryParams.AllKeys.Length);
		Assert.IsTrue(queryParams.AllKeys.Contains(LockDownBrowserKey.EnableCookies));
		Assert.AreEqual(LockDownBrowserConstants.True, queryParams[LockDownBrowserKey.EnableCookies]);
		Assert.IsTrue(queryParams.AllKeys.Contains(LockDownBrowserKey.EnableQuizNavigation));
		Assert.AreEqual(LockDownBrowserConstants.True, queryParams[LockDownBrowserKey.EnableQuizNavigation]);
		Assert.IsTrue(queryParams.AllKeys.Contains(LockDownBrowserKey.EnableSecurityLevelMediumHigh));
		Assert.AreEqual(LockDownBrowserConstants.True, queryParams[LockDownBrowserKey.EnableSecurityLevelMediumHigh]);
	}

	[TestMethod]
	public async Task HandleChallengeAsync_ShouldSucceedWithDomainAllowList()
	{
		const string domainAllowList = "(google.com)";
		_mockConfiguration.Setup(c => c.DomainAllowList).Returns(domainAllowList);

		_cache.Add(ChallengeCacheKey, _challenge);

		var expectedClientIsLockDownBrowserValue = LockDownBrowserConstants.True;
		var expectedChallengeResponseValue = ChallengeResponseValue;

		var requestCookiesMock = new Mock<IRequestCookieCollection>();
		requestCookiesMock.Setup(c => c.TryGetValue(LockDownBrowserKey.ClientIsLockDownBrowser, out expectedClientIsLockDownBrowserValue))
			.Returns(true);
		requestCookiesMock.Setup(c => c.TryGetValue(LockDownBrowserKey.ChallengeResponseValue, out expectedChallengeResponseValue))
			.Returns(true);
		var httpRequestMock = new Mock<HttpRequest>();
		httpRequestMock.Setup(r => r.Cookies).Returns(requestCookiesMock.Object);

		var service = GetService(_dbContext);
		var (user, entityId, redirectPath, queryParams) = await service.HandleChallengeAsync(ChallengeId, httpRequestMock.Object);

		Assert.AreEqual(_challenge.UserId, user.Id);
		Assert.AreEqual(_challenge.EntityId, entityId);
		Assert.AreEqual(_challenge.RedirectPath, redirectPath);

		Assert.AreEqual(4, queryParams.AllKeys.Length);
		Assert.IsTrue(queryParams.AllKeys.Contains(LockDownBrowserKey.EnableCookies));
		Assert.AreEqual(LockDownBrowserConstants.True, queryParams[LockDownBrowserKey.EnableCookies]);
		Assert.IsTrue(queryParams.AllKeys.Contains(LockDownBrowserKey.EnableQuizNavigation));
		Assert.AreEqual(LockDownBrowserConstants.True, queryParams[LockDownBrowserKey.EnableQuizNavigation]);
		Assert.IsTrue(queryParams.AllKeys.Contains(LockDownBrowserKey.EnableSecurityLevelMediumHigh));
		Assert.AreEqual(LockDownBrowserConstants.True, queryParams[LockDownBrowserKey.EnableSecurityLevelMediumHigh]);
		Assert.IsTrue(queryParams.AllKeys.Contains(LockDownBrowserKey.DomainAllowList));
		Assert.AreEqual(domainAllowList, queryParams[LockDownBrowserKey.DomainAllowList]);
	}

	#endregion HandleChallengeAsync

	#region Private Methods

	private ILockDownBrowserService GetService(TestBaseDbContext dbContext)
	{
		dbContext.ChangeTracker.Clear();

		return new LockDownBrowserService<TestBaseDbContext, TestBaseUser, TestBaseGroup, BaseConfiguration>(
			dbContext,
			_cacheClient,
			Deps.ShardKeyProvider,
			_mockConfiguration.Object,
			_mockLockDownBrowserUtils.Object);
	}

	private static LockDownBrowserChallenge CreateChallenge(int? entityId, string redirectPath)
	{
		return new LockDownBrowserChallenge
		{
			Id = ChallengeId,
			UserId = Deps.StudentId,
			EntityId = entityId,
			RedirectPath = redirectPath,
			Nonce = Nonce
		};
	}

	#endregion Private Methods
}