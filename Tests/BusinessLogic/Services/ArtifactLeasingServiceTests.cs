using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MSTestExtensions;
using StudioKit.Cloud.Storage.Blob;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.Scaffolding.BusinessLogic.Services;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Properties;
using StudioKit.Scaffolding.Tests.TestData.DataAccess;
using StudioKit.Tests;
using StudioKit.Utilities.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Tests.BusinessLogic.Services;

[TestClass]
public class ArtifactLeasingServiceTests : BaseTest
{
	private static readonly ScaffoldingTestDependencies Deps = new(ScaffoldingTestDependencies.StartDate);
	private TestBaseDbContext _dbContext;
	private IDbContextTransaction _transaction;
	private FileArtifact _artifact;

	private const string TotpSharedSecret = "so-secret";
	private const int TotpDurationSeconds = 60;

	[ClassInitialize]
	public static async Task BeforeAll(TestContext testContext)
	{
		// seed common data that will persist across tests
		await using var dbContext = Deps.PersistentDbContextFactory();
		await Deps.SeedRolesAndUsersAsync(dbContext);
		await Deps.SeedExternalProvidersAsync(dbContext);
	}

	[TestInitialize]
	public void BeforeEach()
	{
		Deps.Reset(ScaffoldingTestDependencies.StartDate);
		(_dbContext, _transaction) = Deps.PersistentDbContextAndTransactionFactory(Deps.StudentPrincipalProvider);
		_artifact = new FileArtifact
			{ FileName = "test.doc", Url = "http://hostname/fileartifacts/foo.doc", CreatedById = Deps.StudentId };
	}

	[TestCleanup]
	public void AfterEach()
	{
		_transaction.Rollback();
		_transaction.Dispose();
		_dbContext.Dispose();
	}

	[ClassCleanup]
	public static void AfterAll()
	{
		Deps.Dispose();
	}

	[TestMethod]
	public async Task AssociateTotp_ShouldReturnToken()
	{
		_dbContext.Artifacts.Add(_artifact);
		await _dbContext.SaveChangesAsync();

		_dbContext.ChangeTracker.Clear();

		var service = GetService(_dbContext);
		var token = service.AssociateTotp(_artifact.Id).Replace('-', '/').Replace('_', '+');
		var substrings = token.Split('|');
		Assert.AreEqual(substrings.Length, 2);
		var salt = substrings[0];
		var payload = substrings[1];
		// Ensure the following do not throw
		// TODO: figure out how to mock BouncyCastle's SecureRandom seed generation
		var saltByteArray = Convert.FromBase64String(salt);
		var plaintext = Utilities.Encryption.DecryptStringAes(payload, TotpSharedSecret, saltByteArray);
		substrings = plaintext.Split('|');
		Assert.AreEqual(substrings.Length, 2);
	}

	[TestMethod]
	public void ArtifactFromToken_ShouldThrowWithInvalidParameters()
	{
		Assert.ThrowsAsync<EntityNotFoundException>(
			Task.Run(async () =>
			{
				var service = GetService(_dbContext);
				await service.ArtifactFromTokenAsync(null);
			}));

		Assert.ThrowsAsync<EntityNotFoundException>(
			Task.Run(async () =>
			{
				var service = GetService(_dbContext);
				await service.ArtifactFromTokenAsync("BADBAD");
			}));

		Assert.ThrowsAsync<EntityNotFoundException>(
			Task.Run(async () =>
			{
				const int totpDuration = 1;
				var service = GetService(_dbContext, totpDurationSeconds: totpDuration);
				var totpCode = service.AssociateTotp(99);
				Thread.Sleep(totpDuration * 2);
				await service.ArtifactFromTokenAsync(totpCode);
			}));

		Assert.ThrowsAsync<EntityNotFoundException>(
			Task.Run(async () =>
			{
				const int totpDuration = 1;
				var service = GetService(_dbContext, totpDurationSeconds: totpDuration);
				await service.ArtifactFromTokenAsync("foo|bar|baz");
			}));

		Assert.ThrowsAsync<EntityNotFoundException>(
			Task.Run(async () =>
			{
				const int totpDuration = 1;
				var service = GetService(_dbContext, totpDurationSeconds: totpDuration);
				var (salt, encryptedPayload) = Utilities.Encryption.EncryptStringAes("foobar", TotpSharedSecret);

				await service.ArtifactFromTokenAsync($"{salt}|{encryptedPayload}".Replace('/', '-').Replace('+', '_'));
			}));

		Assert.ThrowsAsync<EntityNotFoundException>(
			Task.Run(async () =>
			{
				const int totpDuration = 1;
				var service = GetService(_dbContext, totpDurationSeconds: totpDuration);
				var (salt, encryptedPayload) = Utilities.Encryption.EncryptStringAes("foo|bar|baz", TotpSharedSecret);

				await service.ArtifactFromTokenAsync($"{salt}|{encryptedPayload}".Replace('/', '-').Replace('+', '_'));
			}));
	}

	[TestMethod]
	public void StreamArtifactAsync_ShouldThrowWithInvalidParameters()
	{
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				var service = GetService(_dbContext);
				await service.StreamArtifactAsync(null, new MemoryStream());
			}));

		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				var service = GetService(_dbContext);
				await service.StreamArtifactAsync(new FileArtifact(), null);
			}),
			ExceptionUtils.ArgumentNullExceptionMessage("stream"));
	}

	[TestMethod]
	public async Task StreamArtifactAsync_ShouldThrowWithExpiredCode()
	{
		var mockDateTimeProvider = new Mock<IDateTimeProvider>();
		mockDateTimeProvider.SetupGet(d => d.UtcNow).Returns(new Queue<DateTime>(new[]
		{
			ScaffoldingTestDependencies.StartDate,
			ScaffoldingTestDependencies.StartDate.AddSeconds(2)
		}).Dequeue);
		var dateTimeProvider = mockDateTimeProvider.Object;

		_dbContext.Artifacts.Add(_artifact);
		await _dbContext.SaveChangesAsync();

		const int totpDurationSeconds = 1;
		var service = GetService(_dbContext, dateTimeProvider: dateTimeProvider, totpDurationSeconds: totpDurationSeconds);
		var token = service.AssociateTotp(_artifact.Id);
		Assert.ThrowsAsync<EntityNotFoundException>(
			Task.Run(async () =>
			{
				await service.ArtifactFromTokenAsync(token);
			}));
	}

	[TestMethod]
	public async Task StreamArtifactAsync_ShouldThrowIfStreamIsNotWritable()
	{
		_dbContext.Artifacts.Add(_artifact);
		await _dbContext.SaveChangesAsync();

		var service = GetService(_dbContext);
		var totpCode = service.AssociateTotp(_artifact.Id);
		var stream = new MemoryStream(new byte[] { 0x00 }, false);
		Assert.ThrowsAsync<ApplicationException>(
			Task.Run(async () =>
			{
				var artifact = await service.ArtifactFromTokenAsync(totpCode);
				await service.StreamArtifactAsync(artifact, stream);
			}), Strings.StreamNotWritable);
	}

	[TestMethod]
	public async Task StreamArtifactAsync_ShouldStreamArtifact()
	{
		var azureBlobStorageMock = new Mock<IBlobStorage>();
		azureBlobStorageMock
			.Setup(s =>
				s.WriteBlockBlobToStreamAsync(
					It.IsAny<Stream>(),
					It.IsAny<string>(),
					It.IsAny<CancellationToken>()))
			.Callback<Stream, string, CancellationToken>((stream, _, _) =>
			{
				var bytes = Encoding.UTF8.GetBytes("Hello World");
				stream.Write(bytes, 0, bytes.Length);
			})
			.Returns(Task.FromResult(0));

		_dbContext.Artifacts.Add(_artifact);
		await _dbContext.SaveChangesAsync();

		var service = GetService(_dbContext, blobStorage: azureBlobStorageMock.Object);
		var totpCode = service.AssociateTotp(_artifact.Id);
		var artifact = await service.ArtifactFromTokenAsync(totpCode, CancellationToken.None);
		var stream = new MemoryStream(new byte[Encoding.UTF8.GetBytes("Hello World").Length]);
		await service.StreamArtifactAsync(artifact, stream, CancellationToken.None);
		stream.Position = 0;
		var reader = new StreamReader(stream);
		Assert.AreEqual("Hello World", await reader.ReadToEndAsync());
	}

	#region Private Methods

	private static ArtifactLeasingService GetService(
		TestBaseDbContext dbContext,
		IBlobStorage blobStorage = null,
		IDateTimeProvider dateTimeProvider = null,
		int totpDurationSeconds = TotpDurationSeconds)
	{
		dbContext.ChangeTracker.Clear();

		return new ArtifactLeasingService(dbContext,
			blobStorage ?? Deps.BlobStorage,
			dateTimeProvider ?? Deps.DateTimeProvider,
			TotpSharedSecret,
			totpDurationSeconds);
	}

	#endregion Private Methods
}