﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MSTestExtensions;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.BusinessLogic.Services;
using StudioKit.Scaffolding.BusinessLogic.Services.Authorization;
using StudioKit.Scaffolding.BusinessLogic.Services.OAuth;
using StudioKit.Scaffolding.Common;
using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Properties;
using StudioKit.Scaffolding.Tests.TestData.DataAccess;
using StudioKit.Scaffolding.Tests.TestData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Tests.BusinessLogic.Services;

[TestClass]
public class BaseUserServiceTests : BaseTest
{
	private static readonly ScaffoldingTestDependencies Deps = new(ScaffoldingTestDependencies.StartDate);
	private TestBaseDbContext _dbContext;
	private IDbContextTransaction _transaction;
	private IBaseUserService<TestBaseUser, BaseUserBusinessModel<TestBaseUser>, BaseUserEditBusinessModel<TestBaseUser>> _service;

	[ClassInitialize]
	public static async Task BeforeAll(TestContext testContext)
	{
		// seed common data that will persist across tests
		await using var dbContext = Deps.PersistentDbContextFactory();
		await Deps.SeedRolesAndUsersAsync(dbContext);
		await Deps.SeedExternalProvidersAsync(dbContext);
	}

	[TestInitialize]
	public void BeforeEach()
	{
		Deps.Reset(ScaffoldingTestDependencies.StartDate);
		(_dbContext, _transaction) = Deps.PersistentDbContextAndTransactionFactory();
		_service = GetService(_dbContext);
	}

	[TestCleanup]
	public void AfterEach()
	{
		_transaction.Rollback();
		_transaction.Dispose();
		_dbContext.Dispose();
	}

	[ClassCleanup]
	public static void AfterAll()
	{
		Deps.Dispose();
	}

	#region GetAsync

	[TestMethod]
	public void GetAsync_ShouldThrowWithNullArguments()
	{
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await _service.GetAsync(null, null, CancellationToken.None, null);
			}));
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await _service.GetAsync("", null, CancellationToken.None, relationIds: null);
			}));
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await _service.GetAsync(Deps.StudentId, null, CancellationToken.None);
			}));
	}

	[TestMethod]
	public void GetAsync_ShouldThrowWithoutAuthorization()
	{
		Assert.ThrowsAsync<ForbiddenException>(
			Task.Run(async () =>
			{
				await _service.GetAsync(Deps.InstructorId, Deps.StudentPrincipal);
			}));
	}

	[TestMethod]
	public async Task GetAsync_ShouldSucceed()
	{
		var userBusinessModel = await _service.GetAsync(Deps.StudentId, Deps.AdminPrincipal);

		Assert.IsNotNull(userBusinessModel);
		Assert.AreEqual(Deps.StudentId, userBusinessModel.User.Id);
	}

	#endregion GetAsync

	#region GetAsync_Search

	[TestMethod]
	public void GetAsync_Search_ShouldThrowWithNullArguments()
	{
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await _service.GetAsync(null, "meow");
			}));
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await _service.GetAsync(Deps.AdminPrincipal, null);
			}));
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await _service.GetAsync(Deps.AdminPrincipal, "");
			}));
	}

	[TestMethod]
	public void GetAsync_Search_ShouldThrowWithoutAuthorization()
	{
		Assert.ThrowsAsync<ForbiddenException>(
			Task.Run(async () =>
			{
				await _service.GetAsync(Deps.InstructorPrincipal, "joe schmoe");
			}));
	}

	[TestMethod]
	public async Task GetAsync_Search_ShouldSucceed()
	{
		var userBusinessModels = await _service.GetAsync(Deps.AdminPrincipal, "localhost.com");

		Assert.IsNotNull(userBusinessModels);
		Assert.AreEqual(5, userBusinessModels.Count());
	}

	[TestMethod]
	public async Task GetAsync_Search_ShouldSucceed_OneResult()
	{
		var userBusinessModels = await _service.GetAsync(Deps.AdminPrincipal, Deps.InstructorId);
		Assert.IsNotNull(userBusinessModels);

		var userBusinessModelsList = userBusinessModels.ToList();
		Assert.AreEqual(1, userBusinessModelsList.Count);
		Assert.AreEqual(Deps.InstructorId, userBusinessModelsList.First().User.Id);
	}

	#endregion GetAsync_Search

	#region StartImpersonationAsync

	[TestMethod]
	public void StartImpersonationAsync_ShouldThrowWithNullArguments()
	{
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await _service.StartImpersonationAsync(null, null);
			}));
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await _service.StartImpersonationAsync("", null);
			}));
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await _service.StartImpersonationAsync(Deps.StudentId, null);
			}));
	}

	[TestMethod]
	public void StartImpersonationAsync_ShouldThrowWithoutAuthorization()
	{
		Assert.ThrowsAsync<ForbiddenException>(
			Task.Run(async () =>
			{
				await _service.StartImpersonationAsync(Deps.InstructorId, Deps.StudentPrincipal);
			}));
		Assert.ThrowsAsync<ForbiddenException>(
			Task.Run(async () =>
			{
				await _service.StartImpersonationAsync(Deps.AdminId, Deps.InstructorPrincipal);
			}));
	}

	[TestMethod]
	public void StartImpersonationAsync_ShouldThrowIfAlreadyImpersonating()
	{
		var (impersonatedPrincipal, _) = ScaffoldingTestDependencies.GeneratePrincipalAndProvider(Deps.InstructorId,
			new List<Claim>
			{
				new(ScaffoldingClaimTypes.ImpersonatorUserId, Deps.AdminId, ClaimValueTypes.String)
			});

		Assert.ThrowsAsync<ForbiddenException>(
			Task.Run(async () =>
			{
				await _service.StartImpersonationAsync(Deps.InstructorId, impersonatedPrincipal);
			}), Strings.ImpersonationAlreadyStarted);
	}

	[TestMethod]
	public void StartImpersonationAsync_ShouldThrowIfUserIsNotFound()
	{
		const string userId = "user-that-does-not-exist";

		Assert.ThrowsAsync<EntityNotFoundException>(
			Task.Run(async () =>
			{
				await _service.StartImpersonationAsync(userId, Deps.AdminPrincipal);
			}), string.Format(Strings.EntityNotFoundById, nameof(TestBaseUser), userId));
	}

	[TestMethod]
	public async Task StartImpersonationAsync_ShouldSucceed()
	{
		var response = await _service.StartImpersonationAsync(Deps.InstructorId, Deps.AdminPrincipal);
		Assert.IsNotNull(response.Code);

		var newIdentity = DeserializeCode(response);
		Assert.AreEqual(Deps.InstructorId, newIdentity.Claims.SingleOrDefault(c => c.Type.Equals(ClaimTypes.NameIdentifier))?.Value);
		Assert.AreEqual(Deps.AdminId,
			newIdentity.Claims.SingleOrDefault(c => c.Type.Equals(ScaffoldingClaimTypes.ImpersonatorUserId))?.Value);
	}

	#endregion StartImpersonationAsync

	#region StopImpersonationAsync

	[TestMethod]
	public void StopImpersonationAsync_ShouldThrowWithNullArguments()
	{
		Assert.ThrowsAsync<ArgumentNullException>(
			Task.Run(async () =>
			{
				await _service.StopImpersonationAsync(null);
			}));
	}

	[TestMethod]
	public void StopImpersonationAsync_ShouldThrowIfNotImpersonating()
	{
		Assert.ThrowsAsync<ForbiddenException>(
			Task.Run(async () =>
			{
				await _service.StopImpersonationAsync(Deps.AdminPrincipal);
			}), Strings.ImpersonationNotStarted);
	}

	[TestMethod]
	public void StopImpersonationAsync_ShouldThrowIfImpersonatorIsNotFound()
	{
		const string impersonatorId = "impersonator-that-got-deleted-somehow";

		var (impersonatedPrincipal, _) = ScaffoldingTestDependencies.GeneratePrincipalAndProvider(Deps.InstructorId,
			new List<Claim>
			{
				new(ScaffoldingClaimTypes.ImpersonatorUserId, impersonatorId, ClaimValueTypes.String)
			});

		Assert.ThrowsAsync<EntityNotFoundException>(
			Task.Run(async () =>
			{
				await _service.StopImpersonationAsync(impersonatedPrincipal);
			}), string.Format(Strings.EntityNotFoundById, nameof(TestBaseUser), impersonatorId));
	}

	[TestMethod]
	public async Task StopImpersonationAsync_ShouldSucceed()
	{
		var (impersonatedPrincipal, _) = ScaffoldingTestDependencies.GeneratePrincipalAndProvider(Deps.InstructorId,
			new List<Claim>
			{
				new(ScaffoldingClaimTypes.ImpersonatorUserId, Deps.AdminId, ClaimValueTypes.String)
			});

		var response = await _service.StopImpersonationAsync(impersonatedPrincipal);
		Assert.IsNotNull(response.Code);

		var newIdentity = DeserializeCode(response);
		Assert.AreEqual(Deps.AdminId, newIdentity.Claims.SingleOrDefault(c => c.Type.Equals(ClaimTypes.NameIdentifier))?.Value);
		Assert.IsNull(newIdentity.Claims.SingleOrDefault(c => c.Type.Equals(ScaffoldingClaimTypes.ImpersonatorUserId)));
	}

	#endregion StopImpersonationAsync

	#region Private Methods

	private static IBaseUserService<TestBaseUser, BaseUserBusinessModel<TestBaseUser>, BaseUserEditBusinessModel<TestBaseUser>> GetService(
		TestBaseDbContext dbContext)
	{
		dbContext.ChangeTracker.Clear();

		var dataProtectorMock = new Mock<IDataProtector>();
		dataProtectorMock
			.Setup(d => d.Protect(It.IsAny<byte[]>()))
			.Returns<byte[]>(ticketBytes => ticketBytes);
		dataProtectorMock
			.Setup(d => d.Unprotect(It.IsAny<byte[]>()))
			.Returns<byte[]>(ticketBytes => ticketBytes);

		var oauthServiceMock = new Mock<IDataProtectionProvider>();
		oauthServiceMock
			.Setup(p => p.CreateProtector(It.IsAny<string>()))
			.Returns(dataProtectorMock.Object);

		return new BaseUserService<TestBaseDbContext, TestBaseUser, TestBaseGroup, BaseConfiguration, BaseUserBusinessModel<TestBaseUser>,
			BaseUserEditBusinessModel<TestBaseUser>>(
			dbContext,
			Deps.GetUserManager(dbContext),
			new UserAuthorizationService<TestBaseDbContext>(dbContext),
			new OAuthService<TestBaseDbContext, TestBaseUser, TestBaseGroup, BaseConfiguration>(
				dbContext,
				Deps.DateTimeProvider,
				oauthServiceMock.Object,
				Deps.ShardKeyProvider,
				new Mock<ILogger<OAuthService<TestBaseDbContext, TestBaseUser, TestBaseGroup, BaseConfiguration>>>().Object,
				Deps.ErrorHandler));
	}

	private static ClaimsIdentity DeserializeCode(CodeResponse response)
	{
		var serializedTicket = Convert.FromBase64String(response.Code);
		// No need to unprotect using IDataProtector since it is mocked above
		var serializer = new TicketSerializer();
		var ticket = serializer.Deserialize(serializedTicket);
		if (ticket == null)
		{
			throw new NullReferenceException();
		}

		return (ClaimsIdentity)ticket.Principal.Identity;
	}

	#endregion Private Methods
}