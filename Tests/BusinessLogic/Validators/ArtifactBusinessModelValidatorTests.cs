using FluentValidation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSTestExtensions;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.BusinessLogic.Validators;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Tests.BusinessLogic.Validators;

[TestClass]
public class ArtifactBusinessModelValidatorTests : BaseTest
{
	private static readonly ScaffoldingTestDependencies Deps = new(ScaffoldingTestDependencies.StartDate);

	private TextArtifactBusinessModel _textArtifactBusinessModel;
	private FileArtifactBusinessModel _fileArtifactBusinessModel;
	private UrlArtifactBusinessModel _urlArtifactBusinessModel;

	[TestInitialize]
	public void BeforeEach()
	{
		Deps.Reset(ScaffoldingTestDependencies.StartDate);

		_textArtifactBusinessModel = new TextArtifactBusinessModel
		{
			Text = "<p>Hello World!</p>",
			WordCount = 2
		};

		_fileArtifactBusinessModel = new FileArtifactBusinessModel
		{
			FileName = "cat3.txt",
			ContentType = "text/plain",
			File = Deps.GenerateFormFile("content")
		};

		_urlArtifactBusinessModel = new UrlArtifactBusinessModel
		{
			Url = "https://dummy.data/new"
		};
	}

	[TestMethod]
	public async Task AssertValidAsync_ShouldNotThrowException()
	{
		await new ArtifactBusinessModelValidator().AssertIsValidAsync(_textArtifactBusinessModel);
		await new ArtifactBusinessModelValidator().AssertIsValidAsync(_fileArtifactBusinessModel);
		await new ArtifactBusinessModelValidator().AssertIsValidAsync(_urlArtifactBusinessModel);
	}

	[TestMethod]
	public void AssertValidAsync_ShouldThrowExceptionWithTextArtifact_MissingText()
	{
		_textArtifactBusinessModel.Text = "";

		Assert.ThrowsAsync<ValidationException>(
			Task.Run(async () =>
			{
				await new ArtifactBusinessModelValidator().AssertIsValidAsync(_textArtifactBusinessModel);
			}));
	}

	[TestMethod]
	public void AssertValidAsync_ShouldThrowExceptionWithTextArtifact_MissingWordCount()
	{
		_textArtifactBusinessModel.WordCount = 0;

		Assert.ThrowsAsync<ValidationException>(
			Task.Run(async () =>
			{
				await new ArtifactBusinessModelValidator().AssertIsValidAsync(_textArtifactBusinessModel);
			}));
	}

	[TestMethod]
	public void AssertValidAsync_ShouldThrowExceptionWithFileArtifact_MissingFileName()
	{
		_fileArtifactBusinessModel.FileName = "";

		Assert.ThrowsAsync<ValidationException>(
			Task.Run(async () =>
			{
				await new ArtifactBusinessModelValidator().AssertIsValidAsync(_fileArtifactBusinessModel);
			}));
	}

	[TestMethod]
	public void AssertValidAsync_ShouldThrowExceptionWithFileArtifact_MissingContentType()
	{
		_fileArtifactBusinessModel.ContentType = "";

		Assert.ThrowsAsync<ValidationException>(
			Task.Run(async () =>
			{
				await new ArtifactBusinessModelValidator().AssertIsValidAsync(_fileArtifactBusinessModel);
			}));
	}

	[TestMethod]
	public void AssertValidAsync_ShouldThrowExceptionWithFileArtifact_MissingFile()
	{
		_fileArtifactBusinessModel.File = Deps.GenerateFormFile();

		Assert.ThrowsAsync<ValidationException>(
			Task.Run(async () =>
			{
				await new ArtifactBusinessModelValidator().AssertIsValidAsync(_fileArtifactBusinessModel);
			}));
	}

	[TestMethod]
	public void AssertValidAsync_ShouldThrowExceptionWithUrlArtifact_MissingUrl()
	{
		_urlArtifactBusinessModel.Url = "";

		Assert.ThrowsAsync<ValidationException>(
			Task.Run(async () =>
			{
				await new ArtifactBusinessModelValidator().AssertIsValidAsync(_urlArtifactBusinessModel);
			}));
	}
}