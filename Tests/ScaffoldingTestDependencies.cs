﻿using StudioKit.Scaffolding.Models;
using StudioKit.Scaffolding.Tests.TestData.DataAccess;
using StudioKit.Scaffolding.Tests.TestData.Models;
using System;

namespace StudioKit.Scaffolding.Tests;

public class ScaffoldingTestDependencies : BaseScaffoldingTestDependencies<TestBaseDbContext, TestBaseUser, TestBaseGroup,
	BaseConfiguration>
{
	public ScaffoldingTestDependencies(DateTime dateTime) : base(dateTime)
	{
	}
}