﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using StudioKit.Caching.Interfaces;
using StudioKit.Cloud.Storage.Blob;
using StudioKit.Cloud.Storage.Queue;
using StudioKit.Data.Entity.Identity;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Services;
using StudioKit.Encryption;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Models;
using StudioKit.ExternalProvider.Lti.BusinessLogic.Utils;
using StudioKit.ExternalProvider.Lti.Models;
using StudioKit.ExternalProvider.Models.Interfaces;
using StudioKit.ExternalProvider.UniTime.BusinessLogic.Services;
using StudioKit.ExternalProvider.UniTime.Models;
using StudioKit.Notification.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Interfaces;
using StudioKit.Scaffolding.BusinessLogic.Models;
using StudioKit.Scaffolding.BusinessLogic.Services;
using StudioKit.Scaffolding.BusinessLogic.Services.Authorization;
using StudioKit.Scaffolding.BusinessLogic.Validators;
using StudioKit.Scaffolding.DataAccess;
using StudioKit.Scaffolding.DataAccess.Queues.QueueMessages;
using StudioKit.Scaffolding.Models;
using StudioKit.Security.BusinessLogic.Interfaces;
using StudioKit.Sharding.Interfaces;
using StudioKit.Tests;
using StudioKit.TransientFaultHandling.Http.Interfaces;
using StudioKit.UserInfoService;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Scaffolding.Tests;

public class BaseScaffoldingTestDependencies<TContext, TUser, TGroup, TConfiguration> : BaseTestDependencies<TContext>
	where TContext : BaseDbContext<TUser, TGroup, TConfiguration>, new()
	where TUser : IdentityUser, IUser, new()
	where TGroup : BaseGroup, new()
	where TConfiguration : BaseConfiguration, IBaseShardConfiguration, new()
{
	#region Readonly Properties

	public readonly string SuperAdminId = Guid.NewGuid().ToString();
	public readonly string AdminId = Guid.NewGuid().ToString();
	public readonly string InstructorId = Guid.NewGuid().ToString();
	public readonly string GraderId = Guid.NewGuid().ToString();
	public readonly string StudentId = Guid.NewGuid().ToString();

	#endregion Readonly Properties

	#region Instance Properties

	public TUser SuperAdmin;
	public TUser Admin;
	public TUser Instructor;
	public TUser Grader;
	public TUser Student;

	public IPrincipalProvider NullPrincipalProvider { get; }

	public IPrincipal SuperAdminPrincipal { get; }
	public IPrincipal AdminPrincipal { get; }
	public IPrincipal InstructorPrincipal { get; }
	public IPrincipal GraderPrincipal { get; }
	public IPrincipal StudentPrincipal { get; }

	public IPrincipalProvider SuperAdminPrincipalProvider { get; }
	public IPrincipalProvider AdminPrincipalProvider { get; }
	public IPrincipalProvider InstructorPrincipalProvider { get; }
	public IPrincipalProvider GraderPrincipalProvider { get; }
	public IPrincipalProvider StudentPrincipalProvider { get; }

	public IUserInfoService UserInfoService { get; }
	public Mock<IBlobStorage> BlobStorageMock { get; }
	public IBlobStorage BlobStorage => BlobStorageMock.Object;
	public IQueueManager<SyncAllRostersMessage> SyncAllRostersQueueManager { get; }
	public IQueueManager<SyncGroupRosterMessage> SyncGroupRosterQueueManager { get; }
	public INotificationService<TGroup> NotificationService { get; }
	public ILookupNormalizer LookupNormalizer { get; }
	public IUserFactory<TUser> UserFactory { get; }
	public Mock<IAsyncCacheClient> CacheClientMock { get; }
	public IAsyncCacheClient CacheClient => CacheClientMock.Object;

	public UniTimeExternalProvider UniTimeExternalProvider { get; private set; }
	public LtiExternalProvider LtiExternalProvider { get; private set; }
	public ExternalTerm UniTimeExternalTerm1 { get; private set; }
	public ExternalTerm UniTimeExternalTerm2 { get; private set; }
	public UniTimeGroup UniTimeGroup { get; private set; }
	public UniTimeGroup UniTimeGroup2 { get; private set; }
	public LtiLaunch LtiLaunch { get; private set; }

	#endregion Instance Properties

	public BaseScaffoldingTestDependencies(DateTime dateTime) : base(dateTime)
	{
		// setup EncryptedConfigurationManager to be able to access ENV variables and optionally appsettings.json
		var currentDirectory = Directory.GetCurrentDirectory();
		var configuration = new ConfigurationBuilder()
			.SetBasePath(currentDirectory)
			.AddJsonFile("appsettings.json", true, true)
			.AddEnvironmentVariables()
			.Build();
		EncryptedConfigurationManager.Configuration = configuration;

		Reset(dateTime);

		LookupNormalizer = new UpperInvariantLookupNormalizer();
		UserFactory = new BaseUserFactory<TUser>(LookupNormalizer);

		var principalProviderMock = new Mock<IPrincipalProvider>();
		principalProviderMock.Setup(p => p.GetPrincipal()).Returns((IPrincipal)null);
		NullPrincipalProvider = principalProviderMock.Object;

		(SuperAdminPrincipal, SuperAdminPrincipalProvider) = GeneratePrincipalAndProvider(SuperAdminId);
		(AdminPrincipal, AdminPrincipalProvider) = GeneratePrincipalAndProvider(AdminId);
		(InstructorPrincipal, InstructorPrincipalProvider) = GeneratePrincipalAndProvider(InstructorId);
		(GraderPrincipal, GraderPrincipalProvider) = GeneratePrincipalAndProvider(GraderId);
		(StudentPrincipal, StudentPrincipalProvider) = GeneratePrincipalAndProvider(StudentId);

		UserInfoService = new Mock<IUserInfoService>().Object;

		BlobStorageMock = new Mock<IBlobStorage>();
		BlobStorageMock.Setup(s => s.UrlForBlockBlob(It.IsAny<string>()))
			.Returns<string>(path => $"https://127.0.0.1/{path}");
		BlobStorageMock.Setup(s =>
				s.UploadBlockBlobAsync(It.IsAny<Stream>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<CancellationToken>()))
			.Returns(() => Task.CompletedTask);
		BlobStorageMock.Setup(s => s.DeleteBlockBlobAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()))
			.Returns(() => Task.CompletedTask);

		CacheClientMock = new Mock<IAsyncCacheClient>();

		SyncAllRostersQueueManager = GenerateQueueManager<SyncAllRostersMessage>();
		SyncGroupRosterQueueManager = GenerateQueueManager<SyncGroupRosterMessage>();
		NotificationService = new Mock<INotificationService<TGroup>>().Object;

		UniTimeExternalProvider = new UniTimeExternalProvider
		{
			Name = "UniTime",
			Key = "key",
			Secret = "secret",
		};

		UniTimeExternalTerm1 = new ExternalTerm
		{
			Name = "Current Term",
			ExternalId = "CurrentTerm",
			ExternalProvider = UniTimeExternalProvider,
			StartDate = DateTime.AddMonths(-2),
			EndDate = DateTime.AddMonths(2)
		};

		UniTimeExternalTerm2 = new ExternalTerm
		{
			Name = "Future Term",
			ExternalId = "FutureTerm",
			ExternalProvider = UniTimeExternalProvider,
			StartDate = DateTime.AddMonths(2),
			EndDate = DateTime.AddMonths(4)
		};

		UniTimeGroup = new UniTimeGroup
		{
			ExternalProvider = UniTimeExternalProvider,
			CreatedById = InstructorId,
			ExternalId = "12345",
			Name = "Some UniTime Course",
			Description = "Some UniTime Course Description"
		};

		UniTimeGroup2 = new UniTimeGroup
		{
			ExternalProvider = UniTimeExternalProvider,
			CreatedById = InstructorId,
			ExternalId = "12345",
			Name = "Some UniTime Course",
			Description = "Some UniTime Course Description"
		};

		LtiExternalProvider = TestUtils.GetLtiExternalProvider();
		LtiExternalProvider.GradePushEnabled = true;

		LtiLaunch = new LtiLaunch
		{
			ExternalProvider = LtiExternalProvider,
			ExternalId = "5678",
			CreatedById = InstructorId,
			Name = "Some LTI Context",
			Description = "Some LTI Context Description",
			RosterUrl = "http://courses.edu/5678",
			GradesUrl = "http://courses.edu/5678/lineitems",
			LtiVersion = LtiVersion.Lti1P3,
			Roles = LtiRole.MembershipInstructor,
			ReturnUrl = "http://courses.edu/return"
		};
	}

	/// <summary>
	/// Reset anything that could have been changed between tests (mostly data)
	/// </summary>
	/// <param name="dateTime"></param>
	public override void Reset(DateTime dateTime)
	{
		base.Reset(dateTime);
		BlobStorageMock?.Invocations.Clear();
		CacheClientMock?.Invocations.Clear();
	}

	public static (IPrincipal, IPrincipalProvider) GeneratePrincipalAndProvider(string userId, List<Claim> claims = null)
	{
		var principalMock = new Mock<IPrincipal>();
		var identity = new GenericIdentity(userId);
		identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, userId));
		if (claims != null) identity.AddClaims(claims);
		principalMock.SetupGet(m => m.Identity).Returns(identity);
		var principal = principalMock.Object;

		var principalProviderMock = new Mock<IPrincipalProvider>();
		principalProviderMock.Setup(p => p.GetPrincipal()).Returns(principal);
		var principalProvider = principalProviderMock.Object;

		return (principal, principalProvider);
	}

	#region DbConnection and DbContext

	public TContext TransientDbContextFactory(IPrincipalProvider principalProvider = null)
	{
		return base.TransientDbContextFactory(args =>
		{
			var argList = args.ToList();
			argList.Add(principalProvider ?? NullPrincipalProvider);
			return argList.ToArray();
		});
	}

	public TContext PersistentDbContextFactory(IPrincipalProvider principalProvider = null, bool shouldUseSqlServer = false)
	{
		return base.PersistentDbContextFactory(shouldUseSqlServer, args =>
		{
			var argList = args.ToList();
			argList.Add(principalProvider ?? NullPrincipalProvider);
			return argList.ToArray();
		});
	}

	public (TContext, IDbContextTransaction) PersistentDbContextAndTransactionFactory(IPrincipalProvider principalProvider = null,
		bool shouldUseSqlServer = false)
	{
		return base.PersistentDbContextAndTransactionFactory(shouldUseSqlServer, args =>
		{
			var argList = args.ToList();
			argList.Add(principalProvider ?? NullPrincipalProvider);
			return argList.ToArray();
		});
	}

	protected override void LoadSeededEntities(TContext dbContext)
	{
		base.LoadSeededEntities(dbContext);

		SuperAdmin = dbContext.Users.Single(u => u.Id.Equals(SuperAdminId));
		Admin = dbContext.Users.Single(u => u.Id.Equals(AdminId));
		Instructor = dbContext.Users.Single(u => u.Id.Equals(InstructorId));
		Grader = dbContext.Users.Single(u => u.Id.Equals(GraderId));
		Student = dbContext.Users.Single(u => u.Id.Equals(StudentId));
		UniTimeExternalProvider = dbContext.UniTimeExternalProviders.Single(p => p.Id.Equals(UniTimeExternalProvider.Id));
		LtiExternalProvider = dbContext.LtiExternalProviders.Single(p => p.Id.Equals(LtiExternalProvider.Id));
		UniTimeExternalTerm1 = dbContext.ExternalTerms.Single(p => p.Id.Equals(UniTimeExternalTerm1.Id));
		UniTimeExternalTerm2 = dbContext.ExternalTerms.Single(p => p.Id.Equals(UniTimeExternalTerm2.Id));
		UniTimeGroup = dbContext.UniTimeGroups.Single(p => p.Id.Equals(UniTimeGroup.Id));
		UniTimeGroup2 = dbContext.UniTimeGroups.Single(p => p.Id.Equals(UniTimeGroup2.Id));
		LtiLaunch = dbContext.LtiLaunches.Single(p => p.Id.Equals(LtiLaunch.Id));
	}

	#endregion DbConnection and DbContext

	#region Services

	public static IQueueManager<T> GenerateQueueManager<T>(Action addMessageHandler = null)
		where T : AzureQueueMessage
	{
		var queueManagerMock = new Mock<IQueueManager<T>>();
		var handler = addMessageHandler ?? (() => { });
		queueManagerMock.Setup(m =>
				m.AddMessageAsync(It.IsAny<T>(), null, null, It.IsAny<CancellationToken>()))
			.Callback(handler)
			.Returns(Task.CompletedTask);
		return queueManagerMock.Object;
	}

	public UserManager<TContext, TUser, TGroup, TConfiguration> GetUserManager(TContext dbContext)
	{
		var userStore = new UserStore<TUser, TContext>(dbContext);
		return new UserManager<TContext, TUser, TGroup, TConfiguration>(
			userStore,
			null,
			null,
			null,
			null,
			LookupNormalizer,
			null,
			null,
			null,
			dbContext,
			null,
			null);
	}

	public RoleManager GetRoleManager(TContext dbContext)
	{
		return new RoleManager(
			new RoleStore<TContext>(dbContext),
			null,
			LookupNormalizer,
			null,
			null);
	}

	public UserRoleService<BaseDbContext<TUser, TGroup, TConfiguration>, TUser, TGroup, TConfiguration> GetUserRoleService(
		TContext dbContext)
	{
		dbContext.ChangeTracker.Clear();

		return new UserRoleService<BaseDbContext<TUser, TGroup, TConfiguration>, TUser, TGroup, TConfiguration>(
			dbContext,
			ShardKeyProvider,
			UserInfoService,
			new UserRoleAuthorizationService<BaseDbContext<TUser, TGroup, TConfiguration>>(dbContext),
			GetRoleManager(dbContext),
			UserFactory);
	}

	public BaseGroupUserRoleService<TContext, TUser, TGroup, TConfiguration> GetGroupUserRoleService(
		TContext dbContext)
	{
		dbContext.ChangeTracker.Clear();

		return new BaseGroupUserRoleService<TContext, TUser, TGroup, TConfiguration>(dbContext,
			ShardKeyProvider,
			UserInfoService,
			new GroupAuthorizationService<TContext>(dbContext),
			GetRoleManager(dbContext),
			SyncGroupRosterQueueManager,
			DateTimeProvider,
			UserFactory,
			new Mock<INotificationService<TGroup>>().Object);
	}

	public IBaseGroupService<TGroup, BaseGroupBusinessModel<TGroup, TUser>, BaseGroupEditBusinessModel<TGroup>, TUser>
		GetBaseGroupService(TContext dbContext,
			IQueueManager<SyncGroupRosterMessage> syncGroupRosterQueueManager = null,
			INotificationService<TGroup> notificationService = null)
	{
		dbContext.ChangeTracker.Clear();

		return new BaseGroupService<TContext, TUser, TGroup, TConfiguration,
			BaseGroupBusinessModel<TGroup, TUser>, BaseGroupEditBusinessModel<TGroup>>(dbContext,
			new GroupAuthorizationService<TContext>(dbContext),
			new BaseGroupBusinessModelValidator<TGroup>(),
			SyncAllRostersQueueManager,
			syncGroupRosterQueueManager ?? SyncGroupRosterQueueManager,
			new LtiLaunchService<TContext, TGroup>(dbContext,
				new LtiLaunchAuthorizationService<TContext, TGroup>(dbContext),
				new Mock<ILtiService>().Object),
			new UniTimeService<TContext, TUser, TGroup, TConfiguration>(
				new ExternalGroupAuthorizationService<TContext>(dbContext), dbContext,
				new UniTimeRosterProviderService<TUser, ExternalGroup, ExternalGroupUser>(
					ErrorHandler,
					new Mock<ILogger<UniTimeRosterProviderService<TUser, ExternalGroup, ExternalGroupUser>>>().Object,
					new Mock<UniTimeApiService>(
						new Mock<ILogger<UniTimeApiService>>().Object,
						() => new Mock<IHttpClient>().Object).Object,
					UserInfoService,
					UserFactory)),
			ShardKeyProvider,
			GetGroupUserRoleService(dbContext),
			DateTimeProvider, notificationService ?? NotificationService);
	}

	#endregion Services

	#region Seeding

	public TUser GenerateUser(string userId, string firstName = null, string lastName = null)
	{
		var email = $"{userId}@localhost.com";
		var user = UserFactory.CreateUser(
			email,
			email,
			firstName ?? $"{userId} FirstName",
			lastName ?? $"{userId} LastName",
			uid: userId,
			id: userId);
		return user;
	}

	public IFormFile GenerateFormFile(string content = "")
	{
		var fileMock = new Mock<IFormFile>();
		fileMock.Setup(f => f.OpenReadStream())
			.Returns(() =>
			{
				var stream = new MemoryStream();
				var writer = new StreamWriter(stream);
				writer.Write(content);
				writer.Flush();
				stream.Position = 0;
				return stream;
			});
		fileMock.Setup(f => f.Length)
			.Returns(content.Length);

		return fileMock.Object;
	}

	public override async Task SeedDatabaseAsync(TContext dbContext, CancellationToken cancellationToken = default)
	{
		await base.SeedDatabaseAsync(dbContext, cancellationToken);
		await BaseSeed.SeedDatabaseAsync(dbContext, cancellationToken);
	}

	public virtual async Task SeedRolesAndUsersAsync(TContext dbContext, CancellationToken cancellationToken = default)
	{
		await SeedDatabaseAsync(dbContext, cancellationToken);

		dbContext.Configurations.Add(new TConfiguration());

		SuperAdmin = GenerateUser(SuperAdminId, "SuperAdmin", "TestUser");
		Admin = GenerateUser(AdminId, "Admin", "TestUser");
		Instructor = GenerateUser(InstructorId, "Instructor", "TestUser");
		Grader = GenerateUser(GraderId, "Grader", "TestUser");
		Student = GenerateUser(StudentId, "Student", "TestUser");
		dbContext.Users.Add(SuperAdmin);
		dbContext.Users.Add(Admin);
		dbContext.Users.Add(Instructor);
		dbContext.Users.Add(Grader);
		dbContext.Users.Add(Student);

		await dbContext.SaveChangesAsync(cancellationToken);

		var userManager = GetUserManager(dbContext);
		await userManager.AddToRoleAsync(SuperAdmin, BaseRole.SuperAdmin);
		await userManager.AddToRoleAsync(SuperAdmin, BaseRole.Admin);
		await userManager.AddToRoleAsync(Admin, BaseRole.Admin);
		await userManager.AddToRoleAsync(Instructor, BaseRole.Creator);
		await dbContext.SaveChangesAsync(cancellationToken);
	}

	public async Task SeedExternalProvidersAsync(TContext dbContext, CancellationToken cancellationToken = default)
	{
		dbContext.ExternalProviders.AddRange(new List<ExternalProvider.Models.ExternalProvider>
		{
			UniTimeExternalProvider,
			LtiExternalProvider
		});
		dbContext.ExternalTerms.AddRange(new List<ExternalTerm>
		{
			UniTimeExternalTerm1,
			UniTimeExternalTerm2
		});
		await dbContext.SaveChangesAsync(cancellationToken);

		UniTimeGroup.ExternalTermId = UniTimeExternalTerm1.Id;
		dbContext.UniTimeGroups.Add(UniTimeGroup);

		UniTimeGroup2.ExternalTermId = UniTimeExternalTerm2.Id;
		dbContext.UniTimeGroups.Add(UniTimeGroup2);

		dbContext.LtiLaunches.Add(LtiLaunch);
		await dbContext.SaveChangesAsync(cancellationToken);
	}

	public virtual async Task SeedGroupUsersAsync(TContext dbContext, int groupId, CancellationToken cancellationToken = default)
	{
		var groupOwnerRole =
			await dbContext.Roles.SingleAsync(r => r.Name.Equals(BaseRole.GroupOwner), cancellationToken);
		var groupGraderRole =
			await dbContext.Roles.SingleAsync(r => r.Name.Equals(BaseRole.GroupGrader), cancellationToken);
		var groupLearnerRole =
			await dbContext.Roles.SingleAsync(r => r.Name.Equals(BaseRole.GroupLearner), cancellationToken);

		var groupUserRoles = new List<GroupUserRole>
		{
			new()
			{
				UserId = InstructorId,
				RoleId = groupOwnerRole.Id,
				GroupId = groupId
			},
			new()
			{
				UserId = GraderId,
				RoleId = groupGraderRole.Id,
				GroupId = groupId
			},
			new()
			{
				UserId = StudentId,
				RoleId = groupLearnerRole.Id,
				GroupId = groupId
			}
		};
		var groupUserRoleLogs = groupUserRoles.Select(gur => new GroupUserRoleLog
		{
			UserId = gur.UserId,
			RoleId = gur.RoleId,
			GroupId = gur.GroupId,
			Type = GroupUserRoleLogType.Added
		}).ToList();
		dbContext.GroupUserRoles.AddRange(groupUserRoles);
		dbContext.GroupUserRoleLogs.AddRange(groupUserRoleLogs);
		await dbContext.SaveChangesAsync(cancellationToken);
	}

	public async Task<TGroup> SeedGroupWithTermAsync(TContext dbContext, CancellationToken cancellationToken = default)
	{
		var group = new TGroup
		{
			Name = "InitialGroupName",
			ExternalTermId = UniTimeExternalTerm1.Id,
			CreatedById = InstructorId
		};
		dbContext.Groups.Add(group);
		await dbContext.SaveChangesAsync(cancellationToken);
		await SeedGroupUsersAsync(dbContext, group.Id, cancellationToken);
		return group;
	}

	public async Task<TGroup> SeedGroupWithDatesAsync(TContext dbContext, CancellationToken cancellationToken = default)
	{
		var group = new TGroup
		{
			Name = "InitialGroupName",
			StartDate = DateTime,
			EndDate = DateTime.AddMonths(2),
			CreatedById = InstructorId
		};
		dbContext.Groups.Add(group);
		await dbContext.SaveChangesAsync(cancellationToken);
		await SeedGroupUsersAsync(dbContext, group.Id, cancellationToken);
		return group;
	}

	public async Task<(TGroup, ExternalGroup)> SeedGroupWithTermAndUniTimeExternalGroupAsync(TContext dbContext,
		CancellationToken cancellationToken = default)
	{
		var group = await SeedGroupWithTermAsync(dbContext, cancellationToken);
		var externalGroup = new ExternalGroup
		{
			GroupId = group.Id,
			ExternalProviderId = UniTimeExternalProvider.Id,
			UserId = InstructorId,
			ExternalId = "12345",
			Name = "Some UniTime Course",
			Description = "Some UniTime Course Description"
		};
		dbContext.ExternalGroups.Add(externalGroup);
		await dbContext.SaveChangesAsync(cancellationToken);
		return (group, externalGroup);
	}

	public async Task<(TGroup, ExternalGroup)> SeedGroupWithLtiExternalGroupAsync(TContext dbContext,
		CancellationToken cancellationToken = default)
	{
		var group = await SeedGroupWithDatesAsync(dbContext, cancellationToken);
		var externalGroup = new ExternalGroup
		{
			GroupId = group.Id,
			ExternalProviderId = LtiExternalProvider.Id,
			UserId = InstructorId,
			ExternalId = "12345",
			Name = "Some Lti Course",
			Description = "Some Lti Course Description"
		};
		dbContext.ExternalGroups.Add(externalGroup);
		await dbContext.SaveChangesAsync(cancellationToken);
		return (group, externalGroup);
	}

	#endregion Seeding
}