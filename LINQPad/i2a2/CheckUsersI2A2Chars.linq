<Query Kind="Program">
  <NuGetReference>Newtonsoft.Json</NuGetReference>
  <Namespace>Newtonsoft.Json.Linq</Namespace>
  <Namespace>Newtonsoft.Json</Namespace>
</Query>

void Main()
{
	var alias = "wgrauvog";
	var i2a2Claim = AspNetUserClaims
		.Where(c => c.User.Uid.Equals(alias))
		.Single(c => c.ClaimType.Equals("StudioKit.Cas.I2A2Characteristics"));
	var i2a2Chars = LoadI2A2Dictionary();
	i2a2Claim.ClaimValue.Split(',')
		.ToDictionary(c => c, c =>
		{
			if (!i2a2Chars.ContainsKey(c))
				throw new Exception("NOT FOUND: download newest chars.txt from https://www.purdue.edu/apps/account/html/chars.txt");
			return i2a2Chars[c];
		})
		.Dump($"{alias}'s I2A2 Chars");
}

public Dictionary<string, string> LoadI2A2Dictionary()
{
	var streamReader = new StreamReader(Path.Combine(Path.GetDirectoryName(Util.CurrentQueryPath), "chars.txt"));
	var delimiter = '\t';
	var dictionary = new Dictionary<string, string>();

	while (streamReader.Peek() > 0)
	{
		var line = streamReader.ReadLine();
		if (!line.Contains(delimiter) || line.Split(delimiter).Length != 2)
			continue;
		var items = line.Split(delimiter);
		dictionary[items[0]] = items[1];
	}

	return dictionary;
}